const express = require('express');
const cors = require("cors");
const session = require('express-session');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const path = require('path');

const Routes = require("./routes");
const {
    PassportClass
} = require('./core/PassportClass/PassportClass');

const {
    Core
} = require('./core');

const {
    SystemUserClass
} = require('./core/SystemUserClass/SystemUserClass');

const {
    LanguageСlass
} = require('./core/LanguageСlass');

const authWhiteList = [
    'login',
    'login',
    'register',
    'langs',
    'version'
];


const SESSION_TTL_TIME_MS = 30 * 60 * 1000; // 30 mins session timeout

require('express-async-errors');

console.log(`NODE_ENV: ${process.env.NODE_ENV}`);

/**
 * utils
 */
const {
    AppLoggerClass
} = __UTILS;

/**
 * interfaces
 */
const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("./interfaces/basicResponse");
const {
    HttpRestResponse,
    HttpStatus
} = require('./interfaces/httpRestResponse');


/**
 * Class
 */
class App {
    constructor() {
        this.express = express();
        this.logger = new AppLoggerClass('App', process.env.APP_LOG_LEVEL);
        //this.passport = new PassportClass();
        this.systemUsers = new SystemUserClass();

    }

    async create() {
        try {
            //create core
            this.core = new Core();
            this.lang = new LanguageСlass();

            let initResult = await this.core.init();

            if (initResult.status != ResultStatus.OK) {
                throw new Error(`${initResult.message} - ${initResult.status}`);
            }

            this.logger.info(`Init Core ${initResult}`);

            //link json parser middleware
            this.express.use(express.json({
                limit: '50mb'
            }));

            this.express.use(helmet());

            //ACCEPT all request source by cors
            this.express.use(cors({
                orign: "*"
            }));

            //link urlencoded parser middleware
            this.express.use(express.urlencoded({
                extended: false
            }));

            //link cookie parser parser middleware
            this.express.use(cookieParser());

            //add load static files from public dir
            this.express.use(express.static(path.join(__dirname, 'public')));

            /**
             * Passport auth
             */
            this.express.use(session({
                secret: "admin_service",
                resave: true,
                saveUninitialized: true,
                cookie: {
                    maxAge: SESSION_TTL_TIME_MS
                }
            }));

            this.express.use(require('express-status-monitor')());

            /**
             * start system user module with use passport 
             */
            let initPasss = await this.systemUsers.init(this.express, this.core.db);

            if (initPasss.status != ResultStatus.OK) {
                throw initPasss;
            }

            /**
             * App midleware, for assig local class
             */
            this.express.use((_req, _res, _next) => {
                _req.core = this.core;
                _req.lang = this.lang;
                _req.systemUsers = this.systemUsers;

                if (process.env.SKIP_USE_AUTH != null) {
                    _req.user = {
                        email: null,
                        fullName: null,
                        id: 1,
                        imgDist: null,
                        lang: "ru",
                        lastAuthorization: 1562250707928,
                        username: "root",
                        password: "$2a$06$VUxVySn.nMoJ9XA3WddmeOC.UScnuoRO0gLx7PAvbDuE3Y9Dvuosu",
                        role: "administrator",
                        shortName: null,
                        state: 1,
                    }
                }
                _next();
            });

            if (process.env.SKIP_USE_AUTH == null) {
                this.express.use(this.isAutheenticated.bind(this));
            } else {
                this.logger.warn('Set `SKIP_USE_AUTH` is true!!!.');
            }
            /**
             * routers
             */

            this.express.use('/uploads', express.static(path.join(__dirname, 'uploads')));

            this.express.use(Routes);

            this.express.use(errorHandlerMiddle);
            this.express.use(defaultRoute);

            return new BasicResponse(ResultStatus.OK, 'Create done new App successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    isAutheenticated(_req, _res, _next) {
        //!!!
        var path = _req.path.split('/');

        var checkSkip = authWhiteList.some(_path => path.some(_p => _p == _path));

        if (checkSkip == true) {
            _next();
        } else {
            if (_req.isAuthenticated()) {

                //check role rule for route
                var skipAuthCheck = process.env.DONT_USE_ROLE || false;

                if (typeof process.env.DONT_USE_ROLE == "string") {
                    skipAuthCheck = (skipAuthCheck == "true") ? true : false;
                }

                if (skipAuthCheck != true) {
                    let checkRole = _req.systemUsers.routeList.checkAccess(_req.user.role, _req.method, _req.path);

                    if (checkRole.status != ResultStatus.OK) {
                        throw new HttpRestResponse(403, checkRole.message, 'FORBIDDEN', checkRole);
                    }
                }

                _next();
            } else {
                //_res.redirect('/login.html');
                throw new HttpRestResponse(401, 'Unauthorized user', 'UNAUTH_USER', {});
            }
        }
    }
} //class App

/**
 * Handle function
 */

/**
 * Handle function
 */


function errorHandlerMiddle(_outBatch, _req, _res, _next) {
    var code = HttpStatus.INTERNAL_SERVER_ERROR;

    if (_outBatch.code == null) {
        console.error(_outBatch);
    } else {
        code = _outBatch.code;
    }

    _res.status(code).send(_outBatch);
};

function defaultRoute(_req, _res, _next) {
    var respStub = {
        time: Date.now(),
        code: HttpStatus.NOT_FOUND,
        message: "Route doesn't found. " + _req.path,
        meassgeCode: "SYSTEM_ROUTE_NOT_FOUND",
        method: _req.method,
    };

    console.warn(respStub.code + ":" + respStub.message);

    _res.status(HttpStatus.NOT_FOUND).send(respStub);
};

module.exports.App = App;