exports.searchData = function (_data, _searchValue) {
    var searchValue = _searchValue.toLowerCase();

    for (var key in _data) {
        var str = _data[key];

        if (typeof str != 'object') {
            str = String(str).toLowerCase();

            if (str.indexOf(searchValue) > -1) {
                return true;
            }
        } else {
            if (searchData(str, searchValue)) {
                return true;
            }
        }
    }
}