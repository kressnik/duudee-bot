const fs = require("fs");
const path = require('path');
const uuidv1 = require('uuid/v1');
const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');


async function deleteFile(_dst) {
    return new Promise(function (_resolve, _reject) {
        try {
            fs.unlink(_dst, (_error) => {
                if (_error) {
                    _resolve(new BasicResponse(ResultStatus.UNEXPTED_ERROR, _error.message, _error));
                } else {
                    _resolve(new BasicResponse(ResultStatus.OK, "File successfully deleted", {
                        dst: _dst
                    }));
                }
            });
        } catch (_error) {
            _resolve(new BasicResponse(ResultStatus.UNEXPTED_ERROR, _error.message, _error));
        }
    });
}
exports.deleteFile = deleteFile;

function linkFolderCreate(_uploadDir) {
    try {

        const env = process.env["ENV_FILE"];
        let dir = null;
        let linkFolder = "";
        for (const key in _uploadDir) {

            linkFolder += "/" + _uploadDir[key];

            if (env == "build") {
                dir = path.join(path.dirname(process.execPath), "." + linkFolder);
            } else {
                dir = path.join(__dirname, ".." + linkFolder);
            }

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
        }

        return new BasicResponse(ResultStatus.OK, "Link folder create successfully", {
            link: linkFolder
        });

    } catch (_error) {
        return new BasicResponse(ResultStatus.UNEXPTED_ERROR, _error.message, _error);
    }
}
exports.linkFolderCreate = linkFolderCreate;

let uploadAvatar = async function (_new, _folderPattern) {
    return new Promise(function (_resolve, _reject) {
        try {
            let checkArgsIsNull = [_new, _folderPattern].some(_a => _a == null);

            if (checkArgsIsNull == true) {
                _resolve(new BasicResponse(ResultStatus.ERROR_INVALID_DATA, "No attach file to requset. Or pattern dir is null"), arguments);
            } else {

                let type = _new.type;
                let format = "png"; //default
                let linkFolder = linkFolderCreate(_folderPattern);

                if (linkFolder.status != ResultStatus.OK) {
                    _resolve(linkFolder);
                } else {

                    linkFolder = linkFolder.data.link;

                    if (type) {
                        format = type.split('/');
                        if (format[1]) {
                            format = format[1];
                        }
                    }

                    var base64Data = _new.raw.split(",");
                    base64Data = base64Data[1];


                    let dst = "." + linkFolder + '/' + uuidv1() + '.' + format;

                    let dstForSave = path.join(__dirname, `../${dst}`);

                    fs.writeFile(dstForSave, base64Data, 'base64', function (err) {
                        if (err) {
                            _resolve(new BasicResponse(ResultStatus.ERROR, err.message, err));
                        } else {
                            _resolve(new BasicResponse(ResultStatus.OK, "WR_OK", {
                                dst: dst
                            }));
                        }
                    });
                } //check link folder
            } //check incoming raw value
        } catch (_error) {
            _resolve(new BasicResponse(ResultStatus.UNEXPTED_ERROR, _error.message, _error));
        }
    }); //promise
}

exports.uploadAvatarFile = async function (_avatar, _oldDist, _folderPattern) {
    try {
        //check avatar
        //let link = _oldDist;

        if (_avatar) {
            //upload new image
            let resUploadAvatar = await uploadAvatar(_avatar, _folderPattern);

            if (resUploadAvatar.status != ResultStatus.OK) {
                //todo rework to local logger!
                console.error("uploadAvatar: ", resUploadAvatar.status, resUploadAvatar.message);

                return new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, "Upload file to folder done with errors", {
                    link: _oldDist
                });
            } else {
                //delete file
                if (_oldDist != null) {
                    let resDel = await deleteFile(_oldDist);
                } else {
                    //todo rework wtih plce for better check errors amd return to invoker
                }

                link = resUploadAvatar.data.dst;

                return new BasicResponse(ResultStatus.OK, "Upload file to folder done successfully", {
                    link: resUploadAvatar.data.dst
                });
            }
        } else {
            return new BasicResponse(ResultStatus.ERROR_INVALID_DATA, "Null avatar value.", arguments)
        } //check uploadf avatar
    } catch (_error) {
        return new BasicResponse(ResultStatus.UNEXPTED_ERROR, _error.message, _error);
    }
}

exports.createFile = (body, path) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, body, (err) => {
            if (err) {
                return reject(new BasicResponseFromError(err));
            };

            return new BasicResponse(ResultStatus.OK, `Create file:[${path}] done successfully.`);
        });
    });
}