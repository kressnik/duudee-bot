/**
 * Module for DI utils modulse
 */
global.__UTILS = {};

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse.js');

__UTILS.INTERFACES = {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
};

const {
    AppLoggerClass,
    Loglevel
} = require('./logger/AppLoggerClass');

__UTILS.AppLoggerClass = AppLoggerClass;
__UTILS.Loglevel = Loglevel;


const {
    SequelizeClass
} = require('./sequelize/sequelize_class');

__UTILS.SequelizeClass = SequelizeClass;