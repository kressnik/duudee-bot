const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');
const {
    AppLoggerClass
} = require('../AppLoggerClass');


const amqp = require('amqplib/channel_api');

const DEF_CONNECTION_TIMEOUT = 6000000;

const {
    RabbitRpcListinerClass,
} = require('./RabbitRpcListinerClass');

const {
    RabbitRpcClientClass
} = require('./RabbitRpcClientClass');


class RabbitConnectionClass {
    constructor() {
        this.logger = new AppLoggerClass('RabbitRPCinputConnectorClass', process.env.RABBIT_RPC_INPUT_CONNECTOR_CLASS_LOG_LEVEL);
        this.rpcLisitners = {};
        this.rpcClients = {};

        this.opt = {
            connectionTimeout: DEF_CONNECTION_TIMEOUT
        }
    }

    async run({
        host,
        user,
        password,
        route,
        port = 5672
    }, {
        connectionTimeout
    }) {
        try {
            const args = Array.from(Object.values({
                host,
                user,
                password,
                port
            })).some(p => p == null);


            if (args) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Wrong values.');
            }

            //set local props
            this.props = {
                host,
                user,
                password,
                route,
                port
            }

            this.opt = {
                connectionTimeout: connectionTimeout || DEF_CONNECTION_TIMEOUT
            }

            let resConn = await this.connect();

            if (resConn.status != ResultStatus.OK) {
                throw resConn;
            }

            this.logger.info(`${resConn.toString()}`);

            this.connection = resConn.data;

            return resConn;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    buildUrl() {
        return `amqp://${this.props.user}:${this.props.password}@${this.props.host}:${this.props.port}/${this.props.route || ''}`;
    }

    async connect() {
        try {
            let url = this.buildUrl();

            this.logger.debug(`Try connect ${url}`);

            this.amqp = await amqp.connect(url, {
                connectionTimeout: this.opt.connectionTimeout
            });

            if (this.amqp == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Null connection');
            }

            return new BasicResponse(ResultStatus.OK, 'Connected.', this.amqp);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //;

    async createChannel() {
        try {
            if (this.amqp == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Connetion is not esteblished.');
            }

            let crChannel = await this.amqp.createChannel();

            let response = new BasicResponse(ResultStatus.OK, 'Channel create successfully', crChannel);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async makeRpcQueueClient({
        queue,
    }, 
        opt
    ) {
        try {
            let id = `${queue}`;

            if (this.rpcClients[queue] != null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Listiner already added to list.');
            }

            //addd listiner to list
            this.rpcClients[id] = new RabbitRpcClientClass(this.connection);

            const response = await this.rpcClients[id].run({
                id: id,
                queue: queue,
                opt
            });

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    async makeRpcListiner({
        ex,
        queue,
        messageProcessor
    },
        opt
    ) {
        try {
            let argsIsNull  = [queue, messageProcessor].some(a => a == null);

            if(argsIsNull == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Incoming parameters is wrong', arguments);
            }

            let id = `${queue}`;

            if (this.rpcLisitners[id] != null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_READY, 'Listiner already added to list.');
            }

            let crChannel = await this.createChannel();

            if (crChannel.status != ResultStatus.OK) {
                throw crChannel;
            }

            //addd listiner to list
            this.rpcLisitners[id] = new RabbitRpcListinerClass();

            const response = await this.rpcLisitners[id].run({
                id: id,
                queue: queue,
                channel: crChannel.data,
                messageProcessor: messageProcessor,
                opt
            });

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }
} //RabbitRPCinputConnectorClass

module.exports.RabbitConnectionClass = RabbitConnectionClass;