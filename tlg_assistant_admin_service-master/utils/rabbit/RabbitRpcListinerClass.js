const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    AppLoggerClass
} = require('../AppLoggerClass');


class RabbitRpcListinerClass {
    constructor () {
        this.logger = new AppLoggerClass('RabbitRpcListinerClass', process.env.RABBIT_RPC_LISTINER_CLASS_LOG_LEVEL);
    }//constructor


    async run ({
        queue,
        channel,
        messageProcessor, 
        opt
    }) {
        try {
            if(queue == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, '`name` have to be set.');
            }
    
            if(channel == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA,  '`channel` have to be set.');
            }

            if(messageProcessor == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, '`messageProcessor` have to be set.');
            }

            this.messageProcessor = messageProcessor;
            this.channel = channel;


            //make props
            this.props = {
                queue: queue,
                opt: {
                    durable: opt ? opt.durable : false
                }
            }
        
            this.channel.assertQueue(this.props.queue, this.props.opt);

            this.channel.consume(queue, this.rpcQueueReciever.bind(this));
 
            const response =  new BasicResponse(ResultStatus.OK, `Consume queue: [${this.props.queue}]. Awaiting RPC requests`);
           
            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }//run

    parseMessage (msg) {
        try {
            msg.content = JSON.parse(msg.content.toString());
            
            return new BasicResponse(ResultStatus.OK, 'Message presed succsfully.', msg);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    replyTo (queue, correlationId, payload, opt = {}) {
        return new Promise(async (resolve) => {
            try {
                this.logger.info(`Reply to:[${queue}] correlationId:[${correlationId}]`);

                let {
                    drainTimeout
                } = opt;
        
                drainTimeout = drainTimeout || 5000;


                this.logger.info(`opt.drainTimeout:[${drainTimeout}]`);

                //start timer
                this.TimeoutDrainTimer = setTimeout(()=>{
                    return resolve(new BasicResponse(ResultStatus.ERROR_TIMEOUT, 'Message drain time out'));
                }, opt ? (opt.drainTimeout || 5000) : 5000);


                let msg = Buffer.from(JSON.stringify(payload));

                let opts = {
                    correlationId: correlationId
                }
                
                let send = await this.channel.sendToQueue(queue, msg, opts);

                if(send != true) {
                    throw new BasicResponse(ResultStatus.ERROR, 'Send message to queue error.');
                }

                const response = new BasicResponse(ResultStatus.OK, `Send messge to queue:[${queue}] with correlationId:[${correlationId}] done successfully.`);

                this.logger.info(response.toString());

                return resolve(response);
            } catch (e) {
                let error = new BasicResponseByError(e);

                this.logger.error(error.toString());

                return resolve(error);
            }
        }); 
    }

    async rpcQueueReciever (rabbitRpcCall) {
        try {
            if(rabbitRpcCall == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Incoming rpc call is null');
            }

            const {
                correlationId,
                replyTo,
                byteLength
            } = rabbitRpcCall.properties;


            this.logger.info(`Recive message correlationId[${correlationId}], replyTo:[${replyTo}], size(bytes):[${byteLength}]`);
            
            rabbitRpcCall = this.parseMessage(rabbitRpcCall);

            if(rabbitRpcCall.status != ResultStatus.OK) {
                throw rabbitRpcCall;
            }

            this.logger.info(rabbitRpcCall.toString());

            //reduce 
            rabbitRpcCall = rabbitRpcCall.data;

            let procRes = await this.messageProcessor.call(rabbitRpcCall.content);

            // if(procRes.status != ResultStatus.OK) {
            //     throw procRes;
            // }

            let send = await this.replyTo(
                rabbitRpcCall.properties.replyTo,
                rabbitRpcCall.properties.correlationId,
                procRes
            );

            if(send.status != ResultStatus.OK) {
                throw send
            }

            this.channel.ack(rabbitRpcCall);

            const response = new BasicResponse(ResultStatus.OK, `Send message with correlationId:[${correlationId}] to queue:[${replyTo}], done successfully.`);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } 
}//RabbitRPCListinerClass


module.exports.RabbitRpcListinerClass = RabbitRpcListinerClass;