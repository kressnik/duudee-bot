const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    AppLoggerClass
} = require('../AppLoggerClass');

const uuid = require('uuid');
const EventEmitter = require('events');


const DEFAULT_TIMOUT_REQ_REP_MS = 2000;
const replyToQueue = 'amq.rabbitmq.reply-to';


class RabbitRpcClientClass {
    constructor(connection) {
        this.logger = new AppLoggerClass('RabbitRpcClientClass', process.env.RABBIT_RPC_LISTINER_CLASS_LOG_LEVEL);
        this.connection = connection;

        if (this.connection == null) {
            throw new Error('Connection have tobe set.');
        }

        this.channel = null;
    } //constructor


    async run({
        queue,
        opt
    }) {
        try {
            if (queue == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, '`name` have to be set.');
            }

            this.channel = await this.connection.createChannel();

            if (this.channel == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Error create return null channel.');
            }

            this.logger.debug(`Create cahnnel:[${this.channel.ch}] done.`);

            // create an event emitter where rpc responses will be published by correlationId
            this.channel.responseEmitter = new EventEmitter();
            this.channel.responseEmitter.setMaxListeners(0);


            // channel.consume(REPLY_QUEUE,
            // (msg) => channel.responseEmitter.emit(msg.properties.correlationId, msg.content),
            // {noAck: true});

            //consume response from reply to queue
            await this.channel.consume(replyToQueue, this.consumeHandler.bind(this), {
                noAck: true
            });

            let response = new BasicResponse(ResultStatus.OK, 'run ', this);

            this.queue = queue;

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //run

    async consumeHandler(msg) {
        try {
            msg.content = msg.content.toString();

            msg.content = JSON.parse(msg.content);

            //this.channel.close();
            const descr = `Recieve message - cahnnel:[${this.channel.ch}] corrId[${msg.properties.correlationId}] queue:[${this.queue}]`;

            this.logger.debug(descr);

            this.channel.responseEmitter.emit(msg.properties.correlationId, msg);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            this.channel.responseEmitter.emit(msg.properties.correlationId, msg);
        }

    }

    parseMessage(msg) {
        try {
            msg.content = JSON.parse(msg.content.toString());

            return new BasicResponse(ResultStatus.OK, 'Message presed succsfully.', msg);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    requestReply({
        payload,
        timeOut = DEFAULT_TIMOUT_REQ_REP_MS
    }) {
        return new Promise(async resolve => {
            try {
                if ([payload, timeOut].some(a => a == null)) {
                    throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Wrong incoming arguments', arguments);
                }

                if(this.channel == null) {
                    throw new BasicResponse(ResultStatus.ERROR_NOT_READY, 'Channel not ready or connect to server.');
                }

                let recieveTimeOutTimer = null;

                const corrId = uuid.v4();
                const queue = this.queue;

                this.logger.debug(`Start request to queue:[${queue}] corrId:[${corrId}]`);

                let reciveMessageHanndler = (response) => {
                    if (response.properties.correlationId == corrId) {
                        clearTimeout(recieveTimeOutTimer);

                        const {
                            status,
                            message,
                            data
                        } = response.content;
                       
                        let content = new BasicResponse(status, message, data);

                        resolve(content);
                    } else {
                        const descr = `Recived message with wrong correlationId cahnnel:[${this.channel.ch}] corrId[${msg.properties.correlationId}] queue:[${queue}]`;

                        this.logger.warn(descr);

                        resolve(new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, descr, msg.content));
                    }
                }

                this.channel.responseEmitter.once(corrId, reciveMessageHanndler);

                this.logger.debug(`Consume done - cahnnel:[${this.channel.ch}] corrId[${corrId}] queue:[${queue}]`);

                //prepare msg
                let msg = Buffer.from(JSON.stringify(payload));
                let props = {
                    correlationId: corrId,
                    replyTo: replyToQueue
                };

                let sendResults = await this.channel.sendToQueue(queue, msg, props);

                if (sendResults == false) {
                    throw new BasicResponse(ResultStatus.ERROR, `Send message done with error. cahnnel:[${channel.ch}] corrId[${corrId}] queue:[${queue}]`)
                }

                this.logger.info(`Send message by cahnnel:[${this.channel.ch}] corrId:${props.correlationId} to queue:[${queue}] done with error.`);

                this.logger.debug(`Start waiting reply message - cahnnel:[${this.channel.ch}] corrId[${corrId}] queue:[${queue}]`);

                recieveTimeOutTimer = setTimeout(() => {
                    const descr = `Recieve message fault, timeout:[${timeOut}] cahnnel:[${this.channel.ch}] corrId[${corrId}] queue:[${queue}]`;

                    this.logger.error(descr);

                    this.channel.responseEmitter.removeListener(props.correlationId, reciveMessageHanndler)

                    resolve(new BasicResponse(ResultStatus.ERROR_TIMEOUT, descr));
                }, timeOut);
            } catch (e) {
                let error = new BasicResponseByError(e);

                this.logger.error(error.toString());

                resolve(error);
            }
        });
    }

    reduceMsg(msg) {
        try {
            let response = JSON.parse(msg.toString());

            return response;

        } catch (e) {
            return {};
        }
    };

} //RabbitRpcClientClass


module.exports.RabbitRpcClientClass = RabbitRpcClientClass;