(function ($) {
    $.fn.buttonLoader = function (action) {
      var self = $(this);
      if (action == 'start') {
        $(self).prop("disabled", true);
        $(self).addClass("btn-loading");
      }
      if (action == 'stop') {
        $(self).removeClass('btn-loading');
        $(self).prop("disabled", false);
      }
      return self;
    }
  })(jQuery);

  (function ($) {
    $.fn.boxLoader = function (action) {
      var self = $(this);
      if (action == 'start') {
        $(".dimmer", self).addClass("active");
      }
      if (action == 'stop') {
        $(".dimmer", self).removeClass('active');
      }
      return $(this);
    }
  })(jQuery);