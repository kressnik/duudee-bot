/*** Вывод сообчения по номеру ***/
function ShowMessage(_typeNumMessage, _messageTextOrNum, _type) {//(Номер цвета выводв notie, Тип сообщения,Номер сообщения)

    var numMessage = _messageTextOrNum;
    var errorType = _type;
    var textMessage = "ERRORs";

    if (numMessage == null) {
        numMessage = "DEFAULT_MESSAGE";
    }

    if(errorType == null){
        errorType = "error";
    }

    if(LANG_FILE != null && LANG_FILE.message != null){
        if (LANG_FILE.message[errorType][numMessage] == null) {
            textMessage = "#" + numMessage;
        }
        else {
            textMessage = LANG_FILE.message[errorType][numMessage];
        }
    }

    notie.alert({
        type: _typeNumMessage,
        text: textMessage,
        time: 2.5
    });

}
/*** Вывод сообчения в случии если пришол error в запросе ***/
function CheckResponse(json, jqXHR) {

    var textCode = "";
    var code = parseInt(jqXHR.status);
    var messageCode = "DEFAULT_MESSAGE";

    if (json != undefined) {
        code = (json.code == null ? code : json.code);
        textCode = json.status === undefined ? code : json.status;
        messageCode = json.messageCode === undefined ? "DEFAULT_MESSAGE" : json.messageCode;
    }
    
    if (((code >= 200) && (code <= 299) && (code != "")) || textCode === "OK") {
        return true;
    }
    else { 
        ShowMessage("error", messageCode, "error");
        return false;
    }
}


function ShowMessageError(_request, _error, defaultMessage) {

    // console.log(_request);

    let _responseJSON = _request.responseJSON;
    let status = _request.status;

    if (status === 401 || status === 0) {
        if (_error === "error") {
            window.location.href = "/login.html";
        }
    }
    else {
        if (_responseJSON === undefined || _responseJSON.messageCode === undefined) {
            if (defaultMessage === undefined || defaultMessage === undefined) {
                _responseJSON = { messageCode: _responseJSON.messageCode };
            }
            else {
                _responseJSON = { messageCode: defaultMessage };
            }
        }

        if (_responseJSON.messageCode === "LOGIN_ALREADY_DONE") {
            window.location.href = "/index.html";
        }
        else {
            switch (_error) {
                case "error":
                    ShowMessage("error", _responseJSON.messageCode, _error);
                    break;
                default:
                    ShowMessage("error", defaultMessage, _error);
                    break;
            }
        }
    }



}