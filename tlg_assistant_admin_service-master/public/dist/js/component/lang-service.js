// Create the event
var eventLoadLangReady = new CustomEvent("lang-ready", { "detail": "Language loading successfuly" });
var LANG_SYSTEM = "en";
var LANG_IN_SYSTEM = ["ru", "en"];
var LANG_FILE = {};
var app = new Vue({
    data: {
        lang: {}
    }
});

document.addEventListener("header-box-ready", LangSystem);

function getListLangs(_cb) {
    $.getJSON("/langs/", function (json) {//Получаем файл с переводом

        if (json != null && json.data != null && json.data.length != 0) {
            LANG_IN_SYSTEM = [];
            for (const key in json.data) {
                let lang = json.data[key];
                let option = document.createElement('option');
                option.text = lang.toUpperCase();
                option.value = lang;
                $("#j-lang-system").append(option);
                LANG_IN_SYSTEM.push(lang);
            }

        }

        _cb(LANG_IN_SYSTEM);
    });
}

function LangSystem() {

    getListLangs(function (_allLangInSystem) {

        if ($.inArray(LANG_SYSTEM, _allLangInSystem) == -1) {
            LANG_SYSTEM = _allLangInSystem[0];
        }

        let lang = $.cookie('lang');

        if (lang == "undefined") {
            lang = LANG_SYSTEM;
        } else {
            if ($.inArray(lang, _allLangInSystem) == -1) {
                lang = LANG_SYSTEM;
            }
        }

        if ($.cookie('lang') != "undefined") {
            LANG_SYSTEM = lang;
        } else {
            $.cookie('lang', lang);
        }

        getLangSystem(lang);
    });
}

function getLangSystem(_lang) {//Функция, которая изменяет язык системы

    if (_lang === undefined) {
        let langDefault = $("html").attr("lang");

        if (LANG_IN_SYSTEM.indexOf(langDefault) === -1) {//Проверка есть ли язык в системе
            _lang = LANG_SYSTEM;
        } else {
            _lang = langDefault;
        }

        LANG_SYSTEM = langDefault;
    }

    let fileLang = "langs/" + _lang;
    $.getJSON(fileLang, function (json) {//Получаем файл с переводом
        LANG_FILE = json.data;
        if (app.$el !== undefined) {
            app.lang = LANG_FILE;
        } else {
            app.lang = LANG_FILE;
            app.$mount("#app");
        }
        $("#j-lang-system option[value='" + _lang + "']").prop("selected", true);
        // Dispatch/Trigger/Fire the event
        console.log("Загрузились переводы");
        document.dispatchEvent(eventLoadLangReady);

    });
}

$(document).on("change", "#j-lang-system", function () {//Изменяем язык системы на ту которую выбрал пользователь

    let selectedLang = $(this).val();
    $("html").attr("lang", selectedLang);

    $.ajax({
        url: "/change-language",
        method: "PUT",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ lang: selectedLang }),
        timeout: 10000
    });

    $.cookie('lang', selectedLang);
    window.location.reload();

});