redirectAuthorizedUserToIndex(function (result) {
    if (result !== false) {
        window.location.href = "/index.html";
    }
    //autoLogin();
});


$(document).on("click", "#login-user", LoginUser);
$(document).keydown(function (eventObject) { //отлавливаем нажатие клавиш
    if (eventObject.keyCode == 13) { //если нажали Enter, то true
        $("#login-user").click();
    }
});

function LoginUser() {

    var $thisBtn = $(this);
    let $form = $thisBtn.parents(".j-login-form").first();
    let login = $("[name='login']", $form).val();
    let password = $("[name='password']", $form).val();
    let twoFactorCode = $("[name='twoFactor']", $form).val();
    var $boxTwoFactor = $(".j-two-factor-login");

    if (twoFactorCode == '') {
        twoFactorCode = null;
    }

    $thisBtn.buttonLoader("start");

    var paramsAjax = {
        url: "login",
        method: "POST",
        defaultMessage: "DEFAULT_MESSAGE",
        data: {
            "login": login,
            "password": password,
            "twoFactorCode": twoFactorCode
        }
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            let response = json.data;

            if (response.status == 'OK') {
                let lang = json.data.lang;

                if (lang != null) {
                    $.cookie('lang', lang);
                }
                window.location.href = "/index.html";
            } else if (response.status == 'SUCCESS_NO_CONTENT') {
                $("[name='login']", $form).prop("disabled", true);
                $("[name='password']", $form).prop("disabled", true);
                $boxTwoFactor.removeClass("collapse");
            } else {
                //todod popup errer
            }
        } else {
            //todod popup errer
        }

        $thisBtn.buttonLoader("stop");
    })
}

function autoLogin() {
    var paramsAjax = {
        url: "login",
        method: "POST",
        defaultMessage: "DEFAULT_MESSAGE",
        data: {
            "login": "root",
            "password": "root"
        }
    }
    QueryAjax(paramsAjax, function (result, json) {

        if (result) {

            let lang = json.data.lang;

            if (lang != null) {
                $.cookie('lang', lang);
            }
            window.location.href = "/index.html";
        }

        $thisBtn.buttonLoader("stop");
    })
}