const IS_FREE_CHANNEL = {
    TRUE: "FREE",
    FALSE: "PAYED"
};

const CHANNEL_STATES_TYPES = {
    ACTIVE: 1,
    DISACTIVE: 0,
    DELETE: 2
};

document.addEventListener("lang-ready", function () {
    getAndDrawChannelsList();
    initColorInputToMotalChannel();
    $('[data-inputmask]').inputmask();
});

$(document).on("click", ".j-channel-list .j-refresh", refreshChannelsList); //Обновления информации о конвертеров
$(document).on("click", ".j-channel-list .j-edit-add", showModalForAddOrEditChannel);
$(document).on("change", "#add-edit-channel .j-channel-type", isShowFieldsPayedChannel);
$(document).on("click", "#add-edit-channel .j-add", verifyAndAddChannel);
$(document).on("click", ".j-channel-list [name='state']", changeChannelState);
$(document).on('hide.bs.modal', "#add-edit-channel", cleaningModalAddConverter);
$(document).on("click", ".j-delete", showDeleteChannelModal);
$(document).on("keyup input", "#modal-delete .j-input-type", checkDeleteInputVal);
$(document).on("click", "#modal-delete .j-btn-del", softDeleteChannel);
$(document).on('hide.bs.modal', "#modal-delete", clearDeleteModal);

$(document).on("click", ".j-channel-list .j-statistic", showStatisticInfo);
$(document).on("click", ".j-reff-channel-statistic .j-hide-statistic", hideStatisticInfo);

/**
 * channels list
 */

function getAndDrawChannelsList() {
    let $channelsTable = $('#table-channels');

    $channelsTable.DataTable().destroy();
    $channelsTable.DataTable({
        ajax: {
            url: '/channels/all',
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);
            },
            dataFilter: function (resultData) {
                let data = JSON.parse(resultData).data;
                return JSON.stringify(data);
            }
        },
        processing: false,
        serverSide: true,
        createdRow: function (row, channel, numRow) {
            let $newChannelTr = $(row);
            let id = channel.id;
            let name = channel.name;
            let tlgId = channel.tlgId;
            let currency = channel.currency;
            let isFree = channel.isFree;
            let state = channel.state;
            let description = channel.description;
            let price = channel.price;
            let color = channel.color;

            $newChannelTr.removeClass("tr-warning tr-success");
            $newChannelTr.attr({
                "data-id": id,
                "data-name": name,
                "data-tlg-id": tlgId,
                "data-currency": currency,
                "data-is-free": isFree,
                "data-state": state,
                "data-description": description,
                "data-price": price,
                "data-color": color
            });

            switch (channel.state) {
                case CHANNEL_STATES_TYPES.ACTIVE:
                    $newChannelTr.addClass("tr-success");
                    break;
                case CHANNEL_STATES_TYPES.DISACTIVE:
                    $newChannelTr.addClass("tr-warning");
                case CHANNEL_STATES_TYPES.DELETE:
                    $newChannelTr.addClass("tr-danger");
                    break;
            }
        },
        columns: [
            {
                name: "id",
                data: "id",
                orderable: false,
                render: function (id, type, data) {
                    return `<span class="text-muted">${id}</span>`;
                }
            }, {
                name: "name",
                data: "name",
                orderable: false,
                render: function (name, type, data) {
                    return [
                        `<span class="status-icon" style="background-color:${rgbA2hex(data.color)}"></span>`,
                        `<span class="j-name">${name}</span>`,
                        `<span class="form-help collapse j-desc" tabindex="0"`,
                        `data-toggle="popover" data-trigger="focus" data-content="">?</span>`,
                        `<div class="small text-muted"></div>`
                    ].join('');
                }
            }, {
                name: "state",
                data: "state",
                orderable: false,
                render: function (state, type, data) {
                    let checked = '';
                    let disabled = '';

                    switch (state) {
                        case CHANNEL_STATES_TYPES.ACTIVE:
                            checked = 'checked';
                            break;
                        case CHANNEL_STATES_TYPES.DISACTIVE:
                            break;
                        case CHANNEL_STATES_TYPES.DELETE:
                            disabled = 'disabled';
                            break;
                    };

                    return [
                        '<label class="custom-switch">',
                        '<input type="checkbox" name="state"',
                        `class="custom-switch-input" ${checked} ${disabled}>`,
                        '<span class="custom-switch-indicator"></span>',
                        '</label>'
                    ].join('');
                }
            }, {
                name: "isFree",
                data: "isFree",
                orderable: false,
                render: function (isFree, type, data) {
                    let isFreeChannel = isFree != true ? IS_FREE_CHANNEL.FALSE : IS_FREE_CHANNEL.TRUE;

                    return `<span class="badge badge-default ml-1">${isFreeChannel.toUpperCase()}</span>`;
                }
            }, {
                name: "price",
                data: "price",
                orderable: false,
                render: function (price, type, data) {

                    return `<span>${price || "-"} ${price != 0 ? data.currency : ""}</span>`;
                }
            },
            {
                name: "options",
                data: "state",
                orderable: false,
                render: function (state, type, row) {
                    let html = '';
                    let statsButton = [
                        '<button type="button"',
                        'class="btn btn-icon btn-primary btn-secondary j-statistic"',
                        `title="${LANG_FILE.stats}">`,
                        '<i class="fe fe-bar-chart"></i>',
                        '</button>'
                    ].join('');

                    let editButton = [
                        '<button type="button"',
                        'class="btn btn-icon btn-primary btn-secondary j-edit-add"',
                        'data-toggle="modal" data-target="#edit-refferal-link"',
                        `title="${LANG_FILE.edit}">`,
                        '<i class="fe fe-edit"></i>',
                        '</button>'
                    ].join('');

                    let deleteButton = [
                        '<button type="button"',
                        'class="btn btn-icon btn-primary btn-secondary j-delete"',
                        `title="${LANG_FILE.del}">`,
                        '<i class="fe fe-trash"></i>',
                        '</button>'
                    ].join('');

                    switch (state) {
                        case CHANNEL_STATES_TYPES.ACTIVE:
                            html = [
                                editButton,
                                statsButton
                            ].join('');
                        break;
                        case CHANNEL_STATES_TYPES.DISACTIVE:
                            html = [
                                editButton,
                                statsButton,
                                deleteButton
                            ].join('');
                        break;
                        case CHANNEL_STATES_TYPES.DELETE:
                            html = statsButton;
                        break;
                    };

                    return ['<div class="btn-list">', html, '</div>'].join('');
                }
            }
        ],
        paging: true,
        info: true, //Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        language: LANG_FILE.dataTablesLanguage,
        order: [], //сортировка
    });
}

function isShowFieldsPayedChannel() {
    let $modalBox = $(this).closest(".modal");
    let idHTML = $modalBox.attr("id");

    if (idHTML != null) {
        let $modalBox = $(`#${idHTML}`);
        let $currencyInputBox = $(".j-channel-currency", $modalBox).closest(".form-group");
        let $priceSelectBox = $(".j-channel-price", $modalBox).closest(".form-group");
        let selectedType = $(".j-channel-type", $modalBox).val();

        if (selectedType == IS_FREE_CHANNEL.TRUE) {
            $currencyInputBox.addClass("collapse");
            $priceSelectBox.addClass("collapse");
        } else {
            $currencyInputBox.removeClass("collapse");
            $priceSelectBox.removeClass("collapse");
        }
    }
}

function showModalForAddOrEditChannel() { //Открытия модального окна для редактирования канала
    let $thisBtn = $(this); //Кнопка на которую был сделан клик
    let $modal = $("#add-edit-channel"); //Модальное окно которое нужно открыть
    let $modalTitle = $(".modal-title", $modal);
    let $repeated = $(".j-repeated", $modal);

    let channelInfo = $thisBtn.parents("tr").data();

    let {
        id,
        name,
        tlgId,
        currency,
        isFree,
        state,
        description,
        price,
        color
    } = channelInfo || {};

    $modalTitle.text(`${LANG_FILE.channels.modalCreate.edit} #${id} - ${name}`);

    $(".j-channel-name", $modal).val(name || '');
    $(".j-channel-tlg-id", $modal).val(tlgId || '');
    $(".j-channel-price", $modal).val(price || '-');
    $(".j-channel-description", $modal).val(description || '');
    $("[name='active-channel']", $modal).prop("checked", state);
    $("[name='color-channel']", $modal).colorpicker('setValue', color || 'rgba(133, 211, 202, 0.2)');
    $("[name='color-channel']", $modal).click();
    $(`.j-channel-type [value="${isFree == true ? IS_FREE_CHANNEL.TRUE : IS_FREE_CHANNEL.FALSE}"]`, $modal).prop("selected", true).change();
    $(`.j-channel-currency [value="${currency}"]`, $modal).prop("selected", true);

    $(".j-add", $modal)
        .text(LANG_FILE.edit)
        .data("id", id);

    $repeated.prop("checked", false).parents(".form-group").addClass("collapse");
    $modal.modal('show');
};

function changeChannelState() {
    let $this = $(this);
    let $tr = $this.parents("tr").first();
    let state = $this.prop("checked");
    let channelInfo = $tr.data();
    let channelID = channelInfo.id;

    $this.prop("disabled", true);

    let paramsAjax = {
        url: `channel/${channelID}/state/${state}`,
        dataType: "JSON",
        method: "PUT",
        timeout: 2000,
        defaultMessage: "DEFAULT_MESSAGE"
    };

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            ShowMessage("success", json.messageCode, "ok");
            refreshChannelsList();
        } else {
            $this.prop("checked", (state == true ? false : true));//Возвращаем каретку в обратное положения если пришла ошибка
        }
    });
};

function sendAddChannel(_channelInfo, _$thisBtn) {
    _$thisBtn.buttonLoader("start");

    let paramsAjax = {
        url: "channel",
        dataType: "json",
        method: "POST",
        data: _channelInfo,
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE"
    };

    if (_channelInfo.id != null &&
        _channelInfo.id != "" &&
        _channelInfo.id > 0
    ) {
        paramsAjax.url = "channel/" + _channelInfo.id;
        paramsAjax.method = "PUT"
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            refreshChannelsList();
            ShowMessage("success", json.messageCode, "ok");

            if (_channelInfo.repeated === false) {
                $('#add-edit-channel').modal('hide');
            } else {
                cleaningModalAddConverter(true);
            }
        }

        _$thisBtn.buttonLoader("stop");
    });
}

function verifyAndAddChannel() {
    let $thisBtn = $(this);
    let $modal = ("#add-edit-channel");
    let channelInfo = $(".j-add", $modal).data();

    let $tlgIdInput = $(".j-channel-tlg-id", $modal);
    let $nameInput = $(".j-channel-name", $modal);
    let $priceInput = $(".j-channel-price", $modal);

    let type = $(".j-channel-type option:selected", $modal).val();
    let description = $(".j-channel-description", $modal).val();
    let currency = $(".j-channel-currency", $modal).val();
    let inUse = $("[name='active-channel']", $modal).prop("checked");
    let repeated = $(".j-repeated", $modal).prop("checked");
    let color = $("[name='color-channel']", $modal).colorpicker('getValue');
    let idInDB = null;

    if (channelInfo != null) {
        idInDB = channelInfo.id;
    };

    let poolPropsAdd = {
        id: idInDB,
        type,
        name: ValidateInput($nameInput),
        tlgId: ValidateInput($tlgIdInput),
        currency,
        price: ValidateInput($priceInput),
        description,
        inUse,
        repeated,
        color
    };

    if (poolPropsAdd.tlgId != "error") {
        let tlgId = $tlgIdInput.inputmask('unmaskedvalue');

        if (tlgId.length != 10) {
            $tlgIdInput.removeClass("state-valid").addClass("state-invalid");
            poolPropsAdd.tlgId = "error";
        } else {
            $tlgIdInput.removeClass("state-invalid").addClass("state-valid");
        }
    }

    const argsCheck = Object.values(poolPropsAdd).some(_a => _a == "error");

    if (argsCheck == true) {
        ShowMessage("error", LANG_FILE.message.error.EMPTY_INPUT, "error");
    } else {
        sendAddChannel(poolPropsAdd, $thisBtn);
    }
};

function cleaningModalAddConverter(_repeated) {
    let $modalAddConverter = ("#add-edit-channel"); //Модальное окно
    let clearInputArr = [
        ".j-channel-name",
        ".j-channel-tlg-id",
        ".j-channel-description",
        ".j-channel-price"
    ]; //Инпуты которые нужно очистить

    let repeated = (_repeated === true ? true : false);

    for (let index = 0; index < clearInputArr.length; index++) { //Чистка инпутов
        $(clearInputArr[index], $modalAddConverter)
            .removeClass("state-valid state-invalid")
            .val('')
            .prop("disabled", false);
    }

    $(".j-channel-type", $modalAddConverter).prop("selectedIndex", 0).change();
    $(".j-channel-currency", $modalAddConverter).prop("selectedIndex", 0).change();
    $("[name='color-channel']", $modalAddConverter).colorpicker('setValue', "rgba(220, 223, 226, 0.2)");
    $("[name='color-channel']", $modalAddConverter).click();
    $("[name='active-channel']", $modalAddConverter).prop("checked", false);

    $(".modal-title", $modalAddConverter).text(LANG_FILE.channels.modalCreate.add); //Изменения заголовка модального окна

    $(".j-repeated", $modalAddConverter)
        .prop("checked", repeated)
        .parents(".form-group")
        .removeClass("collapse"); //Отображения кнопки повторного добавления

    $(".j-add", $modalAddConverter)
        .text(LANG_FILE.add)
        .removeData(); //Изменяем названия кнопки
}

function refreshChannelsList() {
    $('#table-channels').DataTable().ajax.reload(null, false);
}

function showDeleteChannelModal() {
    let $thisBtn = $(this);
    let $modal = $("#modal-delete");
    let tr = $thisBtn.parents("tr").first();
    let channelInfo = tr.data();
    let id = channelInfo.id;

    let modalTitle = `${channelInfo.name} #${id}`;
    let defaultWord = (channelInfo.name == "" ? "DELETE" : channelInfo.name);

    $(".j-type-name", $modal).text(channelInfo.name);
    $(".j-type", $modal).text(defaultWord);
    $(".j-input-type", $modal).data({
        "original-type": defaultWord,
        "id": id
    });

    $(".modal-title", $modal).text(modalTitle);
    $modal.modal('show');
}

function checkDeleteInputVal() {
    let $thisInput = $(this);
    let $modal = $thisInput.parents("#modal-delete");
    let $btnDelete = $(".j-btn-del", $modal);
    let typeInput = $.trim($thisInput.val());
    let originalType = $thisInput.data("original-type");

    if (typeInput == originalType) {
        $btnDelete
            .addClass("btn-danger")
            .removeClass("btn-gray")
            .prop("disabled", false);
    } else {
        $btnDelete
            .removeClass("btn-danger")
            .addClass("btn-gray")
            .prop("disabled", true);
    };
}

function softDeleteChannel() {
    let $thisBtn = $(this);
    let $modal = $("#modal-delete");
    let { id } = $(".j-input-type", $modal).data();

    $thisBtn.buttonLoader("start");

    let paramsAjax = {
        url: `channel/${id}`,
        dataType: "json",
        method: "PUT",
        data: {
            id,
            inUse: CHANNEL_STATES_TYPES.DELETE
        },
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE"
    };

    QueryAjax(paramsAjax, function (result, json) {
        if (result == true) {
            ShowMessage('success', json.messageCode, "ok");
            refreshChannelsList();

            $thisBtn.buttonLoader("stop");
            $('#modal-delete').modal('hide');
        } else {
            ShowMessageError(request, error, "DEFAULT_MESSAGE");
            $thisBtn.buttonLoader("stop");
        }
    });
}

function clearDeleteModal() {
    let $this = $(this);
    $(".j-input-type", $this).removeData().val("");
    $(".j-btn-del", $this)
        .removeClass("btn-danger")
        .addClass("btn-gray")
        .prop("disabled", true);
}


/**
 * stats
 */

function showStatisticInfo() {
    var $thisBtn = $(this);
    var $channelTr = $thisBtn.parents("tr").first();
    var channelInfo = $channelTr.data();

    var $boxTable = $(".j-channels .j-channel-list");
    var $boxChannelReffStats = $(".j-reff-channel-statistic");

    $thisBtn.buttonLoader("start");

    $(".j-id", $boxChannelReffStats).text('');
    $(".j-name", $boxChannelReffStats).text('');
    $(".j-is-free", $boxChannelReffStats).text('');
    $(".j-price", $boxChannelReffStats).text('');

    DrawControllerTr($("#reff-channel-statistic .j-tr"), channelInfo);

    $boxTable.addClass("collapse");
    $boxChannelReffStats.removeClass("collapse");

    $thisBtn.buttonLoader("stop");
}

function DrawControllerTr($channelTr, _channelInfo) {
    $channelTr.removeClass("collapse").show();

    let {
        id,
        name,
        price,
        currency,
        color,
        state,
        isFree
    } = _channelInfo;

    let chName = `<span class="j-name">${name}</span>`;
    let chColor = `<span class="status-icon j-color" style="background-color:${rgbA2hex(color)}"></span>`;

    let channelIsFree = isFree != true ? IS_FREE_CHANNEL.FALSE : IS_FREE_CHANNEL.TRUE;
    channelIsFree = `<span class="j-is-free badge badge-default ml-1">${channelIsFree.toUpperCase()}</span>`;

    price = `${price || "-"} ${price != 0 ? currency : ""}`;

    $channelTr.data("id", id);
    $(".j-id", $channelTr).text(id);
    $(".j-name", $channelTr).replaceWith(chName);
    $(".j-color", $channelTr).replaceWith(chColor);
    $(".j-is-free", $channelTr).replaceWith(channelIsFree);
    $(".j-price", $channelTr).text(price);

    $channelTr.removeClass("tr-success tr-danger tr-offline");

    switch (state) {
        case CHANNEL_STATES_TYPES.ACTIVE:
            $channelTr.addClass("tr-success");
            break;
        case CHANNEL_STATES_TYPES.DISACTIVE:
            $channelTr.addClass("tr-warning");
        case CHANNEL_STATES_TYPES.DELETE:
            $channelTr.addClass("tr-danger");
            break;
    };

    let checked = state !== CHANNEL_STATES_TYPES.ACTIVE ? "" : "checked";
    state = [
        '<span class="j-state">',
        '<label class="custom-switch">',
        '<input type="checkbox" name="active"',
        `class="custom-switch-input" ${checked} disabled>`,
        '<span class="custom-switch-indicator"></span>',
        '</label>',
        '</span>'
    ].join('');

    $(".j-state", $channelTr).replaceWith(state);

    getStatistic({
        channelId: id
    });
}

async function getStatistic(_prms) {
    const { channelId } = _prms;

    let data = {
        filter: {
            id: [],
            status: [],
            tokenId: [],
            buttonId: [],
            channelId: [channelId],
            orderId: [],
            sessionId: [],
            error: []
        }
    };

    let paramsAjax = {
        url: `channel/reff/stats/${channelId}`,
        method: "POST",
        data,
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE",
    };

    let { result, json } = await QueryAjaxPromise(paramsAjax);
    if (result) {
        drawStatisticCharts(json.data);
    } else {
        ShowMessage("error", LANG_FILE.message.error.DATA_NOT_FOUND, "error")
    };
}

function drawStatisticCharts(data) {
    if (data != null) {
        $('#charts-stats .j-stats').remove();
        $('#charts-stats').append('<canvas class="j-stats"> <canvas>');
        let $totalVisitsChart = $('#charts-stats .j-stats');

        const {
            newSubscribes,
            returnSubscribes,
            errorSubscribes,
            stopSubscribes,
            uniqueComeUsers
        } = data.uniqueChannelReffStats;

        let doughnutLegend = {
            position: "right",
            align: "end"
        };
        let titleOptions = {
            display: true,
            position: 'top',
            fontSize: 14,
            padding: 10
        };

        Chart.defaults.global.defaultFontFamily = 'Neue';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#444';

        new Chart($totalVisitsChart, {
            type: 'doughnut',
            data: {
                labels: [
                    `${LANG_FILE.channels.stats.table.doneSubscribes}: ${newSubscribes}`,
                    `${LANG_FILE.channels.stats.table.returnedSubscribes}: ${returnSubscribes}`,
                    `${LANG_FILE.channels.stats.table.errorSubscribes}: ${errorSubscribes}`,
                    `${LANG_FILE.channels.stats.table.stopSubscribes}: ${stopSubscribes}`
                ],
                datasets: [{
                    data: [
                        newSubscribes,
                        returnSubscribes,
                        errorSubscribes,
                        stopSubscribes
                    ],
                    backgroundColor: [
                        'rgba(75, 192, 192, 0.4)',
                        'rgba(255, 159, 64, 0.4)',
                        'rgba(255, 99, 132, 0.4)',
                        'rgba(54, 162, 235, 0.4)'
                    ],
                    borderColor: [
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    text: `${LANG_FILE.channels.stats.table.uniqueTracksUsers}: ${uniqueComeUsers}`,
                    ...titleOptions
                },
                legend: doughnutLegend,
                tooltips: {
                    enabled: false
                }
            }
        });

        drawChannelReffStats(data);
    }
}

function drawChannelReffStats(_data) {
    const {
        statsByReffLink,
        totalChannelReffStats
    } = _data;

    let {
        returnSubscribes,
        newSubscribes,
        errorSubscribes,
        stopSubscribes,
        uniqueComeUsers
    } = totalChannelReffStats;

    let $table = $('#table-channel-reff-links');
    let $tBody = $('tbody', $table);

    let $channelsTrs = statsByReffLink.map(function (link) {
        let returnSubscribes = link.tracksStats.returnSubscribes != null ? link.tracksStats.returnSubscribes : 0;
        let newSubscribes = link.tracksStats.newSubscribes != null ? link.tracksStats.newSubscribes : 0;
        let errorSubscribes = link.tracksStats.errorSubscribes != null ? link.tracksStats.errorSubscribes : 0;
        let stopSubscribes = link.tracksStats.stopSubscribes != null ? link.tracksStats.stopSubscribes : 0;
        let uniqueComeUsers = link.tracksStats.uniqueComeUsers != null ? link.tracksStats.uniqueComeUsers : 0;

        return [
            `<tr>`,
            `<td>${link.reffLinkId}</td>`,
            `<td class="text-left">${link.reffLinkName != null ? link.reffLinkName : '-'}</td>`,
            `<td>${newSubscribes}</td>`,
            `<td>${returnSubscribes}</td>`,
            `<td>${errorSubscribes}</td>`,
            `<td>${stopSubscribes}</td>`,
            `<td>${uniqueComeUsers}</td>`,
            `</tr>`].join('')
    }).join('');

    $tBody.replaceWith(
        `<tbody>${$channelsTrs}</tbody>`
    );

    newSubscribes = newSubscribes != null ? newSubscribes : 0;
    returnSubscribes = returnSubscribes != null ? returnSubscribes : 0;
    errorSubscribes = errorSubscribes != null ? errorSubscribes : 0;
    stopSubscribes = stopSubscribes != null ? stopSubscribes : 0;
    uniqueComeUsers = uniqueComeUsers != null ? uniqueComeUsers : 0;

    let totalTr = [
        `<tr>`,
        `<td>#</td>`,
        `<td class="text-left">${LANG_FILE.channels.stats.table.allRequests}</td>`,
        `<td>${newSubscribes}</td>`,
        `<td>${returnSubscribes}</td>`,
        `<td>${errorSubscribes}</td>`,
        `<td>${stopSubscribes}</td>`,
        `<td>${uniqueComeUsers}</td>`,
        `</tr>`].join('');

    $('tbody', $table).append(totalTr);
}

function hideStatisticInfo() {
    $(".j-channels .j-channel-list").removeClass("collapse");
    $(".j-reff-channel-statistic").addClass("collapse");
}

/**
 * helpers
 */


function initColorInputToMotalChannel() {
    let $input = $('#add-edit-channel [name="color-channel"]');

    $input.colorpicker();
    $input.on('colorpickerChange', function (event) {

        if (event.color != null) {
            $('.jumbotron').css('background-color', event.color.toString());
        } else {
            $input.colorpicker('setValue', 'rgba(133, 211, 202, 0.2)');
        }
    });
}