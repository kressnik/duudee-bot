var usersListJSON = []; //Список конвертеров

document.addEventListener("lang-ready", function (e) {
    getUsersList();
});


/** Работа с конверторами **/
$(document).on("click", ".j-user-list .j-refresh", refreshPoolList); //Обновления таблицы о подписчике
$(document).on("click", ".j-user-list .j-btn-eye", viewUserInfo); //Информация о подписчике
$(document).on("click", ".j-user-info .j-hide-info-user", HideInfoUser);//Закрытия блока информации о подписчике

function HideInfoUser() {
    $(".j-user-list").removeClass("col-md-8").addClass("col-md-12");//Изменения размера блока списка подписчиков
    $(".j-user-info").addClass("collapse");//Закрытия блока информации о подписчике
}

function getUsersList() { //Получения всего списка подписчиков

    var $tableConverters = $('#table-users');
    $tableConverters.DataTable().destroy();
    $tableConverters.DataTable({
        ajax: {
            method: 'post',
            url: '/users',
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);
            },
            dataFilter: function (resultData) {
                let data = JSON.parse(resultData).data;
                return JSON.stringify(data);
            }
        },
        processing: true,
        serverSide: true,
        createdRow: function (row, user, numRow) {

            var $newConverterTr = $(row);
            var userID = user.id;

            usersListJSON[numRow] = user;
            $newConverterTr.attr({
                "data-id": userID,
                "data-num": numRow
            });

            if (numRow % 2) {
                $newConverterTr.css("background-color", "#f4f6f9");
            }
        },
        columns: [{
            name: "id",
            data: "id",
            orderable: false,
            render: function (id, type, data) {
                return id;
            }
        }, {
            name: "name",
            orderable: false,
            render: function (name, type, data) {
                var username = data.username == "" ? "" : `@${data.username}`;
                var userLink = data.username == "" ? "" : `https://t.me/${data.username}`;

                return `
                    <span>${data.name}</span>
                    <div class="small text-muted"><a href="${userLink}" target="_blank">${username}</a></div>
                `;
            }
        }, {
            name: "role",
            data: "role",
            orderable: false,
            render: function (role, type, data) {
                role = role == 0 ? "admin" : "user";

                return role;
            }
        }, {
            name: "tlgInviteLink",
            data: "tlgInviteLink",
            orderable: false,
            render: function (tlgInviteLink, type, data) {
                tlgInviteLink = tlgInviteLink == null ? "--" : tlgInviteLink;

                return tlgInviteLink;
            }
        }, {
            name: "tlgUserId",
            data: "tlgUserId",
            orderable: false,
            render: function (tlgUserId, type, data) {
                return tlgUserId;
            }
        }, {
            name: "channels",
            data: "channels",
            orderable: false,
            render: function (channels, type, data) {
                var channelsArr = channels.map(function (data) {
                    return `<span class="badge badge-default ml-1" style="margin: 2px;">
                    ${data.name}
                    </span>`;
                });

                return `<div style="display: flex; flex-wrap: wrap;">${channelsArr.join("")}</div>`;
            }
        }, 
        // {
        //     name: "options",
        //     orderable: false,
        //     render: function (data, type, row) {
        //         return `
        //         <button type="button"
        //             class="btn btn-icon btn-primary btn-secondary j-btn-eye" title="${LANG_FILE.optionsBtn.eyeControllers}">
        //             <i class="fe fe-eye"></i>
        //         </button>
        //         `;
        //     }
        // }
        ],
        paging: true,
        info: true, //Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        language: LANG_FILE.dataTablesLanguage,
        pageLength: 25,
        order: []
    });
}


function refreshPoolList() {
    $('#table-users').DataTable().ajax.reload(null, false);
}

/** !Работа с конверторами **/

/** Работа с контроллерами **/

function viewUserInfo() {

    var $thisBtn = $(this);
    var $cnverterTr = $thisBtn.parents("tr").first();
    var userId = $cnverterTr.data("id"); //Получения id конвертера

    var $boxTable = $(".j-user-list");
    var $boxUserInfo = $(".j-user-info");

    let userInfo = null;

    for (let user in usersListJSON) {
        if (usersListJSON[user].id == userId) {
            userInfo = usersListJSON[user];
        }
    }

    let channels = userInfo.channels;

    let finalChannelArr = [];

    for (let i = 0; i < channels.length; i++) {
        finalChannelArr.push(channels[i].name);
    }

    finalChannelArr = finalChannelArr.join(' ');

    $(".j-id", $boxUserInfo).html(userId + "<br>" + userInfo.name);
    $(".j-channels-list", $boxUserInfo).text(finalChannelArr);

    $boxTable.removeClass("col-md-12").addClass("col-md-8");//Изменения размера блока списка контроллеров
    $boxUserInfo.removeClass("collapse");//Открытия блоки информации о контролёре



    $thisBtn.buttonLoader("stop");

}
/** !Работа с контроллерами **/