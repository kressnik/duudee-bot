document.addEventListener("lang-ready", function (e) {
    getAuthorizedUserInfo();
    setTimeout(function () {
        $("#fakeLoader").hide();
    }, 500);
});


$(document).on("click", "[data-color-box] input[name='color']", function () {
    var $this = $(this);
    var color = $this.val();
    var colorInput = $this.parents("[data-color-box]").data("color-box");
    $(colorInput).colorpicker('setValue', color);
});

$(document).on("colorpickerHide", "[data-color-input]", selectedColor);
$(document).on("click", "[data-color-input]", selectedColor);

function selectedColor() {
    var color = $(this).colorpicker('getValue');
    $("[data-color-box] input").prop("checked", false);
    $("[data-color-box] input[value='" + color + "']").prop("checked", true);
}

function ValidateInput(input, checked) {

    let val = $(input).val();
    $.trim(val);

    if (checked == null || checked == true) {
        if (val == null || val == "") {
            $(input).removeClass("state-valid").addClass("state-invalid");
            return "error";
        }
        else {
            $(input).removeClass("state-invalid").addClass("state-valid");
            return val;
        }
    } else {
        $(input).removeClass("state-invalid state-valid");
        return val;
    }


}

function validateEmail(email) {
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(String(email).toLowerCase());
}

/** Проверка ip **/
function ValidateInt(metka, low_limit, hight_limit, empty) /*функция проверки числа, границы*/ {
    if ($(metka).val() === undefined) {
        return "error"
    }

    var reg = /\D+/;
    var numbers = parseInt($(metka).val());
    if ((empty != true && isNaN(numbers)) || !isNaN(numbers)) {
        if ((!reg.test(numbers)) && (numbers >= low_limit) && (numbers <= hight_limit) && (numbers != "")) {
            $(metka).removeClass("state-invalid").addClass("state-valid");
        }
        else {
            $(metka).removeClass("state-valid").addClass("state-invalid");
            return "error";
        }
    }

    return numbers;
}

var table = {};

function activeSearchTable(_className, _isStateSave, _isOrder) {//Функция, которая активирует сортировку колонок в таблице  

    table[_className] = $(_className + " table").DataTable({
        paging: false,
        info: false,
        stateSave: (_isStateSave == null) ? true : _isStateSave,
        dom: "<'myfilter'f>",
        language: {
            searchPlaceholder: LANG_FILE.search,
            search: ""
        },
        order: (_isOrder) ? _isOrder : undefined
    })

}


function clearTable(_className) { //Функция, которая деактивирует сортировку колонок в таблице  

    if (table[_className] !== undefined) {
        table[_className].destroy();
    }

}


function createTreeOption(_parent_id, $level, $arr, _$option, _addPeriodsToOption) {

    var html = "";

    if ($arr != null) {

        if ($arr[_parent_id] !== undefined) { //Если категория с таким parent_id существует 

            var obj = $arr[_parent_id];

            for (var key in obj) {
                //Обходим ее 
                var $newOption = _$option.clone();

                let name = ""
                let id = obj[key]["id"];
                let periods = obj[key]["periods"];

                for (let index = 0; index < $level; index++) {
                    name += "-";
                }

                if (_addPeriodsToOption === true) {
                    $newOption.attr("data-periods", JSON.stringify(periods));
                }

                name += obj[key]["name"];

                $newOption.text(name).val(id);
                html += $newOption[0].outerHTML;

                $level++;

                html += createTreeOption(parseInt(obj[key]["id"]), $level, $arr, _$option, _addPeriodsToOption);

                $level--;
            }
        }
        return html;
    }
}

function createTree(_parent_id, _$arr, $departmentsUl) {

    if (_$arr != null) {

        $newUl;

        if (_$arr[_parent_id] !== undefined) { //Если категория с таким parent_id существует 

            var $newUl = $departmentsUl.clone();
            var $li = $("li", $newUl).first().clone();
            $newUl.empty();

            var obj = _$arr[_parent_id];

            for (var key in obj) {
                //Обходим ее 
                var $newLi = $li.clone();

                let id = parseInt(obj[key]["id"]);

                $newLi.data("id", id);
                $newLi.attr("id", id);
                $newLi.append(createTree(id, _$arr, $departmentsUl));
                $newUl.append($newLi);

            }
        }
        return $newUl;
    }
}

//перобразем файл в бинарный контейнер упакованный в объект
function makeBlob(_src, _cb) {
    var reader = new FileReader();
    // this function is triggered once a call to readAsDataURL returns
    reader.onload = function (event) {
        var data = {};

        // заполняем объект данных файлами в подходящем для отправки формате
        $.each(_src, function (key, value) {
            data[key] = value;
        });

        data.raw = event.target.result;

        _cb(data);
    };
    // trigger the read from the reader...
    reader.readAsDataURL(_src);
}

// javascript function that uploads a blob to _url
function uploadBlob(_url, _files, _cb, _timeoutTime) {
    // create a blob here for testing
    var reader = new FileReader();
    // this function is triggered once a call to readAsDataURL returns
    reader.onload = function (event) {
        var data = new FormData();

        // заполняем объект данных файлами в подходящем для отправки формате
        $.each(_files, function (key, value) {
            data.append(key, value);
        });

        data.append('raw', event.target.result);
        //data.append('avatar', _files, _files.name);
        //data.append('name', "avatar");

        // AJAX запрос
        $.ajax({
            url: CreateUrl(_url),
            type: 'POST', // важно!
            data: data,
            cache: false,
            // отключаем обработку передаваемых данных, пусть передаются как есть
            processData: false,
            // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
            contentType: false,
            // функция успешного ответа сервера
            timeout: (_timeoutTime == null ? 15000 : _timeoutTime),
            success: function (json, textStatus, jqXHR) {
                if (CheckResponse(json, jqXHR)) {//Проверка на результат запроса
                    ShowMessage("success", json.messageCode, "ok");
                    _cb(json.data);
                }
                else {
                    _cb(json.data);
                }
            },
            // функция ошибки ответа сервера
            error: function (request, error) {
                ShowMessageError(request, error, "DEFAULT_MESSAGE");
                _cb(false);
            }

        });
    };
    // trigger the read from the reader...
    reader.readAsDataURL(_files);
}

function uploadFiles(_url, _files, _cb, _timeoutTime) {

    // ничего не делаем если files пустой
    if (typeof _files == 'undefined') {
        _cb(false);
        return;
    }

    // создадим объект данных формы
    var data = new FormData();

    // заполняем объект данных файлами в подходящем для отправки формате
    $.each(_files, function (key, value) {
        data.append(key, value);
    });

    // добавим переменную для идентификации запроса
    data.append('file_upload', 1);

    // AJAX запрос
    $.ajax({
        url: CreateUrl(_url),
        type: 'POST', // важно!
        data: data,
        cache: false,
        dataType: 'json',
        // отключаем обработку передаваемых данных, пусть передаются как есть
        processData: false,
        // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
        contentType: false,
        // функция успешного ответа сервера
        timeout: (_timeoutTime == null ? 15000 : _timeoutTime),
        success: function (json, textStatus, jqXHR) {
            if (CheckResponse(json, jqXHR)) {//Проверка на результат запроса
                ShowMessage("success", json.messageCode, "ok");
                _cb(true);
            }
            else {
                _cb(false);
            }
        },
        // функция ошибки ответа сервера
        error: function (request, error) {
            ShowMessageError(request, error, "DEFAULT_MESSAGE");
            _cb(false);
        }

    });
}

(function ($) {
    $.fn.settingsPopover = function (_desc) {
        var _$element = $(this);
        if (_desc == "" || _desc == undefined) {
            _$element
                .popover('dispose')
                .addClass("collapse")
                .data({
                    "content": "",
                    "popover": false
                });
        } else {
            if (_$element.data("popover") != true) {
                _$element
                    .removeClass("collapse")
                    .data({
                        "content": _desc,
                        "popover": true
                    })
                    .popover();
            }

            if (_desc != _$element.data("content")) {
                _$element
                    .popover("dispose")
                    .data({
                        "content": _desc,
                        "popover": true
                    })
                    .popover();
            }

        }
    }
})(jQuery);

function QueryAjax(_paramsObj, _cb) {
    $.ajax({
        url: CreateUrl(_paramsObj.url),//Запрос на изменения данных контроллера
        method: _paramsObj.method, //"GET","POST"
        contentType: "application/json; charset=utf-8",
        dataType: _paramsObj.dataType || "json",
        cache: false,
        async: true,
        data: JSON.stringify(_paramsObj.data),
        timeout: _paramsObj.timeout || 10000,
        success: function (json, textStatus, jqXHR) {
            if (CheckResponse(json, jqXHR)) {//Проверка на результат запроса

                if (typeof _cb === "function") {

                    if (json == null) {
                        json = {
                            data: null
                        }
                    }
                    _cb(true, json);
                }
            }
            else {
                if (typeof _cb === "function") {
                    _cb(false, json);
                }
            }
        },
        error: function (request, error) {
            ShowMessageError(request, error, _paramsObj.defaultMessage || "DEFAULT_MESSAGE");
            if (typeof _cb === "function") {
                _cb(false, request.responseJSON);
            }
        }
    })
}


function QueryAjaxPromise(_paramsObj) {
    return new Promise((resolve) => {
        QueryAjax(_paramsObj, (result, json) => {
            resolve({ result, json });
        });
    })
}

function redirectAuthorizedUserToIndex(_cb) {

    $.ajax({
        url: CreateUrl("user"),
        method: "get",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        success: function (json, textStatus, jqXHR) {
            if (CheckResponse(json, jqXHR)) {//Проверка на результат запроса
                _cb(json);
            }
            else {
                _cb(false);
            }
        },
        error: function (request, error) {
            _cb(false);
        }
    });
}

function initInputFileListeners(_file) {
    //колбек при имзменении файла
    $(document).on("change", "[name='example-file-input-custom']", function (e) {
        let file = e.target.files[0];
        if (file != null) {
            loadImage(
                file,
                function (_$img) {
                    $(".j-avatar-row").html('');
                    $(_$img).addClass("avatar avatar-xl");
                    $(_$img).attr({
                        'name': 'avatar-img',
                        'onerror': "this.src='dist/img/default-user-icon.jpg';"
                    });

                    //перобразем файл в бинарный контейнер упакованный в объект
                    makeBlob(file, function (_bfile) {
                        //применяем к текущему объекту через поле data
                        $(".j-avatar-row").data('file', _bfile);

                        $(".j-avatar-row").append(_$img);
                    })
                },
                {
                    maxWidth: 300,
                    maxHeigth: 300
                } // Options
            );
        }
    });
}

/* Работа с ключами */

function codeTxt2Hex(instr) {
    var l = instr.length;
    var p = instr.indexOf(",");
    var codeP1 = instr.substring(0, p);
    var codeP2 = instr.substring(p + 1);
    var codeP1I = parseInt(codeP1, 10);
    var codeP2I = parseInt(codeP2, 10);
    var codeP1H = codeP1I.toString(16);
    var codeP2H = codeP2I.toString(16);
    var codeHex = codeP1H + ("0000" + codeP2H).slice(-4);
    codeHex = ("000000" + codeHex).slice(-6);
    return codeHex.toUpperCase();
}

function codeHex2Txt(instr) {
    var codeHex = instr.toUpperCase();
    var l = codeHex.length;
    var codeP1 = codeHex.substring(0, l - 4);
    var codeP2 = codeHex.substring(l - 4);
    var codeP1I = parseInt(codeP1, 16);
    var codeP2I = parseInt(codeP2, 16);
    var codeText = ("000" + codeP1I).slice(-3) + ',' + ("00000" + codeP2I).slice(-5);
    return codeText;
}

function codeHex2Dec(instr) {
    var codeHex = instr.toUpperCase();
    var codeInt = parseInt(codeHex, 16);
    var codeDec = ("0000000000" + codeInt).slice(-10);
    return codeDec;
}

function codeDec2Hex(instr) {
    var codeInt = parseInt(instr, 10);
    var codeHex = codeInt.toString(16);
    var codeHex = ("000000" + codeHex).slice(-6);
    return codeHex.toUpperCase();
}

function millisecondsToTime2(_sec) {

    let sec = (_sec / 1000);
    let h = sec / 3600 ^ 0;
    let m = (sec - h * 3600) / 60 ^ 0;
    let s = sec - h * 3600 - m * 60;


    if (h.toString().length === 1) {
        h = ('0' + h).slice(-2);
    }

    if (m.toString().length === 1) {
        m = ('0' + m).slice(-2);
    }

    return {
        hr: h,
        min: m,
        sec: s

    }
}

function millisecondsToTime(_sec, _notZero) {
    var sec = (_sec / 1000);
    const secInYear = 31536000;
    var year = sec / secInYear ^ 0;
    var month = (sec - (year * secInYear)) / 2678400 ^ 0;
    var xx = (year * secInYear) + (month * 2678400);
    var day = (sec - xx) / 86400 ^ 0;
    var h = sec / 3600 ^ 0;
    var m = (sec - h * 3600) / 60 ^ 0;
    var s = sec - h * 3600 - m * 60;

    let result = {};
    if (_notZero === true) {
        result = {
            year: year,
            month: month,
            day: day,
            hr: h,
            min: m,
            sec: s
        }
    }
    else {

        result = {

            hr: ('0' + h).slice(-2),
            min: ('0' + m).slice(-2),
            sec: ('0' + s).slice(-2)
            // hr: h.toLocaleString(),
            // min: m.toLocaleString(),
            // sec: s.toLocaleString()
        }
    }
    return result;
}

function getAuthorizedUserInfo() {

    if (LANG_FILE.role == null) {
        setTimeout(function () {
            getAuthorizedUserInfo();
        }, 200);
    } else {
        $.ajax({
            url: "/user",
            dataType: "json",
            cache: false,
            async: false,
            timeout: 10,
            success: function (json, textStatus, jqXHR) {

                let data = json.data;
                let fullName = (data.fullName == null ? "" : data.fullName);
                let shortName = (data.shortName == null ? "" : data.shortName);
                let user = data.user;
                let img = data.imgDist;

                if (shortName != "") {
                    user = shortName;
                } else if (fullName != "") {
                    user = fullName;
                }

                if (img != null) {
                    $(".j-system-user-avatar").css("background-image", "url(" + img + ")");
                }

                let role = "-";

                if (LANG_FILE.systemUser.managementUser.role[data.role] != null) {
                    role = LANG_FILE.systemUser.managementUser.role[data.role];
                }
                $(".j-system-user-role").text((data.role) ? role : "");
                $(".j-system-user-name").text(user);

            }
        });
    }
}

function rgbA2hex(_rgba) {
    if(_rgba == null) {
        return "";
    } else {
        let rgba = _rgba.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);

        return (rgba && rgba.length === 4) ? "#" +
        ("0" + parseInt(rgba[1], 10).toString(16)).slice(-2) +
        ("0" + parseInt(rgba[2], 10).toString(16)).slice(-2) +
        ("0" + parseInt(rgba[3], 10).toString(16)).slice(-2) : '';
    }
}

String.prototype.trunc = function () {
    return function (n = null) {
        if (n == null) {
            n = 40
        }

        var val = this.valueOf();
        var result = val.substr(0, n - 1) + (val.length > n ? '...' : '');

        return result;
    }
}(String.prototype.trunc);