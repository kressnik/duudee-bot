var subscribesListJSON = [];
var channelsListJSON = [];

document.addEventListener("lang-ready", function (e) {
    $('[data-inputmask]').inputmask();
    moment.locale(LANG_SYSTEM);
    getChannelsList();

    getSubscribesList();
});


$(document).on("click", ".j-subscribe-list .j-refresh", refreshSubsList);
$(document).on("click", ".j-subscribe-list .j-btn-eye", viewDepoInfo);
$(document).on("click", ".j-subscribe-list .j-btn-add", addSubscribe);

$(document).on("click", ".j-depo-info .j-hide-info-depo", hideDepositInfo);

$(document).on("click", "#add-subscribe .j-add", addSubscribe);
$(document).on('hide.bs.modal', "#add-subscribe", CleaningmodalAddSubscribe);

function getSubscribesList() {

    var $tableSubscribes = $('#table-subscribes');
    $tableSubscribes.DataTable().destroy();
    $tableSubscribes.DataTable({
        ajax: {
            method: 'post',
            url: '/subscribes',
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);
            },
            dataFilter: function (resultData) {
                let data = JSON.parse(resultData).data;
                return JSON.stringify(data);
            }
        },
        processing: false,
        serverSide: true,
        createdRow: function (row, subscribe, numRow) {
            subscribesListJSON[numRow] = subscribe;

            var $newSubscribeTr = $(row);
            var subscribeID = subscribe.id;
            var status = subscribe.status;
            /* Наполнения tr информацией о subscribe */
            $newSubscribeTr.attr({
                "data-id": subscribeID,
                "data-num": numRow
            });

            if (status != null) {
                status = status.toUpperCase();
            }

            switch (status) {
                case 'GRANDED':
                    $newSubscribeTr.addClass("tr-success");
                    break;
            }
        },

        columns: [
            {
                name: "id",
                data: "id",
                orderable: false,
                render: function (id, type, data) {
                    return id;
                }
            }, {
                name: "subChannelId",
                data:"channelId",
                orderable: false,
                render: function (channelId, type, data) {
                    let channelInfo = channelsListJSON[channelId];
                    let mainInfo = "-";
                    let price = "";

                    if (channelInfo != null) {
                        mainInfo = `${channelId} - ${channelInfo.name}`;
                        price = `${channelInfo.price} ${channelInfo.currency}`;
                    }

                    return `
                        <span>${mainInfo}</span>
                        <div class="small text-muted">${price}</div>
                    `;
                }
            }, {
                name: "subOrderId",
                data: "orderId",
                orderable: false,
                render: function (subOrderId, type, data) {
                    return subOrderId;
                }
            }, {
                name: "subUserId",
                data:"userInfo",
                orderable: false,
                render: function (userInfo, type, data) {
                    var fullName = `${userInfo.firstName} ${userInfo.lastName}`;
                    var username = userInfo.username == "" ? "" : `@${userInfo.username}`;
                    var userLink = userInfo.username == "" ? "" : `https://t.me/${userInfo.username}`;

                    return `
                        <span>${fullName}</span>
                        <div class="small text-muted"><a href="${userLink}" target="_blank">${username}</a></div>
                        <div class="small text-muted">ID: ${userInfo.id}</div>
                    `;
                }
            }, {
                name: "status",
                data:"status",
                orderable: false,
                render: function (status, type, data) {
                    return status;
                }
            }, {
                name: "isFree",
                data: "isFree",
                orderable: false,
                render: function (isFree, type, data) {

                    switch (isFree) {
                        case true:
                            isFree = "FREE";
                            break;
                        case false:
                            isFree = "PAYED";
                            break;
                        default:
                            isFree = "???";
                            break;
                    }

                    return isFree;
                }
            },
            {
                name: "options",
                orderable: false,
                render: function (options, type, data) {
                    if(data.isFree != null && data.isFree == false){
                        return `
                        <button type="button"
                            class="btn btn-icon btn-primary btn-secondary j-btn-eye"
                            title="${LANG_FILE.optionsBtn.eyeControllers}">
                            <i class="fe fe-eye"></i>
                        </button>
                        `;
                    } else {
                        return "";
                    }
                }
            }],
        paging: true,
        info: true, //Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        language: LANG_FILE.dataTablesLanguage,
        order: [], //сортировка
        drawCallback: function () {

        }
    });
}

function getChannelsList() {

    var paramsAjax = {
        url: "channels/all/list",
        method: "GET",
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE",
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            let channels = json.data;

            for (channelId in channels) {
                let channelSysId = channels[channelId].id;
                channelsListJSON[channelSysId] = channels[channelId];
            }
        }
    });
}


function sendAddSubscribe(subscribeInfo, _$thisBtn, _numSubscribe) {
    _$thisBtn.buttonLoader("start");

    var paramsAjax = {
        url: "subscribe",
        dataType: "json",
        method: "POST",
        data: subscribeInfo,
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            refreshSubsList();
            ShowMessage("success", json.messageCode, "ok");

            if (subscribeInfo.repeated === false) {
                $('#add-subscribe').modal('hide');
            } else {
                CleaningmodalAddSubscribe(true);
            }
        }

        _$thisBtn.buttonLoader("stop");
    });
}

function addSubscribe() {
    let $thisBtn = $(this);
    let $modalAddSubscribe = ("#add-subscribe");

    let subId = $thisBtn.data("num");
    let idInbase = null;
    let subProps = subscribesListJSON[subId];

    if (subProps) {
        idInbase = subProps.id;
    }

    subProps = {
        id: idInbase,
        channelTlgId: $(".j-subscribe-channel", $modalAddSubscribe).val(),
        userId: $(".j-subscribe-user", $modalAddSubscribe).val(),
        type: $(".j-subscribe-type", $modalAddSubscribe).val(),
        description: $(".j-subscribe-description", $modalAddSubscribe).val(),
    };

    sendAddSubscribe(subProps, $thisBtn, subId);
};

function CleaningmodalAddSubscribe(_repeated) {
    let $modalAddSubscribe = ("#add-subscribe"); //Модальное окно
    let clearInputArr = [
        ".j-subscribe-name",
        ".j-subscribe-host",
        ".j-subscribe-port",
        ".j-subscribe-description",
        ".j-subscribe-token"
    ]; //Инпуты которые нужно очистить

    let repeated = (_repeated === true ? true : false);

    for (let index = 0; index < clearInputArr.length; index++) { //Чистка инпутов
        $(clearInputArr[index], $modalAddSubscribe)
            .removeClass("state-valid state-invalid")
            .val('')
            .prop("disabled", false);
    }

    $(".j-subscribe-type", $modalAddSubscribe).prop("selectedIndex", 0).change();
    $(".j-subscribe-protocol", $modalAddSubscribe).prop("selectedIndex", 0).change();

    $(".modal-title", $modalAddSubscribe).text(LANG_FILE.converters.addBtn); //Изменения заголовка модального окна

    $(".j-repeated", $modalAddSubscribe)
        .prop("checked", repeated)
        .parents(".form-group")
        .removeClass("collapse"); //Отображения кнопки повторного добавления

    $(".j-add", $modalAddSubscribe)
        .text(LANG_FILE.add)
        .removeData(); //Изменяем названия кнопки
}

function refreshSubsList() {
    $('#table-subscribes').DataTable().ajax.reload(null, false);
}

function viewDepoInfo() {

    var $thisBtn = $(this);
    var $subscribeTr = $thisBtn.parents("tr").first();
    var subscribeID = $subscribeTr.data("id");

    var $boxTable = $(".j-subscribe-list .j-subscribes");
    var $boxDepoInfo = $(".j-depo-info");

    $thisBtn.buttonLoader("start");

    var paramsAjax = {
        url: `subscribe/${subscribeID}/deposit`,
        method: "GET",
        timeout: 15000
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            let depoInfo = json.data;

            let fullPriceStr = null;

            if (depoInfo.amount != null) {
                fullPriceStr = `${depoInfo.amount} + ${depoInfo.fee}`;
            }

            $(".j-id", $boxDepoInfo).text(depoInfo.id);
            $(".j-status", $boxDepoInfo).text(depoInfo.status);
            $(".j-reference-id", $boxDepoInfo).text(depoInfo.referenceId);
            $(".j-price", $boxDepoInfo).text(fullPriceStr);
            $(".j-currency", $boxDepoInfo).text(depoInfo.currency);

            $(".j-service-method", $boxDepoInfo).text(depoInfo.serviceMethod);
            $(".j-serial", $boxDepoInfo).text(depoInfo.sn);

            $boxTable.removeClass("col-md-12").addClass("col-md-8");//Изменения размера блока списка контроллеров
            $boxDepoInfo.removeClass("collapse");//Открытия блоки информации о контролёре

        }

        $thisBtn.buttonLoader("stop");

    });
}

function hideDepositInfo() {
    $(".j-subscribe-list .j-subscribes").removeClass("col-md-8").addClass("col-md-12");//Изменения размера блока списка контроллеров
    $(".j-depo-info").addClass("collapse");//Закрытия блока информации о боте
    clearDepoInfo();
}

function clearDepoInfo() {
    var $boxDepoInfo = $(".j-depo-info");

    $(".j-id", $boxDepoInfo).text('');
    $(".j-status", $boxDepoInfo).text('');
    $(".j-reference-id", $boxDepoInfo).text('');
    $(".j-price", $boxDepoInfo).text('');
    $(".j-currency", $boxDepoInfo).text('');

    $(".j-service-method", $boxDepoInfo).text('');
    $(".j-serial", $boxDepoInfo).text('');
}

