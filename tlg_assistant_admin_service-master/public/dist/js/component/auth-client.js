document.addEventListener("lang-ready", function () {
    getAuthStatusWatcher();
});

$(document).on("click", ".j-auth-client-no-box .j-get-auth-key-btn", getAuthKey);
$(document).on("click", ".j-auth-client-no-box .j-send-auth-key-btn", sendAuthKey);
$(document).on("click", ".j-auth-client-init-box .j-init-auth-key-btn", initAuthKey);


function getAuthStatusWatcher() {
    let $authClientOkBox = $(".j-auth-client-ok-box");
    let $authClientNoBox = $(".j-auth-client-no-box");
    let $authClientInitBox = $(".j-auth-client-init-box");
    let $authClientNotConnectBox = $(".j-auth-client-not-connect-box");
    let paramsAjax = {
        url: 'channel/watcher/auth/status',
        dataType: "JSON",
        method: "GET"
    };

    QueryAjax(paramsAjax, function (result, json) {

        switch (json.data.status) {
            case "NOT_INIT":
                $authClientInitBox.removeClass("collapse");
                $authClientOkBox.addClass("collapse");
                $authClientNoBox.addClass("collapse");
                $authClientNotConnectBox.addClass("collapse");
                break;
            case "DONE":
                $authClientOkBox.removeClass("collapse");
                $authClientNoBox.addClass("collapse");
                $authClientInitBox.addClass("collapse");
                $authClientNotConnectBox.addClass("collapse");
                $(".j-phone-client", $authClientOkBox).text(json.data.phone);
                break;
            case "PENDING":
                $('[name="phone-input"]', $authClientNoBox).val(json.data.phone);
                $('[name="auth-key-input"]', $authClientNoBox).prop("disabled", false).parent().removeClass("collapse");
                $('.j-get-auth-key-btn', $authClientNoBox).text("Еще один код");;
                $('.j-send-auth-key-btn', $authClientNoBox).parent().removeClass("collapse");

                $authClientOkBox.addClass("collapse");
                $authClientNoBox.removeClass("collapse");
                $authClientInitBox.addClass("collapse");
                $authClientNotConnectBox.addClass("collapse");
                break;
            case "EMPTY":
                $authClientOkBox.addClass("collapse");
                $authClientNoBox.removeClass("collapse");
                $authClientInitBox.addClass("collapse");
                $authClientNotConnectBox.addClass("collapse");
                break;
            default:
                $authClientOkBox.addClass("collapse");
                $authClientNoBox.addClass("collapse");
                $authClientInitBox.addClass("collapse");
                $authClientNotConnectBox.removeClass("collapse");
                break;
        }

        setTimeout(getAuthStatusWatcher, 10000);
    });
}

function initAuthKey() {
    let $authClientinitBox = $(".j-auth-client-init-box");
    let apiId = $.trim($('[name="api-id"]', $authClientinitBox).val()).replace(/[\s.,+]/g, '');
    let apiHash = $.trim($('[name="api-hash"]', $authClientinitBox).val()).replace(/[\s.,+]/g, '');


    let paramsAjax = {
        url: 'channel/watcher/api/info',
        dataType: "JSON",
        method: "POST",
        data: {
            apiId: apiId,   // number
            apiHash: apiHash   // string
        }
    };

    QueryAjax(paramsAjax, function (result, json) {
        getAuthStatusWatcher();
    });
}

function getAuthKey() {

    let $authClientNoBox = $(".j-auth-client-no-box");
    let phone = $.trim($('[name="phone-input"]', $authClientNoBox).val()).replace(/[\s.,+]/g, '');
    let $authKeyInput = $('[name="auth-key-input"]', $authClientNoBox);
    let $authKeyGetBtn = $('.j-get-auth-key-btn', $authClientNoBox);
    let $authKeySendBtn = $('.j-send-auth-key-btn', $authClientNoBox);

    paramsAjax = {
        url: 'channel/watcher/auth/start',
        dataType: "JSON",
        method: "POST",
        data: {
            phone: `+${phone}`
        }
    };

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            $authKeyInput.prop("disabled", false).parent().removeClass("collapse");
            $authKeyGetBtn.text("Еще один код");
            $authKeySendBtn.parent().removeClass("collapse");
        } else {

        }

    });
}

function sendAuthKey() {
    let $authClientNoBox = $(".j-auth-client-no-box");
    let authKey = $.trim($('[name="auth-key-input"]', $authClientNoBox).val());

    let paramsAjax = {
        url: 'channel/watcher/auth/code',
        dataType: "JSON",
        method: "POST",
        data: {
            code: authKey
        }
    };

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            getAuthStatusWatcher();
        } else {

        }

    });
}