function СreateTreeObject(_data) {//Создается дерево с массива

    var departmentsTree = {};
    var departmentsName = {};

    for (const key in _data) {
        var _department = _data[key];

        if (_department.parentId == null) {
            _department.parentId = 0
        }

        if (departmentsTree[_department.parentId] == undefined) {
            departmentsTree[_department.parentId] = {};
        }
        departmentsName[_department.id] = _department.name;
        departmentsTree[_department.parentId][Object.keys(departmentsTree[_department.parentId]).length] = _department;
    }

    var data = {
        objectTree: departmentsTree,
        names: departmentsName,
        data: _data
    }

    return data;
}

function RemovalParentalChild(_data, _selectedElementsArr) {//Удаления выбранных родительских или дочерних елементов

    if (Array.isArray(_selectedElementsArr) && _selectedElementsArr.length > 0) {

        for (const key in _selectedElementsArr) {
            var elementId = _selectedElementsArr[key];

            if (_data.objectTree[elementId] != null) {
                _data.objectTree[elementId] = null;
            }
        }
    }

    return _data;
}

function СreateTreeArr(_parent_id, _level, _data, _newArr, _breadcrumb) { //Фильтруется дерево в нужный массив для вывода

    if (!Array.isArray(_newArr)) {
        _newArr = [];
    }

    if (_data.objectTree != null) {

        if (_data.objectTree[_parent_id] != null) { //Если категория с таким parent_id существует 

            var objects = _data.objectTree[_parent_id];

            if (_breadcrumb == null) {
                _breadcrumb = "";
            }

            for (var key in objects) {
                //Обходим ее 
                var obj = objects[key];
                var name = ""
                var id = parseInt(obj.id);

                _breadcrumb += "->" + obj.name;

                for (var index = 0; index < _level; index++) {
                    name += "-";
                }

                obj.name = name += obj.name;
                obj.breadcrumb = _breadcrumb;

                _newArr.push(obj);

                _level++;

                СreateTreeArr(id, _level, _data, _newArr, _breadcrumb);

                _breadcrumb = "";
                _level--;
            }
        }
        return _newArr;
    }
}

function onChangeOptions(_this) {//Перерисовка списка в зависимости что выбрал пользователь

    var values = _this.getValue();
    var valuesArr = values.split(',');
    var allData = JSON.parse(JSON.stringify(_this.allDataInput));
    var filtrationData = RemovalParentalChild(allData, valuesArr);
    var data = СreateTreeArr(0, 0, filtrationData);
    var clearValues = [];

    for (const key in valuesArr) {
        var value = valuesArr[key];
        var inArray = false;

        for (const key in data) {
            var element = data[key];

            if (element.id == value) {
                inArray = true;
                break;
            }
        }

        if (inArray == true) {
            clearValues.push(value);
        }
    }

    _this.clear(true);

    _this.clearOptions();
    _this.addOption(data);
    _this.refreshOptions();

    _this.setValue(clearValues, true);

    delete allData, filtrationData, data;
}