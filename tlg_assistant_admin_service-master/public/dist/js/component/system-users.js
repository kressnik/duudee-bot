document.addEventListener("lang-ready", function (e) {
    moment.locale(LANG_SYSTEM);
    $(":input").inputmask();
    initInputFileListeners();
    GetLangsInSystem();
    GetSystemUsers();
});

$(document).on("click", "#system-user-table .j-btn-edit", EditSystemUser);
$(document).on("click", "#system-user-table .j-btn-del", openModalDeleteUser);
$(document).on("click", "#modal-delete-user .j-btn-delete-user", DeleteSystemUser);
$(document).on("click", ".j-open-add-box", OpenBoxAddUser);
$(document).on("click", ".j-back-to-list", OpenBoxUsersList);
$(document).on("click", ".j-management-user .j-add-system-user", AddAndUpdateSystemUser);
$(document).on("click", ".j-management-user .j-edit-system-user", AddAndUpdateSystemUser);
$(document).on("change", ".j-users-two-factor-switch", switchTwoFactorState);
$(document).on("click", ".j-drop-two-factor", openModalDropTwoFactorUser);
$(document).on("click", "#modal-qrcode-two-factor .j-activate-two-factor", activateTwoFactor);
$(document).on("click", "#modal-drop-two-factor .j-btn-drop-two-factor-user", dropUserTwoFactorAuth);
$(document).on("click", ".j-management-user .j-btn-qrcode-two-factor", checkTwoFactorBtn);
$(document).on("click", ".j-users-list .j-refresh", function (e) {
    if (e != undefined) e.preventDefault();
    $('#system-user-table').DataTable().ajax.reload(null, false);
});


function OpenBoxAddUser(arg) {
    if (arg == true) {
        $(".j-drop-two-factor").removeClass("collapse");
    }

    $(".j-open-add-box").addClass("collapse");
    $(".j-users-list").addClass("collapse");
    $(".j-back-to-list").removeClass("collapse");
    $(".j-management-user").removeClass("collapse");
}

function OpenBoxUsersList() {
    $(".j-open-add-box").removeClass("collapse")
    $(".j-users-list").removeClass("collapse");
    $(".j-back-to-list").addClass("collapse");
    $(".j-management-user").addClass("collapse");
    ClearFormAddSystemUser();
}

function EditSystemUser() {
    let $thisBtn = $(this);//Кнопка на которую выполнено нажатия 
    $thisBtn.buttonLoader("start");//Запуск процессинга на кнопке

    let $systemUserForm = $(".j-management-user");
    let $thisTr = $thisBtn.parents("tr").first();
    let idSystemUser = $thisTr.data("id");
    let paramsAjax = {
        url: "user/" + idSystemUser,
        method: "GET",
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            let systemUser = json.data;
            let twoFactor = systemUser.twoFactor == null ? null : systemUser.twoFactor;
            let twoFactorWasShown = 0;

            if (twoFactor != null) {
                twoFactorWasShown = twoFactor.wasShown == null ? 0 : twoFactor.wasShown;
            }

            if (systemUser.avatarUserLink == null) {
                $('img[name=avatar-img]', $systemUserForm).attr("src", "dist/img/default-user-icon.jpg");//todo rework
            } else {
                $('img[name=avatar-img]', $systemUserForm).attr("src", systemUser.avatarUserLink);
            }

            if (systemUser.username != "root") {
                $("[name='role']", $systemUserForm).prop("disabled", false);
            } else {
                $("[name='role']", $systemUserForm).prop("disabled", true);
            }

            if (twoFactorWasShown == 0) {
                $("[name='is-use-two-fa']", $systemUserForm).prop("disabled", true);
                $(".j-btn-qrcode-two-factor", $systemUserForm)
                    .text("Сгенерировать")
                    .data({
                        "type-btn": "create",
                        "system-user-id": systemUser.id
                    })
                    .removeClass("btn-danger")
                    .addClass("btn-success");
            } else {
                $("[name='is-use-two-fa']", $systemUserForm).prop("disabled", false);
                $(".j-btn-qrcode-two-factor", $systemUserForm)
                    .text("Удалить 2FA")
                    .data({
                        "type-btn": "delete",
                        "system-user-id": systemUser.id
                    })
                    .addClass("btn-danger")
                    .removeClass("btn-success");
            }

            $(".j-two-factor-input", $systemUserForm).removeClass("collapse");
            $("[name='full-name']", $systemUserForm).val(systemUser.fullName);
            $("[name='login']", $systemUserForm).val(systemUser.username).prop("disabled", true);
            $("[name='email']", $systemUserForm).val(systemUser.email);
            $("[name='role'] option[value='" + systemUser.role + "']", $systemUserForm).prop("selected", true);
            $("[name='lang-system'] option[value='" + systemUser.lang + "']", $systemUserForm).prop("selected", true);
            $("[name='is-use-two-fa'][value='" + systemUser.isUseTwoFa + "']", $systemUserForm).prop("checked", true);

            $(".card-title", $systemUserForm).text(LANG_FILE.systemUser.managementUser.changeTitle);
            $(".j-add-system-user", $systemUserForm)
                .removeClass("j-add-system-user")
                .addClass("j-edit-system-user")
                .text(LANG_FILE.systemUser.managementUser.btnEdit);

            $(".j-edit-system-user", $systemUserForm)
                .text("Изменить")
                .data({
                    "system-user-id": systemUser.id
                });

            $(".j-drop-two-factor", $systemUserForm).data({
                "system-user-id": systemUser.id
            });

            OpenBoxAddUser(true);
        };

        $thisBtn.buttonLoader("stop");//Остановка процессинга на кнопке

    });

}

function openModalDeleteUser() {
    let $thisBtn = $(this);//Кнопка на которую выполнено нажатия
    let $tr = $thisBtn.parents("tr").first();
    let idSystemUser = $tr.data("id");
    let $modal = $("#modal-delete-user"); //Модальное окно удаления
    let defaultWord = "DELETE";

    $(".j-user-id", $modal).text(idSystemUser);
    $(".j-type", $modal).text(defaultWord);
    $(".j-input-type", $modal).data("original-type", defaultWord);

    $(".j-btn-delete-user", $modal).data({
        "idSystemUser": idSystemUser
    });
}

function DeleteSystemUser() {

    let $thisBtn = $(this);//Кнопка на которую выполнено нажатия
    $thisBtn.buttonLoader("start");//Запуск процессинга на кнопке

    let $modal = $("#modal-delete-user");
    let infoUser = $thisBtn.data();
    let paramsAjax = {
        url: "user/" + infoUser.idSystemUser,
        method: "DELETE",
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            $(".j-users-list .j-refresh").click();
            $modal.modal("hide");
            $modal.find("input").val("");
            ShowMessage("success", LANG_FILE.message.ok.SYS_USER_DELETED_OK, "ok");
        }

        $thisBtn.buttonLoader("stop");
    });
}

$(document).on("keyup input", "#modal-delete-user .j-input-type", function () { //Отслеживания что введено в input для разблокировки кнопки удалить

    let $thisInput = $(this);
    let $modal = $thisInput.parents("#modal-delete-user"); //Модальное окно
    let $btnDelete = $(".j-btn-delete-user", $modal); //Кнопка удаления
    let typeInput = $.trim($thisInput.val()); //Текс, который ввели в input
    let originalType = $thisInput.data("original-type"); //Текст который нужно ввести

    if (typeInput == originalType) { //Проверка совпадения текста
        $btnDelete
            .addClass("btn-danger")
            .removeClass("btn-gray")
            .prop("disabled", false); //Активация кнопки
    } else {
        $btnDelete
            .removeClass("btn-danger")
            .addClass("btn-gray")
            .prop("disabled", true); //Деактивация кнопки
    }

});

function GetSystemUsers() {

    $('#system-user-table').DataTable({
        ajax: {
            url: 'users',
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);
            }
        },
        processing: true,
        serverSide: true,
        createdRow: function ($row, data) {
            switch (data.state) {
                case 0:
                    $($row).attr("style", "background-color: rgba(199, 30, 30, 0.06);");
                    break;
                case 1:
                    $($row).attr("style", "background-color: rgba(30, 199, 98, 0.06);");
                    break;
                case 2:
                    $($row).attr("style", "background-color: rgba(255, 200, 0, 0.15)");
                    $("td:last", $row).empty();
                    break;
            }
            $($row).attr({
                'data-id': data.id,
                'data-role': data.role
            });
            $($row).find("td").eq(2).css("padding-left", "0");
            $(".j-users-two-factor-switch", $row).prop("checked", data.isUseTwoFa);
        },
        columns: [
            {
                "data": "id",
                "orderable": false,
                render: function (id, type, row) {
                    return id;
                }
            }, {
                "data": "avatar",
                "orderable": false,
                "orderable": false,
                render: function (avatarUserLink, type, data) {

                    if (avatarUserLink == null || avatarUserLink == "") {
                        avatarUserLink = "dist/img/default-user-icon.jpg";
                    }
                    return `
                        <div class="avatar d-block j-avatar" style="background-image:url(${avatarUserLink})"></div>
                    `;
                }
                //"defaultContent":'' 
            }, {
                "data": "name",
                "orderable": false,
                render: function (name, type, data) {
                    return `
                        <div class="text-left">
                            <div style="max-width: 150px; white-space: initial;">${name.username}</div>
                            <div class="j-user-mail small text-muted" style="max-width: 150px; white-space: initial;">${data.email}</div>
                            <div class="j-user-lang small text-muted" style="max-width: 150px; white-space: initial;">${data.lang}</div>
                        </div>
                    `;
                }
            }, {
                "data": "state",
                "orderable": false,
                render: function (state, type, data) {
                    let deletedDate = "";
                    let stateText = (LANG_FILE.systemUser.table.userState[state] == null ? "No parameter" : LANG_FILE.systemUser.table.userState[state]);

                    if (data.deletedDate != null && data.deletedDate != "") {
                        deletedDate = "(" + moment.unix((data.deletedDate / 1000)).format("L LTS") + ")";
                    }

                    let html = `<div class="j-user-role small text-muted">${data.role}</div>`

                    return stateText + deletedDate + html;
                }
            },
            // {
            //     "data": "lastAuthorization",
            //     "orderable": false,
            //     render: function (lastAuthorization, type, data) {
            //         if (lastAuthorization != null && lastAuthorization != "") {
            //             lastAuthorization = moment.unix((lastAuthorization / 1000)).format("L LTS");
            //         }
            //         return lastAuthorization;
            //     }
            // },
            {
                "data": "isUseTwoFa",
                "orderable": false,
                render: function (isUseTwoFa, type, data) {
                    let html = '';
                    let wasShown = 0;

                    if (data.twoFactor != null) {
                        data.twoFactor.wasShown == null ? 0 : data.twoFactor.wasShown;
                    }

                    if (wasShown == 1) {
                        html = `
                        <div class="">
                            <label class="form-label custom-switch d-flex justify-content-center j-system-users-two-factor">
                                <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input j-users-two-factor-switch">
                                <span class="custom-switch-indicator"></span>
                            </label>
                        </div>`
                    } else {
                        html = `<i class="fa fa-qrcode mr-2"></i> Ключ не найден`
                    }
                    return html;
                }
            }, {
                "data": "name",
                "orderable": false,
                render: function (data, type, data) {

                    let html = `
                   <div class="btn-list">
                        <button type="button" class="btn btn-icon btn-primary btn-secondary j-btn-edit"
                            title="${LANG_FILE.edit}">
                            <i class="fe fe-edit"></i>
                        </button>`

                    if (data.name.username != "root") {
                        html += `
                        <button type="button" class="btn btn-icon btn-primary btn-secondary j-btn-del"
                        data-toggle="modal" data-target="#modal-delete-user"
                            title="${LANG_FILE.del}">
                            <i class="fe fe-trash"></i>
                        </button>`;
                    }

                    html += `</div>`;
                    return html;
                }
            }
        ],
        paging: true,
        info: true,//Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        language: LANG_FILE.dataTablesLanguage,
        //dom: "<'myfilter'f>",
        order: [],//сортировка
    });
}

function switchTwoFactorState() {

    let $thisBtn = $(this);
    $thisBtn.prop("disabled", true);

    let $tr = $thisBtn.parents("tr").first();
    let idSystemUser = $tr.data("id");
    let state = $thisBtn.prop("checked") ? 1 : 0;//Получения нового статуса
    let paramsAjax = {
        url: `user/${idSystemUser}/twoFactor/${state}`,
        dataType: "JSON",
        method: "PUT",
        timeout: 2000
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            ShowMessage("success", LANG_FILE.message.ok.CHANGE_STATE_TWO_FACTOR_OK, "ok");
            $(".j-users-list .j-refresh").click();
        } else {
            ShowMessage('error', LANG_FILE.message.error.CHANGE_STATE_TWO_FACTOR_ERROR, 'error');
            // $thisBtn.prop("checked", (state ? 0 : 1));//Возвращаем каретку в обратное положения если пришла ошибка
        }

        $thisBtn.prop("disabled", false);
    });
}

function AddAndUpdateSystemUser() {

    let $thisBtn = $(this);
    let systemUserId = $thisBtn.data("system-user-id");//Этот параметр есть только в случаи редактировании
    let $systemUserForm = $(".j-management-user");
    let $login = $("[name='login']", $systemUserForm);
    let login = ValidateInput($login);
    let fullName = $("[name='full-name']", $systemUserForm).val();
    let $email = $("[name='email']", $systemUserForm);
    let email = $email.val();
    let langSystem = $("[name='lang-system'] option:selected", $systemUserForm).val();
    let $newPassword = $("[name='new-password']", $systemUserForm);
    let $reenterPassword = $("[name='reenter-password']", $systemUserForm);
    let newPassword = $newPassword.val();
    let reenterPassword = $reenterPassword.val();
    let avatarUser = $(".j-avatar-row", $systemUserForm).data('file');
    let checkPassword = true;//Флаг, для того чтобы понимать, нужно ли проверять пароль
    let role = "administrator";
    let isUseTwoFa = $("[name='is-use-two-fa']:checked", $systemUserForm).val();

    if (login != "root") {
        role = $("[name='role'] option:selected", $systemUserForm).val();
    }

    if (systemUserId != null) { //Проверка на то нужно ли проверить пароль 
        if (reenterPassword.length <= 0 && newPassword.length <= 0) {
            checkPassword = false;
        } else {
            newPassword = ValidateInput($newPassword);
            reenterPassword = ValidateInput($reenterPassword);
        }
    } else {
        newPassword = ValidateInput($newPassword);
        reenterPassword = ValidateInput($reenterPassword);
    }

    if (reenterPassword === "error" || login === "error") {
        ShowMessage("error", LANG_FILE.message.error.EMPTY_INPUT, "error");
        return false;
    }

    if (email.length == 0) {
        $email.removeClass("state-valid state-invalid");
    } else if (validateEmail(email) == true) {
        $email.removeClass("state-invalid").addClass("state-valid");
    } else {
        $email.removeClass("state-valid").addClass("state-invalid");
        ShowMessage("error", LANG_FILE.message.error.ERROR_VALIDATE_EMAIL, "error");
        return false;
    }

    if (checkPassword == true) {
        if (newPassword != reenterPassword || reenterPassword.length <= 3) {

            $newPassword.removeClass("state-valid").addClass("state-invalid");
            $reenterPassword.removeClass("state-valid").addClass("state-invalid");

            if (reenterPassword.length <= 3) {
                ShowMessage("error", LANG_FILE.message.error.PASSWORDS_NOT_MATCHL, "error");
            } else {
                ShowMessage("error", LANG_FILE.message.error.PASSWORDS_NOT_EQUALLY, "error");
            }
            return false;
        }
    }

    let systemUserInfo = {
        avatarUser: (avatarUser || null),
        firstName: fullName,
        email: email,
        username: login,
        languageCode: langSystem,
        role: role,
        password: reenterPassword,
        isUseTwoFa: isUseTwoFa
    }
    let paramsAjax = {
        url: "user" + (systemUserId == null ? "" : "/" + systemUserId),
        method: (systemUserId == null ? "POST" : "PUT"),
        defaultMessage: "DEFAULT_MESSAGE",
        data: systemUserInfo
    };

    $thisBtn.buttonLoader("start");

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            $(".j-users-list .j-refresh").click();
            ShowMessage("success", LANG_FILE.message.ok.SYS_USER_DATA_SAVE_OK, "ok");
            OpenBoxUsersList();
        } else {
            if (json != null && json.messageCode === "SYSTEM_USER_LOGIN_ALREADY_EXIST") {
                $login.removeClass("state-valid").addClass("state-invalid");
            }
        }

        $thisBtn.buttonLoader("stop");
    });

}

function ClearFormAddSystemUser() {

    let $defaultImg = $("<img />", {
        src: "dist/img/default-user-icon.jpg",
        name: "avatar-img",
        class: "avatar avatar-xl",
        onerror: "this.src='dist/img/default-user-icon.jpg';"
    });
    let $userForm = $(".j-management-user");
    let clearInputArr = [
        "[name='full-name']",
        "[name='login']",
        "[name='email']",
        "[name='new-password']",
        "[name='reenter-password']",
    ];//Инпуты которые нужно очистить

    $(".card-title", $userForm).text(LANG_FILE.systemUser.managementUser.addTitle);
    $(".j-two-factor-input", $userForm).addClass("collapse");
    $("[name='is-use-two-fa']", $userForm).prop("disabled", true);
    $("[name='is-use-two-fa'][value='false']", $userForm).prop("checked", true);
    $(".j-btn-qrcode-two-factor", $userForm)
        .text("Сгенерировать")
        .data({
            "type-btn": "create",
            "system-user-id": 0
        })
        .removeClass("btn-danger")
        .addClass("btn-success");
    $(".j-edit-system-user", $userForm)
        .removeClass("j-edit-system-user")
        .addClass("j-add-system-user")
        .text(LANG_FILE.systemUser.managementUser.btnAdd)
        .data({
            "system-user-id": null
        });

    for (let index = 0; index < clearInputArr.length; index++) {//Чистка инпутов
        $(clearInputArr[index], $userForm)
            .removeClass("state-valid state-invalid")
            .val('');
    }
    $("[name='role'] option[value='administrator']", $userForm).prop("selected", true);
    $("[name='login']").prop("disabled", false);
    $(".j-avatar-row", $userForm).removeData("file").html('').append($defaultImg);
}

function GetLangsInSystem() {

    let $langSystemSelect = $(".j-management-user [name='lang-system']");
    let paramsAjax = {
        url: "langs",
        method: "GET"
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            let langs = json.data;
            langs.forEach(lang => {
                $langSystemSelect.append($("<option/>").text(lang.toUpperCase()).val(lang));
            });
        }
    })
}

/** Управление двух факторной авторизацией **/

function checkTwoFactorBtn() { //Определение какое действие выполнять при нажатии кнопки 
    let $thisBtn = $(this);
    let typeBtn = $(this).data("type-btn");
    let systemUserId = $(this).data("system-user-id");
    let $managementUserBox = $(".j-management-user");

    $thisBtn.buttonLoader("start");

    switch (typeBtn) {
        case "delete":
            openModalDropTwoFactorUser($thisBtn);
            $thisBtn.buttonLoader("stop");
            break;
        case "create":
            generateTwoFactor(systemUserId, function (_result) {
                if (_result == true) {
                    $("[name='is-use-two-fa']", $managementUserBox).prop("disabled", false);
                    $(".j-btn-qrcode-two-factor", $managementUserBox)
                        .text("Удалить 2FA")
                        .data({
                            "type-btn": "delete"
                        })
                        .addClass("btn-danger")
                        .removeClass("btn-success");
                }

                $thisBtn.buttonLoader("stop");
            });
            break;
        default:
            $thisBtn.buttonLoader("stop");
            break;
    }
}

function generateTwoFactor(_systemUserId, _cb) { //Запрос на генерацию двух факторной авторизации
    let $modalQrcodeTwoFactor = $("#modal-qrcode-two-factor");
    let paramsAjax = {
        url: `totp-generate-secret-key/${_systemUserId}`,
        method: "POST",
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            $modalQrcodeTwoFactor.find('img[name=qr-img]').attr("src", json.data || "dist/img/default-user-icon.jpg");
            $modalQrcodeTwoFactor.find(".j-activate-two-factor").data("system-user-id", _systemUserId).removeClass("collapse");
            $modalQrcodeTwoFactor.modal('show');
            $(".j-users-list .j-refresh").click();
        }

        _cb(result);
    });
}

function activateTwoFactor() {

    let $thisBtn = $(this);
    $thisBtn.buttonLoader("start");

    let idSystemUser = $thisBtn.data("system-user-id");
    let paramsAjax = {
        url: `user/${idSystemUser}/twoFactor/1`,
        dataType: "JSON",
        method: "PUT",
        timeout: 2000
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            ShowMessage("success", LANG_FILE.message.ok.CHANGE_STATE_TWO_FACTOR_OK, "ok");
            $(".j-management-user [name='is-use-two-fa'][value='true']").prop("checked", true);
            $(".j-users-list .j-refresh").click();
            $thisBtn.addClass("collapse");
        } else {
            ShowMessage('error', LANG_FILE.message.error.CHANGE_STATE_TWO_FACTOR_ERROR, 'error');
        }

        $thisBtn.buttonLoader("stop");
    });
}

$(document).on("keyup input", "#modal-drop-two-factor .j-input-type", function () { //Отслеживания что введено в input для разблокировки кнопки удалить

    let $thisInput = $(this);
    let $modal = $thisInput.parents("#modal-drop-two-factor"); //Модальное окно
    let $btnDelete = $(".j-btn-drop-two-factor-user", $modal); //Кнопка удаления
    let typeInput = $.trim($thisInput.val()); //Текс, который ввели в input
    let originalType = $thisInput.data("original-type"); //Текст который нужно ввести

    if (typeInput == originalType) { //Проверка совпадения текста
        $btnDelete
            .addClass("btn-danger")
            .removeClass("btn-gray")
            .prop("disabled", false); //Активация кнопки
    } else {
        $btnDelete
            .removeClass("btn-danger")
            .addClass("btn-gray")
            .prop("disabled", true); //Деактивация кнопки
    }

});

function openModalDropTwoFactorUser($thisBtn) { //Открытия модального окна генерации двух факторной авторизации

    let idSystemUser = $thisBtn.data("system-user-id")
    let $modal = $("#modal-drop-two-factor"); //Модальное окно удаления
    let defaultWord = "DELETE";

    $(".j-user-id", $modal).text(idSystemUser);
    $(".j-type", $modal).text(defaultWord);
    $(".j-input-type", $modal).data("original-type", defaultWord).val("");
    $(".j-btn-drop-two-factor-user", $modal).data({
        "id-system-user": idSystemUser
    });

    $modal.modal('show');
}

function dropUserTwoFactorAuth() { //Удаление двух факторной авторизации у пользователя
    let $thisBtn = $(this);
    $thisBtn.buttonLoader("start");

    let $managementUserBox = $(".j-management-user");
    let $modalDropTwoFactor = $("#modal-drop-two-factor");
    let systemUserId = $thisBtn.data("id-system-user");//Этот параметр есть только в случаи редактировании
    let paramsAjax = {
        url: `user/${systemUserId}/dropTwoFactor`,
        method: "PUT",
        defaultMessage: "DEFAULT_MESSAGE"
    };

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            $(".j-users-list .j-refresh").click();
            $("[name='is-use-two-fa']", $managementUserBox).prop("disabled", true);
            $("[name='is-use-two-fa'][value='false']", $managementUserBox).prop("checked", true);
            $(".j-btn-qrcode-two-factor", $managementUserBox)
                .text("Сгенерировать")
                .data({
                    "type-btn": "create",
                    "system-user-id": systemUserId
                })
                .removeClass("btn-danger")
                .addClass("btn-success");
            $modalDropTwoFactor.modal('hide');

            ShowMessage("success", LANG_FILE.message.ok.TWO_FACTOR_DROP_OK, "ok");

            $("input", $modalDropTwoFactor).val("");
        } else {
            // if (json != null && json.messageCode === "SYSTEM_USER_LOGIN_ALREADY_EXIST") {
            //     $login.removeClass("state-valid").addClass("state-invalid");
            // }
        }

        $thisBtn.buttonLoader("stop");
    });
}
