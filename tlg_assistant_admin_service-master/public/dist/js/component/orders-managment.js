let channelsListJSON = {};
let statusesListJSON = [];

const ORDERS_STATUSES_TYPES = {
    DONE: "done",
    CANCELED: "cancel",
    PENDING: "pending"
}

document.addEventListener("lang-ready", function (e) {
    $('[data-inputmask]').inputmask();

    moment.locale("ru");
    getOrdersSatatuses({});
    DrawOrdersSN();
});

$(document).on("click", ".j-orders-list .j-refresh", refreshOrdersList);
$(document).on("click", ".select-edit-group .j-change-status", changeDepositsStatus);
$(document).on("click", ".select-edit-group .j-set-status", setNewDepositStatus);
$(document).on("change", ".j-is-include-orders [name='is-include-free-orders']", getAndDrawParamsFromFilters);
$(document).on("change", ".j-is-include-orders [name='is-display-manual-only']", getAndDrawParamsFromFilters);

async function getOrdersDataTable() {
    await getChannelsList();
    await getStatusesListForDeposit();

    getAndDrawOrdersList({});
}

function getAndDrawParamsFromFilters() {
    let $filterBox = $(".j-filter-box");
    let $fiterIsInclFree = $("[name = 'is-include-free-orders']", $filterBox);
    let $fiterIsManualOnly = $("[name = 'is-display-manual-only']", $filterBox);
    let $fiterSelectedStatuses = $(".j-orders-statuses", $filterBox);
    let $filterSectedSNs = $(".j-sn", $filterBox);
    let $filterSelectDepoStatuses = $(".j-deposit-statuses", $filterBox);

    let filtersParams = {
        isIncludeFree: false,
        isManualOnly: false,
        selectedSNsArr: null,
        selectedStatusesArr: null,
        selectedStatusesDepo: null
    }

    let isIncludeFree = $fiterIsInclFree.prop("checked");
    let isManualOnly = $fiterIsManualOnly.prop("checked");
    let selectedStatusesArr = $fiterSelectedStatuses.val();
    let selectedSNsArr = $filterSectedSNs.val();
    let selectedStatusesDepo = $filterSelectDepoStatuses.val();

    filtersParams.isIncludeFree = isIncludeFree != false ? true : false;
    filtersParams.isManualOnly = isManualOnly != false ? true : false;
    filtersParams.selectedStatusesArr = selectedStatusesArr == "all" || selectedStatusesArr == "" ? null : selectedStatusesArr;
    filtersParams.selectedSNsArr = selectedSNsArr == "all" || selectedSNsArr == "" ? null : selectedSNsArr;
    filtersParams.selectedStatusesDepo = selectedStatusesDepo == "all" || selectedStatusesDepo == "" ? null : selectedStatusesDepo;

    getAndDrawOrdersList(filtersParams);
}

function refreshOrdersList() {
    $('#table-orders').DataTable().ajax.reload(null, false);
}

async function getChannelsList() {
    let paramsAjax = {
        url: "channels/all/list",
        method: "GET",
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE",
    }

    let { result, json } = await QueryAjaxPromise(paramsAjax);

    if (result === true) {
        let channels = json.data;

        for (const channelId in channels) {
            let channelSysId = channels[channelId].id;
            channelsListJSON[channelSysId] = channels[channelId];
        }
    }
}

async function getStatusesListForDeposit() {
    let paramsAjax = {
        url: "deposit/statuses",
        method: "GET",
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE",
    }


    let { result, json } = await QueryAjaxPromise(paramsAjax);
    if (result) {
        statusesListJSON = [...json.data];
        DrawDepositStatuses(json.data);
    }

}

let getSatatusesGlobal = false;
function getOrdersSatatuses() {
    let $selectEmployees = $('.j-filter-orders .j-orders-statuses');

    if ($selectEmployees[0].selectize != null) {
        $selectEmployees[0].selectize.disable();
    }

    let paramsAjax = {
        url: "orders/statuses",
        method: "GET",
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result == true && json.data != null) {
            DrawOrdersStatuses(json.data);
        } else {
            DrawOrdersStatuses([]);
        }
        getSatatusesGlobal = true;
        checkedGetFilter();
    });
}//end getOrdersSatatuses

function DrawOrdersStatuses(_allData) {
    if (_allData != null) {
        let $selectStatuses = $('.j-filter-orders .j-orders-statuses');
        let statuses = _allData.map(status => {
            return {
                name: LANG_FILE.orderStatuses[status],
                value: status
            }
        });

        statuses.unshift({
            name: LANG_FILE.all,
            value: "all"
        })

        $selectStatuses = $selectStatuses.selectize({
            plugins: ['remove_button'],
            persist: false,
            maxItems: null,
            valueField: 'value',
            labelField: 'name',
            searchField: ['name'],
            options: statuses,
            render: {
                item: function (item, escape) {
                    let name = item.name ? item.name : "-";
                    name = [...name].slice(0, 25).join('');

                    let $div = $('<div/>')
                        .addClass("tag")
                        .text(escape(name))
                        .prepend(
                            $('<span/>')
                        );
                    return $div.prop('outerHTML');
                },
                option: function (item, escape) {
                    let name = item.name ? item.name : "-";
                    name = [...name].slice(0, 25).join('');

                    let $div = $('<div/>')
                        .text(escape(name))
                        .append(
                            $('<span/>')
                        );
                    return $div.prop('outerHTML');
                }
            }
        })[0].selectize;

        $selectStatuses.enable();
        $selectStatuses.setValue("all");
        $selectStatuses.on("change", onChancgeSelect);
    }
}//end DrawOrdersStatuses

function DrawDepositStatuses(_allData) {
    if (_allData != null) {
        let $selectStatuses = $('.j-filter-orders .j-deposit-statuses');
        let statuses = _allData.map(status => {
            return {
                name: LANG_FILE.depositStatuses[status],
                value: status
            }
        });

        statuses.unshift({
            name: LANG_FILE.all,
            value: "all"
        });

        $selectStatuses = $selectStatuses.selectize({
            plugins: ['remove_button'],
            persist: false,
            maxItems: null,
            valueField: 'value',
            labelField: 'name',
            searchField: ['name'],
            options: statuses,
            render: {
                item: function (item, escape) {
                    let name = item.name ? item.name : "-";
                    name = [...name].slice(0, 25).join('');

                    let $div = $('<div/>')
                        .addClass("tag")
                        .text(escape(name))
                        .prepend(
                            $('<span/>')
                        );
                    return $div.prop('outerHTML');
                },
                option: function (item, escape) {
                    let name = item.name ? item.name : "-";
                    name = [...name].slice(0, 25).join('');

                    let $div = $('<div/>')
                        .text(escape(name))
                        .append(
                            $('<span/>')
                        );
                    return $div.prop('outerHTML');
                }
            }
        })[0].selectize;

        $selectStatuses.enable();
        $selectStatuses.setValue("all");
        $selectStatuses.on("change", onChancgeSelect);
    }
}//end DrawDepositsStatuses

let getSatatusesOrdersListGlobal = false;

function DrawOrdersSN() {
    getSatatusesOrdersListGlobal = true;
    checkedGetFilter();

    let $selectSN = $('.j-filter-orders .j-sn');

    $selectSN = $selectSN.selectize({
        plugins: ['remove_button'],
        persist: false,
        maxItems: null,
        valueField: 'value',
        labelField: 'name',
        searchField: ['name'],
        options: [{
            name: LANG_FILE.all,
            value: "all"
        }],
        render: {
            item: function (item, escape) {
                let name = item.name ? item.name : "-";

                let $div = $('<div/>')
                    .addClass("tag")
                    .text(escape(name))
                    .prepend(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            },
            option: function (item, escape) {
                let name = item.name ? item.name : "-";

                let $div = $('<div/>')
                    .text(escape(name))
                    .append(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            }
        },
        load: function (query, callback) {
            if (query.length < 6) return callback();
            $.ajax({
                url: `/orders/get/filtered`,
                type: 'get',
                dataType: 'json',
                data: {
                    start: 0,
                    length: 10,
                    search: {
                        value: query
                    }
                },
                error: function () {
                    callback();
                },
                success: function (res) {
                    let orders = res.data.data.map(order => {
                        return {
                            name: order.sn,
                            value: order.sn
                        }
                    });
                    callback(orders);
                }
            });
        }
    })[0].selectize;

    $selectSN.enable();
    $selectSN.setValue("all");
    $selectSN.on("change", onChancgeSelect);
}//end DrawOrdersSN

function checkedGetFilter() {
    if (getSatatusesOrdersListGlobal == true && getSatatusesGlobal == true) {
        getOrdersDataTable();
    }
}

function getAndDrawOrdersList(_paramsForFilter) {
    let $tableOrders = $('#table-orders');

    let {
        isIncludeFree = false,
        isManualOnly = false,
        selectedSNsArr = null,
        selectedStatusesArr = null,
        selectedStatusesDepo = null
    } = _paramsForFilter;

    isIncludeFree = isIncludeFree != true ? `isIncludeFree=${isIncludeFree}&` : "";
    isManualOnly = isManualOnly != false ? `isManualOnly=${isManualOnly}&` : "";
    selectedSNsArr = selectedSNsArr != null && selectedSNsArr != LANG_FILE.all ? `selectedSNsArr=${selectedSNsArr}&` : "";
    selectedStatusesArr = selectedStatusesArr != null && selectedStatusesArr != LANG_FILE.all ? `selectedStatusesArr=${selectedStatusesArr}&` : "";
    selectedStatusesDepo = selectedStatusesDepo != null && selectedStatusesDepo != LANG_FILE.all ? `selectedStatusesDepo=${selectedStatusesDepo}` : "";


    $tableOrders.DataTable().destroy();
    $tableOrders.DataTable({
        ajax: {
            method: 'get',
            url: `orders/get/filtered/?${isIncludeFree}${isManualOnly}${selectedStatusesArr}${selectedSNsArr}${selectedStatusesDepo}`,
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);
            },
            dataFilter: function (resultData) {
                let data = JSON.parse(resultData).data;
                return JSON.stringify(data);
            }
        },
        processing: false,
        serverSide: true,
        createdRow: function (row, order, numRow) {
            let $newOrderTr = $(row);
            let status = order.status;
            let orderId = order.id;

            $newOrderTr.attr({
                "data-order-id": orderId
            });

            switch (status) {
                case ORDERS_STATUSES_TYPES.DONE:
                    $newOrderTr.addClass("tr-success");
                    break;
                case ORDERS_STATUSES_TYPES.CANCELED:
                    $newOrderTr.addClass("tr-danger");
                    break;
                case ORDERS_STATUSES_TYPES.PENDING:
                    $newOrderTr.addClass("tr-info");
                    break;
            }

            if (order.isFree == true) {
                $(".j-order-status", $newOrderTr).closest(".select-edit-group").text(LANG_FILE.free);
            }

        },

        columns: [
            {
                name: "id",
                data: "id",
                orderable: false,
                render: function (id, type, data) {
                    return id;
                }
            }, {
                name: "channel_id",
                data: "channelId",
                orderable: false,
                render: function (channelId, type, data) {
                    let channelInfo = channelsListJSON[channelId];
                    let price = "";
                    let date = moment(data.createdAt).format("L LTS");

                    if (channelInfo != null) {
                        price = `${channelInfo.price} ${channelInfo.currency}`;
                    }

                    return `
                        <div class="small text-muted">${date}</div>
                        <span>${channelInfo.name != null ? channelInfo.name : '-'}</span>
                        <div class="small text-muted">${price}</div>
                    `;
                }
            }, {
                name: "status",
                data: "status",
                orderable: false,
                render: function (status, type, data) {
                    return LANG_FILE.orderStatuses[status];
                }
            }, {
                name: "status",
                data: "depositInfo.status",
                orderable: false,
                render: function (status, type, data) {
                    return ` 
                    <div class="select-edit-group" data-toggle="false" style="max-width: 200px;">
                        <div class="edit-group">
                            <span class="j-order-status" style="white-space: pre-wrap" data-deposit-status="${status}">${LANG_FILE.depositStatuses[status]}</span>
                            <button class="icon icon-sm ml-1 j-change-status" :title=${LANG_FILE.edit}>
                                <i class="fe fe-edit-3"></i>
                            </button>
                        </div>
                        <div class="select-group collapse">
                            <select type="text" class="form-control j-select-status">
                            </select>
                            <span class="select-group-append">
                                <button class="btn btn-primary j-set-status" type="button" :title=${LANG_FILE.save}>
                                    <i class="fe fe-save"></i>
                                </button>
                            </span>
                        </div> 
                    </div>
                `
                }
            }, {
                name: "userInfo",
                data: "userInfo",
                orderable: false,
                render: function (userInfo, type, data) {
                    let fullName = `${userInfo.firstName} ${userInfo.lastName}`;
                    let username = userInfo.username == "" ? "" : `@${userInfo.username}`;
                    let userLink = userInfo.username == "" ? "" : `${LANG_FILE.telegrammLink}${userInfo.username}`;

                    return `
                        <span>${fullName}</span>
                        <div class="small text-muted"><a href="${userLink}" target="_blank">${username}</a></div>
                        <div class="small text-muted">${LANG_FILE.id}: ${userInfo.id}</div>
                    `;
                }
            }, {
                name: "referenceId",
                orderable: false,
                render: function (referenceId, type, data) {
                    referenceId = "-";

                    if (data.depositInfo != null && data.depositInfo.referenceId != null) {
                        referenceId = data.depositInfo.referenceId;
                    }

                    return `
                        <span>${data.sn}</span>
                        <div class="small text-muted">${LANG_FILE.kuna}: ${referenceId}</div>
                    `;
                }
            }, {
                name: "serviceAlias",
                data: "serviceAlias",
                orderable: false,
                render: function (serviceAlias, type, data) {
                    serviceAlias = "-";

                    if (data.depositInfo != null && data.depositInfo.serviceAlias != null) {
                        serviceAlias = data.depositInfo.serviceAlias;
                    }

                    return serviceAlias;
                }
            }],
        paging: true,
        info: true, //Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        retrieve: true,
        language: LANG_FILE.dataTablesLanguage,
        order: [], //сортировка
        pageLength: 25
        // drawCallback: function () {}
    });
}

function changeDepositsStatus() {
    let $this = $(this);
    let $selectEditGroup = $this.parents(".select-edit-group");
    let $editGroup = $(".edit-group", $selectEditGroup);
    let $valueBox = $("span", $editGroup);
    let value = $.trim($valueBox.data("deposit-status"));
    let $selectGroup = $(".select-group", $selectEditGroup);
    let $selectOfTypes = $("select", $selectGroup);

    for (const i in statusesListJSON) {
        let status = statusesListJSON[i];
        $selectOfTypes.append($(`<option value="${status}">${LANG_FILE.depositStatuses[status]}</option>`));
    }

    value = value == "-" ? "" : value;
    $selectOfTypes.val(value);
    $editGroup.hide();
    $selectGroup.css('display', 'flex');

}

function setNewDepositStatus() {
    let $thisButton = $(this);
    let $selectEditGroup = $(".select-edit-group .j-change-status").parents(".select-edit-group");
    let $selectGroup = $(".select-group", $selectEditGroup);
    let $editGroup = $(".edit-group", $selectEditGroup);
    let $valueBox = $("span", $editGroup);
    let statusVal = $(".j-select-status option:selected").val();
    let $ordersTr = $thisButton.parents("tr").first();
    let orderId = $ordersTr.data("order-id");

    $thisButton.buttonLoader("start");
    let paramsAjax = {
        url: `order/${orderId}/deposit`,
        method: "PUT",
        data: {
            orderId: orderId,
            status: statusVal,
            silent: false
        },
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, jsonObjectList) {
        if (result) {
            ShowMessage("success", LANG_FILE.message.ok.SET_NEW_DEPOSIT_STATUS_OK, "ok");
            $valueBox.data("deposit-status", statusVal).text(statusVal);
        } else {
            ShowMessage("error", LANG_FILE.message.error.SET_NEW_DEPOSIT_STATUS_ERROR, "error");

        }

        $selectGroup.hide();
        $editGroup.show();
        refreshOrdersList()

        $thisButton.buttonLoader("stop");
    });
}

function onChancgeSelect() {
    let values = this.getValue().split(',');
    let nowSelected = values[values.length - 1];

    if (this.getValue() != "") {
        if (values.length > 1 && values.indexOf("all") != -1) {
            if (nowSelected != "all") {
                values.splice(values.indexOf("all"), 1);
                this.clear(true);
                this.setValue(values, true);
            } else {
                this.clear(true);
                this.setValue("all", true);
            }
        }
    } else {
        this.setValue("all", true);
    }

    getAndDrawParamsFromFilters();
}