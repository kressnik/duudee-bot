let isAutoUpdateStatsGlodal = false;
const LINKS_STATES_TYPES = {
    ACTIVE: true,
    DISACTIVE: false
}

const TYPE_OF_REFF_LINK = {
    START: "start",
    CHANNEL: "channel"
}

const TOKEN_LENGTH = 15;
const DEF_FIRST_PATH_LINK = "https://t.me";
const DEF_BOT_FOR_LINK = "/duudeesport_assistant_bot?start=";
const PERCENT = "%";
const TINT_VAL_SIX = 0.6;
const TINT_VAL_FIVE = 0.5;

const TRACK_ERROR = {
    NULL: null,
    ERROR_ALREADY_ASSIGNED: 'ERROR_ALREADY_ASSIGNED',
    ERROR_RECIVE_STOP_SIGNAL: 'ERROR_RECIVE_STOP_SIGNAL',
    ERROR_INVALID_HANDLE: 'ERROR_INVALID_HANDLE',
    ERROR_UNEXP_ERR: 'ERROR_UNEXP_ERR',
    ERROR_INVALID_DATA: 'ERROR_INVALID_DATA',
    ERROR_CANCEL: 'ERROR_CANCEL'
};

const TRACK_STATUS = {
    DONE: 'DONE'
};

document.addEventListener("lang-ready", function (e) {
    moment.locale("ru");

    InitDatetimepicker();
    getReferalLinksList();
    getInfoForCreateNewLink();
});


$(document).on("click", ".j-referal-list .j-refresh", refreshLinksList);
$(document).on("click", ".j-refresh-activity", refreshTracksList);
$(document).on("change", "#table-referals [name='active']", changeReffLinkState);
$(document).on("click", "#table-referals .j-btn-full-delete", deleteSoftReffLink);
$(document).on("click", "#table-referals .j-btn-edit", getInfoForLink);
$(document).on("click", "#edit-refferal-link .j-add", editRefferalLink);
$(document).on("click", "#add-refferal-link .j-create", createRefferalLink);
$(document).on("click", "#add-refferal-link .j-generate-reff-link", generateRefferalLink);
$(document).on("click", ".j-full-link", CopyReffLink);
$(document).on('hidden.bs.modal', "#edit-refferal-link", ClearModalAddReffLink);
$(document).on('hidden.bs.modal', "#add-refferal-link", ClearModalAddReffLink);

$(document).on("change", ".j-watch-in-real-time [name='real-time']", getStatsInRealTime);

$(document).on("click", ".j-referal-list .j-btn-statistic", showStatisticInfo);
$(document).on("click", ".j-referal-list .j-btn-activity", showActivityInfo);
$(document).on("click", ".j-reff-link-statistic .j-back", hideStatisticInfo);
$(document).on("click", ".j-reff-link-activity .j-back", hideActivityInfo);
$(document).on("click", ".j-filter-stats-save", GetParamsFromFilter);
$(document).on("click", ".j-filter-activity-save", getAndDrawParamsFromFilters);

$(document).on("change", "#add-refferal-link .j-select-types", hideShowSelectRefferalModal);
$(document).on("change", "#edit-refferal-link .j-select-types", hideShowSelectRefferalModal);

function refreshLinksList() {
    $('#table-referals').DataTable().ajax.reload(null, false);
}

function getReferalLinksList() {

    let $tableRefferalLinks = $('#table-referals');
    $tableRefferalLinks.DataTable().destroy();
    $tableRefferalLinks.DataTable({
        ajax: {
            method: 'get',
            url: 'referals/all',
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);
            },
            dataFilter: function (resultData) {
                let data = JSON.parse(resultData).data;
                return JSON.stringify(data);
            }
        },
        processing: false,
        serverSide: true,
        createdRow: function (row, link, numRow) {
            let $newRefferalLinkTr = $(row);
            let state = link.isActive;
            let isDelete = link.isDelete;
            let linkId = link.id;
            let linkChannel = link.channels.name;
            let linkType = link.type;
            let linkButton = link.button.name;
            let linkDescription = link.description;
            let linkToken = link.link;
            let linkTitle = link.title;
            let channelName = link.channels.name;
            let channelPrice = link.channels.price;
            let channelCurrency = link.channels.currency;
            let createdAt = link.createdAt;


            $newRefferalLinkTr.attr({
                "data-id": linkId,
                "data-type": linkType,
                "data-channel": linkChannel,
                "data-button": linkButton,
                "data-description": linkDescription,
                "data-token": linkToken,
                "data-title": linkTitle,
                "data-is-delete": isDelete,
                "data-is-active": state,
                "data-channel-name": channelName,
                "data-channel-price": channelPrice,
                "data-channel-currency": channelCurrency,
                "data-created-at": createdAt
            });

            $('.j-full-link', $newRefferalLinkTr).data("linkToken", link.link);

            switch (state) {
                case LINKS_STATES_TYPES.ACTIVE:
                    $newRefferalLinkTr.addClass("tr-success");
                    break;
                case LINKS_STATES_TYPES.DISACTIVE:
                    $newRefferalLinkTr.addClass("tr-danger");
                    break;
            }

            if (isDelete == true) {
                $newRefferalLinkTr.addClass("tr-offline");
            }
        },

        columns: [
            {
                name: "id",
                data: "id",
                orderable: false,
                render: function (id, type, data) {
                    return id;
                }
            },
            {
                name: "title",
                data: "title",
                orderable: false,
                render: function (title, type, data) {
                    let date = moment(data.createdAt).format("L LTS");

                    return [
                        `<div class="small text-muted">${date}</div>`,
                        `<span>${title != null ? title : '-'}</span>`
                    ].join('');;
                }
            },
            {
                name: "link",
                data: "link",
                orderable: false,
                render: function (link, type, data) {
                    return `<a href="#" class="j-full-link">${link}</a>`;
                }
            },
            {
                name: "linkType",
                data: "type",
                orderable: false,
                render: function (linkType, type, data) {
                    return LANG_FILE.refferal.linkType[linkType];
                }
            },
            {
                data: "isActive",
                orderable: false,
                render: function (active, type, data) {
                    let checked = active == false ? "" : "checked";
                    let state = [
                        '<label class="custom-switch">',
                        '<input type="checkbox" name="active"',
                        `class="custom-switch-input" ${checked}>`,
                        '<span class="custom-switch-indicator"></span>',
                        '</label>'
                    ].join('');

                    if (data.isDelete != false) {
                        state = LANG_FILE.refferal.deleted;
                    }

                    return state;
                }
            },
            {
                orderable: false,
                render: function (channelId, type, data) {
                    let channelInfo = data.channels;
                    let price = "";

                    if (channelInfo != null) {
                        price = `${channelInfo.price || ""} ${channelInfo.price != 0 ? channelInfo.currency : ""}`;
                    }

                    return [
                        `<span>${channelInfo.name != null ? channelInfo.name : '-'}</span>`,
                        `<div class="small text-muted">${price}</div>`
                    ].join('');
                }
            },
            {
                data: "isActive",
                orderable: false,
                render: function (isActive, type, data) {
                    let html = [
                        '<button type="button"',
                        'class="btn btn-icon btn-primary btn-secondary j-btn-statistic"',
                        `title="${LANG_FILE.stats}">`,
                        '<i class="fe fe-bar-chart"></i>',
                        '</button>',
                        '<button type="button"',
                        'class="btn btn-icon btn-primary btn-secondary j-btn-activity"',
                        `title="Активность">`, /// to translate 
                        '<i class="fe fe-activity"></i>',
                        '</button>'
                    ].join('');

                    if (isActive == 0) {
                        if (data.isDelete == false) {
                            html = [
                                '<div class="btn-list">',
                                '<button type="button"',
                                'class="btn btn-icon btn-primary btn-secondary j-btn-full-delete"',
                                `title="${LANG_FILE.del}">`,
                                '<i class="fe fe-trash"></i>',
                                '</button>',
                                ...html,
                                '</div>'
                            ].join('');
                        }
                    } else {
                        if (data.isDelete == false) {
                            html = [
                                '<div class="btn-list">',
                                '<button type="button"',
                                'class="btn btn-icon btn-primary btn-secondary j-btn-edit"',
                                'data-toggle="modal" data-target="#edit-refferal-link"',
                                `title="${LANG_FILE.edit}">`,
                                '<i class="fe fe-edit"></i>',
                                '</button>',
                                ...html,
                                '</div>'
                            ].join('');
                        }
                    }
                    return html;
                }
            },
        ],
        paging: true,
        info: true, //Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        language: LANG_FILE.dataTablesLanguage,
        order: []
    });
}

function hideShowSelectRefferalModal() {
    let $modalBox = $(this).closest(".modal");
    let idHTML = $modalBox.attr("id");

    if (idHTML != null) {
        let $modalBox = $(`#${idHTML}`);
        let $channelSelectBox = $(".j-select-channels", $modalBox).closest(".form-group");
        let $buttonsSelectBox = $(".j-select-buttons", $modalBox).closest(".form-group");
        let selectedType = $(".j-select-types", $modalBox).val();

        if (selectedType == TYPE_OF_REFF_LINK.CHANNEL) {
            $channelSelectBox.removeClass("collapse");
            $buttonsSelectBox.removeClass("collapse");
        } else {
            $channelSelectBox.addClass("collapse");
            $buttonsSelectBox.addClass("collapse");
        }
    }
}

function changeReffLinkState() {//Изменения статуса DISACTIVE/ACTIVE

    let $this = $(this);
    let $tr = $this.parents("tr").first();
    let active = $this.prop("checked");//Получения нового статуса
    let refferalInfo = $tr.data();
    let refferalLinkId = refferalInfo.id;

    $this.prop("disabled", true);

    let paramsAjax = {
        url: `referal/${refferalLinkId}/edit`,
        method: "PUT",
        data: {
            id: refferalLinkId,
            isActive: active
        }
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result == false) {
            $this.prop("checked", active ? false : true);
            ShowMessage("success", LANG_FILE.message.error.CHANGE_LINK_STATE_ERROR, "error");
        } else {
            ShowMessage("success", LANG_FILE.message.ok.CHANGE_LINK_STATE_OK, "ok");
        }

        $this.prop("disabled", false);
        refreshLinksList();
    });
};

function deleteSoftReffLink() {
    let $this = $(this);
    let $tr = $this.parents("tr").first();
    let refferalInfo = $tr.data();
    let refferalLinkId = refferalInfo.id;

    let paramsAjax = {
        url: `referal/${refferalLinkId}`,
        method: "DELETE",
        data: {
            id: refferalLinkId
        }
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result == false) {
            ShowMessage("error", LANG_FILE.message.error.DELETE_REFF_LINK_ERROR, "error");
        } else {
            ShowMessage("success", LANG_FILE.message.ok.DELETE_REFF_LINK_OK, "ok");
        }
        refreshLinksList();
    });
}

function getInfoForLink() {
    let $this = $(this);
    let $tr = $this.parents("tr").first();
    let refferalInfo = $tr.data();

    let linkId = refferalInfo.id;
    let linkTitle = refferalInfo.title;
    let linkType = refferalInfo.type;
    let linkChannel = refferalInfo.channel;
    let linkButton = refferalInfo.button;
    let comment = refferalInfo.description;
    let linkToken = refferalInfo.token;
    let botPathLink = DEF_BOT_FOR_LINK;                               // LANG_FILE.botId[botId] || 
    let fullReffLink = [DEF_FIRST_PATH_LINK, botPathLink, linkToken].join('');

    let $modalBox = $('#edit-refferal-link');
    let $title = $('.j-title-link', $modalBox);
    let $selectTypes = $('.j-select-types', $modalBox);
    let $selectChannels = $('.j-select-channels', $modalBox);
    let $selectButtons = $('.j-select-buttons', $modalBox);
    let $comment = $('[name="description"]', $modalBox);
    let $linkFullText = $('.j-full-link', $modalBox);

    $modalBox.data("id", linkId);

    $linkFullText.data("linkToken", linkToken);  //for copy link by click
    $linkFullText.text(fullReffLink);

    $title.val(linkTitle);

    $selectTypes.append(`<option value="${linkType}" data-type="0">${LANG_FILE.refferal.linkType[linkType]}</option>`);
    $(`[value=${linkType}]`, $selectTypes).prop("selected", true).change();

    $selectChannels.append(`<option data-channel="0">${linkChannel}</option>`);
    $('[data-channel=0]', $selectChannels).prop("selected", true).change();

    $selectButtons.append(`<option data-button="0">${linkButton}</option>`);
    $('[data-button=0]', $selectButtons).prop("selected", true).change();

    $comment.val(comment);
}

function editRefferalLink() {
    let $modalBox = $('#edit-refferal-link');
    let $comment = $('[name="description"]', $modalBox);
    let $title = $('.j-title-link', $modalBox);

    let checkedTitle = ValidateInput($title);

    if (checkedTitle == "error") {
        ShowMessage("error", LANG_FILE.message.error.EMPTY_INPUT, "error");
    } else {

        let linkId = $modalBox.data("id");
        let comment = $comment.val();
        let title = $title.val();

        let paramsAjax = {
            url: `referal/${linkId}/edit`,
            method: "PUT",
            data: {
                id: linkId,
                description: comment,
                title: title
            }
        }

        QueryAjax(paramsAjax, function (result, json) {
            if (result) {
                ShowMessage("success", LANG_FILE.message.ok.EDIT_REFF_LINK_OK, "ok");
                refreshLinksList();
            } else {
                ShowMessage("error", LANG_FILE.message.error.EDIT_REFF_LINK_ERROR, "error");
                refreshLinksList();
            }
        });

        $modalBox.modal('hide');
    }
}

function generateRefferalLink() {
    let $modalBox = $('#add-refferal-link');
    let $linkInput = $('.j-generate-link-input', $modalBox);
    let $linkFullText = $('.j-full-link', $modalBox);
    // let botId = $modalBox.data("bot-id");

    let token = randomString(TOKEN_LENGTH);
    let botPathLink = DEF_BOT_FOR_LINK;                               // LANG_FILE.botId[botId] || 
    let fullReffLink = [DEF_FIRST_PATH_LINK, botPathLink, token].join('');

    $modalBox.data("link-token", token);
    $linkInput.val(token);
    $linkFullText.empty();
    $linkFullText.data("linkToken", token);   //for copy link by click
    $linkFullText.text(fullReffLink);
}

function CopyReffLink(e) {
    if (e != null) { e.preventDefault(); }

    let linkToken = $(this).data("linkToken");
    let botPathLink = DEF_BOT_FOR_LINK;                               // LANG_FILE.botId[botId] || 
    let reffLink = [DEF_FIRST_PATH_LINK, botPathLink, linkToken].join('');

    let $tmp = $("<textarea>");
    $("body").append($tmp); // Вставляем временную переменную с полем для ввода
    $tmp.val(reffLink).select(); // Вставляем в это поле данные нужного элемента и выделяем
    document.execCommand("copy"); // Копируем эти данные
    $tmp.remove(); // Удаляем временную переменную

    ShowMessage("success", LANG_FILE.message.ok.LINK_COPY_SUCCESSFULL, "ok");
}

function createRefferalLink() {
    let $modalBox = $('#add-refferal-link');
    let $selectTypes = $('.j-select-types', $modalBox);
    let $selectChannels = $('.j-select-channels', $modalBox);
    let $selectButtons = $('.j-select-buttons', $modalBox);
    let $comment = $('[name="description"]', $modalBox);
    let $title = $('.j-title-link', $modalBox);
    let $inputLinkToken = $('.j-generate-link-input', $modalBox);
    let checkedTitle = ValidateInput($title);
    let checkedLink = ValidateInput($inputLinkToken);

    if (checkedLink == "error" || checkedTitle == "error") {
        ShowMessage("error", LANG_FILE.message.error.EMPTY_INPUT, "error");
    } else {

        let selectType = $selectTypes.val();
        let linToken = $modalBox.data("link-token");
        let selectChannel = $selectChannels.val();
        let selectButton = $selectButtons.val();
        let comment = $comment.val();
        let title = $title.val();

        let paramsAjax = {
            url: 'referal',
            method: "POST",
            data: {
                link: linToken,
                type: selectType,
                channelId: selectChannel,
                buttonId: selectButton,
                description: comment,
                title: title
            }
        }

        QueryAjax(paramsAjax, function (result, json) {
            if (result) {
                ShowMessage("success", LANG_FILE.message.ok.CREATE_REFF_LINK_DONE_OK, "ok");
                refreshLinksList();
            } else {
                ShowMessage("error", LANG_FILE.message.error.CREATE_REFF_LINK_DONE_ERROR, "error");
                refreshLinksList();
            }
        });

        $modalBox.modal('hide');
    }
}

function ClearModalAddReffLink() {
    let $this = $(this);
    let $selectTypes = $('.j-select-types', $this);
    let $selectChannels = $('.j-select-channels', $this);
    let $selectButtons = $('.j-select-buttons', $this);
    let $comment = $('[name="description"]', $this);
    let $linkFullText = $('.j-full-link', $this);
    let $linkInput = $('.j-generate-link-input', $this);
    let $title = $('.j-title-link', $this);

    $title.val("").removeClass("state-valid state-invalid");
    $("option:eq(1)", $selectTypes).prop('selected', true).change();
    $("option:first", $selectChannels).prop('selected', true);
    $("option:first", $selectButtons).prop('selected', true);
    $linkFullText.empty();
    $linkInput.val("").removeClass("state-valid state-invalid");
    $comment.val("");
};

function getInfoForCreateNewLink(_cb) {
    let paramsAjax = {
        url: `info/create/new/link`,
        method: "GET"
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            let $modalBox = $('#add-refferal-link');
            let $selectTypes = $('.j-select-types', $modalBox);
            let $selectChannels = $('.j-select-channels', $modalBox);
            let $selectButtons = $('.j-select-buttons', $modalBox);
            $selectTypes.empty();
            $selectChannels.empty();
            $selectButtons.empty();

            json.data.linkTypes.forEach(function (type) {
                $selectTypes.append(`<option data-type=${type} value=${type}>${LANG_FILE.refferal.linkType[type]}</option>`);
            });
            $('[data-type="channel"]', $selectTypes).prop("selected", true).change();

            json.data.channels.forEach(function (channel) {
                if (channel.inUse == true) {
                    $selectChannels.append(`<option data-channel=${channel.id} value=${channel.id}>${channel.name}</option>`);
                }
            });
            $('option:first', $selectChannels).prop("selected", true).change();

            json.data.buttons.forEach(function (buttons) {
                if (buttons.isActive != true) {
                    $selectButtons.append(`<option data-buttons=${buttons.id} value=${buttons.id}>${buttons.name}</option>`);
                }
            });
            $('option:first', $selectButtons).prop("selected", true).change();
        }
    });
}


/**
 * stats
 */

function showStatisticInfo() {
    let $thisBtn = $(this);
    let $reffLinkTr = $thisBtn.parents("tr").first();
    let reffLinkInfo = $reffLinkTr.data();

    let $boxTable = $(".j-refferal-links .j-referal-list");
    let $boxLinkStatistic = $(".j-reff-link-statistic");
    let $timeBox = $(".j-period-for-stats");

    $thisBtn.buttonLoader("start");

    $(".j-id", $boxLinkStatistic).text('');
    $(".j-status", $boxLinkStatistic).text('');
    $(".j-reference-id", $boxLinkStatistic).text('');
    $(".j-price", $boxLinkStatistic).text('');
    $(".j-currency", $boxLinkStatistic).text('');
    $(".j-service-method", $boxLinkStatistic).text('');
    $(".j-serial", $boxLinkStatistic).text('');

    defaultTimeToPage(reffLinkInfo.createdAt, $timeBox);
    DrawControllerTr($("#reff-link-statistic .j-tr"), reffLinkInfo, function () {
        GetParamsFromFilter();
    });

    $boxTable.addClass("collapse");
    $boxLinkStatistic.removeClass("collapse");

    $thisBtn.buttonLoader("stop");
}

function GetParamsFromFilter() {
    let tokenId = $("#reff-link-statistic .j-tr").data("id");

    let $statsPeriodBox = $(".j-period-for-stats");
    let $inputFromDate = $("[name='from-date']", $statsPeriodBox);
    let $inputToDate = $("[name='to-date']", $statsPeriodBox);
    let $isWatchInRealTime = $(".j-watch-in-real-time [name='real-time']");
    let inputFromDateValidateStatus = ValidateInput($inputFromDate);
    let inputToDateValidateStatus = ValidateInput($inputToDate);

    if (inputFromDateValidateStatus === "error" || inputToDateValidateStatus === "error") {
        ShowMessage("error", "EMPTY_INPUT", "error");
    }
    else {
        let isWatchInRealTime = $isWatchInRealTime.prop("checked");
        let toDate = null;
        let fromDate = $inputFromDate.data("datetimepicker").viewDate().valueOf();
        fromDate = moment.utc(fromDate).valueOf();

        if (isWatchInRealTime == false) {
            toDate = $inputToDate.data("datetimepicker").viewDate().valueOf();
            toDate = moment.utc(toDate).valueOf();
        }

        getStatistic({
            tokenId: tokenId,
            fromDate: fromDate,
            toDate: toDate
        });
    }
}// end GetParamsFromFilter

async function getStatistic(_prms) {
    let {
        tokenId,
        toDate = null,
        fromDate = null
    } = _prms;

    let dateRange = toDate != null && fromDate != null ? [fromDate, toDate] : [];

    let data = {
        dateRange,
        filter: {
            id: [],
            status: [],
            tokenId: [tokenId],
            buttonId: [],
            channelId: [],
            orderId: [],
            sessionId: [],
            error: []
        }
    }

    let paramsAjax = {
        url: `refferal/link/stats/${tokenId}`,
        method: "POST",
        data,                             //will rework
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE",
    }

    let { result, json } = await QueryAjaxPromise(paramsAjax);
    if (result) {
        drawStatisticCharts(json.data);
    } else {
        ShowMessage("error", LANG_FILE.message.error.DATA_NOT_FOUND, "error")
    }

}

function drawStatisticCharts(data) {
    if (data != null) {
        $('#charts-stats .j-stats').remove();
        $('#charts-stats').append('<canvas class="j-stats"> <canvas>');
        let $totalVisitsChart = $('#charts-stats .j-stats');

        $('#charts-subscribes .j-subscribes').remove();
        $('#charts-subscribes').append('<canvas class="j-subscribes"> <canvas>');
        let $subscribesChart = $('#charts-subscribes .j-subscribes');

        $('#channels-subscribes .j-channels-subscribes').remove();
        $('#channels-subscribes').append('<canvas class = "j-channels-subscribes"> <canvas>');
        let $channelsSubscChart = $('#channels-subscribes .j-channels-subscribes');

        let commingRate = data.channelsTotalStats.commingRate;
        let lossRate = data.channelsTotalStats.lossRate;


        let totalVisitData = [
            data.newUsers,
            data.returnUsers
        ];
        let totalSubscribesData = [
            commingRate,
            lossRate
        ];

        let doughnutLegend = {
            position: "right",
            align: "end"
        };
        let titleOptions = {
            display: true,
            position: 'top',
            fontSize: 14,
            padding: 10
        };

        let preparedInfoByChannels = preparationInfoByChannels(data.channelsStats);

        let chanSubscData = {
            label: LANG_FILE.refferal.stats.channelsSubscChart.doneSubscribes,
            data: preparedInfoByChannels.subscribesArr,
            backgroundColor: preparedInfoByChannels.subscColorsArr,
            borderColor: preparedInfoByChannels.borderColorsArr,
            borderWidth: 1
        };

        let chanReturnSubscData = {
            label: LANG_FILE.refferal.stats.channelsSubscChart.returnSubscribes,
            data: preparedInfoByChannels.returnSubscribesArr,
            backgroundColor: preparedInfoByChannels.retSubscColorsArr,
            borderColor: preparedInfoByChannels.borderColorsArr,
            borderWidth: 1
        };

        Chart.defaults.global.defaultFontFamily = 'Neue';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#444';

        new Chart($totalVisitsChart, {
            type: 'doughnut',
            data: {
                labels: [
                    `${LANG_FILE.refferal.stats.totalVisitsChart.newUsers}: ${data.newUsers}`,
                    `${LANG_FILE.refferal.stats.totalVisitsChart.returnUsers}: ${data.returnUsers}`
                ],
                datasets: [{
                    data: totalVisitData,
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.4)',
                        'rgba(255, 159, 64, 0.4)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 159, 64, 0.6)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    text: `${LANG_FILE.refferal.stats.totalVisitsChart.title}: ${data.totalUsers}`,
                    ...titleOptions
                },
                legend: doughnutLegend,
                tooltips: {
                    enabled: false
                }
            }
        });

        new Chart($subscribesChart, {
            type: 'doughnut',
            data: {
                labels: [
                    `${LANG_FILE.refferal.stats.subscribesChart.doneSubscribes}: ${[commingRate, PERCENT].join('')}`,
                    `${LANG_FILE.refferal.stats.subscribesChart.lossRate}: ${[lossRate, PERCENT].join('')}`
                ],
                datasets: [{
                    data: totalSubscribesData,
                    backgroundColor: [
                        'rgba(75, 192, 192, 0.4)',
                        'rgba(255, 99, 132, 0.4)'
                    ],
                    borderColor: [
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(255, 99, 132, 0.6)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    text: `${LANG_FILE.refferal.stats.subscribesChart.title}: ${data.channelsTotalStats.uniqUsersSubscribes}`,
                    ...titleOptions
                },
                legend: doughnutLegend,
                tooltips: {
                    enabled: false
                }
            }
        });

        new Chart($channelsSubscChart, {
            type: 'bar',
            data: {
                labels: preparedInfoByChannels.namesArr,
                datasets: [
                    chanSubscData,
                    chanReturnSubscData
                ]
            },
            options: {
                legend: {
                    display: false
                },
                title: {
                    text: [
                        LANG_FILE.refferal.stats.channelsSubscChart.title.firstPart,
                        data.channelsTotalStats.subscribes,
                        `|`,
                        LANG_FILE.refferal.stats.channelsSubscChart.title.secondPart,
                        data.channelsTotalStats.returnSubscribes
                    ].join(' '),
                    ...titleOptions
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0
                        }
                    }]
                }
            }
        });

        drawChannelsTable(data);
    }
}

function drawChannelsTable(data) {
    let $table = $('#table-channels-subscribes');
    let $tBody = $('tbody', $table);


    let $channelsTrs = data.channelsStats.map(function (channel) {
        let returnSubscribes = channel.tracksStats.returnSubscribes != null ? channel.tracksStats.returnSubscribes : 0;
        let subscribes = channel.tracksStats.subscribes != null ? channel.tracksStats.subscribes : 0;
        let totalSubscRequests = Number(returnSubscribes) + Number(subscribes);

        return [`<tr>`,
            `<td>${channel.id}</td>`,
            `<td class="text-left">${channel.name = ! null ? channel.name : '-'}</td>`,
            `<td>${subscribes}</td>`,
            `<td>${returnSubscribes}</td>`,
            `<td>${totalSubscRequests}</td>`,
            `</tr>`].join('')
    }).join('');

    $tBody.replaceWith(
        `<tbody>${$channelsTrs}</tbody>`
    );

    let chanTotalStats = data.channelsTotalStats != null ? data.channelsTotalStats : {};
    let subscribes = chanTotalStats.subscribes != null ? chanTotalStats.subscribes : 0;
    let returnSubscribes = chanTotalStats.returnSubscribes != null ? chanTotalStats.returnSubscribes : 0;
    let totalSubscs = Number(chanTotalStats.subscribes) + Number(chanTotalStats.returnSubscribes);

    let totalTr = [
        `<tr>`,
        `<td>#</td>`,
        `<td class="text-left">${LANG_FILE.refferal.stats.channelsTable.allChannels}</td>`,
        `<td>${subscribes}</td>`,
        `<td>${returnSubscribes}</td>`,
        `<td>${totalSubscs}</td>`,
        `</tr>`
    ].join('');

    $('tbody', $table).append(totalTr);
}

function hideStatisticInfo() {
    $(".j-refferal-links .j-referal-list").removeClass("collapse");
    $(".j-reff-link-statistic").addClass("collapse");
    // clearDepoInfo(); очистка блока статистики
}

function getStatsInRealTime() {
    let $statsPeriodBox = $(".j-period-for-stats");
    let $inputFromDate = $("[name='from-date']", $statsPeriodBox);
    let $inputToDate = $("[name='to-date']", $statsPeriodBox);
    let isWatchInRealTime = $(this).prop("checked");

    if (isWatchInRealTime == false) {
        isAutoUpdateStatsGlodal = false;
        $inputFromDate.prop('disabled', false);
        $inputToDate.prop('disabled', false);
    } else {
        isAutoUpdateStatsGlodal = true;
        autoUpdateStats();
        $inputFromDate.prop('disabled', true);
        $inputToDate.prop('disabled', true);
    }
    GetParamsFromFilter();
}//end getStatsInRealTime


function autoUpdateStats() {
    if (isAutoUpdateStatsGlodal === true) {
        GetParamsFromFilter();
        setTimeout(autoUpdateStats, 3000);
    }
}//end autoUpdateStats

/**
 * activity
 */


async function awaitDownloadFilter() {
    await getChannelsList();
    DrawUsersSelect();
    DrawIsNewUserSelect();
    DrawStatusSelect();
}

function refreshTracksList() {
    $('#table-activity').DataTable().ajax.reload(null, false);
}

async function showActivityInfo() {
    let $thisBtn = $(this);
    let $reffLinkTr = $thisBtn.parents("tr").first();
    let reffLinkInfo = $reffLinkTr.data();

    let $boxTable = $(".j-refferal-links .j-referal-list");
    let $boxLinkActivity = $(".j-reff-link-activity");
    let $timeBox = $(".j-period-for-activity");

    $thisBtn.buttonLoader("start");

    defaultTimeToPage(reffLinkInfo.createdAt, $timeBox);
    await awaitDownloadFilter();
    DrawControllerTr($("#reff-link-activity .j-tr"), reffLinkInfo, function () {
        getAndDrawParamsFromFilters();
    });

    $boxTable.addClass("collapse");
    $boxLinkActivity.removeClass("collapse");

    $thisBtn.buttonLoader("stop");
}

function getAndDrawTracksList(_prms) {
    let $tableActivity = $('#table-activity');

    let {
        tokenId,
        toDate = null,
        fromDate = null,
        selectedChannelsArr = null,
        selectedIsNewUser = null,
        selectedErrorsArr = null,
        selectedUsersArr = null,
        selectedStatusesArr = null
    } = _prms;

    let dateRange = toDate != null && fromDate != null ? [fromDate, toDate] : [];

    let data = {
        dateRange: dateRange.toString(),
        tokenId,
        channelId: selectedChannelsArr,
        error: selectedErrorsArr,
        status: selectedStatusesArr,
        userId: selectedUsersArr,
        isNewUser: selectedIsNewUser
    };

    $tableActivity.DataTable().destroy();
    $tableActivity.DataTable({
        ajax: {
            method: 'post',
            url: `refferal/link/activity/${tokenId}`,
            data,
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);

            },
            dataFilter: function (resultData) {
                let data = JSON.parse(resultData).data;
                return JSON.stringify(data);
            }
        },
        processing: false,
        serverSide: true,
        createdRow: function (row, track, numRow) {
            let $newTrackTr = $(row);
            let error = track.error;

            switch (error) {
                case TRACK_ERROR.NULL:
                    $newTrackTr.addClass("tr-success");
                    break;
                case TRACK_ERROR.ERROR_ALREADY_ASSIGNED:
                    $newTrackTr.addClass("tr-warning");
                    break;
                case TRACK_ERROR.ERROR_RECIVE_STOP_SIGNAL:
                    $newTrackTr.addClass("tr-info");
                    break;
                case TRACK_ERROR.ERROR_INVALID_HANDLE:
                case TRACK_ERROR.ERROR_UNEXP_ERR:
                case TRACK_ERROR.ERROR_INVALID_DATA:
                case TRACK_ERROR.ERROR_CANCEL:
                    $newTrackTr.addClass("tr-danger");
                default:
                    $newTrackTr.addClass("tr-danger");
                    break;
            }
        },

        columns: [
            {
                name: "id",
                data: "id",
                orderable: false,
                render: function (id, type, data) {
                    return id;
                }
            },
            {
                name: "createdAt",
                data: "createdAt",
                orderable: false,
                render: function (createdAt, type, data) {
                    let updatedAtD = new Date(data.updatedAt);
                    let createdAtD = new Date(createdAt);
                    let {
                        hr,
                        min,
                        sec
                    } = millisecondsToTime(Math.abs(updatedAtD.getTime() - createdAtD.getTime()));

                    let timeDiff = `${hr}:${min}:${sec}`;

                    return [
                        `<span>${moment(data.updatedAt).format("L LTS")}</span>`,
                        `<div class="small text-muted">${moment(createdAt).format("L LTS")}</div>`,
                        `<div class="small text-muted">${timeDiff}</div>`
                    ].join('');
                }
            },
            {
                name: "user",
                data: "user",
                orderable: false,
                render: function (user, type, data) {
                    let fullName = `${user.firstName} ${user.lastName}`;
                    let username = user.username == "" ? "" : `@${user.username}`;
                    let userLink = user.username == "" ? "" : `${LANG_FILE.telegrammLink}${user.username}`;

                    return [
                        `<span>${fullName}</span>`,
                        `<div class="small text-muted"><a href="${userLink}" target="_blank">${username}</a></div>`,
                        `<div class="small text-muted">${LANG_FILE.id}: ${user.id}</div>`
                    ].join('');
                }
            },
            {
                name: "isNewUser",
                data: "isNewUser",
                orderable: false,
                render: function (isNewUser, type, data) {

                    return `<i class="fe ${isNewUser != true ? 'fe-x' : 'fe-check'}"></i>`;
                }
            },
            {
                name: "status",
                data: "error",
                orderable: false,
                render: function (error, type, data) {
                    let status = error;
                    let errrorCase = `<div class="small text-muted">${error}</div>`;

                    if (error == TRACK_ERROR.NULL) {
                        status = TRACK_STATUS.DONE;
                        errrorCase = '';
                    }

                    let trackStatus = LANG_FILE.refferal.activity.table.trackStatus[status] != null ? LANG_FILE.refferal.activity.table.trackStatus[status].toUpperCase() : status;

                    return [
                        `<span class="badge badge-default ml-1">${trackStatus}</span>`,
                        errrorCase
                    ].join('');
                }
            },
            {
                name: "channel",
                data: "channel",
                orderable: false,
                render: function (channel, type, data) {
                    let price = "";
                    let channelInfo = `
                    <span>-</span>
                `;

                    if (channel != null) {
                        price = `${channel.price} ${channel.currency}`;
                        channelInfo = [
                            `<span>${channel.name != null ? channel.name : '-'}</span>`,
                            `<div class="small text-muted">${price}</div>`
                        ].join('');
                    }

                    return channelInfo;
                }
            }
        ],
        paging: true,
        info: true, //Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        retrieve: true,
        language: LANG_FILE.dataTablesLanguage,
        order: [], //сортировка
        pageLength: 25
        // drawCallback: function () {}
    });
}

function hideActivityInfo() {
    $(".j-refferal-links .j-referal-list").removeClass("collapse");
    $(".j-reff-link-activity").addClass("collapse");
}

function getAndDrawParamsFromFilters() {
    let tokenId = $("#reff-link-activity .j-tr").data("id");

    let $fiterSelectedChannels = $(".j-tracks-channel");
    let $fiterSelectedStatuses = $(".j-tracks-status");
    let $filterSectedUsers = $(".j-tracks-user");
    let $filterSelectedIsNewUser = $(".j-tracks-isNewUser");
    let $filterPeriodForActivity = $(".j-period-for-activity");
    let $inputFromDate = $("[name='from-date']", $filterPeriodForActivity);
    let $inputToDate = $("[name='to-date']", $filterPeriodForActivity);

    let inputFromDateValidateStatus = ValidateInput($inputFromDate);
    let inputToDateValidateStatus = ValidateInput($inputToDate);

    let filtersParams = {
        tokenId: parseInt(tokenId),
        fromDate: null,
        toDate: null,
        selectedUsersArr: null,
        selectedErrorsArr: null,
        selectedChannelsArr: null,
        selectedIsNewUser: null,
        selectedStatusesArr: null
    }

    if (inputFromDateValidateStatus === "error" || inputToDateValidateStatus === "error") {
        ShowMessage("error", "EMPTY_INPUT", "error");
    }
    else {
        filtersParams.toDate = $inputToDate.data("datetimepicker").viewDate().valueOf();
        filtersParams.fromDate = $inputFromDate.data("datetimepicker").viewDate().valueOf();

        filtersParams.toDate = moment.utc(filtersParams.toDate).valueOf();
        filtersParams.fromDate = moment.utc(filtersParams.fromDate).valueOf();
    }

    let selectedChannelsArr = $fiterSelectedChannels.val();
    let selectedErrorsArr = $fiterSelectedStatuses.val();
    let selectedUsersArr = $filterSectedUsers.val();
    let selectedIsNewUser = $filterSelectedIsNewUser.val();

    filtersParams.selectedErrorsArr = selectedErrorsArr == "all" || selectedErrorsArr == "" ? null : selectedErrorsArr;

    if (filtersParams.selectedErrorsArr == TRACK_STATUS.DONE) {
        filtersParams.selectedErrorsArr = null;
        filtersParams.selectedStatusesArr = TRACK_STATUS.DONE;
    };

    filtersParams.selectedUsersArr = selectedUsersArr == "all" || selectedUsersArr == "" ? null : selectedUsersArr;
    filtersParams.selectedChannelsArr = selectedChannelsArr == "all" || selectedChannelsArr == "" ? null : selectedChannelsArr;
    filtersParams.selectedIsNewUser = selectedIsNewUser == "all" || selectedIsNewUser == "" ? null : selectedIsNewUser;

    getAndDrawTracksList(filtersParams);
}

function DrawUsersSelect(_allData) {

    let $selectUsers = $('.j-tracks-user');

    if ($selectUsers[0].selectize != null) {
        $selectUsers[0].selectize.destroy();
    };

    $selectUsers = $selectUsers.selectize({
        plugins: ['remove_button'],
        persist: false,
        maxItems: null,
        valueField: 'value',
        labelField: 'name',
        searchField: ['name'],
        options: [{
            name: LANG_FILE.all,
            value: "all"
        }],
        render: {
            item: function (item, escape) {
                let name = item.name ? item.name : "-";
                name = [...name].slice(0, 50).join('');

                let $div = $('<div/>')
                    .addClass("tag")
                    .text(escape(name))
                    .prepend(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            },
            option: function (item, escape) {
                let name = item.name ? item.name : "-";
                name = [...name].slice(0, 50).join('');

                let $div = $('<div/>')
                    .text(escape(name))
                    .append(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            }
        },
        load: function (query, callback) {
            if (query.length < 3) return callback();
            $.ajax({
                url: `/users`,
                type: 'post',
                dataType: 'json',
                data: {
                    length: 10,
                    search: {
                        value: query
                    }
                },
                error: function () {
                    callback();
                },
                success: function (res) {
                    let users = res.data.data.map(user => {
                        return {
                            name: user.name,
                            value: user.id
                        }
                    });
                    callback(users);
                }
            });
        }
    })[0].selectize;

    $selectUsers.enable();
    $selectUsers.setValue("all");
    $selectUsers.on("change", onChancgeSelect);
}//end DrawUsersSelect

async function getChannelsList() {
    let paramsAjax = {
        url: "channels/all/list",
        method: "GET",
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE",
    }


    let { result, json } = await QueryAjaxPromise(paramsAjax);
    if (result) {
        DrawChannelsSelect(json.data);
    }
}

function DrawChannelsSelect(_allData) {
    if (_allData != null) {
        let $selectChannels = $('.j-tracks-channel');
        let channels = _allData.map(channel => {
            return {
                name: channel.name,
                value: channel.id
            }
        });

        channels.unshift({
            name: LANG_FILE.all,
            value: "all"
        });

        if ($selectChannels[0].selectize != null) {
            $selectChannels[0].selectize.destroy();
        };

        $selectChannels = $selectChannels.selectize({
            plugins: ['remove_button'],
            persist: false,
            maxItems: null,
            valueField: 'value',
            labelField: 'name',
            searchField: ['name'],
            options: channels,
            render: {
                item: function (item, escape) {
                    let name = item.name ? item.name : "-";
                    name = [...name].slice(0, 50).join('');

                    let $div = $('<div/>')
                        .addClass("tag")
                        .text(escape(name))
                        .prepend(
                            $('<span/>')
                        );
                    return $div.prop('outerHTML');
                },
                option: function (item, escape) {
                    let name = item.name ? item.name : "-";
                    name = [...name].slice(0, 50).join('');

                    let $div = $('<div/>')
                        .text(escape(name))
                        .append(
                            $('<span/>')
                        );
                    return $div.prop('outerHTML');
                }
            }
        })[0].selectize;

        $selectChannels.enable();
        $selectChannels.setValue("all");
        $selectChannels.on("change", onChancgeSelect);
    }
}//end DrawChannelsSelect

function DrawIsNewUserSelect() {
    let $selectOptions = $('.j-tracks-isNewUser');
    let options = [
        {
            name: LANG_FILE.refferal.activity.table.isNewUser.true,
            value: true
        }, {
            name: LANG_FILE.refferal.activity.table.isNewUser.false,
            value: false
        }, {
            name: LANG_FILE.all,
            value: "all"
        }
    ];

    if ($selectOptions[0].selectize != null) {
        $selectOptions[0].selectize.destroy();
    };

    $selectOptions = $selectOptions.selectize({
        plugins: ['remove_button'],
        persist: false,
        maxItems: null,
        valueField: 'value',
        labelField: 'name',
        searchField: ['name'],
        options,
        render: {
            item: function (item, escape) {
                let name = item.name ? item.name : "-";
                name = [...name].slice(0, 50).join('');

                let $div = $('<div/>')
                    .addClass("tag")
                    .text(escape(name))
                    .prepend(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            },
            option: function (item, escape) {
                let name = item.name ? item.name : "-";
                name = [...name].slice(0, 50).join('');

                let $div = $('<div/>')
                    .text(escape(name))
                    .append(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            }
        }
    })[0].selectize;

    $selectOptions.enable();
    $selectOptions.setValue("all");
    $selectOptions.on("change", onChancgeSelect);
}//end DrawIsNewUserSelect

function DrawStatusSelect() {
    let $selectStatuses = $('.j-tracks-status');
    let options = Object.values(TRACK_ERROR).map(error => {
        let errorCase = '';
        let nameCase = LANG_FILE.refferal.activity.table.trackStatus.DONE;

        if (error != null) {
            errorCase = `| ${error}`;
            nameCase = LANG_FILE.refferal.activity.table.trackStatus[error];
        }

        return {
            name: `${nameCase}${errorCase}`,
            value: error != null ? error : TRACK_STATUS.DONE
        }
    });

    options.unshift({
        name: LANG_FILE.all,
        value: "all"
    });

    if ($selectStatuses[0].selectize != null) {
        $selectStatuses[0].selectize.destroy();
    };

    $selectStatuses = $selectStatuses.selectize({
        plugins: ['remove_button'],
        persist: false,
        maxItems: null,
        valueField: 'value',
        labelField: 'name',
        searchField: ['name'],
        options,
        render: {
            item: function (item, escape) {
                let name = item.name ? item.name : "-";
                name = [...name].slice(0, 50).join('');

                let $div = $('<div/>')
                    .addClass("tag")
                    .text(escape(name))
                    .prepend(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            },
            option: function (item, escape) {
                let name = item.name ? item.name : "-";
                name = [...name].slice(0, 50).join('');

                let $div = $('<div/>')
                    .text(escape(name))
                    .append(
                        $('<span/>')
                    );
                return $div.prop('outerHTML');
            }
        }
    })[0].selectize;

    $selectStatuses.enable();
    $selectStatuses.setValue("all");
    $selectStatuses.on("change", onChancgeSelect);
}//end DrawStatusSelect

/** 
 * helpers
*/

function onChancgeSelect() {
    let values = this.getValue().split(',');
    let nowSelected = values[values.length - 1];

    if (this.getValue() != "") {
        if (values.length > 1 && values.indexOf("all") != -1) {
            if (nowSelected != "all") {
                values.splice(values.indexOf("all"), 1);
                this.clear(true);
                this.setValue(values, true);
            } else {
                this.clear(true);
                this.setValue("all", true);
            }
        }
    } else {
        this.setValue("all", true);
    }
}

function InitDatetimepicker() {
    let maxDate = new Date();

    let fromDate = {
        locale: "ru",
        maxDate: "moment",
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'top'
        }
    };

    let toDate = {
        locale: "ru",
        maxDate,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'top'
        }
    };

    $('.j-period-for-stats [name="from-date"]').datetimepicker(fromDate);
    $('.j-period-for-activity [name="from-date"]').datetimepicker(fromDate);

    $('.j-period-for-stats [name="to-date"]').datetimepicker(toDate);
    $('.j-period-for-activity [name="to-date"]').datetimepicker(toDate);

    $('.j-period-for-stats [name="from-date"]').on("change.datetimepicker", function (e) {
        $('.j-period-for-stats [name="to-date"]').datetimepicker('minDate', e.date);
    });

    $('.j-period-for-activity [name="from-date"]').on("change.datetimepicker", function (e) {
        $('.j-period-for-activity [name="to-date"]').datetimepicker('minDate', e.date);
    });


}//end InitDatetimepicker

function defaultTimeToPage(_linkCreatedAt, _$timeBox) {
    let {
        toDate,
        fromDate
    } = createToAndFromDate(_linkCreatedAt);

    $("[name='from-date']", _$timeBox).data("datetimepicker").date(fromDate);
    $("[name='to-date']", _$timeBox).data("datetimepicker").date(toDate);
}//end defaultTimeToPage

/**
 * 
 * @param {number} _rawFromDate link data createdAt
 */

function createToAndFromDate(_rawFromDate) {
    date = new Date();
    date.setMinutes(date.getMinutes() - 5);
    day = date.getDate();
    month = date.getMonth();
    year = date.getFullYear();
    hours = date.getHours();
    minutes = date.getMinutes();
    milliseconds = date.getMilliseconds();
    seconds = date.getSeconds();

    return {
        toDate: new Date(year, month, day, hours, minutes, seconds, milliseconds),
        fromDate: new Date(_rawFromDate)
    }
}//end createToAndFromDate

function randomString(_length) {
    let rnd = '';
    while (rnd.length < _length)
        rnd += Math.random().toString(36).substring(2);
    return rnd.substring(0, _length);
};

/**
 * 
 * @param {html} $reffLinkTr jQuery-element, table tr for complete
 * @param {object} reffLink info { 
 * button: "",
 * channel: "",
 * channelCurrency: "",
 * channelName: "",
 * channelPrice: 0,
 * createdAt: "",
 * description: "",
 * id: 0,
 * isActive: boolean,
 * isDelete: boolean,
 * title: "",
 * token: "",
 * type: ""
 * }
 * @param {function getParamsFromFilter () {} } _cb function to get params from page filter and draw info
 */

function DrawControllerTr($reffLinkTr, reffLink, _cb) {
    $reffLinkTr.removeClass("collapse").show();

    $reffLinkTr.data("id", reffLink.id);
    $(".j-id", $reffLinkTr).text(reffLink.id);

    let date = moment(reffLink.createdAt).format("L LTS");
    let linkMainInfo = [
        '<span class="j-title">',
        `<div class="small text-muted">${date}</div>`,
        `<span>${reffLink.title != null ? reffLink.title : '-'}</span>`,
        '</span>'
    ].join('');

    $(".j-title", $reffLinkTr).replaceWith(linkMainInfo);
    $(".j-link", $reffLinkTr).text(reffLink.token);
    $(".j-type", $reffLinkTr).text(reffLink.type);

    $reffLinkTr.removeClass("tr-success tr-danger tr-offline");

    switch (reffLink.isActive) {
        case LINKS_STATES_TYPES.ACTIVE:
            $reffLinkTr.addClass("tr-success");
            break;
        case LINKS_STATES_TYPES.DISACTIVE:
            $reffLinkTr.addClass("tr-danger");
            break;
    }

    if (reffLink.isDelete != false) {
        $(".j-state", $reffLinkTr).text(LANG_FILE.refferal.deleted);
        $reffLinkTr.addClass("tr-offline");

    } else {
        let checked = reffLink.isActive !== false ? "checked" : "";
        let state = [
            '<span class="j-state">',
            '<label class="custom-switch">',
            '<input type="checkbox" name="active"',
            `class="custom-switch-input" ${checked} disabled>`,
            '<span class="custom-switch-indicator"></span>',
            '</label>',
            '</span>'
        ].join('');

        $(".j-state", $reffLinkTr).replaceWith(state);
    }

    let mainInfo = `${reffLink.channelName || "-"}`;
    let price = `${reffLink.channelPrice || ""} ${reffLink.channelPrice != 0 ? reffLink.channelCurrency : ""}`;

    $(".j-channel", $reffLinkTr).text(mainInfo);
    $(".j-price", $reffLinkTr).text(price);

    if (typeof _cb === 'function') {
        _cb();
    };
}//end DrawControllerTr

/**
 * 
 * @param {Array} channelsData each channel {color: "", id: 0, name: "", tracksStats: { returnSubscribes: 0, subscribes: 0 }
 */
function preparationInfoByChannels(channelsData) {
    let channelsBatch = {
        namesArr: [],
        subscribesArr: [],
        returnSubscribesArr: [],
        subscColorsArr: [],
        retSubscColorsArr: [],
        borderColorsArr: []
    }

    for (let channel of channelsData) {

        channelsBatch.namesArr.push(channel.name);
        channelsBatch.subscribesArr.push(channel.tracksStats.subscribes);
        channelsBatch.returnSubscribesArr.push(channel.tracksStats.returnSubscribes);
        channelsBatch.retSubscColorsArr.push(channel.color);

        let channelBorderColor = createColorTint(channel.color, TINT_VAL_SIX);
        let subscColor = createColorTint(channel.color, TINT_VAL_FIVE);

        channelsBatch.borderColorsArr.push(channelBorderColor);
        channelsBatch.subscColorsArr.push(subscColor);
    }

    return channelsBatch;
}//end preparationInfoByChannels

function createColorTint(color, tintVal) {
    color = color.slice(5);
    color = color.slice(0, -1);

    let colorToArr = color.split(',');
    colorToArr[colorToArr.length - 1] = tintVal.toString();

    let colorTint = [
        'rgba',
        '(',
        ...colorToArr.join(),
        ')'
    ].join('');

    return colorTint;
}//end createColorTint