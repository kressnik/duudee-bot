// Returns an array of maxLength (or less) page numbers
// where a 0 in the returned array denotes a gap in the series.
// Parameters:
//   totalPages:     total number of pages
//   page:           current page
//   maxLength:      maximum size of returned array

var paginationPageParam = {
    page: 1,
    start: 1,
    length: 10,
    currentPage: 1
}

// Use event delegation, as these items are recreated later
$(document).on("click", ".pagination li.current-page:not(.active)", function () {

    $(this).find("a").buttonLoader("start");

    var page = parseInt($(this).text());

    $(document).trigger('page-changing', {
        page: page,
        start: ((page - 1) * paginationPageParam.length),
        length: paginationPageParam.length
    });
});

$(document).on("click", "#next-page:not(.disabled)", function () {
    $(this).find("a").buttonLoader("start");

    var page = (parseInt(paginationPageParam.currentPage) + 1);

    $(document).trigger('page-changing', {
        page: page,
        start: (page - 1) * paginationPageParam.length,
        length: paginationPageParam.length
    });
});

$(document).on("click", "#previous-page:not(.disabled)", function () {

    $(this).find("a").buttonLoader("start");

    var page = (parseInt(paginationPageParam.currentPage) - 1);

    $(document).trigger('page-changing', {
        page: page,
        start: (page - 1) * paginationPageParam.length,
        length: paginationPageParam.length
    });
});

function drawPagination(_parameters) {
    // Number of items and limits the number of items per page
    var page = (_parameters.page == null ? 1 : _parameters.page);
    var numberOfItems = (_parameters.recordsTotal == null ? 0 : _parameters.recordsTotal);//Количество записей в базе
    var limitPerPage = (_parameters.recordsTotal == null ? 10 : _parameters.length);//Количество записей на станице

    paginationPageParam.page = page;
    paginationPageParam.length = limitPerPage;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);//Количество страниц
    // Number of buttons at the top, not counting prev/next,
    // but including the dotted buttons.
    // Must be at least 5:
    var paginationSize = 7;
    var currentPage;
    function showPage(whichPage) {
        if (whichPage < 1 || whichPage > totalPages) return false;
        currentPage = whichPage;
        paginationPageParam.currentPage = currentPage;
        // $("#jar .content")
        //     .hide()
        //     .slice((currentPage - 1) * limitPerPage, currentPage * limitPerPage)
        //     .show();
        // Replace the navigation items (not prev/next):

        $(".pagination li").slice(1, -1).remove();
        getPageList(totalPages, currentPage, paginationSize).forEach(item => {

            $("#previous-page a").buttonLoader("stop");
            $("#next-page a").buttonLoader("stop");
            if (currentPage <= 1) {
                $("#previous-page").addClass("disabled");
            } else {
                $("#previous-page").removeClass("disabled");
            }

            if (currentPage >= totalPages) {
                $("#next-page").addClass("disabled");
            } else {
                $("#next-page").removeClass("disabled");
            }

            $("<li>")
                .addClass(
                    "page-item " +
                    (item ? "current-page " : "") +
                    (item === currentPage ? "active " : "")
                )
                .append(
                    $("<a>")
                        .addClass("page-link")
                        .attr({
                            href: "javascript:void(0)"
                        })
                        .text(item || "...")
                )
                .insertBefore("#next-page");
        });
        return true;
    }

    if (numberOfItems > 0) {

        let end = paginationPageParam.length * paginationPageParam.page;
        let total = numberOfItems;
        let recordsText = LANG_FILE.dataTablesLanguage.sInfo;

        end = (total < end ? total : end);

        recordsText = recordsText.replace("_START_", paginationPageParam.start);
        recordsText = recordsText.replace("_END_", end);
        recordsText = recordsText.replace("_TOTAL_", total);
        $(".number-of-records").text(recordsText);

        // Include the prev/next buttons:
        $(".pagination").append(
            $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
                $("<a>")
                    .addClass("page-link")
                    .attr({
                        href: "javascript:void(0)"
                    })
                    .text(LANG_FILE.dataTablesLanguage.oPaginate.sPrevious)
            ),
            $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
                $("<a>")
                    .addClass("page-link")
                    .attr({
                        href: "javascript:void(0)"
                    })
                    .text(LANG_FILE.dataTablesLanguage.oPaginate.sNext)
            )
        )
        $(".pagination-box").show();

        // Show the page links
        //$("#jar").show();
        showPage(page);
    } else {
        $(".pagination-box").hide();
    }

};

function getPageList(totalPages, page, maxLength) {
    if (maxLength < 5) throw "maxLength must be at least 5";

    function range(start, end) {
        return Array.from(Array(end - start + 1), (_, i) => i + start);
    }

    var sideWidth = maxLength < 9 ? 1 : 2;
    var leftWidth = (maxLength - sideWidth * 2 - 3) >> 1;
    var rightWidth = (maxLength - sideWidth * 2 - 2) >> 1;
    if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength - sideWidth - 1)
            .concat([0])
            .concat(range(totalPages - sideWidth + 1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
            .concat([0])
            .concat(
                range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages)
            );
    }
    // Breaks on both sides
    return range(1, sideWidth)
        .concat([0])
        .concat(range(page - leftWidth, page + rightWidth))
        .concat([0])
        .concat(range(totalPages - sideWidth + 1, totalPages));
}
