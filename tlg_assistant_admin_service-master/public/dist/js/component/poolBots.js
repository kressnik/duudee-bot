
var botsList = [];//Список контроллеров

//var //interviewControllersActive = false;
//var //interviewHistoryEventsActive = false;

document.addEventListener("lang-ready", function (e) {
    $('[data-inputmask]').inputmask();
    moment.locale(LANG_SYSTEM);
    // (function () {

    //     // setInterval(function () {
    //     //     if (//interviewControllersActive === true) {
    //     //         RefreshController();
    //     //     }
    //     // }, 3000);

    //     // setInterval(function () {
    //     //     if (//interviewHistoryEventsActive === true) {
    //     //         //interviewControllersActive = false;
    //     //         RefreshControllerEvents();
    //     //     }
    //     // }, 3000);
    // })();
});

$(document).on("click", ".j-controllers-list .j-refresh", RefreshController);//Пере получения информации о контроллерах
$(document).on("click", ".j-controllers-list .j-btn-eye", showBotInfo);//Просмотр информации о контроллере
$(document).on("click", ".j-controllers-list .j-btn-key", showBotCreds);//Просмотр крeдов по боту
$(document).on("click", ".j-controllers-list .j-btn-open-door button", OpenTheDoor);//Кнопка открытия дверей
$(document).on("click", ".j-controllers-list .j-events-history", OpenHistoryControllers);//Получения истории всех событий контроллера
$(document).on("click", ".j-controllers-list .j-is-sync", StartSync);//Выполнить синхронизацию 
$(document).on("change", ".j-controllers-list .j-change-state", editBotState);//Изменения статуса автоматической синхронизации
$(document).on("click", ".j-controllers-list .j-btn-replace", OpenModalReplaceController)//Полная замена устройства с одного на другое

$(document).on("click", "#replace-controllers-modal .j-btn-replace-select", ReplaceController)//Полная замена устройства с одного на другое

$(document).on("click", ".j-controller-history .j-back", HideHistoryController);//Вернуться к контролерам с истории контролеров
$(document).on("click", ".j-controller-history .j-refresh", RefreshControllerEvents);//Повторный запрос на получения истории событий контроллера
$(document).on("click", ".j-controller-history .j-clear-history", ClearingHistoryController);//Очистка истории событий контроллера

$(document).on("click", ".j-bot-info .j-hide-info-controller", HideBotInfo);//Закрытия блока информации о боте
$(document).on("click", ".j-bot-creds .j-hide-bot-creds", HideBotCreds);//Закрытия блока информации о кредсах

$(document).on("click", ".j-bot-info .j-edit", EditBotProps);//Изменить информацию о контроллере
$(document).on("click", ".j-bot-creds .j-edit-creds", EditBotCreds);//Изменить информацию о контроллере

$(document).on("click", "#add-bot .j-add", createNewBot);
// $(document).on('hide.bs.modal', "#add-bot", cleaningModalWindowAddBot);

function HideBotInfo() {
    $(".j-controllers-list").removeClass("col-md-8").addClass("col-md-12");//Изменения размера блока списка контроллеров
    $(".j-bot-info").addClass("collapse");//Закрытия блока информации о боте
    $(".j-bot-creds").addClass("collapse");//Закрытия блока информации о creds
}

function HideBotCreds() {
    $(".j-controllers-list").removeClass("col-md-8").addClass("col-md-12");//Изменения размера блока списка контроллеров
    $(".j-bot-info").addClass("collapse");//Закрытия блока информации о контролёре
    $(".j-bot-creds").addClass("collapse");//Закрытия блока информации о creds
}

function HideHistoryController() {
    //interviewControllersActive = true;
    //interviewHistoryEventsActive = false;

    $(".j-controllers-list").removeClass("collapse");//Открытия блока контроллеров
    $(".j-controller-history").addClass("collapse");//Закрытия блока истории событий
}

function ReplaceController() {
    //interviewControllersActive = false;

    let $thisBtn = $(this);//Кнопка на которую был сделан клик
    $thisBtn.buttonLoader("start");

    var $controllersModal = $("#replace-controllers-modal");
    let $controllerTr = $thisBtn.parents("tr").first();
    let selectedControllerId = $controllerTr.data("id")//Номер порядка конвертера которы находиться в таблице
    let newControllerInfo = $controllersModal.data("info-controller-replace");

    var paramsAjax = {
        url: "controller/" + selectedControllerId + "/replace",
        dataType: "json",
        method: "PUT",
        data: newControllerInfo,
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (_result, _json) {
        if (_result) {
            $(".j-controllers-list .j-refresh").click();
            $controllersModal.modal('hide');
            ShowMessage("success", _json.messageCode, "ok");
        }
        $thisBtn.buttonLoader("stop");
        //interviewControllersActive = true;
    });
}

function OpenModalReplaceController() {

    //interviewControllersActive = false;

    var $controllersModal = $("#replace-controllers-modal");
    $controllersModal.modal('show');

    let $thisBtn = $(this);//Кнопка на которую был сделан клик
    $thisBtn.buttonLoader("start");

    let $converterTr = $thisBtn.parents("tr").first();
    let idBotInList = $converterTr.data("num")//Номер порядка конвертера которы находиться в таблице
    let controllerInfo = botsList[idBotInList];//Информация о контроллере

    var paramsAjax = {
        url: "converter/" + controllerInfo.converter.id + "/controllers/inSystem",
        dataType: "json",
        method: "GET",
        timeout: 25000,
        defaultMessage: "DEFAULT_MESSAGE"
    }


    QueryAjax(paramsAjax, function (_result, _json) {

        if (_result) {

            var $controllersModal = $("#replace-controllers-modal");
            var $converterTr = $(".j-controller-tr", $controllersModal)
                .clone()
                .removeClass("j-controller-tr collapse")
                .addClass("controller-tr");//Получаем ранее подготовлен дизайн tr

            $controllersModal.data("info-controller-replace", controllerInfo);
            $(".controller-tr", $controllersModal).remove();
            $(".modal-title .j-type", $controllersModal).text(controllerInfo.type);
            $(".modal-title .j-serial", $controllersModal).text(controllerInfo.serial);

            _json.data.forEach(function (controller, num) {

                var id = controller.id;
                var name = (controller.name == null) ? "--" : controller.name; //Проверка пришло ли nema
                var desc = (controller.desc == null) ? false : controller.desc;//Проверка пришло ли desc
                var $newConverterTr = $converterTr.clone();

                $newConverterTr.data("id", id);//Добавления параметров для дальнейшей работы с конвертером
                $(".j-id", $newConverterTr).text(id);
                $(".j-name", $newConverterTr).text(name);
                $(".j-id", $newConverterTr).text(controller.id);
                $(".j-type", $newConverterTr).text(controller.type);
                $(".j-serial", $newConverterTr).text(controller.serial);

                if (desc) {
                    $(".j-desc", $newConverterTr)
                        .data("content", desc)
                        .removeClass("collapse");
                    $(".j-desc", $newConverterTr).popover();//Активируем вывод описания
                }
                else {
                    $(".j-desc", $newConverterTr).addClass("collapse");
                }

                $("table tbody", $controllersModal).append($newConverterTr);
            });

            $controllersModal.modal('show');
        }

        $thisBtn.buttonLoader("stop");
        //interviewControllersActive = true;
    });
}

function GetBots(poolId = null, _cb) {

    var url = 'bots';
    var $tableController = $('#table-bots-list');

    var $modalAddBot = $("#add-bot");

    let $addNewBot = $("#j-add-bot-class");

    if (poolId != null) {
        url = "pool/" + poolId + "/bots";
        $(".j-add-new-bot", $addNewBot).show();
    } else {
        $(".j-add-new-bot", $addNewBot).hide();
    }

    $tableController.DataTable().destroy();
    $tableController.DataTable({
        ajax: {
            url: url,
            error: function (e, r, t) {
                CheckResponse(e.responseJSON, e.responseJSON);
                _cb();
            }
        },
        processing: false,
        serverSide: true,
        createdRow: function (row, bot, numRow) {
            botsList[numRow] = bot;

            if (bot.type == 0) {
                bot.type = 'Arbitrage';
            } else {
                bot.type = 'Dont know';
            }

            let $newBotTr = $(row);
            let id = bot.id;
            let idInPool = bot.idInPool;
            let botState = bot.state;
            let status = bot.status;
            let serial = bot.serial;
            let name = (typeof bot.name === "undefined" ? "-" : bot.name);//Проверка пришло ли nema
            let desc = (typeof bot.desc === "undefined" ? false : bot.desc);//Проверка пришло ли desc

            //debugger
            let sync = bot.state === "ACTIVE" ? true : false;

            // let hasBlockMode = bot.subFlags.hasBlockMode;
            var dateString = "";
            let $name = $(".j-name", $newBotTr);//Добавления поля в переменную
            let $btnBoxDoor = $(".j-btn-open-door", $newBotTr);//Добавления поля в переменную
            let $btnBoxOptions = $(".j-btn-options", $newBotTr);//Добавления поля в переменную
            let $checkboxSync = $(".j-change-state", $newBotTr);

            /* Наполнения tr информацией о контроллерах */
            $newBotTr.attr({
                "data-id": idInPool,
                "data-pool": poolId,
                "data-num": numRow
            });//Добавления параметров для дальнейшей работы с контроллерами

            // let state = $thisBtn.prop("checked")

            // $thisBtn.prop("checked", (state ? 0 : 1))

            if (bot.lastUpdate != undefined) {
                dateString = moment.utc(bot.lastUpdate).format("L LTS");
            }

            $name.text(name);
            $checkboxSync.prop("checked", botState);

            $(".j-time", $newBotTr).text(dateString);
            $(".j-state", $newBotTr).text(bot.status);
            $(".j-type", $newBotTr).text(bot.type);
            $(".j-serial", $newBotTr).text(bot.serial);
            $(".j-firmware", $newBotTr).text(bot.firmwareVersion);
            $(".j-license", $newBotTr).text(bot.license);
            $(".j-in-system", $newBotTr).text(bot.cardsQty);
            $(".j-max-qty", $newBotTr).text(bot.cardsMemorySize);
            $(".j-id-on-bus", $newBotTr).text(bot.idOnBus);
            $(".j-converter-name", $newBotTr).text(bot.name); //+ bot.converter.id + " " + bot.converter.name);
            $(".j-id-in-system", $newBotTr).text(bot.id);
            // $(".j-converter-type", $newBotTr).text("Desc here");

            $(".j-converter-desc", $newBotTr).settingsPopover(desc);
            $(".j-desc", $newBotTr).settingsPopover(desc);

            $newBotTr.removeClass("tr-success tr-offline tr-disactive tr-danger");

            $(".j-error-msg", $newBotTr).settingsPopover(bot.errorMsg);

            if (bot.inSystem) { //Смотрим новый или старый это бот
                $(".j-btn-add", $btnBoxOptions).hide();
                $(".j-btn-replace", $btnBoxOptions).hide();
                $("button", $btnBoxOptions).not(".j-btn-replace").not(".j-btn-add").show();
                $("button", $btnBoxOptions).prop("disabled", false);

                $checkboxSync.prop("disabled", false);

                switch (status) {
                    case "RUNNING":
                        $("button", $btnBoxDoor).show().not(".btn-loading").prop("disabled", false);
                        $(".j-del-conv-or-cont", $btnBoxOptions).hide(); // скрытие кнопки удаление если бот запущен
                        // $btnIsSync.prop("disabled", false);
                        $newBotTr.addClass("tr-success");
                        break;
                    case "STOPPED":
                        $newBotTr.addClass("tr-offline");
                        break;
                    case "NOT FOUND":
                        $newBotTr.addClass("tr-disactive");
                        $("button", $btnBoxOptions).not(".j-btn-replace").not(".j-del-conv-or-cont ").hide();//Удалить все кнопки кроме кнопки добавления
                        // $("button", $btnBoxDoor).prop("disabled", true);
                        break;
                    case "FINISHED":
                        $newBotTr.addClass("tr-info");
                        $("button", $btnBoxDoor).prop("disabled", true);
                        break;
                    case "ERROR":
                        $newBotTr.addClass("tr-danger");
                        break;
                }
            } else {//Добавляем кнопку добавления
                $("button", $btnBoxOptions).not(".j-btn-replace").not(".j-btn-add").hide();//Удалить все кнопки кроме кнопки добавления
                $("button", $btnBoxDoor).hide();
                $checkboxSync.parents(".j-active").hide();
            }


        },
        columns: [
            {
                name: "id",
                render: function (data, id, bot) {
                    return `
                    <div class="small text-muted">
                    <span class="j-id-in-system"></span>
                    </div>
                        `;
                }
            },
            {
                "data": "state",
                name: "state",
                render: function (data, type, bot) {
                    return `
                        <span class="j-state"></span>
                        <span class="form-help collapse j-error-msg" tabindex="0"
                            data-toggle="popover" data-trigger="focus"
                            data-content="">?</span>
                        <div class="small text-muted">
                            <span class="j-time"></span>
                        </div>
                    `;
                }
            }, {
                name: "poolName",
                render: function (data, type, bot) {
                    return `
                        <span class="j-converter-name"></span>
                        <span class="form-help collapse j-converter-desc"
                            tabindex="0" data-toggle="popover" data-trigger="focus"
                            data-content="">?</span>
                        <div class="small text-muted">
                            <span class="j-converter-type"></span>
                        </div>
                    `;
                }
            },
            {
                name: "identifier",
                render: function (data, type, row) {
                    return `
                    <div class="j-type">
                        <span class="j-in-system"></span>
                    </div>
                    `;
                }
            }, {
                name: "active",
                render: function (data, type, row) {
                    return `
                    <div class="j-active">
                        <label class="custom-switch">
                            <input type="checkbox" name="custom-switch-checkbox"
                                class="custom-switch-input j-change-state" disabled>
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </div>
                    `;
                }
            }, {
                name: "options",
                orderable: false,
                render: function (data, type, row) {
                    return `
                    <div class="btn-list j-btn-options">
                        <button type="button"
                            class="btn btn-icon btn-primary btn-secondary j-btn-eye"
                            disabled :title="lang.optionsBtn.botInfo">
                            <i class="fa fa-cog"></i>
                        </button>
                        <button type="button"
                            class="btn btn-icon btn-primary btn-secondary j-btn-key"
                            disabled :title="lang.optionsBtn.historyController">
                            <i class="fa fa-key"></i>
                        </button>
                        <button type="button"
                            class="btn btn-icon btn-primary btn-secondary j-events-history"
                            disabled :title="lang.optionsBtn.historyController">
                            <i class="fa fa-sticky-note"></i>
                        </button>
           
                        <button type="button"
                            class="btn btn-icon btn-primary btn-secondary j-del-conv-or-cont"
                            disabled :title="lang.del">
                            <i class="fa fa-trash"></i>
                        </button>
                        <button type="button"
                            class="btn btn-outline-success j-btn-add">
                            <i class="fe fe-plus mr-2"></i>${LANG_FILE.add}
                        </button>
                    </div>
                    `;
                }
            }
        ],
        paging: true,
        info: true,//Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        language: LANG_FILE.dataTablesLanguage,
        //dom: "<'myfilter'f>",
        //pageLength: 2,
        //"searching": false,
        order: [[2, 'asc']],//сортировка
        drawCallback: function () {
            if (typeof _cb === "function") {
                _cb(true);
            }
            //interviewControllersActive = true;
            $modalAddBot.find('.j-add').attr("pool-id", poolId);
        }

    });
}

function OpenTheDoor() {//Кнопка открытия дверей

    //interviewControllersActive = false;

    var $thisBtn = $(this);//Кнопка на которую был сделан клик
    var $tr = $thisBtn.parents("tr").first();//Tr который нужно заполнить информацией
    var idBotInLists = $tr.data("num");//id контроллера
    var idBots = $tr.data("id");//id контроллера
    var direction = $thisBtn.data("direction");//Способ открытия дверей

    $thisBtn.buttonLoader("start");//Запуск процессинга на кнопке

    $.ajax({//Отправка запроса открытия дверей определённым контроллером
        url: CreateUrl("converter/" + botsList[idBotInLists].converter.id + "/controller/" + idBots + "/door"),
        dataType: "JSON",
        method: "POST",
        data: {
            "direction": direction
        },
        cache: false,
        async: true,
        timeout: 10000,
        success: function (json, textStatus, jqXHR) {
            if (CheckResponse(json, jqXHR)) {//Проверка на результат запроса
                ShowMessage('success', 1, "ok");
            }
            $thisBtn.buttonLoader("stop");//Остановка процессинга на кнопке
            //interviewControllersActive = true;
        },
        error: function (request, error) {
            ShowMessageError(request, error, "DEFAULT_MESSAGE");
            $thisBtn.buttonLoader("stop");//Остановка процессинга на кнопке
            //interviewControllersActive = true;
        }
    });
}

function OpenHistoryControllers() {//Просмотр всей истории событий на контроллере

    var $thisBtn = $(this);//Кнопка на которую был сделан клик
    var $boxControllerList = $(".j-controllers-list");
    var $boxControllerHistory = $(".j-controller-history");
    var idBots = $thisBtn.parents("tr").first().data("id");//id контроллера

    $(".j-refresh", $boxControllerHistory).data("controller", idBots);

    $thisBtn.buttonLoader("start");//Запуск процессинга на кнопке

    $('#table-bots-events').DataTable().destroy();
    $('#table-bots-events').DataTable({
        ajax: {
            url: "controller/" + idBots + "/events",
            error: function (resultData) {
                let responseJSON = JSON.parse(resultData.responseText);
                CheckResponse(responseJSON, resultData);
            },
            dataFilter: function (resultData) {
                let data = JSON.parse(resultData).data;
                return JSON.stringify(data);
            }
        },
        processing: true,
        serverSide: true,
        columns: [{
            "data": "time",
            "name": "time",
            render: function (time, type, data) {
                return moment.unix((time / 1000)).format("L LTS.SSS");
            }
        }, {
            "data": "type",
            "name": "type"
        }, {
            "data": "direction",
            "name": "direction"
        }, {
            "data": "code",
            "name": "code"
        }],
        paging: true,
        info: true,//Информация сколько записей в базе и на какой мы находимся
        //stateSave: true,//сохранения последнего выбора сортировки
        language: LANG_FILE.dataTablesLanguage,
        //dom: "<'myfilter'f>",
        pageLength: 100,
        order: [[0, 'desc']],//сортировка
        drawCallback: function (settings) {

            //interviewControllersActive = false;
            //interviewHistoryEventsActive = true;

            HideBotInfo();

            $boxControllerList.addClass("collapse");
            $boxControllerHistory.removeClass("collapse");//Открытия блока истории

            $thisBtn.buttonLoader("stop");//Остановка процессинга на блоке
        }
    });
};

function showBotInfo() {
    //interviewControllersActive = false;

    var $thisBtn = $(this);
    var $boxBotsList = $(".j-controllers-list");
    var $boxBotInfo = $(".j-bot-info");

    var idBotInList = $thisBtn
        .parents("tr")
        .first()
        .attr("data-num");// get bot id in list

    var botInfo = botsList[idBotInList];//Вся информация о контроллере

    $boxBotInfo.find('.j-edit').attr("data-id", idBotInList);

    $thisBtn.buttonLoader("start");

    var paramsAjax = {
        url: `pool/${botInfo.poolId}/bot/${botInfo.idInPool}/params`,
        method: "GET",
        timeout: 15000
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            let botInfo = json.data.params;

            let tradingPairs = json.data.pairList;

            let name = botInfo.name;
            let status = json.data.status;
            let type = json.data.type;

            let idInPool = botInfo.idInPool;

            let volume = (typeof botInfo.volume === undefined ? "-" : botInfo.volume);
            let coef = (typeof botInfo.coef === undefined ? "-" : botInfo.coef);
            let quoteRate = (typeof botInfo.quoteRate === undefined ? "-" : botInfo.quoteRate);
            let tradingPair = (typeof botInfo.tradingPair === undefined ? "-" : botInfo.pair);
            let tradingDirection = (typeof botInfo.tradingDirection === undefined ? "-" : botInfo.buySell);

            let desc = (typeof botInfo.desc === undefined ? "-" : botInfo.desc);//Проверка пришло ли desc

            // switch (botInfo.status) {
            //     case "RUNNING":
            //         statusColor = "badge-success";
            //         break;
            //     case "STOPPED":
            //         statusColor = "badge-warning";
            //         break;
            //     case "NOT FOUND":
            //         statusColor = "badge-secondary";
            //         break;
            //     case "ERROR":
            //         statusColor = "badge-danger";
            //         break;
            //     default:
            //         statusColor = "badge-default";
            //         break;
            // }



            // switch (botInfo.state) {
            //     case "ACTIVE":
            //         stateColor = "badge-success";
            //         break;
            //     case "DISACTIVE":
            //         stateColor = "badge-danger";
            //         break;
            //     default:
            //         stateColor = "badge-default";
            //         break;
            // }

            //Выводим информацию в блок\\
            $(".j-id", $boxBotInfo).text(botInfo.id);

            $(".j-name", $boxBotInfo).text(name);
            $(".j-status", $boxBotInfo).text(status);

            if (status == 'RUNNING') {
                $boxBotInfo.find('.j-edit').addClass("collapse");
            } else {
                $boxBotInfo.find('.j-edit').removeClass("collapse");
            }

            $(".j-type", $boxBotInfo).text(type);
            //debugger
            $(".j-volume", $boxBotInfo).val(volume);
            $(".j-coef", $boxBotInfo).val(coef);
            $(".j-quote-rate", $boxBotInfo).val(quoteRate);

            //fill 

            let sourcePairs = tradingPairs[botInfo.source.exchange];

            let distPairs = tradingPairs[botInfo.dist.exchange];

            $('.j-trading-pair > option').remove();

            for (pair in sourcePairs) {
                if (pair == botInfo.pair) {
                    $(`.j-trading-pair`, $boxBotInfo).append(`<option value="${pair}" selected="selected">${sourcePairs[pair].symbol}-${distPairs[pair].symbol}</option>`);
                } else {
                    $(`.j-trading-pair`, $boxBotInfo).append(`<option value="${pair}">${sourcePairs[pair].symbol}</option>`);
                }
            }

            $('.j-trading-direction > option').remove();

            if (tradingDirection == 'srcBuy') {
                $(`.j-trading-direction`, $boxBotInfo).append(`<option value="${tradingDirection}" selected="selected">${LANG_FILE.buy}</option>`);
                $(`.j-trading-direction`, $boxBotInfo).append(`<option value="srcSell">${LANG_FILE.sell}</option>`);
            } else if (tradingDirection == 'srcSell') {
                $(`.j-trading-direction`, $boxBotInfo).append(`<option value="srcBuy">${LANG_FILE.buy}</option>`);
                $(`.j-trading-direction`, $boxBotInfo).append(`<option value="${tradingDirection}" selected="selected">${LANG_FILE.sell}</option>`);
            } else {
                $(`.j-trading-direction`, $boxBotInfo).append(`<option value="srcBuy">${LANG_FILE.buy}</option>`);
                $(`.j-trading-direction`, $boxBotInfo).append(`<option value="srcSell">${LANG_FILE.sell}</option>`);
            }

            $(".j-desc", $boxBotInfo).val(desc);

            // $(".j-event", $boxBotInfo).text("");

            // if (botInfo.converter.desc) {
            //     $(".j-converter-desc", $boxBotInfo)
            //         .data("content", botInfo.converter.desc)
            //         .removeClass("collapse");
            //     $(".j-converter-desc", $boxBotInfo).popover();//Активируем вывод описания
            // }



            $boxBotsList.removeClass("col-md-12").addClass("col-md-8");//Изменения размера блока списка контроллеров
            $boxBotInfo.removeClass("collapse");//Открытия блоки информации о контролёре

        }

        $thisBtn.buttonLoader("stop");

        //interviewControllersActive = true;
    });
}

function showBotCreds() {

    //interviewControllersActive = false;

    var $thisBtn = $(this);
    var $boxControllersList = $(".j-controllers-list");
    var $boxController = $(".j-bot-creds");
    var idBotInList = $thisBtn
        .parents("tr")
        .first()
        .attr("data-num");//Получаем номер контроллера

    var botInfo = botsList[idBotInList];//Вся информация о контроллере

    let status = botInfo.status;

    $boxController.find('.j-edit-creds').attr("data-id", idBotInList);

    $thisBtn.buttonLoader("start");

    var paramsAjax = {
        url: `pool/${botInfo.poolId}/bot/${botInfo.idInPool}/params`,
        method: "GET",
        timeout: 15000
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {

            let botInfo = {
                source: null,
                dist: null
            }

            botInfo = json.data.params;

            let idInPool = botInfo.idInPool;

            let sourceExchange = botInfo.source.exchange;

            let distExchange = botInfo.dist.exchange;

            if (status == 'RUNNING') {
                $boxController.find('.j-edit-creds').addClass("collapse");
            } else {
                $boxController.find('.j-edit-creds').removeClass("collapse");
            }

            $('.j-source-exchange > option').remove();

            $('.j-dist-exchange > option').remove();

            $(".j-id", $boxController).text(botInfo.id);

            if (sourceExchange != null && distExchange != null) {
                $(`.j-source-exchange`, $boxController).append(`<option value="${sourceExchange}" selected="selected">${sourceExchange.toUpperCase()}</option>`);
                $(`.j-dist-exchange`, $boxController).append(`<option value="${distExchange}" selected="selected">${distExchange.toUpperCase()}</option>`);
            } else {
                $(`.j-source-exchange`, $boxController).append(`<option value="def">def</option>`);
                $(`.j-dist-exchange`, $boxController).append(`<option value="binance">BINANCE</option>`);
            }

            $boxControllersList.removeClass("col-md-12").addClass("col-md-8");//Изменения размера блока списка контроллеров
            $boxController.removeClass("collapse");//Открытия блоки информации о контролёре
        }
        $thisBtn.buttonLoader("stop");
        //interviewControllersActive = true;
    });
}

// function openModalWindowCreatingBot() { //Открытия модального окна для редактирования конвертера
//     let $thisBtn = $(this); //Кнопка на которую был сделан клик
//     let $modal = $("#add-bot"); //Модальное окно которое нужно открыть
//     let $modalTitle = $(".modal-title", $modal);

//     let $repeated = $(".j-repeated", $modal);

//     let idPool = $thisBtn
//         .parents("tr")
//         .first()
//         .data("num"); //Получения номера конвертера который прописан в tr

//     let {
//         id,
//         name,
//         host,
//         port,
//         type,
//         protocol,
//         description,
//     } = ConvertersListJSON[idPool];

//     /* Наполнения imput информацией о конвертере */
//     $modalTitle.text(`${LANG_FILE.converters.edit} #${id}`); //Изменения заголовка модального окна


//     $(".j-pool-name", $modal).val(name || '');
//     $(".j-pool-host", $modal).val(host || '');
//     $(".j-pool-port", $modal).val(port || '-');
//     $(".j-pool-description", $modal).val(description || '');

//     $repeated.prop("checked", false).parents(".form-group").addClass("collapse");

//     $(".j-pool-type [value='" + type + "']", $modal).prop("selected", true);
//     $(".j-pool-protocol [value='" + protocol + "']", $modal).prop("selected", true);

//     $(".j-add", $modal)
//         .text(LANG_FILE.edit)
//         .data("num", idPool); //Изменяем названия кнопки и добавления id изменяемого конвертера из глобального списка
//     /* Наполнения imput информацией о конвертере */

//     $modal.modal('show'); //Открытия модального окна
// };


function createNewBot() {
    let $thisBtn = $(this); //Кнопка на которую был сделан клик
    let $modalAddConverter = $("#add-bot"); //Модальное окно

    let poolId = $thisBtn.attr("pool-id");

    let botInfo = {
        poolId: poolId,
        name: $(".j-name", $modalAddConverter).val(),
        source: $(".j-source-exchange-add", $modalAddConverter).val(),
        dist: $(".j-dist-exchange-add", $modalAddConverter).val()
    };

    sendCreateBot(botInfo, $thisBtn); //Отправка информации о конвертере и также отправка кнопки на которую был совершен клик
};

function sendCreateBot(botInfo, _$thisBtn, _numConverter) {
    // var url = "";

    _$thisBtn.buttonLoader("start");

    let paramsAjax = {
        url: `pool/${botInfo.poolId}/addBot`,
        method: "POST",
        data: botInfo
    }

    QueryAjax(paramsAjax, function (result, json) {
        if (result) {
            ShowMessage("success", json.messageCode, "ok");
            RefreshController();
            // if (_converterInfo.repeated === false) {
            $('#add-bot').modal('hide');
            // } else {
            cleaningModalAddBot();
            // }
        }

        _$thisBtn.buttonLoader("stop");
    });

}


function editBotState() {//Изменения статуса auto-sync/not-auto-sync

    let $thisBtn = $(this);

    let state = $thisBtn.prop("checked") ? 1 : 0;//Получения нового статуса

    // let state = $thisBtn.prop("checked");

    let botId = $thisBtn
        .parents("tr")
        .first()
        .data("id");//Получения id bot

    let poolId = $thisBtn
        .parents("tr")
        .first()
        .data("pool");//Получения id pool

    $thisBtn.prop("disabled", true);

    var paramsAjax = {
        url: `pool/${poolId}/bot/${botId}/state/${state}`,
        dataType: "JSON",
        method: "PUT",
        timeout: 2000
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            ShowMessage("success", json.messageCode, "ok");
            RefreshController();
        } else {
            ShowMessage('error', json.messageCode, 'error');
            // $thisBtn.prop("checked", (state ? 0 : 1));//Возвращаем каретку в обратное положения если пришла ошибка
        }

        $thisBtn.prop("disabled", false);
    });
};

function StartSync() {
    let $thisBtn = $(this);
    let idBot = $thisBtn
        .parents("tr")
        .first()
        .data("id");//Получения id контроллера

    $thisBtn.buttonLoader("start");//Запуск процессинга на кнопке
    $.ajax({
        url: CreateUrl("controller/" + idBot + "/sync"),
        method: "POST",
        cache: false,
        async: true,
        timeout: 2000,
        success: function (json, textStatus, jqXHR) {//Отправка запросна на изменения статуса
            if (CheckResponse(json, jqXHR)) {//Проверка на результат запроса
                ShowMessage("success", json.messageCode, "ok");
                $thisBtn.remove();
            }
            else {
                $thisBtn.buttonLoader("stop");//Остановка процессинга на блоке
            }

        },
        error: function (request, error) {
            ShowMessageError(request, error, "DEFAULT_MESSAGE");
            $thisBtn.buttonLoader("stop");//Остановка процессинга на блоке
        }
    });
}

function EditBotProps() {
    var $controllersTable = $(".j-controllers-list");
    var $boxBotInfo = $(".j-bot-info");//Блок информации контроллера

    let $thisBtn = $(this); //Кнопка на которую было выполнено нажатия

    var idBotInList = $thisBtn.attr("data-id");

    let bot = {
        info: null,
        props: {
            volume: null,
            coef: null,
            quoteRate: null,
            tradingPair: null,
            tradingDirection: null
        }
    }

    let volume = $(".j-volume", $boxBotInfo).val();//Получения новой информации с поля
    let coef = $(".j-coef", $boxBotInfo).val();//Получения новой информации с поля
    let quoteRate = $(".j-quote-rate", $boxBotInfo).val();//Получения новой информации с поля

    let tradingPair = $(".j-trading-pair option:selected", $boxBotInfo).val();
    let tradingDirection = $(".j-trading-direction option:selected", $boxBotInfo).val();

    bot.info = botsList[idBotInList];//Старая информации о контроллере

    bot.props.volume = volume;
    bot.props.coef = coef;
    bot.props.quoteRate = quoteRate;
    bot.props.tradingPair = tradingPair;
    bot.props.tradingDirection = tradingDirection;

    $thisBtn.buttonLoader("start");//Запуск процессинга на кнопке

    if (bot.info.status == 'RUNNING') {
        ShowMessage("error", 'Bot is running now, cant change any props');
        HideBotInfo();
        RefreshController();
        $thisBtn.buttonLoader("stop");//Остановка процессинга на блоке
    } else {
        var paramsAjax = {
            url: `pool/${bot.info.poolId}/bot/${bot.info.idInPool}/params`,
            dataType: "JSON",
            method: "PUT",
            timeout: 5000,
            data: bot,
            defaultMessage: "DEFAULT_MESSAGE"
        }

        QueryAjax(paramsAjax, function (result, json) {

            if (result) {
                ShowMessage("success", json.messageCode, "ok");
                HideBotInfo();
                RefreshController();
            } else {
                ShowMessage("error", json.messageCode, "error");
                HideBotInfo();
                RefreshController();
            }

            $thisBtn.buttonLoader("stop");//Остановка процессинга на блоке
        });
    }
}



function EditBotCreds() {
    var $controllersTable = $(".j-controllers-list");
    var $boxBotInfo = $(".j-bot-creds");//Блок информации контроллера

    let $thisBtn = $(this); //Кнопка на которую было выполнено нажатия

    var idBotInList = $thisBtn.attr("data-id");

    let bot = {
        creds: {
            source: {
                exchange: null,
                creds: {
                    apiKey: null,
                    secret: null
                },
            },
            dist: {
                exchange: null,
                creds: {
                    apiKey: null,
                    secret: null
                }
            }
        }
    }

    bot.info = botsList[idBotInList];//Старая информации о контроллере

    bot.creds.source.exchange = $(".j-source-exchange option:selected", $boxBotInfo).val();
    bot.creds.source.creds.apiKey = $(".j-src-api", $boxBotInfo).val()
    bot.creds.source.creds.secret = $(".j-src-secret", $boxBotInfo).val();

    bot.creds.dist.exchange = $(".j-dist-exchange option:selected", $boxBotInfo).val();
    bot.creds.dist.creds.apiKey = $(".j-dist-api", $boxBotInfo).val();
    bot.creds.dist.creds.secret = $(".j-dist-secret", $boxBotInfo).val();


    $thisBtn.buttonLoader("start");//Запуск процессинга на кнопке

    if (bot.info.status == 'RUNNING') {
        ShowMessage("error", 'Bot is running now, cant change any creds');
        HideBotInfo();
        RefreshController();
        $thisBtn.buttonLoader("stop");//Остановка процессинга на блоке
    } else {

        var paramsAjax = {
            url: `pool/${bot.info.poolId}/bot/${bot.info.idInPool}/creds`,
            dataType: "JSON",
            method: "PUT",
            timeout: 5000,
            data: bot,
            defaultMessage: "DEFAULT_MESSAGE"
        }

        QueryAjax(paramsAjax, function (result, json) {

            if (result) {
                ShowMessage("success", json.messageCode, "ok");
                HideBotInfo();
                RefreshController();
                RefreshCreds();
            } else {
                ShowMessage("error", json.messageCode, "error");
                HideBotInfo();
                RefreshController();
            }

            $thisBtn.buttonLoader("stop");//Остановка процессинга на блоке
        });
    }
}

$(document).on("click", ".j-controllers-list .j-btn-add", function () {//Автоматическое добавления по той информации которую вернул сервис
    //interviewControllersActive = false;
    let $thisBtn = $(this);//Кнопка на которую был сделан клик
    let idBotInList = $thisBtn
        .parents("tr")
        .first()
        .data("num");//Номер порядка контроллера которы находиться в таблице
    let botInfo = botsList[idBotInList];//Информация о контроллере

    var paramsAjax = {
        url: "bot",
        dataType: "JSON",
        method: "POST",
        data: botInfo,
        timeout: 5000,
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            ShowMessage("success", json.messageCode, "ok");
            $(".j-controllers-list .j-refresh").click();
        }
        //interviewControllersActive = true;
    });
});

function ClearingHistoryController() {

    var $thisBtn = $(this);
    let idBot = $thisBtn.parents(".j-events").find(".j-refresh").data("controller");
    var paramsAjax = {
        url: "controller/" + idBot + "/events",
        method: "DELETE"
    }

    $thisBtn.buttonLoader("start");

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            ShowMessage("success", json.messageCode, "ok");
            RefreshControllerEvents();
        }
        $thisBtn.buttonLoader("stop");
    });
}

function RefreshControllerEvents() {
    //interviewHistoryEventsActive = false;
    $('#table-bots-events').DataTable().ajax.reload(function () {
        //interviewHistoryEventsActive = true;
    }, false);
}

function RefreshController() {
    //interviewControllersActive = false;
    $('#table-bots-list').DataTable().ajax.reload(function () {
        //interviewControllersActive = true;
    }, false);
}

function RefreshCreds() {
    var $boxBotInfo = $(".j-bot-creds");//Блок информации контроллера

    // $(".j-source-exchange option:selected", $boxBotInfo).val();
    $(".j-src-api", $boxBotInfo).val('');
    $(".j-src-secret", $boxBotInfo).val('');

    // $(".j-dist-exchange option:selected", $boxBotInfo).val();
    $(".j-dist-api", $boxBotInfo).val('');
    $(".j-dist-secret", $boxBotInfo).val('');

}

function cleaningModalAddBot() {
    let $modalAddConverter = $("#add-bot"); //Модальное окно

    $(".j-name", $modalAddConverter).val('');
}