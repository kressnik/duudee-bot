document.addEventListener("lang-ready", function (e) {
    GetUserInfo();
    initInputFileListeners();
});
$(document).on("click", ".j-change-info", ChangeInfo);
$(document).on("click", ".j-change-password", ChangePassword);
$(document).on("click", ".j-generate-two-factor", generateTwoFactor);
$(document).on("change", ".j-two-factor-switch", twoFactorToggle);
$(document).on("click", "#modal-two-factor .j-two-factor-submit", twoFactorSubmit);
$(document).on("click", "#modal-qrcode-two-factor .j-activate-two-factor", activateTwoFactor);
$(document).on('hidden.bs.modal', '#modal-two-factor', function (e) {
    $("input", this).val("").removeClass("state-valid");
})

function GetUserInfo() {

    let $profileInfoBox = $(".j-profile-info-box");
    let paramsAjax = {
        url: "user",
        method: "GET",
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {

            let flag = json.data.twoFactorFlag;

            if (flag == false) {
                $(".j-two-factor-switch-box", $profileInfoBox).removeClass("collapse");
                $(".j-generate-two-factor-box", $profileInfoBox).addClass("collapse").removeClass("d-flex");
            }

            $("[name='full-name']", $profileInfoBox).val(json.data.fullName);
            $("[name='short-name']", $profileInfoBox).val(json.data.shortName);
            $("[name='email']", $profileInfoBox).val(json.data.email);
            $(".j-two-factor-switch", $profileInfoBox).prop("checked", json.data.isUseTwoFa);
            $('img[name=avatar-img]', $profileInfoBox).attr("src", json.data.imgDist || "dist/img/default-user-icon.jpg");
        }

    });
}

function ChangeInfo() {

    let $thisBtn = $(this);
    $thisBtn.buttonLoader("start");

    let $profileInfoBox = $(".j-profile-info-box");
    let img = $(".j-avatar-row", $profileInfoBox).data('file');
    let userInfo = {};

    userInfo.fullName = $("[name='full-name']", $profileInfoBox).val();
    userInfo.shortName = $("[name='short-name']", $profileInfoBox).val();
    userInfo.email = $("[name='email']", $profileInfoBox).val();
    if (img) { userInfo.avatar = img; }

    let paramsAjax = {
        url: "user/info",
        method: "PUT",
        defaultMessage: "DEFAULT_MESSAGE",
        data: userInfo
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            getAuthorizedUserInfo();
            ShowMessage("success", LANG_FILE.message.ok.CHANGE_SYS_USER_INFO_OK, "ok");
        }
        $thisBtn.buttonLoader("stop");
    });

}

function generateTwoFactor() { //Запрос на генерацию двух факторной авторизации
    let $thisBtn = $(this);
    $thisBtn.buttonLoader("start");

    let $profileInfoBox = $(".j-profile-info-box");
    let $modalQrcodeTwoFactor = $("#modal-qrcode-two-factor");
    let paramsAjax = {
        url: "totp-secret",
        method: "POST",
        defaultMessage: "DEFAULT_MESSAGE"
    }

    QueryAjax(paramsAjax, function (result, json) {

        if (result) {
            $modalQrcodeTwoFactor.find('img[name=qr-img]').attr("src", json.data || "dist/img/default-user-icon.jpg");
            $modalQrcodeTwoFactor.modal('show');

            $(".j-two-factor-switch-box", $profileInfoBox).removeClass("collapse");
            $(".j-generate-two-factor-box", $profileInfoBox).addClass("collapse").removeClass("d-flex");
        }

        $thisBtn.buttonLoader("stop");
    });
}

function twoFactorToggle() {

    let $profileInfoBox = $(".j-profile-info-box");
    let state = $(this).prop("checked") ? 1 : 0;//Получения нового статуса
    let $modalTwoFactorCheck = $("#modal-two-factor"); //Модальное окно

    $modalTwoFactorCheck.find('.j-two-factor-submit').attr("state", state);

    $(".j-two-factor-switch", $profileInfoBox).prop("checked", state == 0 ? true : false);


}

function activateTwoFactor() {

    let $thisBtn = $(this);
    $thisBtn.buttonLoader("start");

    let $profileInfoBox = $(".j-profile-info-box");
    let $modalTwoFactorCheck = $("#modal-qrcode-two-factor");
    let info = {
        code: ValidateInput($("[name='two-factor-code']", $modalTwoFactorCheck))
    };
    let paramsAjax = {
        url: `twoFactor/1`,
        dataType: "JSON",
        method: "PUT",
        data: info,
        timeout: 2000
    }

    if (info.code != "error") {
        QueryAjax(paramsAjax, function (result, json) {

            if (result) {
                $(".j-two-factor-switch", $profileInfoBox).prop("checked", true);
                ShowMessage("success", LANG_FILE.message.ok.CHANGE_STATE_TWO_FACTOR_OK, "ok");
                $modalTwoFactorCheck.modal("hide");
            } else {
                ShowMessage('error', LANG_FILE.message.error.CHANGE_STATE_TWO_FACTOR_ERROR, 'error');
            }

            $thisBtn.buttonLoader("stop");
        });
    } else {
        $thisBtn.buttonLoader("stop");
    }
};

function twoFactorSubmit() {

    let $thisBtn = $(this);
    $thisBtn.buttonLoader("start");

    let $profileInfoBox = $(".j-profile-info-box");
    let $modalTwoFactorCheck = $("#modal-two-factor");
    let $inputTwoFactorCode = $("[name='two-factor-code']", $modalTwoFactorCheck);
    let state = $thisBtn.attr("state");
    let info = {
        code: ValidateInput($inputTwoFactorCode)
    };
    let paramsAjax = {
        url: `twoFactor/${state}`,
        dataType: "JSON",
        method: "PUT",
        data: info,
        timeout: 2000
    }

    if (info.code != "error") {

        QueryAjax(paramsAjax, function (result, json) {

            if (result) {
                ShowMessage("success", LANG_FILE.message.ok.CHANGE_STATE_TWO_FACTOR_OK, "ok");
                $(".j-two-factor-switch", $profileInfoBox).prop("checked", state == 1 ? true : false);
                $modalTwoFactorCheck.modal("hide");
            }

            $thisBtn.buttonLoader("stop");
        });

    } else {
        $thisBtn.buttonLoader("stop");
    }
};

function ChangePassword(e) {

    e.preventDefault();

    let $thisBtn = $(this);
    $thisBtn.buttonLoader("start");

    let $userPasswordForm = $(".j-user-password");
    let old = $.trim($("[name='current-password']", $userPasswordForm).val());
    let newPassword = $.trim($("[name='new-password']", $userPasswordForm).val());
    let reenter = $.trim($("[name='reenter-password']", $userPasswordForm).val());
    let userPassword = {
        password: {
            old: old,
            new: newPassword,
            reenter: reenter
        }
    };

    if (reenter.length >= 3) {
        if (newPassword === reenter) {
            let paramsAjax = {
                url: "user/password",
                method: "PUT",
                defaultMessage: "DEFAULT_MESSAGE",
                data: userPassword
            }

            QueryAjax(paramsAjax, function (result, json) {

                if (result) {
                    ShowMessage("success", LANG_FILE.message.ok.CHANGE_USER_PASSWORD_OK, "ok");
                    $("[name='current-password']", $userPasswordForm).val("");
                    $("[name='new-password']", $userPasswordForm).val("");
                    $("[name='reenter-password']", $userPasswordForm).val("")
                }

                $thisBtn.buttonLoader("stop");

            });
        }
        else {
            ShowMessage("error", LANG_FILE.message.error.PASSWORDS_NOT_EQUALLY, "error");
            $thisBtn.buttonLoader("stop");
        }
    } else {
        ShowMessage("error", LANG_FILE.message.error.PASSWORDS_NOT_MATCH, "error");
        $thisBtn.buttonLoader("stop");
    }
}