var eventLoadHeaderBox = new CustomEvent("header-box-ready", { "detail": "Header box loading successfuly" });
/* Загрузка header-box.html */

// Create the event
var loadHeaderContainerStatus = false;
var loadMenuContainerStatus = false;

if (window.location.href.split('/').pop() == "login.html") {
    loadHeaderContainerStatus = true;
    loadMenuContainerStatus = true;
} else {
    $("#header-box").load("html-model/header-container.html", function (response, status, xhr) {//Загрузка интерфейса шапки
        if (status == "error") {
            var msg = "Sorry but there was an error: ";
            $("#error").html(msg + xhr.status + " " + xhr.statusText);
        }

        loadHeaderContainerStatus = true;
    });

    $("#headerMenuCollapse").load("html-model/menu-container.html", function (response, status, xhr) {//Загрузка интерфейса меню
        if (status == "error") {
            var msg = "Sorry but there was an error: ";
            $("#error").html(msg + xhr.status + " " + xhr.statusText);
            loadMenuContainerStatus = true;
        } else {
            $viewManu = $(this);

            QueryAjax({
                url: "user/menu",
                method: "GET"
            }, function (result, json) {
                if (result, json.data != null) {

                    var name = json.data.name;
                    var menuJSON = json.data.menu;
                    var $menuBtnDropdown = $(".j-menu-btn-dropdown", $viewManu);
                    var $newMenuBtnDropdown = $menuBtnDropdown.clone().removeClass("j-menu-btn-dropdown");

                    var $menuBtn = $(".j-menu-btn", $viewManu);
                    var $newMenuBtn = $menuBtn.clone().removeClass("j-menu-btn");

                    $menuBtnDropdown.remove();
                    $menuBtn.remove();

                    if(name != "administrator") {
                        $("[href='/system-users.html']").remove();
                        $("[href='/auth-client.html']").remove();
                    }

                    for (const key in menuJSON) {
                        var menu = menuJSON[key];
                        var subMenu = menu.sub;
                        var translateParent = "{{lang.nav." + key + "}}";
                        var icon = menu.icon == null ? "" : menu.icon;

                        if (subMenu != null) {
                            var $menu = $newMenuBtnDropdown.clone();
                            var $dropdownMenu = $(".dropdown-menu", $menu);
                            var $a = $("a", $dropdownMenu).clone().remove();

                            $("a", $dropdownMenu).remove();

                            $menu.find(".nav-link").first().html(icon + translateParent);

                            for (const key in subMenu) {
                                var translateСhild = "{{lang.nav." + key + "}}";
                                var href = subMenu[key];
                                var $newA = $a.clone();

                                $newA.attr("href", href).text(translateСhild);
                                $dropdownMenu.append($newA);
                            }

                        } else {
                            var $menu = $newMenuBtn.clone();
                            var href = menu.href == null ? "#" : menu.href;
                            var icon = menu.icon == null ? "" : menu.icon;
                            $menu.find(".nav-link").first().attr("href", href).html(icon + translateParent);
                        }

                        $(".nav.nav-tabs", $viewManu).append($menu);
                    }
                    selectedActiveMenu();
                }
                loadMenuContainerStatus = true;
            });
        }
    });
}

checkingLoadHtmlBox();

function checkingLoadHtmlBox() {//Запуск загрузки других компонентов
    if (loadHeaderContainerStatus == true
        && loadMenuContainerStatus == true
    ) {
        document.dispatchEvent(eventLoadHeaderBox);
        console.log("Загрузился header и menu");
    } else {
        setTimeout(checkingLoadHtmlBox, 5);
    }
}

function selectedActiveMenu() { //Выделить выбранный пункт меню
    var location = window.location.href;
    var cur_url = '/' + location.split('/').pop();

    $('.nav-item a').each(function () {
        var link = $(this).attr('href');

        if (cur_url == link) {
            $(this).addClass('active');
            $(this).parents(".nav-item").first().find(".nav-link").addClass('active');
            return false;
        }
    });
}

// var setTimeoutMenu;
// $(document).on("mouseenter", "li.nav-item", function () {
//     clearTimeout(setTimeoutMenu);
//     var $this = $(this);
//     $this
//         .addClass("show")
//         .find(".nav-link")
//         .first()
//         .attr("aria-expanded", true);
//     $this.find(".dropdown-menu")
//         .first()
//         .addClass("show")
//         .attr("style", "position: absolute; transform: translate3d(12px, 55px, 0px); top: 0px; left: 0px; will-change: transform;");
// });

// $(document).on("mouseleave", "li.nav-item", function () {
//     var $this = $(this);
//     setTimeoutMenu = setTimeout(function () {
//         $this
//             .removeClass("show")
//             .find(".nav-link")
//             .first()
//             .attr("aria-expanded", false);
//         $this.find(".dropdown-menu")
//             .first()
//             .removeClass("show")
//             .attr("style", "");
//     }, 500);
// })


