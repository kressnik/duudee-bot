require.config({
    shim: {
        'fullcalendar': ['moment', 'jquery'],
    },
    paths: {
        'fullcalendar': 'dist/plugins/fullcalendar/js/fullcalendar.min',
        'moment': 'dist/plugins/fullcalendar/js/moment.min',
    }
});