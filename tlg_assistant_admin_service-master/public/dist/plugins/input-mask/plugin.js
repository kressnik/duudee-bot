require.config({
    shim: {
        'input-mask': ['jquery', 'core']
    },
    paths: {
        'input-mask': 'dist/plugins/input-mask/js/jquery.mask.min'
    }
});