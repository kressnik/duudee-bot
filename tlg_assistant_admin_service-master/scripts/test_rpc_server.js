var amqp = require('amqplib/callback_api');
const uuid = require('uuid');
const url = 'amqp://wdzqrrlv:VQa_s-So2kndZUAlR9-p9G0dDeZ9QnBe@bulldog.rmq.cloudamqp.com/wdzqrrlv';

//const url = 'amqp://staging-for-kubernetes:staging-for-kubernetes@104.248.139.201';


amqp.connect(url, function (error0, connection) {
    if (error0) {
        throw error0;
    }

    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }
        var queue = 'test-queue';

        channel.assertQueue(queue, {
            durable: false
        });

        channel.prefetch(30);

        console.log(' [x] Awaiting RPC requests');

        channel.consume(queue, function reply(msg) {
            let payload =  msg.content.toString();

            console.log(" [.] recieve [%s]  {%s}", msg.properties.correlationId, payload);

            const msg_ = Buffer.from(JSON.stringify({
               status: "OK",
               message: "pong back after ping",
               data: {
                    reciveStamp: payload.timeStamp,
                    sendStamp: Date.now()
               }
            }));

            const props = {
                correlationId: msg.properties.correlationId
            };

            console.log(" [.] reply to [%s]", msg.properties.replyTo, msg.content.toString());

            channel.sendToQueue(msg.properties.replyTo, msg_, props);

            channel.ack(msg);
        });
    });
});