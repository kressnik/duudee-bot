var amqp = require('amqplib/callback_api');
const replyToQueue = '';
const uuid = require('uuid');
const url = 'amqp://wdzqrrlv:VQa_s-So2kndZUAlR9-p9G0dDeZ9QnBe@bulldog.rmq.cloudamqp.com/wdzqrrlv';


//const url = 'amqp://staging-for-kubernetes:staging-for-kubernetes@104.248.139.201';

const sendto = 'ns_rpc_queue';


amqp.connect(url, function (err, conn) {
    if (err) throw new Error(err);

    conn.createChannel(function (err, ch) {
        ch.assertQueue(replyToQueue, { exclusive: true }, function (err, q) {
            var corr = generateUuid();
            var num = Math.random();

            console.log(' [x] Requesting fib(%d) to [%s] corId [%s]', num, q.queue, corr);


            ch.consume(q.queue, function (msg) {
                if (msg.properties.correlationId == corr) {
                    console.log(' [.] Got %s', msg.content.toString());
                    setTimeout(function () { conn.close(); process.exit(0) }, 500);
                } else {
                    console.error(`Wrong corrId:[${msg.properties.correlationId}}`);
                    console.error(`Normal corrId:[${corr}}`);
                }
            }, { noAck: true });

            let payload = {
                "jsonrpc": "2.0", 
                "method": "test.ping", 
                "params": {"post": "12345", "e1":"will throw some eroro"}, 
                "id": corr
            }

            payload = JSON.stringify(payload);
            
            ch.sendToQueue(sendto,
                new Buffer.from(payload),
                { correlationId: corr, replyTo: q.queue });
        });
    });
});

function generateUuid() {
    return uuid.v4();
}
