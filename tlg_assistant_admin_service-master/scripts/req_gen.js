
'use strict';

/**
 * Sample request generator usage.
 * Contributed by jjohnsonvng:
 * https://github.com/alexfernandez/loadtest/issues/86#issuecomment-211579639
 */

const loadtest = require('loadtest');

const options = {
	url: 'http://localhost:8080',
	concurrency: 5,
	method: 'POST',
	body: '',
	//requestsPerSecond:20,
	maxSeconds: 5,
	requestGenerator: (params, options, client, callback) => {
		const timestamp = Date.now()
		const message = JSON.stringify({
			"jsonrpc": "2.0",
			"method": "event.push",
			"params": {
				"type": "SEND_PDF_TO_EMAIL",
				"userId": 1,
				"payload": {
					"timeStamp": `${timestamp}`,
					"codeId": 1,
					"locale": "ru_RU"
				}
			},
			"id": `${timestamp}`
		});

		options.headers['Content-Length'] = message.length;
		options.headers['Content-Type'] = 'application/json';
		options.body = message;
		options.path = '/v1/rpc';

		const request = client(options, callback);

		request.write(message);
		return request;
	}
};

loadtest.loadTest(options, (error, results) => {
	if (error) {
		return console.error('Got an error: %s', error);
	}
	console.log(results);
	console.log('Tests run successfully');
});