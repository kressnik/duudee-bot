let chai = require('chai');
let should = chai.should();

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = require('../interfaces/basicResponse');

const {
    RpcStatusCode
} = require('../core/rpc_client/RpcProcessorStruct');


const {
    AssistantBotClient
} = require('../core/rpc_client/assistant_bot_client');


function buildClient(mocksVersion, useMocks = true) {
    try {
        let rpcConfigs = {
            host: process.env.RPC_HOST || 'localhost',
            port: process.env.RPC_PORT || 9001,
            mocksVersion,
            useMocks
        }

        return new AssistantBotClient(rpcConfigs);
    } catch (e) {
        console.error(e);

        throw e;
    }
}

describe('AssistantBotClientClass units test.', () => {
    before(async () => {

    });

    describe('test private methods', () => {
        describe('test _reduceErrorRpcToBasic method', () => {
            it('_reduceErrorRpcToBasic should return package ERRO_NOT_FOUND', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.NOT_FOUND,
                    message: "Not found error",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_NOT_FOUND);
                result.should.have.property('data', 'test');
            });

            it('_reduceErrorRpcToBasic should return package ERROR_INVALID_DATA', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.SERVER_ERROR_INVALID_METHOD_PARAMETERS,
                    message: "Not found error",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                result.should.have.property('data', 'test');
            });
        });
    });

    describe('test public methods.', () => {
        describe('test ping method', () => {
            it('function ping should return pong', async function () {
                try {
                    const params = {
                        ping: 'ping'
                    };

                    const response = await buildClient('test_ping_pong').ping(params);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('pong', params);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test getChannels method', () => {
            it('getChannels should return cahnnels list', async () => {
                try {
                    const params = {

                    };

                    const response = await buildClient('generic_mocks').getChannels(params);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    //contract

                    const data = response.data;

                    data.should.have.property('count', 4);
                    data.should.have.property('rows');
                    data.rows.should.have.property('length', 4);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            })
        });

        describe('test getAllTracks method', () => {
            it('getAllTracks should return tracks list', async () => {
                try {
                    const params = {
                        "dateRange": [],
                        "filter": {
                            "id": [],
                            "status": [],
                            "tokenId": [],
                            "buttonId": [],
                            "channelId": [],
                            "orderId": [],
                            "sessionId": [],
                            "error": []
                        },
                        mock: {
                            "jsonrpc": "2.0",
                            "id": 1587056750,
                            "result": {
                                "status": "OK",
                                "message": "Get tracks from db done done successfully",
                                "data": [{}]
                            }
                        }
                    };

                    const response = await buildClient('generic_mocks').getAllTracks(params);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('length', 1);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            })
        });
    });
});


process.on('unhandledRejection', (e) => {
    console.log('you forgot to return a Promise! Check your tests!' + e.message)
})