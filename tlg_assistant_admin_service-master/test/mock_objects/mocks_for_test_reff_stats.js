const mocksTracks = Object.freeze({
    mockTracksCutedData: [
        {
            "id": 385,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": true,
            "createdAt": "2020-04-09T13:54:59.000Z",
            "updatedAt": "2020-04-09T13:54:59.000Z",
            "userId": 440,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)",
            }
        },
        {
            "id": 386,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:42:43.000Z",
            "updatedAt": "2020-04-09T14:42:43.000Z",
            "userId": 4323,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 387,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:44:38.000Z",
            "updatedAt": "2020-04-09T14:44:38.000Z",
            "userId": 7321,
            "tokenId": 9,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 389,
            "status": "done_error",
            "error": "ERROR_ALREADY_ASSIGNED",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 8455,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 3859,
            "status": "done_error",
            "error": "ERROR_ALREADY_ASSIGNED",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 440,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 448,
            "status": "done_error",
            "error": "ERROR_INVALID_DATA",
            "stage": 0,
            "isNewUser": true,
            "createdAt": "2020-04-09T20:46:48.000Z",
            "updatedAt": "2020-04-09T20:46:48.000Z",
            "userId": 6128,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": null,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": null,
            "order": null
        },
        {
            "id": 487,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 154,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 6,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        }
    ],
    mockTracksByOneChannel: [
        {
            "id": 385,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": true,
            "createdAt": "2020-04-09T13:54:59.000Z",
            "updatedAt": "2020-04-09T13:54:59.000Z",
            "userId": 440,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)",
            }
        },
        {
            "id": 386,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:42:43.000Z",
            "updatedAt": "2020-04-09T14:42:43.000Z",
            "userId": 4323,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 387,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:44:38.000Z",
            "updatedAt": "2020-04-09T14:44:38.000Z",
            "userId": 7321,
            "tokenId": 9,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 389,
            "status": "done_error",
            "error": "ERROR_ALREADY_ASSIGNED",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 8455,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 3859,
            "status": "done_error",
            "error": "ERROR_ALREADY_ASSIGNED",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 440,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 448,
            "status": "done_error",
            "error": "ERROR_INVALID_DATA",
            "stage": 0,
            "isNewUser": true,
            "createdAt": "2020-04-09T20:46:48.000Z",
            "updatedAt": "2020-04-09T20:46:48.000Z",
            "userId": 6128,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": null,
            "order": null
        },
        {
            "id": 487,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 154,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        }
    ],
    mockTracksWithDiffErrors: [
        {
            "id": 386,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:42:43.000Z",
            "updatedAt": "2020-04-09T14:42:43.000Z",
            "userId": 4323,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 387,
            "status": "done_error",
            "error": 'ERROR_ALREADY_ASSIGNED',
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:44:38.000Z",
            "updatedAt": "2020-04-09T14:44:38.000Z",
            "userId": 7321,
            "tokenId": 9,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 389,
            "status": "done_error",
            "error": "ERROR_ALREADY_ASSIGNED",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 8455,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 3859,
            "status": "done_error",
            "error": "ERROR_RECIVE_STOP_SIGNAL",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 440,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 448,
            "status": "done_error",
            "error": "ERROR_INVALID_DATA",
            "stage": 0,
            "isNewUser": true,
            "createdAt": "2020-04-09T20:46:48.000Z",
            "updatedAt": "2020-04-09T20:46:48.000Z",
            "userId": 6128,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": null,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": null,
            "order": null
        },
        {
            "id": 487,
            "status": "done_error",
            "error": "ERROR_INVALID_HANDLE",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 154,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 6,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 4837,
            "status": "done_error",
            "error": "ERROR_UNEXP_ERR",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 154,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 6,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 4287,
            "status": "done_error",
            "error": "ERROR_CANCEL",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 154,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 6,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        }
    ],
    mockTracksWithDiffErrorsByDiffUsers: [
        {
            "id": 386,
            "status": "done",
            "error": null,
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:42:43.000Z",
            "updatedAt": "2020-04-09T14:42:43.000Z",
            "userId": 4323,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 387,
            "status": "done_error",
            "error": 'ERROR_ALREADY_ASSIGNED',
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:44:38.000Z",
            "updatedAt": "2020-04-09T14:44:38.000Z",
            "userId": 7321,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 389,
            "status": "done_error",
            "error": "ERROR_ALREADY_ASSIGNED",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 8455,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 3859,
            "status": "done_error",
            "error": "ERROR_RECIVE_STOP_SIGNAL",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-09T14:47:18.000Z",
            "updatedAt": "2020-04-09T14:47:18.000Z",
            "userId": 440,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 10,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 10,
                "name": "Новости",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 448,
            "status": "done_error",
            "error": "ERROR_INVALID_DATA",
            "stage": 0,
            "isNewUser": true,
            "createdAt": "2020-04-09T20:46:48.000Z",
            "updatedAt": "2020-04-09T20:46:48.000Z",
            "userId": 6128,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": null,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": null,
            "order": null
        },
        {
            "id": 487,
            "status": "done_error",
            "error": "ERROR_INVALID_HANDLE",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 154,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 6,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 4837,
            "status": "done_error",
            "error": "ERROR_UNEXP_ERR",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 155,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 6,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        },
        {
            "id": 4287,
            "status": "done_error",
            "error": "ERROR_CANCEL",
            "stage": 0,
            "isNewUser": false,
            "createdAt": "2020-04-10T13:23:29.000Z",
            "updatedAt": "2020-04-10T13:23:29.000Z",
            "userId": 156,
            "tokenId": 8,
            "buttonId": 5,
            "channelId": 6,
            "sessionId": null,
            "token": {},
            "button": {},
            "session": null,
            "channel": {
                "id": 6,
                "name": "Smth",
                "color": "rgba(241, 196, 15, 0.2)"
            },
            "order": {}
        }
    ]
});

const mocksChannelsStats = Object.freeze({
    statsByChannelsFull: [
        {
            "name": "Теннис VIP",
            "id": 1,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [],
                "returnSubscUsersIds": [],
                "subscribes": 0,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Бадминтон",
            "id": 2,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [],
                "returnSubscUsersIds": [],
                "subscribes": 0,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Солянка",
            "id": 3,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [
                    "4053",
                    "6679",
                    "8249",
                    "8856"
                ],
                "returnSubscUsersIds": [],
                "subscribes": 4,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Теннис",
            "id": 4,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [
                    "8856"
                ],
                "returnSubscUsersIds": [],
                "subscribes": 1,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Рейтинг Капперов",
            "id": 5,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [
                    "7588",
                    "8856"
                ],
                "returnSubscUsersIds": [],
                "subscribes": 2,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Футбол",
            "id": 6,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [],
                "returnSubscUsersIds": [],
                "subscribes": 0,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Иван Петрович ЖБ",
            "id": 7,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [
                    "6679",
                    "7588",
                    "8149",
                    "8191",
                    "8755"
                ],
                "returnSubscUsersIds": [],
                "subscribes": 5,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Иван Петрович ЖБ VIP",
            "id": 8,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [],
                "returnSubscUsersIds": [],
                "subscribes": 0,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Бадминтон VIP",
            "id": 9,
            "color": "",
            "tracksStats": {
                "subscUsersIds": [],
                "returnSubscUsersIds": [],
                "subscribes": 0,
                "returnSubscribes": 0
            }
        },
        {
            "name": "Новости",
            "id": 10,
            "color": "rgba(241, 196, 15, 0.2)",
            "tracksStats": {
                "subscUsersIds": [
                    "3",
                    "9",
                    "12",
                    "21",
                    "23",
                    "28",
                    "30"
                ],
                "returnSubscUsersIds": [
                    "2",
                    "30",
                    "83",
                    "99",
                    "654",
                    "4116",
                    "5441",
                    "6679",
                    "8191"
                ],
                "subscribes": 7,
                "returnSubscribes": 9
            }
        }
    ]
});
module.exports.mocksTracks = mocksTracks;
module.exports.mocksChannelsStats = mocksChannelsStats;