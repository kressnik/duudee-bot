let chai = require('chai');
let should = chai.should();

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    RefferalStatsClass
} = require('../../core/stats_class/reff_link_stats_class/stats_class');

const refferalStatsManager = new RefferalStatsClass();


describe('RefferalStatsClass units test.', () => {
    before(async () => {
        try {
            let rpcConfigs = {
                host: process.env.RPC_HOST || 'localhost',
                port: process.env.RPC_PORT || 9001
            }

            console.log(`Start init RefferalStatsClass instance`);

            let resInitMan = await refferalStatsManager.init(rpcConfigs);

            console.log(`End init ${resInitMan.toString()}`);

            resInitMan.should.be.a('object');
            resInitMan.should.have.property('status', ResultStatus.OK);
        } catch (e) {
            console.error(e);

            throw e;
        }
    });

    describe('test public methods.', () => {
        it('function getReffStats should return contract stats', async function () {
            try {
                const params = {

                };

                let statsResponse = await refferalStatsManager.getReffStats(params);

                //console.log('statsResponse', statsResponse);

                statsResponse.should.be.a('object');
                statsResponse.should.have.property('status', ResultStatus.OK);
                statsResponse.should.have.property('data');

                //contract checking
                const data = statsResponse.data;

                //root
                data.should.have.property('newUsers');
                data.should.have.property('returnUsers');
                data.should.have.property('totalUsers');
                data.should.have.property('channelsStats');
                data.should.have.property('channelsTotalStats');

                // -- channelsTotalStats
                data.channelsTotalStats.should.have.property('commingRate');
                data.channelsTotalStats.should.have.property('lossRate');
                data.channelsTotalStats.should.have.property('returnSubscribes');
                data.channelsTotalStats.should.have.property('subscribes');
                data.channelsTotalStats.should.have.property('subscribes');
                data.channelsTotalStats.should.have.property('uniqUsersSubscribes');
                data.channelsTotalStats.should.have.property('usersWithRetSubsc');
                data.channelsTotalStats.should.have.property('usersWithSubsc');

                return
            } catch (e) {
                console.error(e);

                throw e;
            }
        });
    });
});


process.on('unhandledRejection', (e) => {
    console.log('you forgot to return a Promise! Check your tests!' + e.message)
})