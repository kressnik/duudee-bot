let chai = require('chai');
let should = chai.should();

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = require('../../interfaces/basicResponse');
const {
    StatisticsReportClass
} = require('../../core/stats_class/reff_link_stats_class/tracks_stats_report_class');

const {
    AssistantBotClient
} = require('../../core/rpc_client/assistant_bot_client');

const {
    mocksTracks,
    mocksChannelsStats
} = require('../mock_objects/mocks_for_test_reff_stats');

const mockTracksCutedData = mocksTracks.mockTracksCutedData;
const mockTracksByOneChannel = mocksTracks.mockTracksByOneChannel;
const statsByChannelsFull = mocksChannelsStats.statsByChannelsFull;

function checkReportContract(contract) {
    //root
    contract.should.have.property('newUsers');
    contract.should.have.property('returnUsers');
    contract.should.have.property('totalUsers');
    contract.should.have.property('channelsStats');
    contract.channelsStats.should.be.an('array');
    contract.should.have.property('channelsTotalStats');
    contract.channelsTotalStats.should.be.an('object');

    // -- channelsTotalStats
    contract.channelsTotalStats.should.have.property('commingRate');
    contract.channelsTotalStats.should.have.property('lossRate');
    contract.channelsTotalStats.should.have.property('returnSubscribes');
    contract.channelsTotalStats.should.have.property('subscribes');
    contract.channelsTotalStats.should.have.property('subscribes');
    contract.channelsTotalStats.should.have.property('uniqUsersSubscribes');
    contract.channelsTotalStats.should.have.property('usersWithRetSubsc');
    contract.channelsTotalStats.should.have.property('usersWithSubsc');
}

function buildClient(mocksVersion = 'generic_mocks', useMocks = true) {
    try {
        return new AssistantBotClient({
            host: process.env.RPC_HOST || 'localhost',
            port: process.env.RPC_PORT || 9001,
            mocksVersion,
            useMocks
        });
    } catch (e) {
        console.error(e);

        throw e;
    }
}

describe('StatisticsReportClass units test.', () => {
    describe('test private methods.', () => {
        describe('test _checkParamsFLowFunc fucntion', () => {
            it('function _checkParamsFLowFunc should throw error if null', async function () {
                try {
                    const assistantBotClient = buildClient();

                    await new StatisticsReportClass(assistantBotClient)._checkParamsFLowFunc(null);

                    throw new Error('Got return but waiting throw');
                } catch (checkREsponse) {
                    //console.log(e);

                    checkREsponse.should.be.a('object');
                    checkREsponse.should.have.property('status', ResultStatus.ERROR_INVALID_PARAMETER);
                }
            });
            it('function _checkParamsFLowFunc should return empty dateRange arr if dateRange has more than tree dates', async function () {
                try {
                    let params = {
                        dateRange: [73746746794, 73497846817, 73497846817],
                        filter: {
                            id: [],
                            status: [],
                            tokenId: [],
                            buttonId: [],
                            channelId: [],
                            orderId: [],
                            sessionId: [],
                            error: []
                        }
                    };

                    const assistantBotClient = buildClient();

                    const checkREsponse = await new StatisticsReportClass(assistantBotClient)._checkParamsFLowFunc(params);

                    checkREsponse.should.be.a('object');

                    checkREsponse.should.have.property('dateRange');
                    checkREsponse.dateRange.should.be.an('array');
                    checkREsponse.dateRange.should.be.empty;

                    // params.filter
                    checkREsponse.should.have.property('filter');
                    checkREsponse.filter.should.be.a('object');
                    checkREsponse.filter.should.have.property('id');
                    checkREsponse.filter.should.have.property('status');
                    checkREsponse.filter.should.have.property('tokenId');
                    checkREsponse.filter.should.have.property('buttonId');
                    checkREsponse.filter.should.have.property('channelId');
                    checkREsponse.filter.should.have.property('orderId');
                    checkREsponse.filter.should.have.property('sessionId');
                    checkREsponse.filter.should.have.property('error');

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
            it('function _checkParamsFLowFunc should return empty params.filter.id if recived filter.id is not an array', async function () {
                try {
                    let params = {
                        dateRange: [73746746794, 73497846817],
                        filter: {
                            id: 3,
                            status: [],
                            tokenId: [],
                            buttonId: [],
                            channelId: [],
                            orderId: [],
                            sessionId: [],
                            error: []
                        }
                    };

                    const assistantBotClient = buildClient();

                    const checkREsponse = await new StatisticsReportClass(assistantBotClient)._checkParamsFLowFunc(params);

                    checkREsponse.should.be.a('object');

                    checkREsponse.should.have.property('dateRange');

                    // params.filter
                    checkREsponse.should.have.property('filter');
                    checkREsponse.filter.should.be.a('object');

                    checkREsponse.filter.should.have.property('id');
                    checkREsponse.filter.id.should.be.an('array');
                    checkREsponse.filter.id.should.be.empty;

                    checkREsponse.filter.should.have.property('status');
                    checkREsponse.filter.should.have.property('tokenId');
                    checkREsponse.filter.should.have.property('buttonId');
                    checkREsponse.filter.should.have.property('channelId');
                    checkREsponse.filter.should.have.property('orderId');
                    checkREsponse.filter.should.have.property('sessionId');
                    checkREsponse.filter.should.have.property('error');


                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getReffStatsRawDataFlowFunc function', () => {
            it('_getReffStatsRawDataFlowFunc should returned all tracks and ignore date range if date range length > 2', async () => {
                try {
                    let params = {
                        "dateRange": [64769365946, 74361784671, 427618461515],
                        "filter": {
                            "id": [],
                            "status": [],
                            "tokenId": [],
                            "buttonId": [],
                            "channelId": [],
                            "orderId": [],
                            "sessionId": [],
                            "error": []
                        }
                    }

                    const assistantBotClient = buildClient(null, false);

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsRawDataFlowFunc(params);

                    checkReturnedContract.should.be.an('array');
                    // checkReturnedContract.should.be.lengthOf(7);
                } catch (e) {
                    console.error(e.messsage);
                    e.should.be.an('object');
                    e.should.have.property('status', 'ERROR_UNEXP_ERR');
                    e.should.have.property('message', 'Not found any tracks');
                }
            });

            it('_getReffStatsRawDataFlowFunc should returned all tracks if not set filter params or date range', async () => {
                try {
                    let params = {
                        "dateRange": [],
                        "filter": {
                            "id": [],
                            "status": [],
                            "tokenId": [],
                            "buttonId": [],
                            "channelId": [],
                            "orderId": [],
                            "sessionId": [],
                            "error": []
                        }
                    }

                    const assistantBotClient = buildClient(null, false);

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsRawDataFlowFunc(params);

                    checkReturnedContract.should.be.an('array');

                } catch (e) {
                    console.error(e);
                    e.should.be.an('object');
                    e.should.have.property('status', 'ERROR_UNEXP_ERR');
                    e.should.have.property('message', 'Not found any tracks');
                }
            });

            it('_getReffStatsRawDataFlowFunc should return all tracks in db if recived empty obj', async () => {
                try {
                    const assistantBotClient = buildClient(null, false); //use not mock client

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsRawDataFlowFunc({});

                    checkReturnedContract.should.be.an('array');
                } catch (e) {
                    console.error(e);
                    e.should.be.an('object');
                    e.should.have.property('status', 'ERROR_UNEXP_ERR');
                    e.should.have.property('message', 'Not found any tracks');
                }
            });

            it('_getReffStatsRawDataFlowFunc should throw if recived null', async () => {
                try {
                    const assistantBotClient = buildClient(null, false); //use not mock client

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsRawDataFlowFunc(null);

                    throw checkReturnedContract;
                } catch (checkReturnedContract) {

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', 'ERROR_UNEXP_ERR');
                    checkReturnedContract.should.have.property('message', 'Args is null, should be an object');
                }
            });

            it('_getReffStatsRawDataFlowFunc should throw if recived array', async () => {
                try {
                    const assistantBotClient = buildClient(null, false); //use not mock client

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsRawDataFlowFunc([]);

                    throw checkReturnedContract;
                } catch (checkReturnedContract) {

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', 'ERROR_UNEXP_ERR');
                    checkReturnedContract.should.have.property('message', 'Args should be an object');
                }
            });

            it('_getReffStatsRawDataFlowFunc should throw if recived undefined', async () => {
                try {
                    const assistantBotClient = buildClient(null, false); //use not mock client

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsRawDataFlowFunc(undefined);

                    throw checkReturnedContract;
                } catch (checkReturnedContract) {

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', 'ERROR_UNEXP_ERR');
                    checkReturnedContract.should.have.property('message', 'Args is null, should be an object');
                }
            });
        });

        describe('test _reduceToReportShema function', () => {
            it('_reduceToReportShema should return contrtact, if recived null', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const report = await new StatisticsReportClass(assistantBotClient);

                    const contract = report._reduceToReportShema(null);

                    checkReportContract(contract);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_reduceToReportShema should return contrtact, if recived undefined', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const report = await new StatisticsReportClass(assistantBotClient);

                    const contract = report._reduceToReportShema(undefined);

                    checkReportContract(contract);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_reduceToReportShema should return contrtact, if not recived channelsTotalStats', async () => {
                try {
                    const params = {
                        newUsers: 0,
                        returnUsers: 0,
                        totalUsers: 0,
                        channelsStats: [],
                    };

                    const assistantBotClient = buildClient();

                    const report = await new StatisticsReportClass(assistantBotClient);

                    const contract = report._reduceToReportShema(params);

                    checkReportContract(contract);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _calcCommingStats function', () => {
            it('_calcCommingStats should returned calced new users, returned users, total users', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingStats(mockTracksCutedData);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('newUsers');
                    checkReturnedContract.newUsers.should.be.equal(2);
                    checkReturnedContract.should.have.property('returnUsers');
                    checkReturnedContract.returnUsers.should.be.equal(5);
                    checkReturnedContract.should.have.property('totalUsers');
                    checkReturnedContract.totalUsers.should.be.equal(6);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcCommingStats should return contrtact, if recived empty arr', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingStats([]);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('newUsers');
                    checkReturnedContract.should.have.property('returnUsers');
                    checkReturnedContract.should.have.property('totalUsers');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _calcStatsByChannels function', () => {
            it('_calcStatsByChannels should returned calced new users, returned users, total users', async () => {
                try {
                    const assistantBotClient = buildClient('db_requests_mocks');

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcStatsByChannels(mockTracksCutedData);

                    checkReturnedContract.should.be.an('array');
                    checkReturnedContract.should.have.lengthOf(7);
                    checkReturnedContract[6].should.have.property("color");
                    checkReturnedContract[6].should.have.property("id", 10);
                    checkReturnedContract[6].should.have.property("name", "Новости");
                    checkReturnedContract[6].should.have.property("tracksStats");
                    checkReturnedContract[6].tracksStats.should.have.property("returnSubscribes", 2);

                    checkReturnedContract[6].tracksStats.should.have.property("returnSubscUsersIds");
                    checkReturnedContract[6].tracksStats.returnSubscUsersIds.should.have.members(['440', '8455']);

                    checkReturnedContract[6].tracksStats.should.have.property("subscribes", 3);

                    checkReturnedContract[6].tracksStats.should.have.property("subscUsersIds");
                    checkReturnedContract[6].tracksStats.subscUsersIds.should.have.members(['440', '4323', '7321']);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcStatsByChannels should return contrtact by all channels in system, if recived empty arr (not found tracks)', async () => {
                try {
                    const assistantBotClient = buildClient('db_requests_mocks');

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcStatsByChannels([]);

                    checkReturnedContract.should.be.an('array');
                    checkReturnedContract.should.have.lengthOf(7);
                    checkReturnedContract[6].should.have.property("color");
                    checkReturnedContract[6].should.have.property("id", 10);
                    checkReturnedContract[6].should.have.property("name", "Новости");
                    checkReturnedContract[6].should.have.property("tracksStats");
                    checkReturnedContract[6].tracksStats.should.have.property("returnSubscribes", 0);

                    checkReturnedContract[6].tracksStats.should.have.property("returnSubscUsersIds");
                    checkReturnedContract[6].tracksStats.returnSubscUsersIds.should.be.empty;

                    checkReturnedContract[6].tracksStats.should.have.property("subscribes", 0);

                    checkReturnedContract[6].tracksStats.should.have.property("subscUsersIds");
                    checkReturnedContract[6].tracksStats.subscUsersIds.should.be.empty;


                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _calcTotalChannelsStats function', () => {
            it('_calcTotalChannelsStats should returned calced total channels stats', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcTotalChannelsStats(statsByChannelsFull);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('returnSubscribes', 9);
                    data.should.have.property('subscribes', 19);
                    data.should.have.property('uniqUsersSubscribes', 21);
                    data.should.have.property('usersWithRetSubsc', 9);
                    data.should.have.property('usersWithSubsc', 15);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcTotalChannelsStats should return contract total channels stats, if recived empty arr', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcTotalChannelsStats([]);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('returnSubscribes', 0);
                    data.should.have.property('subscribes', 0);
                    data.should.have.property('uniqUsersSubscribes', 0);
                    data.should.have.property('usersWithRetSubsc', 0);
                    data.should.have.property('usersWithSubsc', 0);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _calcCommingAndLossRate function', () => {
            it('_calcCommingAndLossRate should returned comming and loss rate in percent', async () => {
                try {

                    let params = {
                        totalUsers: 222,
                        uniqUsersSubscribes: 220
                    }

                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingAndLossRate(params);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('commingRate', 99.1);
                    data.should.have.property('lossRate', 0.9);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcCommingAndLossRate should return contract comming and loss rate, if recived empty arr', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingAndLossRate([]);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('commingRate', 0);
                    data.should.have.property('lossRate', 100);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcCommingAndLossRate should return contract comming and loss rate, if recived null', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingAndLossRate(null);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('commingRate', 0);
                    data.should.have.property('lossRate', 100);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcCommingAndLossRate should return contract comming and loss rate, if recived undenfined', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingAndLossRate(undefined);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('commingRate', 0);
                    data.should.have.property('lossRate', 100);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcCommingAndLossRate should return contract comming and loss rate, if recived totalUsers and uniqUsersSubscribes are undenfined', async () => {
                try {

                    let params = {
                        totalUsers: undefined,
                        uniqUsersSubscribes: undefined
                    }
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingAndLossRate(params);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('commingRate', 0);
                    data.should.have.property('lossRate', 100);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcCommingAndLossRate should return contract comming and loss rate, if recived totalUsers and uniqUsersSubscribes are null', async () => {
                try {

                    let params = {
                        totalUsers: null,
                        uniqUsersSubscribes: null
                    }
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcCommingAndLossRate(params);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('data');

                    let data = checkReturnedContract.data;
                    data.should.have.property('commingRate', 0);
                    data.should.have.property('lossRate', 100);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _calcChannelsSubscRequest function', () => {
            it('_calcChannelsSubscRequest should returned new subsc requests and returned subsc requests', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcChannelsSubscRequest(mockTracksByOneChannel);

                    checkReturnedContract.should.be.a('object');

                    checkReturnedContract.should.have.property('returnSubscribes', 2);
                    checkReturnedContract.should.have.property('returnSubscUsersIds');
                    checkReturnedContract.returnSubscUsersIds.should.have.members(['440', '8455']);

                    checkReturnedContract.should.have.property('subscribes', 4);
                    checkReturnedContract.should.have.property('subscUsersIds');
                    checkReturnedContract.subscUsersIds.should.have.members(['154', '440', '4323', '7321']);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_calcChannelsSubscRequest should return contract new subsc requests and returned subsc requests, if recived empty arr', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._calcChannelsSubscRequest([]);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('returnSubscribes', 0);
                    checkReturnedContract.should.have.property('returnSubscUsersIds');
                    checkReturnedContract.returnSubscUsersIds.should.be.lengthOf(0);
                    checkReturnedContract.should.have.property('subscribes', 0);
                    checkReturnedContract.should.have.property('subscUsersIds');
                    checkReturnedContract.subscUsersIds.should.be.lengthOf(0);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _buidReffStatsFlowFunc function', () => {
            it('_buidReffStatsFlowFunc should returned reff links stats if recived params is ok', async () => {
                try {
                    const assistantBotClient = buildClient('db_requests_mocks');

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._buidReffStatsFlowFunc(mockTracksCutedData);

                    checkReturnedContract.should.be.a('object');

                    checkReturnedContract.should.have.property('newUsers', 2);
                    checkReturnedContract.should.have.property('returnUsers', 5);
                    checkReturnedContract.should.have.property('totalUsers', 6);

                    checkReturnedContract.should.have.property('channelsStats');
                    checkReturnedContract.channelsStats.should.be.an('array');
                    checkReturnedContract.channelsStats.should.be.lengthOf(7);

                    checkReturnedContract.should.have.property('channelsTotalStats');
                    checkReturnedContract.channelsTotalStats.should.have.property('usersWithRetSubsc', 2);
                    checkReturnedContract.channelsTotalStats.should.have.property('usersWithSubsc', 4);
                    checkReturnedContract.channelsTotalStats.should.have.property('returnSubscribes', 2);
                    checkReturnedContract.channelsTotalStats.should.have.property('subscribes', 4);
                    checkReturnedContract.channelsTotalStats.should.have.property('uniqUsersSubscribes', 5);
                    checkReturnedContract.channelsTotalStats.should.have.property('commingRate', 83.33);
                    checkReturnedContract.channelsTotalStats.should.have.property('lossRate', 16.67);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_buidReffStatsFlowFunc should return deff stats contract if recived empty arr (tracks not found)', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._buidReffStatsFlowFunc([]);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('message', 'Refferal link tracks not found');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_buidReffStatsFlowFunc should throw if recived null', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._buidReffStatsFlowFunc(null);

                    throw checkReturnedContract;
    
                } catch (checkReturnedContract) {
                    console.error(checkReturnedContract);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                    checkReturnedContract.should.have.property('message', 'Tracks should be an array.');
                }
            });

            it('_buidReffStatsFlowFunc should throw if recived undefined', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._buidReffStatsFlowFunc(undefined);
                    
                    throw checkReturnedContract;

                } catch (checkReturnedContract) {
                    console.error(checkReturnedContract);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                    checkReturnedContract.should.have.property('message', 'Tracks should be an array.');
                }
            });
        });

        describe('test _getReffStatsFlow function', () => {
            it('_getReffStatsFlow should returned reff links stats if recived params is ok', async () => {
                try {
                    let params = {
                        "dateRange": [74361784671, 427618461515],
                        "filter": {
                            "id": [],
                            "status": [],
                            "tokenId": [],
                            "buttonId": [],
                            "channelId": [],
                            "orderId": [],
                            "sessionId": [],
                            "error": []
                        }
                    };

                    const assistantBotClient = buildClient('db_requests_mocks');

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsFlow(params);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('message', 'Getting refferal stats done successfully');
                    checkReturnedContract.should.have.property('data');

                    let statsBatch = checkReturnedContract.data;

                    statsBatch.should.have.property('newUsers', 2);
                    statsBatch.should.have.property('returnUsers', 5);
                    statsBatch.should.have.property('totalUsers', 6);

                    statsBatch.should.have.property('channelsStats');
                    statsBatch.channelsStats.should.be.an('array');
                    statsBatch.channelsStats.should.be.lengthOf(7);

                    statsBatch.should.have.property('channelsTotalStats');
                    statsBatch.channelsTotalStats.should.have.property('usersWithRetSubsc', 2);
                    statsBatch.channelsTotalStats.should.have.property('usersWithSubsc', 4);
                    statsBatch.channelsTotalStats.should.have.property('returnSubscribes', 2);
                    statsBatch.channelsTotalStats.should.have.property('subscribes', 4);
                    statsBatch.channelsTotalStats.should.have.property('uniqUsersSubscribes', 5);
                    statsBatch.channelsTotalStats.should.have.property('commingRate', 83.33);
                    statsBatch.channelsTotalStats.should.have.property('lossRate', 16.67);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_getReffStatsFlow should return resolve with error if recived params is empty arr', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsFlow([]);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.ERROR_INVALID_PARAMETER);
                    checkReturnedContract.should.have.property('message', 'Incoming parameters should be an object');
    
                } catch (checkReturnedContract) {
                    console.error(e);

                    throw e;
                }
            });

            it('_getReffStatsFlow should return resolve with error if recived null', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient)._getReffStatsFlow(null);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.ERROR_INVALID_PARAMETER);
                    checkReturnedContract.should.have.property('message', 'Incoming parameters should be an object');
    
                } catch (checkReturnedContract) {
                    console.error(e);

                    throw e;
                }
            });
        });
    });
    describe('test public methods.', () => {
        describe('test build method', () => {
            it('function build should return contract stats', async function () {
                try {
                    const params = {
                        filter: {}
                    };

                    const assistantBotClient = buildClient();

                    const report = await new StatisticsReportClass(assistantBotClient).build(params);
                    //console.log('statsResponse', report);

                    report.should.be.a('object');
                    report.should.have.property('status', ResultStatus.OK);
                    report.should.have.property('data');

                    checkReportContract(report.data);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
            it('function build should return contract stats if tracks not found', async function () {
                try {
                    const params = {
                        filter: {
                            tokenId: [2076576567656]
                        }
                    };
                    const assistantBotClient = buildClient(null, false);

                    const report = await new StatisticsReportClass(assistantBotClient).build(params);

                    report.should.be.a('object');
                    report.should.have.property('status', ResultStatus.OK);
                    report.should.have.property('data');

                    checkReportContract(report.data);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('build should returned reff links stats if recived params is ok', async () => {
                try {
                    let params = {
                        "dateRange": [74361784671, 427618461515],
                        "filter": {
                            "id": [],
                            "status": [],
                            "tokenId": [],
                            "buttonId": [],
                            "channelId": [],
                            "orderId": [],
                            "sessionId": [],
                            "error": []
                        }
                    };

                    const assistantBotClient = buildClient('db_requests_mocks');

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient).build(params);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.OK);
                    checkReturnedContract.should.have.property('message', 'Getting refferal stats done successfully');
                    checkReturnedContract.should.have.property('data');

                    let statsBatch = checkReturnedContract.data;

                    statsBatch.should.have.property('newUsers', 2);
                    statsBatch.should.have.property('returnUsers', 5);
                    statsBatch.should.have.property('totalUsers', 6);

                    statsBatch.should.have.property('channelsStats');
                    statsBatch.channelsStats.should.be.an('array');
                    statsBatch.channelsStats.should.be.lengthOf(7);

                    statsBatch.should.have.property('channelsTotalStats');
                    statsBatch.channelsTotalStats.should.have.property('usersWithRetSubsc', 2);
                    statsBatch.channelsTotalStats.should.have.property('usersWithSubsc', 4);
                    statsBatch.channelsTotalStats.should.have.property('returnSubscribes', 2);
                    statsBatch.channelsTotalStats.should.have.property('subscribes', 4);
                    statsBatch.channelsTotalStats.should.have.property('uniqUsersSubscribes', 5);
                    statsBatch.channelsTotalStats.should.have.property('commingRate', 83.33);
                    statsBatch.channelsTotalStats.should.have.property('lossRate', 16.67);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('build should return resolve with error if recived params is empty arr', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient).build([]);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.ERROR_INVALID_PARAMETER);
                    checkReturnedContract.should.have.property('message', 'Incoming parameters should be an object');
    
                } catch (checkReturnedContract) {
                    console.error(e);

                    throw e;
                }
            });

            it('build should return resolve with error if recived null', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const checkReturnedContract = await new StatisticsReportClass(assistantBotClient).build(null);

                    checkReturnedContract.should.be.a('object');
                    checkReturnedContract.should.have.property('status', ResultStatus.ERROR_INVALID_PARAMETER);
                    checkReturnedContract.should.have.property('message', 'Incoming parameters should be an object');
    
                } catch (checkReturnedContract) {
                    console.error(e);

                    throw e;
                }
            });
        });
    });
});


process.on('unhandledRejection', (e) => {
    console.log('you forgot to return a Promise! Check your tests!' + e.message)
})