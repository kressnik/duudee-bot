let chai = require('chai');
let should = chai.should();

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = require('../../interfaces/basicResponse');
const {
    CnannelReffStatsReportClass
} = require('../../core/stats_class/channel_stats_class/refferal_stats_report_class');

const {
    AssistantBotClient
} = require('../../core/rpc_client/assistant_bot_client');

const {
    mocksTracks
} = require('../mock_objects/mocks_for_test_reff_stats');

const mockTracksWithDiffErrors = mocksTracks.mockTracksWithDiffErrors;
const mockTracksWithDiffErrorsByDiffUsers = mocksTracks.mockTracksWithDiffErrorsByDiffUsers;

function checkReportContract(contract) {
    // -- 
    contract.should.have.property('statsByReffLink');
    contract.should.have.property('totalChannelReffStats');
    contract.should.have.property('uniqueChannelReffStats');
    contract.statsByReffLink.should.be.an('array');
    contract.totalChannelReffStats.should.be.an('object');
    contract.uniqueChannelReffStats.should.be.an('object');

    // -- totalChannelReffStats
    contract.totalChannelReffStats.should.have.property('returnSubscribes');
    contract.totalChannelReffStats.should.have.property('newSubscribes');
    contract.totalChannelReffStats.should.have.property('errorSubscribes');
    contract.totalChannelReffStats.should.have.property('stopSubscribes');
    contract.totalChannelReffStats.should.have.property('uniqueComeUsers');

    // -- uniqueChannelReffStats
    contract.uniqueChannelReffStats.should.have.property('returnSubscribes');
    contract.uniqueChannelReffStats.should.have.property('newSubscribes');
    contract.uniqueChannelReffStats.should.have.property('errorSubscribes');
    contract.uniqueChannelReffStats.should.have.property('stopSubscribes');
    contract.uniqueChannelReffStats.should.have.property('uniqueComeUsers');
}

function buildClient(mocksVersion = 'generic_mocks', useMocks = true) {
    try {
        return new AssistantBotClient({
            host: process.env.RPC_HOST || 'localhost',
            port: process.env.RPC_PORT || 9001,
            mocksVersion,
            useMocks
        });
    } catch (e) {
        console.error(e);

        throw e;
    }
}

describe('CnannelReffStatsReportClass units test.', () => {
    describe('test private methods.', () => {
        describe('test _checkParamsFLowFunc fucntion', () => {
            it('function _checkParamsFLowFunc should throw error if null', async function () {
                try {
                    const assistantBotClient = buildClient();

                    await new CnannelReffStatsReportClass(assistantBotClient)._checkParamsFLowFunc(null);

                    throw new Error('Got return but waiting throw');
                } catch (checkREsponse) {
                    //console.log(e);

                    checkREsponse.should.be.a('object');
                    checkREsponse.should.have.property('status', ResultStatus.ERROR_INVALID_PARAMETER);
                }
            });
            it('function _checkParamsFLowFunc should return empty dateRange arr if dateRange has more than tree dates', async function () {
                try {
                    let params = {
                        dateRange: [73746746794, 73497846817, 73497846817],
                        filter: {
                            id: [],
                            status: [],
                            tokenId: [],
                            buttonId: [],
                            channelId: [],
                            orderId: [],
                            sessionId: [],
                            error: []
                        }
                    };

                    const assistantBotClient = buildClient();

                    const checkREsponse = await new CnannelReffStatsReportClass(assistantBotClient)._checkParamsFLowFunc(params);

                    checkREsponse.should.be.a('object');

                    checkREsponse.should.have.property('dateRange');
                    checkREsponse.dateRange.should.be.an('array');
                    checkREsponse.dateRange.should.be.empty;

                    // params.filter
                    checkREsponse.should.have.property('filter');
                    checkREsponse.filter.should.be.a('object');
                    checkREsponse.filter.should.have.property('id');
                    checkREsponse.filter.should.have.property('status');
                    checkREsponse.filter.should.have.property('tokenId');
                    checkREsponse.filter.should.have.property('buttonId');
                    checkREsponse.filter.should.have.property('channelId');
                    checkREsponse.filter.should.have.property('orderId');
                    checkREsponse.filter.should.have.property('sessionId');
                    checkREsponse.filter.should.have.property('error');

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
            it('function _checkParamsFLowFunc should return empty params.filter.id if recived filter.id is not an array', async function () {
                try {
                    let params = {
                        dateRange: [73746746794, 73497846817],
                        filter: {
                            id: 3,
                            status: [],
                            tokenId: [],
                            buttonId: [],
                            channelId: [],
                            orderId: [],
                            sessionId: [],
                            error: []
                        }
                    };

                    const assistantBotClient = buildClient();

                    const checkREsponse = await new CnannelReffStatsReportClass(assistantBotClient)._checkParamsFLowFunc(params);

                    checkREsponse.should.be.a('object');

                    checkREsponse.should.have.property('dateRange');

                    // params.filter
                    checkREsponse.should.have.property('filter');
                    checkREsponse.filter.should.be.a('object');

                    checkREsponse.filter.should.have.property('id');
                    checkREsponse.filter.id.should.be.an('array');
                    checkREsponse.filter.id.should.be.empty;

                    checkREsponse.filter.should.have.property('status');
                    checkREsponse.filter.should.have.property('tokenId');
                    checkREsponse.filter.should.have.property('buttonId');
                    checkREsponse.filter.should.have.property('channelId');
                    checkREsponse.filter.should.have.property('orderId');
                    checkREsponse.filter.should.have.property('sessionId');
                    checkREsponse.filter.should.have.property('error');


                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getTracksFlowFunc function', () => {
            it('_getTracksFlowFunc should returned all tracks and ignore date range if date range length > 2', async () => {
                try {
                    let params = {
                        "dateRange": [64769365946, 74361784671, 427618461515],
                        "filter": {
                            "id": [],
                            "status": [],
                            "tokenId": [],
                            "buttonId": [],
                            "channelId": [],
                            "orderId": [],
                            "sessionId": [],
                            "error": []
                        }
                    }

                    const assistantBotClient = buildClient(null, false);

                    const checkReturnedContract = await new CnannelReffStatsReportClass(assistantBotClient)._getTracksFlowFunc(params);

                    checkReturnedContract.should.be.an('array');
                    // checkReturnedContract.should.be.lengthOf(7);
                } catch (e) {
                    console.error(e.messsage);
                    e.should.be.an('object');
                    e.should.have.property('status', 'ERROR_UNEXP_ERR');
                    e.should.have.property('message', 'Not found any tracks');
                }
            });

            it('_getTracksFlowFunc should returned all tracks if not set filter params or date range', async () => {
                try {
                    let params = {
                        "dateRange": [],
                        "filter": {
                            "id": [],
                            "status": [],
                            "tokenId": [],
                            "buttonId": [],
                            "channelId": [],
                            "orderId": [],
                            "sessionId": [],
                            "error": []
                        }
                    }

                    const assistantBotClient = buildClient(null, false);

                    const checkReturnedContract = await new CnannelReffStatsReportClass(assistantBotClient)._getTracksFlowFunc(params);

                    checkReturnedContract.should.be.an('array');

                } catch (e) {
                    console.error(e);
                    e.should.be.an('object');
                    e.should.have.property('status', 'ERROR_UNEXP_ERR');
                    e.should.have.property('message', 'Not found any tracks');
                }
            });

            it('_getTracksFlowFunc should return all tracks in db if recived empty obj', async () => {
                try {
                    const assistantBotClient = buildClient(null, false); //use not mock client

                    const checkReturnedContract = await new CnannelReffStatsReportClass(assistantBotClient)._getTracksFlowFunc({});

                    checkReturnedContract.should.be.an('array');
                } catch (e) {
                    console.error(e);
                    e.should.be.an('object');
                    e.should.have.property('status', 'ERROR_UNEXP_ERR');
                    e.should.have.property('message', 'Not found any tracks');
                }
            });
        });

        describe('test _reduceToReportShema function', () => {
            it('_reduceToReportShema should return contrtact, if recived null', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const report = await new CnannelReffStatsReportClass(assistantBotClient);

                    const contract = report._reduceToReportShema(null);

                    checkReportContract(contract);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_reduceToReportShema should return contrtact, if recived undefined', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const report = await new CnannelReffStatsReportClass(assistantBotClient);

                    const contract = report._reduceToReportShema(undefined);

                    checkReportContract(contract);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_reduceToReportShema should return contrtact, if not recived uniqueChannelReffStats', async () => {
                try {
                    const params = {
                        statsByReffLink: [],
                        totalChannelReffStats: {}
                    };

                    const assistantBotClient = buildClient();

                    const report = await new CnannelReffStatsReportClass(assistantBotClient);

                    const contract = report._reduceToReportShema(params);

                    checkReportContract(contract);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _completeStatsByLinkBatchFlowFunc function', () => {
            it('_completeStatsByLinkBatchFlowFunc should return contrtact, if recived right incoming data', async () => {
                try {
                    const params = {
                        statsByReffLink: {
                            8: {
                                errorSubscribes: 2,
                                newSubscribes: 0,
                                returnSubscribes: 0,
                                stopSubscribes: 0,
                                uniqueComeUsers: 2
                            }
                        },
                        totalChannelReffStats: {
                            errorSubscribes: 2,
                            newSubscribes: 0,
                            returnSubscribes: 0,
                            stopSubscribes: 0,
                            uniqueComeUsers: 2
                        }
                    };

                    const assistantBotClient = buildClient(null, false);

                    const report = await new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._completeStatsByLinkBatchFlowFunc(params);

                    response.should.have.property('statsByReffLink');
                    response.statsByReffLink.should.be.an('array');

                    response.should.have.property('totalChannelReffStats');
                    response.totalChannelReffStats.should.be.an('object');
                    response.totalChannelReffStats.should.have.property('returnSubscribes');
                    response.totalChannelReffStats.should.have.property('newSubscribes');
                    response.totalChannelReffStats.should.have.property('errorSubscribes');
                    response.totalChannelReffStats.should.have.property('stopSubscribes');
                    response.totalChannelReffStats.should.have.property('uniqueComeUsers');

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getTotalStatsByReffLinkFlowFunc function', () => {
            it('_getTotalStatsByReffLinkFlowFunc should return contrtact, if recived right incoming data', async () => {
                try {
                    const params = {
                        2: {
                            newSubscribes: 1,
                            returnSubscribes: 0,
                            errorSubscribes: 0,
                            stopSubscribes: 0,
                            uniqueComeUsers: 1
                        },
                        4: {
                            newSubscribes: 1,
                            returnSubscribes: 0,
                            errorSubscribes: 0,
                            stopSubscribes: 0,
                            uniqueComeUsers: 1
                        },
                        8: {
                            newSubscribes: 219,
                            returnSubscribes: 9,
                            errorSubscribes: 0,
                            stopSubscribes: 0,
                            uniqueComeUsers: 220
                        }
                    };

                    const assistantBotClient = buildClient(null, false);

                    const report = await new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getTotalStatsByReffLinkFlowFunc(params);

                    response.should.have.property('statsByReffLink');
                    response.statsByReffLink.should.be.an('object');
                    response.statsByReffLink.should.have.property('2');
                    response.statsByReffLink.should.have.property('4');
                    response.statsByReffLink.should.have.property('8');

                    response.should.have.property('totalChannelReffStats');
                    response.totalChannelReffStats.should.be.an('object');
                    response.totalChannelReffStats.should.have.property('returnSubscribes');
                    response.totalChannelReffStats.should.have.property('newSubscribes');
                    response.totalChannelReffStats.should.have.property('errorSubscribes');
                    response.totalChannelReffStats.should.have.property('stopSubscribes');
                    response.totalChannelReffStats.should.have.property('uniqueComeUsers');

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _sortTracksByStatuses function', () => {
            it('_sortTracksByStatuses should return contrtact, if some errors tracks by the same user', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const report = await new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._sortTracksByStatuses(mockTracksWithDiffErrors);

                    response.should.be.an('object');
                    response.should.have.property('returnSubscribes', 2);
                    response.should.have.property('newSubscribes', 1);
                    response.should.have.property('errorSubscribes', 2);
                    response.should.have.property('stopSubscribes', 1);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_sortTracksByStatuses should return contrtact, if all tracks by different user', async () => {
                try {
                    const assistantBotClient = buildClient();

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._sortTracksByStatuses(mockTracksWithDiffErrorsByDiffUsers);

                    response.should.be.an('object');
                    response.should.have.property('returnSubscribes', 2);
                    response.should.have.property('newSubscribes', 1);
                    response.should.have.property('errorSubscribes', 4);
                    response.should.have.property('stopSubscribes', 1);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getStatsDataByReffLinksFlowFunc function', () => {
            it('_getStatsDataByReffLinksFlowFunc should return contrtact by some tracks by the same users', async () => {
                try {
                    const params = {
                        4: mockTracksWithDiffErrors
                    }
                    const assistantBotClient = buildClient();

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getStatsDataByReffLinksFlowFunc(params);

                    response.should.be.an('object');
                    response.should.have.property('4');
                    const e = '4';
                    response[e].should.have.property('newSubscribes', 1);
                    response[e].should.have.property('returnSubscribes', 2);
                    response[e].should.have.property('errorSubscribes', 2);
                    response[e].should.have.property('stopSubscribes', 1);
                    response[e].should.have.property('uniqueComeUsers', 6);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_getStatsDataByReffLinksFlowFunc should return contrtact, if all tracks by different user', async () => {
                try {
                    const params = {
                        4: mockTracksWithDiffErrorsByDiffUsers
                    }
                    const assistantBotClient = buildClient();

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getStatsDataByReffLinksFlowFunc(params);

                    response.should.be.an('object');
                    response.should.have.property('4');
                    const e = '4';
                    response[e].should.have.property('newSubscribes', 1);
                    response[e].should.have.property('returnSubscribes', 2);
                    response[e].should.have.property('errorSubscribes', 4);
                    response[e].should.have.property('stopSubscribes', 1);
                    response[e].should.have.property('uniqueComeUsers', 8);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getStatsDataByChannelFlow function', () => {
            it('_getStatsDataByChannelFlow should return contrtact if recived tracks', async () => {
                try {
                    const assistantBotClient = buildClient(null, false);

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getStatsDataByChannelFlow(mockTracksWithDiffErrorsByDiffUsers);

                    response.should.have.property('statsByReffLink');
                    response.should.have.property('totalChannelReffStats');
                    response.statsByReffLink.should.be.an('array');
                    response.totalChannelReffStats.should.be.an('object');

                    // -- totalChannelReffStats
                    response.totalChannelReffStats.should.have.property('returnSubscribes');
                    response.totalChannelReffStats.should.have.property('newSubscribes');
                    response.totalChannelReffStats.should.have.property('errorSubscribes');
                    response.totalChannelReffStats.should.have.property('stopSubscribes');
                    response.totalChannelReffStats.should.have.property('uniqueComeUsers');

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _buidChannelReffStatsFlowFunc function', () => {
            it('_buidChannelReffStatsFlowFunc should return contrtact if recived tracks', async () => {
                try {
                    const assistantBotClient = buildClient(null, false);

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._buidChannelReffStatsFlowFunc(mockTracksWithDiffErrorsByDiffUsers);

                    checkReportContract(response);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getReffStatsFlow function', () => {
            it('_getReffStatsFlow should return contrtact if null', async () => {
                try {
                    const assistantBotClient = buildClient(null, false);

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getReffStatsFlow(null);

                    response.should.have.property('status', 'ERROR_INVALID_PARAMETER');

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getReffStatsFlow function', () => {
            it('_getReffStatsFlow should return contrtact if recived undefined', async () => {
                try {
                    const assistantBotClient = buildClient(null, false);

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getReffStatsFlow(undefined);

                    response.should.have.property('status', 'ERROR_INVALID_PARAMETER');

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test _getReffStatsFlow function', () => {
            it('_getReffStatsFlow should return contrtact if not found any tracks', async () => {
                try {
                    const params = {
                        dateRange: [],
                        filter: {
                            id: [],
                            status: [],
                            tokenId: [],
                            buttonId: [],
                            channelId: [46386585],
                            orderId: [],
                            sessionId: [],
                            error: []
                        }
                    };
                    const assistantBotClient = buildClient(null, false);

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getReffStatsFlow(params);

                    response.should.have.property('status', 'OK');
                    checkReportContract(response.data);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('_getReffStatsFlow should return contrtact if found tracks', async () => {
                try {
                    const params = {
                        dateRange: [],
                        filter: {
                            id: [],
                            status: [],
                            tokenId: [],
                            buttonId: [],
                            channelId: [],
                            orderId: [],
                            sessionId: [],
                            error: []
                        }
                    };
                    const assistantBotClient = buildClient(null, false);

                    const report = new CnannelReffStatsReportClass(assistantBotClient);

                    const response = await report._getReffStatsFlow(params);

                    response.should.have.property('status', 'OK');
                    checkReportContract(response.data);

                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('test public methods.', () => {
            describe('test build method', () => {
                it('build should return contrtact if not found any tracks', async () => {
                    try {
                        const params = {
                            dateRange: [],
                            filter: {
                                id: [],
                                status: [],
                                tokenId: [],
                                buttonId: [],
                                channelId: [46386585],
                                orderId: [],
                                sessionId: [],
                                error: []
                            }
                        };
                        const assistantBotClient = buildClient(null, false);
    
                        const report = new CnannelReffStatsReportClass(assistantBotClient);
    
                        const response = await report.build(params);
    
                        response.should.have.property('status', 'OK');
                        checkReportContract(response.data);
    
                    } catch (e) {
                        console.error(e);
    
                        throw e;
                    }
                });
    
                it('build should return contrtact if found tracks', async () => {
                    try {
                        const params = {
                            dateRange: [],
                            filter: {
                                id: [],
                                status: [],
                                tokenId: [],
                                buttonId: [],
                                channelId: [],
                                orderId: [],
                                sessionId: [],
                                error: []
                            }
                        };
                        const assistantBotClient = buildClient(null, false);
    
                        const report = new CnannelReffStatsReportClass(assistantBotClient);
    
                        const response = await report.build(params);
    
                        response.should.have.property('status', 'OK');
                        checkReportContract(response.data);
    
                    } catch (e) {
                        console.error(e);
    
                        throw e;
                    }
                });

                it('build should return contrtact if params is null', async () => {
                    try {
                        const assistantBotClient = buildClient(null, false);
    
                        const report = new CnannelReffStatsReportClass(assistantBotClient);
    
                        const response = await report.build(null);
    
                        response.should.have.property('status', 'ERROR_INVALID_PARAMETER');
    
                    } catch (e) {
                        console.error(e);
    
                        throw e;
                    }
                });

                it('build should return contrtact if params is empty object', async () => {
                    try {
                        const assistantBotClient = buildClient(null, false);
    
                        const report = new CnannelReffStatsReportClass(assistantBotClient);
    
                        const response = await report.build({});
    
                        response.should.have.property('status', 'OK');
                        checkReportContract(response.data);
    
                    } catch (e) {
                        console.error(e);
    
                        throw e;
                    }
                });

                it('build should return contrtact if params is undenfined', async () => {
                    try {
                        const assistantBotClient = buildClient(null, false);
    
                        const report = new CnannelReffStatsReportClass(assistantBotClient);
    
                        const response = await report.build(undefined);
    
                        response.should.have.property('status', 'ERROR_INVALID_PARAMETER');
    
                    } catch (e) {
                        console.error(e);
    
                        throw e;
                    }
                });
            });
        });
    });


    process.on('unhandledRejection', (e) => {
        console.log('you forgot to return a Promise! Check your tests!' + e.message)
    })
});
