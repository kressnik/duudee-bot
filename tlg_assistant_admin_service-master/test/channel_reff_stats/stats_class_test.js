let chai = require('chai');
let should = chai.should();

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    ChannelStatsClass
} = require('../../core/stats_class/channel_stats_class/stats_class');

const channelReffStatsManager = new ChannelStatsClass();


describe('ChannelStatsClass units test.', () => {
    before(async () => {
        try {
            let rpcConfigs = {
                host: process.env.RPC_HOST || 'localhost',
                port: process.env.RPC_PORT || 9001
            }

            console.log(`Start init ChannelStatsClass instance`);

            let resInitMan = await channelReffStatsManager.init(rpcConfigs);

            console.log(`End init ${resInitMan.toString()}`);

            resInitMan.should.be.a('object');
            resInitMan.should.have.property('status', ResultStatus.OK);
        } catch (e) {
            console.error(e);

            throw e;
        }
    });

    describe('test public methods.', () => {
        it('function getReffStats should return contract stats', async function () {
            try {
                const params = {

                };

                let statsResponse = await channelReffStatsManager.getChannelReffStats(params);

                //console.log('statsResponse', statsResponse);

                statsResponse.should.be.a('object');
                statsResponse.should.have.property('status', ResultStatus.OK);
                statsResponse.should.have.property('data');

                //contract checking
                const data = statsResponse.data;

                //root
                data.should.have.property('statsByReffLink');
                data.should.have.property('totalChannelReffStats');
                data.should.have.property('uniqueChannelReffStats');

                data.statsByReffLink.should.be.an('array');
                data.totalChannelReffStats.should.be.an('object');
                data.uniqueChannelReffStats.should.be.an('object');

                //totalChannelReffStats
                data.totalChannelReffStats.should.have.property('returnSubscribes');
                data.totalChannelReffStats.should.have.property('newSubscribes');
                data.totalChannelReffStats.should.have.property('errorSubscribes');
                data.totalChannelReffStats.should.have.property('stopSubscribes');
                data.totalChannelReffStats.should.have.property('uniqueComeUsers');

                //uniqueChannelReffStats
                data.uniqueChannelReffStats.should.have.property('returnSubscribes');
                data.uniqueChannelReffStats.should.have.property('newSubscribes');
                data.uniqueChannelReffStats.should.have.property('errorSubscribes');
                data.uniqueChannelReffStats.should.have.property('stopSubscribes');
                data.uniqueChannelReffStats.should.have.property('uniqueComeUsers');

                return
            } catch (e) {
                console.error(e);

                throw e;
            }
        });
    });
});


process.on('unhandledRejection', (e) => {
    console.log('you forgot to return a Promise! Check your tests!' + e.message)
})