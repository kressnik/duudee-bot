/* jshint indent: 2 */

'use strict';

const {
    AppLoggerClass,
    Loglevel
} = __UTILS

const uniqid = require('uniqid');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


var EncryptedField = require('sequelize-encrypted');

// secret key should be 32 bytes hex encoded (64 characters)
const SECRET_KEY = process.env.SECRET_KEY;

if (SECRET_KEY == null) {
    throw Error('Secret key have to been set in env')
}

let Sequelize = require('sequelize');

module.exports = function (sequelize, DataTypes) {
    let enc_fields = EncryptedField(Sequelize, SECRET_KEY);
    let TwoFactor = sequelize.define('twoFactor', {
        encrypted: enc_fields.vault('encrypted'),
        userId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        wasShown: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            default: 0
        },
        token: enc_fields.field('token', {
            type: Sequelize.TEXT,
            defaultValue: ''
        })
    }, {
        hooks: true,
        underscored: true
    });

    TwoFactor.logger = new AppLoggerClass(`SEQUELIZE::TwoFactor`);

    TwoFactor.associate = async function (models) {
        // models.User.sync();

        await this.belongsTo(models.User, {
            foreignKey: {
                name: 'userId',
            }
        });
    }

    TwoFactor.default = async function (models) {
        // try {
        //     let find = await this.findAll();

        //     if (find.length == 0) {
        //         for (let state in STATES) {
        //             let add = await this.createOne({
        //                 id: STATES[state],
        //                 state: state
        //             });

        //             this.logger.info(`Make default for ${state} ${add.toString()}`);
        //         }
        //     }
        // } catch (e) {
        //     let error = new BasicResponseByError(e);

        //     this.logger.error(error.toString());

        //     return error;
        // }
    }

    TwoFactor.createOne = async function (userId) {
        try {
            let [twoFactorInst, isNew] = await this.findOrCreate({
                where: {
                    userId: userId
                },
                defaults: {
                    userId: userId,
                    // token: 'dwqqwdwdqwddwq'
                }
            })

            // let creds = {
            //     key: '111qml;dwmdwl;'
            // }

            // let crStatus = await twoFactorInst.createCred({
            //     name: 'twoFactorKey',
            //     type: 'OBJECT',
            //     value: JSON.stringify(creds)
            // });

            // console.log(crStatus);

            let response = new BasicResponse(ResultStatus.OK, `Add two factor field for user with id:[${userId}] done successfully.`, twoFactorInst.dataValues);

            if (isNew == false) {
                response = new BasicResponse(ResultStatus.OK, `User with id:[${userId}] already added to system`, twoFactorInst.dataValues);
            }

            // this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    TwoFactor.checkFlagById = async function (userId) {
        try {
            let twoFactorInfo = await this.findOne({
                where: {
                    userId: userId
                }
            });

            if (twoFactorInfo == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `twoFactorInfo for user id:[${userId}] not found`);
            };

            let response = new BasicResponse(ResultStatus.OK, `Two factor info for user id:[${userId}] got successfully`, twoFactorInfo);

            // this.logger.info(response.toString());

            return response;

        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    TwoFactor.addSecretKey = async function (userId, token) {
        try {
            let twoFactorInst = await this.findOne({
                where: {
                    userId: userId
                }
            });

            if (twoFactorInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User with id:[${userId}] doesnt found in system.`);
            }

            let result = await twoFactorInst.update({
                token: token,
                wasShown: 1
            });

            let response = new BasicResponse(ResultStatus.OK, `Two factor token for user id:[${userId}] set successfully`, result);

            // this.logger.info(response.toString());

            return response;

        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    TwoFactor.updateOne = async function ({
        wasShown,
        token
    }, id) {
        try {
            let userInst = await this.findByPk(id);

            if (userInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User with id:[${id}] doesnt found in system.`);
            }

            let result = await userInst.update({
                wasShown: wasShown,
                token: token
            });

            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update user properties done successfully.', result.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //updateOne

    TwoFactor.beforeValidate(function (model, options, cb) {

        // if (model.isNewRecord) {
        // if (model.isUseTwoFa == null) {
        //     model.isUseTwoFa = false;
        // }

        // if (model.accessHash == null) {
        //     model.accessHash = uniqid();
        // }

        // if (model._changed.password == true) {
        //     var salt = genSaltSync(process.env.SALT_WORK_FACTOR || 8);


        //     //make crypted password
        //     if (model.password == null) {
        //         throw new Error('Wrong Passwords');
        //     }

        //     model.password = hashSync(model.password, salt);
        // }
        // }

        return model;
    });

    TwoFactor.build();

    return TwoFactor;
};