'use strict';

const {
    AppLoggerClass,
    Loglevel
} = __UTILS

const uniqid = require('uniqid');

const {
    compareSync,
    genSaltSync,
    hashSync
} = require('bcrypt');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

module.exports = function (sequelize, DataTypes) {

    const Op = sequelize.Op;

    var User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        avatarUserLink: {
            type: DataTypes.STRING,
            allowNull: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: true
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true
        },
        tlgUserId: {
            type: DataTypes.INTEGER,
            unique: true,
            allowNull: true
        },
        accessHash: {
            type: DataTypes.STRING,
            allowNull: false
        },
        languageCode: {
            type: DataTypes.STRING,
            allowNull: false
        },
        role: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'administrator'
        },
        state: { //0 - DEACTIVATE||1 - ACTIVE||2 - REMOVED
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
        isUseTwoFa: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            default: false
        },
        lastAuthDate: {
            type: DataTypes.BIGINT,
            allowNull: true
        }
    }, {
        underscored: true,
        hooks: true
    });

    User.logger = new AppLoggerClass(`SEQUELIZE::User`);

    User.associate = async function (models) {

        // models.twoFactor.sync();

        await this.hasOne(models.twoFactor, {
            as: 'twoFactor',
            foreignKey: 'userId',
            onDelete: 'cascade',
            hooks: true
        });

    }

    // as: 'BotTrade',
    // foreignKey: 'order_id',
    // onDelete: 'cascade'

    User.default = async function (models) {
        try {
            let rootUser = await this.findOne({
                where: {
                    username: 'root'
                }
            });

            if (rootUser != null) {
                throw new BasicResponse(ResultStatus.OK, 'User Already added');
            }

            let res = await this.getAll();

            let addNewRoot = await this.createOne({
                firstName: 'mr Root',
                username: 'root',
                password: 'root',
                languageCode: 'ru',
            }, models);

            if (addNewRoot.status != ResultStatus.OK) {
                throw addNewRoot;
            }

            let addTwoFactor = await models.twoFactor.createOne(addNewRoot.data.id);

            this.logger.info(`addTwoFactor field status: ${addTwoFactor.status}`);

            return addNewRoot;
        } catch (e) {
            let error = new BasicResponseByError(e);

            return error;
        }
    } //defaults



    User.getUserIdByHash = async function (hash) {
        try {
            let user = await this.findOne({
                where: {
                    accessHash: hash
                }
            });

            if (user == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User not found by hash:[${hash}]`);
            }

            let response = new BasicResponse(ResultStatus.OK, 'Get user done successfully.', user.dataValues);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    User.getAll = async function (_queryParameters, _systemUserAuthId) {
        try {
            const argsCheck = [_queryParameters.limit, _queryParameters.offset, _queryParameters.order].some(_a => _a == null); //Проверка есть ли обязательные параметры
            let result = new BasicResponse(ResultStatus.ERROR_NOT_READY,
                "Variable is not initialized."
            );

            if (argsCheck == true) { //Проверка получены ли все обязательные параметры
                result = new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER,
                    "Wrong incoming data.",
                    arguments
                );
            } else {

                const defaultWhere = {
                    id: {
                        [Op.ne]: _systemUserAuthId
                    }
                };
                let where = {}

                if (_queryParameters.whereLike.length != 0) { //FIXME

                    let attributesArr = [];
                    where = [];

                    for (let key in this.rawAttributes) {
                        let attributeName = key;
                        let type = this.rawAttributes[key].type.key;

                        if (type === "STRING") {
                            attributesArr[attributesArr.length] = attributeName;
                        }
                    }

                    for (const key in attributesArr) {
                        where.push({
                            [attributesArr[key]]: {
                                [Op.like]: `%${_queryParameters.whereLike}%`
                            }
                        })
                    }

                    where = {
                        [Op.and]: {
                            ...defaultWhere
                        },
                        [Op.or]: where
                    };
                } else {
                    where = {
                        ...defaultWhere
                    }
                }

                let recordsTotal = await this.count({
                    where: defaultWhere
                });

                let resultUsers = await this.findAndCountAll({ //Поиск пользователей с параметрами, которые пришли с интерфейса
                    include: [{
                        model: this.sequelize.models.twoFactor,
                        as: "twoFactor",
                        attributes: ['wasShown']
                    }],
                    limit: parseInt(_queryParameters.limit),
                    offset: parseInt(_queryParameters.offset),
                    where: where
                });

                if (resultUsers.rows.length <= 0) { //Проверка, получен ли хотя бы один сотрудник в результате
                    result = new BasicResponse(ResultStatus.ERROR_NOT_FOUND,
                        "Empty employees list",
                        []
                    );
                } else {

                    const resultData = { //Результаты запросов
                        recordsFiltered: resultUsers.count,
                        recordsTotal: recordsTotal,
                        users: resultUsers.rows.map(_user => _user.dataValues)
                    }

                    result = new BasicResponse(ResultStatus.OK,
                        "Pull data employees",
                        resultData
                    );
                }

            }

            return result;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    } //getAll

    User.createOne = async function ({
        firstName,
        lastName,
        username,
        email,
        tlgUserId,
        languageCode,
        password,
        avatarUserLink,
        role,
        state
    }, models) {
        try {
            let [event, isNew] = await this.findOrCreate({
                where: {
                    username: username
                },
                defaults: {
                    firstName: firstName,
                    lastName: lastName,
                    username: username,
                    tlgUserId: tlgUserId,
                    languageCode: languageCode,
                    email: email,
                    password: password,
                    avatarUserLink: avatarUserLink,
                    role: role,
                    state
                }
            })

            let response = new BasicResponse(ResultStatus.OK, `Add new user:[${username}] done successfully.`, event.dataValues);

            if (isNew == false) {
                response = new BasicResponse(ResultStatus.OK, `User:[${username}] already added to system`, event.dataValues);
            }

            let addTwoFactor = await models.twoFactor.createOne(event.dataValues.id);

            this.logger.info(`addTwoFactor field status: ${addTwoFactor.status}`);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    User.updateOne = async function ({
        firstName,
        lastName,
        username,
        email,
        tlgUserId,
        languageCode,
        password,
        avatarUserLink,
        role,
        state,
        isUseTwoFa,
        lastAuthDate
    }, id) {
        try {
            let userInst = await this.findByPk(id);

            if (userInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User with id:[${id}] doesnt found in system.`);
            }

            let result = await userInst.update({
                firstName: firstName || userInst.name,
                lastName: lastName || userInst.firstName,
                email: email || userInst.email,
                tlgUserId: tlgUserId || userInst.tlgUserId,
                languageCode: languageCode || userInst.languageCode,
                password: password || userInst.password,
                avatarUserLink: avatarUserLink || userInst.avatarUserLink,
                role: userInst.username != "root" ? role : "administrator",
                state: state || userInst.state,
                isUseTwoFa: isUseTwoFa == null ? userInst.isUseTwoFa : isUseTwoFa,
                lastAuthDate
            });

            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update user propserties done succsessfully.', result.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //updateOne

    User.updateAuthLastDate = async function ({
        userId,
        lastAuthDate
    }) {
        try {
            let userInst = await this.findByPk(userId);

            if (userInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User with id:[${userId}] doesnt found in system.`);
            }

            let result = await userInst.update({
                lastAuthDate
            });

            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update user propserties done succsessfully.', result.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //updateAuthLastDate


    User.changeInfo = async function ({
        fullName,
        shortName,
        email,
        avatarUserLink,
        role,
        state
    }, id) {
        try {
            let userInst = await this.findByPk(id);

            if (userInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User with id:[${id}] doesnt found in system.`);
            }

            let result = await userInst.update({
                firstName: fullName || userInst.firstName,
                lastName: shortName || userInst.name,
                email: email || userInst.email,
                avatarUserLink: avatarUserLink || userInst.avatarUserLink,
                role,
                state
            });

            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update user propserties done succsessfully.', result.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //changeInfo


    User.checkingLogin = async function (username) { //Проверка существует ли логин в базе данных
        try {
            const argsCheck = [username].some(_a => _a == null);
            let result = new BasicResponse(ResultStatus.ERROR_NOT_READY,
                "Variable is not initialized."
            );

            if (argsCheck == true) {
                result = new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER,
                    "Wrong incoming data.",
                    arguments
                );
            } else {
                await this.sync();

                username = username.toLowerCase();

                const resUser = await this.findOne({
                    where: {
                        username: username
                    }
                });

                if (resUser != null) {
                    result = new BasicResponse(ResultStatus.ERROR_ALREADY_EXIST,
                        "Erorr user alredy exist"
                    );
                } else {
                    result = new BasicResponse(ResultStatus.OK,
                        "username is free "
                    );
                }
            }

            return result;

        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }



    User.authenticate = async function (username, password) {
        //get user by username
        username = username.toLowerCase();

        const qurey = {
            where: {
                username: username
            }
        };

        try {
            await this.sync();

            let user = await this.findOne(qurey);

            if (user == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND,
                    "username not found",
                    null
                );
            }
            if (user.state != 1) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_ACCESS,
                    "Auth decline",
                    null
                );
            }

            const _isMatch = compareSync(password, user.password)

            if (_isMatch == false) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_ACCESS,
                    "Auth decline",
                    null
                );
            }

            return new BasicResponse(ResultStatus.OK,
                "Auth successful",
                user
            );
        } catch (_error) {
            let error = new BasicResponseByError(_error);

            return error;
        }
    } //validPassword

    User.getProps = async function (id) {
        try {
            let user = await this.findByPk(id, {
                include: [{
                    model: this.sequelize.models.twoFactor,
                    as: "twoFactor",
                    attributes: ['wasShown']
                }]
            });

            if (user == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `user with id:[${id}] not found`);
            }

            user.accessHash = null;
            user.password = null;

            return new BasicResponse(ResultStatus.OK, 'User found', user);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    //passport 
    User.serializeUser = function (_user, _done) {
        _done(null, _user);
    }

    //passsport 
    User.deserializeUser = (_models) => {
        return function (_user, _done) {
            const qurey = {
                where: {
                    id: _user.id
                }
            };

            _models.User.findOne(qurey)
                .then(_user => {
                    _done(null, _user);
                })
                .catch(_error => {
                    done(_error, null);
                });
        }
    } //DES


    User.deleteOne = async function (id) {
        try {
            let user = await this.findOne({
                where: {
                    id: id
                }
            });

            if (user == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User  with id:[${id}] not found in to db.`)
            }

            let del = await user.destroy();

            if (del == false) {
                throw new BasicResponse(ResultStatus.ERROR, `User  with id:[${id}] not found in to db.`);
            }

            return new BasicResponse(ResultStatus.OK, 'Remove user in db done succsessfully.', user.dataValues)
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //deleteOne

    User.changePasswordNotCheckOld = async function (newPassword, userId) {
        try {

            let user = await this.findByPk(userId);

            if (user == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `user with id:[${id}] not found`);
            };

            //var salt = genSaltSync(process.env.SALT_WORK_FACTOR || 8);

            //user.password = hashSync(newPassword, salt);

            await this.update({
                password: newPassword
            }, {
                where: {
                    id: user.id
                }
            });

            return new BasicResponse(ResultStatus.OK, 'Changing password done successfully', user);

        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    User.changePassword = async function (password, userId) {
        try {

            let user = await this.findByPk(userId);

            if (user == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `user with id:[${id}] not found`);
            };

            const _isMatch = compareSync(password.old, user.password);

            if (_isMatch === false) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_ACCESS, `Password does not match`, null);
            }

            //var salt = genSaltSync(process.env.SALT_WORK_FACTOR || 8);

            //user.password = hashSync(password.reenter, salt);

            await this.update({
                password: password.reenter
            }, {
                where: {
                    id: user.id
                }
            });

            return new BasicResponse(ResultStatus.OK, 'Changing password done successfully', user);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    //
    User.beforeValidate(function (model, options, cb) {

        if (model.isNewRecord) {
            if (model.isUseTwoFa == null) {
                model.isUseTwoFa = false;
            }

            if (model.accessHash == null) {
                model.accessHash = uniqid();
            }

            if (model._changed.password == true) {
                var salt = genSaltSync(process.env.SALT_WORK_FACTOR || 8);


                //make crypted password
                if (model.password == null) {
                    throw new Error('Wrong Passwords');
                }

                model.password = hashSync(model.password, salt);
            }
        }

        return model;
    });

    User.beforeCreate((_user, _opt) => {
        // console.log('beforeCreate', _user, _opt);
    }) //beforeCreate


    return User;
};