'use strict';
const {
    AppLoggerClass
} = __UTILS;


const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

/**
 * DEFINES
 */

module.exports = function (sequelize, DataTypes) {

    const Op = sequelize.Op;

    let version = sequelize.define('version', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },       
        version: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'base'
            
        }
    },
    { timestamps: false });

    version.logger = new AppLoggerClass(`SEQUELIZE::SubscribeOrder`);

    version.get = async function () {
        try {
         
            let version = await this.findAll();

            if(version.length == 0) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, "version DB not found");
            }

            return new BasicResponse(ResultStatus.OK, "Get version succsesfully", version[0].dataValues);
        } catch (_error) {
            return new BasicResponseByError(_error);
        }
    }
    
    version.versionUpdate = async function (_id, _version) {
        try {

            if(_id == null) {
                await this.create({version: _version});

            } else {
                await this.update({
                    version: _version
                }, {
                    where: {
                            id: _id
                    }
                });
            }
         
            return new BasicResponse(ResultStatus.OK, "Update version succsesfully");
        } catch (_error) {
            return new BasicResponseByError(_error);
        }
    }
   
    return version;
};