module.exports.up = async function up(queryInterface, Sequelize) {
    try {
        await queryInterface.addColumn(
            'users',
            'last_auth_date', {
                type: Sequelize.BIGINT,
                allowNull: true
            }
        );
    } catch (_error) {
        console.error(_error);
    }

};

module.exports.down = async function down(queryInterface, Sequelize) {
    try {
        await queryInterface.removeColumn(
            'users',
            'last_auth_date'
        );
    } catch (_error) {
        console.error(_error);
    }
};