'use strict';

module.exports.up = async function up(queryInterface, Sequelize) {
    try {
        console.log('Start up function for version table');

        await queryInterface.createTable(
            'versions', {
                id: {
                    type: Sequelize.DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                version: {
                    type: Sequelize.DataTypes.STRING,
                    allowNull: false,
                    defaultValue: 'base'
                }
            }
        )
    } catch (_error) {
        console.error(_error);
    }

};

module.exports.down = async function down(queryInterface, Sequelize) {
    try {
        console.log('Start down function for version table');

        await queryInterface.dropTable('versions');
    } catch (_error) {
        console.error(_error);
    }
};