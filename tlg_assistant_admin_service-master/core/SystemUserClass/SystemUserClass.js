'use strict'

const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    waterfall
} = require('async');

const {
    PassportClass
} = require('../PassportClass/PassportClass');

const {
    RouteListClass
} = require('./RouteListClass');

const Speakeasy = require("speakeasy");

const fileSystem = require('../../utils/fileSystem');

var UPLOAD_PHOTO_DIR = ["uploads", "user-avatar"];

const QRCode = require('qrcode');

class SystemUserClass {
    constructor() {
        this._db = {};
        this._passport = new PassportClass();

        this.logger = new AppLoggerClass('SystemUserClass', process.env.SYSTEM_USER_CLASS_LOG_LEVEL);

        this.routeList = new RouteListClass();
    }

    getUser() {
        try {
            let user = this._db.models.getUser(id);
        } catch (error) {

        }
    }

    async init(express, db) {
        try {
            if (express == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Express doesn`t set.');
            }

            this.express = express;
            this._db = db;


            let initFlow = await this.initFlow(express, db);

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //init

    async initFlow(express, db) {
        return new Promise((resolve, reject) => {
            waterfall([
                this.initPassportFlowFunc.bind(this, express, db),
                this.initRouteListFlowFunc.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this.logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init core done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this.logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    }

    async initRouteListFlowFunc() {
        try {
            this.routeList.init();

            let rsp = new BasicResponse(ResultStatus.OK, 'Read user list done succssfully.(fake)');

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    } //start

    async initPassportFlowFunc(express, db) {
        try {
            this.logger.info('Start');

            let resIinitDB = await this._passport.init(express, db);

            if (resIinitDB.status != ResultStatus.OK) {
                throw resIinitDB;
            }

            this.logger.info(`${resIinitDB.toString()}`);

            return resIinitDB;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    } //initConnnectionToDbFlowFunc


    async authenticateRequestFlow(request, strategy = 'local') {
        try {
            // check auth
            if (request.isAuthenticated()) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED);
            }
            //make auth
            let authUser = await this._passport.authenticate(request, strategy);

            if (authUser.status != ResultStatus.OK) {
                throw authUser;
            }

            let user = authUser.data;

            if (user.isUseTwoFa == true) {

                if (request.body.twoFactorCode == null) {
                    throw new BasicResponse(ResultStatus.SUCCESS_NO_CONTENT, 'Need 2FA to login user', {});
                }

                let validateCode = await this.validateTwoFactorCode(user.id, request.body.twoFactorCode);

                if (validateCode != true) {
                    throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, '2FA code is invalid', validateCode);
                }
            }

            authUser.data.password = undefined;

            let login = (user) => {
                return new Promise(resolve => {
                    request.login(user, function (err) {
                        if (err != null) {
                            return resolve(new BasicResponse(ResultStatus.ERROR, `Get auth error: ${err.message}`, err));
                        }

                        return resolve(new BasicResponse(ResultStatus.OK, 'Make local login done', user))
                    });
                })
            }
            //make login
            let loginInfo = await login(user);

            if (loginInfo.status != ResultStatus.OK) {
                throw loginInfo;
            }

            const updAuthDate = await this._db.models.User.updateAuthLastDate({
                userId: user.id,
                lastAuthDate: Date.now()
            });

            if (updAuthDate.isOK() == false) {
                throw updateAuthLastDate;
            }

            console.log(updAuthDate);

            return new BasicResponse(ResultStatus.OK, 'Login done successfully', loginInfo);
        } catch (e) {
            let error = new BasicResponseByError(e);

            if (error.status == ResultStatus.SUCCESS_NO_CONTENT) {
                this.logger.warn(error.toString());
            } else {
                this.logger.error(error.toString());
            }

            throw error;
        }
    }

    async validateTwoFactorCode(id, key) {
        try {
            let getFaInfo = await this._db.models.twoFactor.checkFlagById(id);

            if (getFaInfo.status != ResultStatus.OK) {
                throw getFaInfo;
            }

            let valid = Speakeasy.totp.verify({
                secret: getFaInfo.data.token,
                encoding: "base32",
                token: key,
                window: 0
            })

            return valid;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    }

    async getUsers(_req) {
        try {

            const queryData = _req.query; //Параметры запроса
            const drawNumber = _req.query.draw; //Временный номер для dataTables
            let searchValue = queryData.search.value;
            let queryParameters = { //Параметры для поиска данных в БД
                limit: queryData.length, //Количество записей
                offset: queryData.start, //Позиция с которой начинается получения данных
                order: [],
                whereLike: searchValue
            }

            if (queryData.order != null && queryData.order.length != 0) { //Проверка есть ли способы сортировки

                let columnsSortListOrder = { //Поля по которым может выполнятся сортировка
                    id: "id",
                    avatar: "id",
                    name: "username",
                    email: "email",
                    role: "role",
                    lang: "lang"
                }
                let orderFirst = queryData.order[0];
                let orderColumn = queryData.columns[orderFirst.column];
                let columnSort = columnsSortListOrder[orderColumn.data];

                if (columnSort != null) {
                    queryParameters.order.push([columnSort, orderFirst.dir]);
                }
            }

            let systemUserBatch = { //Дефолтный батч для возврата списка сотрудников
                recordsTotal: 0, //Количество записей в БД
                recordsFiltered: 0, //Количество записей после поиска
                draw: drawNumber,
                data: [] //Список сотрудников
            }

            let resSystemUser = await this._db.models.User.getAll(queryParameters, _req.user.id); //Получения сотрудников с БД

            if (resSystemUser.status == ResultStatus.ERROR_NOT_FOUND) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, 'Getting users done successfully', systemUserBatch);
            }

            if (resSystemUser.status != ResultStatus.OK) {
                throw resSystemUser;
            }

            let resultData = resSystemUser.data.users.map((_user) => { //Перебор списка пользователей и создания объекта нужного формата.
                return {
                    id: _user.id,
                    avatar: (_user.avatarUserLink || ""),
                    email: (_user.email || ""),
                    role: (_user.role || ""),
                    lang: (_user.languageCode || ""),
                    state: _user.state,
                    lastAuthorization: (_user.lastAuthorization || ""),
                    deletedDate: (_user.deletedDate || ""),
                    isUseTwoFa: (_user.isUseTwoFa),
                    twoFactor: (_user.twoFactor),
                    name: {
                        fullName: (_user.fullName || ""),
                        shortName: (_user.shortName || ""),
                        username: (_user.username || "")
                    }
                };
            });

            systemUserBatch = {
                draw: drawNumber,
                recordsTotal: resSystemUser.data.recordsTotal,
                recordsFiltered: resSystemUser.data.recordsFiltered,
                data: resultData
            }


            return new BasicResponse(ResultStatus.OK, 'Getting users done successfully', systemUserBatch);

        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getUsers

    async createNewUser(_req) {
        try {
            var systemUserInfo = _req.body;

            if (systemUserInfo == null) { //Проверка пришли ли данные с интерфейса
                throw systemUserInfo;
            }

            let resultUploadAvatar = await fileSystem.uploadAvatarFile(systemUserInfo.avatarUser, null, UPLOAD_PHOTO_DIR); //Загрузка аватарки для системного пользователя

            if (resultUploadAvatar.status == ResultStatus.OK) { //Проверка загрузилась ли аватарка пользователя
                systemUserInfo.avatarUserLink = resultUploadAvatar.data.link;
                systemUserInfo.avatarUser = null;
            }

            let checkingLogin = await this._db.models.User.checkingLogin(systemUserInfo.username); //Проверка существует ли такой логин в базе

            if (checkingLogin.status != ResultStatus.OK) {
                throw checkingLogin;
            }

            let resAdd = await this._db.models.User.createOne(systemUserInfo, this._db.models); //Добавления пользователя

            if (resAdd.status != ResultStatus.OK) {
                throw resAdd;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Adding user done successfully`, resAdd.data);

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //createNewUser

    async getUserProps(id) {
        try {

            let getUserInfo = await this._db.models.User.getProps(id);

            if (getUserInfo.status != ResultStatus.OK) {
                throw getUserInfo;
            }

            let rsp = new BasicResponse(ResultStatus.OK, getUserInfo.message, getUserInfo.data);

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async changeUserProps(id, userInfo, user) {
        try {

            let resultGetUser = await this._db.models.User.getProps(id);

            if (resultGetUser.status != ResultStatus.OK) {
                throw resultGetUser;
            }

            if (userInfo.avatarUser == null) {
                userInfo.avatar = null;
            } else {

                let resultUploadAvatar = await fileSystem.uploadAvatarFile(userInfo.avatarUser, resultGetUser.data.avatarUserLink, UPLOAD_PHOTO_DIR);

                if (resultUploadAvatar.status == ResultStatus.OK) {
                    user.avatarUserLink = resultUploadAvatar.data.link;
                    userInfo.avatarUserLink = resultUploadAvatar.data.link;
                    userInfo.avatar = null;
                }
            }

            let resUpdate = await this._db.models.User.updateOne(userInfo, resultGetUser.data.id);
            //reducer
            if (resUpdate.status != ResultStatus.OK) {
                throw resUpdate;
            }

            if (userInfo.password.length > 0
                // resultGetUser.data.id != _req.user.id
            ) {
                let resUpdatePassword = await this._db.models.User.changePasswordNotCheckOld(userInfo.password, resultGetUser.data.id);

                //reducer
                if (resUpdatePassword.status != ResultStatus.OK) {
                    throw resUpdatePassword;
                }
            }

            let rsp = new BasicResponse(ResultStatus.OK, resUpdate.message, resUpdate.data);

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async deleteOneUser(id) {
        try {

            let resultDeleteUser = await this._db.models.User.deleteOne(id);

            if (resultDeleteUser.status != ResultStatus.OK) {
                throw resultDeleteUser;
            }

            let rsp = new BasicResponse(ResultStatus.OK, resultDeleteUser.message, resultDeleteUser.data);

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //deleteOneUser

    async changePassword(password, id) {
        try {

            let resChangePass = await this._db.models.User.changePassword(password, id);

            if (resChangePass.status != ResultStatus.OK) {
                throw resChangePass;
            }

            return new BasicResponse(ResultStatus.OK, resChangePass.message, resChangePass.data);

        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //deleteOneUser

    async changeUserInfo(userInfo, user) {
        try {
            let resultUploadAvatar = await fileSystem.uploadAvatarFile(userInfo.avatar, user.avatarUserLink, UPLOAD_PHOTO_DIR);

            if (resultUploadAvatar.status == ResultStatus.OK) {
                user.avatarUserLink = resultUploadAvatar.data.link;
                userInfo.avatarUserLink = resultUploadAvatar.data.link;
                userInfo.avatar = null;
            }

            let resUpdate = await this._db.models.User.changeInfo(userInfo, user.id);

            if (resUpdate.status != ResultStatus.OK) {
                throw resUpdate;
            }

            let rsp = new BasicResponse(ResultStatus.OK, resUpdate.message, resUpdate.data);

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async checkTwoFactorFlag(id) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Id is null');
            }

            let getFlag = await this._db.models.twoFactor.checkFlagById(id);

            if (getFlag.status != ResultStatus.OK) {
                throw getFlag;
            }

            if (getFlag.data.wasShown != null) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_EXIST, 'Two factor QR is already shown');
            }

            return getFlag;
        } catch (e) {
            let error = new BasicResponseByError(e);

            if (error.status != ResultStatus.ERROR_ALREADY_EXIST) {
                this.logger.error(error.toString());
            }

            return error;
        }
    }

    async generateTwoFactorKey(id) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Id is null');
            }

            let getFlag = await this._db.models.twoFactor.checkFlagById(id);

            if (getFlag.status != ResultStatus.OK) {
                throw getFlag;
            }

            if (getFlag.data.wasShown != null) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_EXIST, 'Two factor QR is already shown');
            }

            let secret = Speakeasy.generateSecret({
                length: 20
            });

            let createInDb = await this._db.models.twoFactor.addSecretKey(id, secret.base32);

            if (createInDb.status != ResultStatus.OK) {
                throw createInDb;
            }

            // let stub = {
            //     isUseTwoFa: true
            // }

            // await this._db.models.User.updateOne(stub, id);

            let qrUrl = Speakeasy.otpauthURL({
                secret: secret.ascii,
                label: process.env.TWO_FA_SECRET_NAME || 'Bot manager'
            });

            let img = await new Promise((resolve, reject) => {
                QRCode.toDataURL(qrUrl, function (err, image_data) {
                    if (err) {
                        reject(err);
                    }
                    resolve(image_data);
                });
            })

            let authStub = {
                img,
                secret
            }

            let rsp = new BasicResponse(ResultStatus.OK, 'Generate token for 2fa done successfully', authStub);

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            if (error.status == ResultStatus.ERROR_ALREADY_EXIST) {
                this.logger.warn(error.toString());
            } else {
                this.logger.error(error.toString());
            }

            return error;
        }
    }

    async changeTwoFactorState(id, state, code) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Id is null');
            }

            let resultGetUser = await this._db.models.User.getProps(id);

            if (resultGetUser.status != ResultStatus.OK) {
                throw resultGetUser;
            }

            if (resultGetUser.data.isUseTwoFa == state) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_EXIST, '2FA already in this state', {});
            }

            let stub = {
                isUseTwoFa: state
            }

            let validateCode = await this.validateTwoFactorCode(id, code);

            if (validateCode != true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, '2FA code is invalid', validateCode);
            }

            let changeState = await this._db.models.User.updateOne(stub, id)

            if (changeState.status != ResultStatus.OK) {
                throw changeState;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Change 2FA state user with [${id}] for [${state}] done successfully`, {});

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            if (error.status != ResultStatus.ERROR_ALREADY_EXIST) {
                this.logger.error(error.toString());
            }

            return error;
        }
    } //changeTwoFactorState


    async changeSystemUserTwoFactorState(id, state) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Id is null');
            }

            let resultGetUser = await this._db.models.User.getProps(id);

            if (resultGetUser.status != ResultStatus.OK) {
                throw resultGetUser;
            }

            if (resultGetUser.data.isUseTwoFa == state) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_EXIST, '2FA already in this state', {});
            }

            if (resultGetUser.data.twoFactor != null &&
                resultGetUser.data.twoFactor.wasShown != 1
            ) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_EXIST,
                    'The key for two-factor authorization was not found. Activation of the mode is not possible.', {}
                );
            }

            let stub = {
                isUseTwoFa: state
            }

            let changeState = await this._db.models.User.updateOne(stub, id)

            if (changeState.status != ResultStatus.OK) {
                throw changeState;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Change 2FA state user with [${id}] for [${state}] done successfully`, {});

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            if (error.status != ResultStatus.ERROR_ALREADY_EXIST) {
                this.logger.error(error.toString());
            }

            return error;
        }
    } //changeSystemUserTwoFactorState

    async dropUserTwoFactorAuth(id) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Id is null');
            }

            let resultGetUser = await this._db.models.User.getProps(id);

            if (resultGetUser.status != ResultStatus.OK) {
                throw resultGetUser;
            }

            let stub = {
                isUseTwoFa: false
            }

            let changeState = await this._db.models.User.updateOne(stub, id);

            if (changeState.status != ResultStatus.OK) {
                throw changeState;
            }

            let clearStub = {
                wasShown: null,
                token: null
            }

            let clearShowFlag = await this._db.models.twoFactor.updateOne(clearStub, id);

            if (clearShowFlag.status != ResultStatus.OK) {
                throw clearShowFlag;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Drop 2FA for user [${id}] done successfully`, {});

            // this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

} //SystemUserclass





module.exports.SystemUserClass = SystemUserClass;