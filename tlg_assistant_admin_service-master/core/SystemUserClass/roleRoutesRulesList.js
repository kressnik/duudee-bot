module.exports = {
    administrator: [
        {
            "index": 0,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/logout"
        },
        {
            "index": 1,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/login"
        },
        {
            "index": 2,
            "method": "OPTIONS",
            "accept": true,
            "options": true,
            "path": "/login"
        },
        {
            "index": 3,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/helth-check"
        },
        {
            "index": 4,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/langs/:langName"
        },
        {
            "index": 5,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/langs"
        },
        {
            "index": 6,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pools"
        },
        {
            "index": 7,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pool/:id"
        },
        {
            "index": 8,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/bots"
        },
        {
            "index": 9,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pools/dataTable"
        },
        {
            "index": 10,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pool/:id/bots"
        },
        {
            "index": 11,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pool/:poolId/bot/:botId/params"
        },
        {
            "index": 12,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/pool"
        },
        {
            "index": 13,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/bot"
        },
        {
            "index": 14,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/pool/:poolId/addBot"
        },
        {
            "index": 15,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/bot/:botId/state/:state"
        },
        {
            "index": 16,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/bot/:botId/params"
        },
        {
            "index": 17,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/bot/:botId/creds"
        },
        {
            "index": 18,
            "method": "DELETE",
            "accept": true,
            "delete": true,
            "path": "/pool/:id"
        },
        {
            "index": 19,
            "method": "DELETE",
            "accept": true,
            "delete": true,
            "path": "/pool/:poolId/bot/:botId"
        },
        {
            "index": 20,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/:version/rpc"
        },
        {
            "index": 21,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/totp-secret"
        },
        {
            "index": 22,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/totp-generate"
        },
        {
            "index": 23,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/totp-validate"
        },
        {
            "index": 24,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/twoFactor/:state"
        },
        {
            "index": 25,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/user/:id/twoFactor/:state"
        },
        {
            "index": 26,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/user/:id/dropTwoFactor"
        },
        {
            "index": 27,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/user/language"
        },
        {
            "index": 28,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/user/menu"
        },
        {
            "index": 29,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/user/password"
        },
        {
            "index": 30,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/user/info"
        },
        {
            "index": 31,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/user"
        },
        {
            "index": 32,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/users"
        },
        {
            "index": 33,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/user/:id"
        },
        {
            "index": 34,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/user/:id"
        },
        {
            "index": 35,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/user"
        },
        {
            "index": 36,
            "method": "DELETE",
            "accept": true,
            "delete": true,
            "path": "/user/:id"
        },
        {
            "index": 37,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/state/:state"
        },
        {
            "index": 38,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/totp-generate-secret-key/:systemUser"
        },
        {
            "index": 122,
            "method": "GET",
            "get": true,
            "path": "/status",
            "accept": true
        },
        {
            "index": 123,
            "method": "GET",
            "get": true,
            "path": "/",
            "accept": true
        },
        {
            "index": 124,
            "method": "GET",
            "get": true,
            "path": "/uploads/employees-avatar/:file",
            "accept": true
        },
        {
            "index": 125,
            "method": "GET",
            "get": true,
            "path": "/uploads/user-avatar/:file",
            "accept": true
        },

    ],
    manager: [
        {
            "index": 0,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/logout"
        },
        {
            "index": 1,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/login"
        },
        {
            "index": 2,
            "method": "OPTIONS",
            "accept": true,
            "options": true,
            "path": "/login"
        },
        {
            "index": 3,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/helth-check"
        },
        {
            "index": 4,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/langs/:langName"
        },
        {
            "index": 5,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/langs"
        },
        {
            "index": 6,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pools"
        },
        {
            "index": 7,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pool/:id"
        },
        {
            "index": 8,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/bots"
        },
        {
            "index": 9,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pools/dataTable"
        },
        {
            "index": 10,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pool/:id/bots"
        },
        {
            "index": 11,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/pool/:poolId/bot/:botId/params"
        },
        {
            "index": 12,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/pool"
        },
        {
            "index": 13,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/bot"
        },
        {
            "index": 14,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/pool/:poolId/addBot"
        },
        {
            "index": 15,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/bot/:botId/state/:state"
        },
        {
            "index": 16,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/bot/:botId/params"
        },
        {
            "index": 17,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/bot/:botId/creds"
        },
        {
            "index": 18,
            "method": "DELETE",
            "accept": true,
            "delete": true,
            "path": "/pool/:id"
        },
        {
            "index": 19,
            "method": "DELETE",
            "accept": true,
            "delete": true,
            "path": "/pool/:poolId/bot/:botId"
        },
        {
            "index": 20,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/:version/rpc"
        },
        {
            "index": 21,
            "method": "POST",
            "accept": true,
            "post": true,
            "path": "/totp-secret"
        },
        {
            "index": 22,
            "method": "POST",
            "accept": false,
            "post": false,
            "path": "/totp-generate"
        },
        {
            "index": 23,
            "method": "POST",
            "accept": false,
            "post": false,
            "path": "/totp-validate"
        },
        {
            "index": 24,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/twoFactor/:state"
        },
        {
            "index": 25,
            "method": "PUT",
            "accept": false,
            "put": false,
            "path": "/user/:id/twoFactor/:state"
        },
        {
            "index": 26,
            "method": "PUT",
            "accept": false,
            "put": false,
            "path": "/user/:id/dropTwoFactor"
        },
        {
            "index": 27,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/user/language"
        },
        {
            "index": 28,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/user/menu"
        },
        {
            "index": 29,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/user/password"
        },
        {
            "index": 30,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/user/info"
        },
        {
            "index": 31,
            "method": "GET",
            "accept": true,
            "get": true,
            "path": "/user"
        },
        {
            "index": 32,
            "method": "GET",
            "accept": false,
            "get": false,
            "path": "/users"
        },
        {
            "index": 33,
            "method": "GET",
            "accept": false,
            "get": false,
            "path": "/user/:id"
        },
        {
            "index": 34,
            "method": "PUT",
            "accept": false,
            "put": false,
            "path": "/user/:id"
        },
        {
            "index": 35,
            "method": "POST",
            "accept": false,
            "post": false,
            "path": "/user"
        },
        {
            "index": 36,
            "method": "DELETE",
            "accept": false,
            "delete": false,
            "path": "/user/:id"
        },
        {
            "index": 37,
            "method": "PUT",
            "accept": true,
            "put": true,
            "path": "/pool/:poolId/state/:state"
        },
        {
            "index": 122,
            "method": "GET",
            "get": true,
            "path": "/status",
            "accept": true
        },
        {
            "index": 123,
            "method": "GET",
            "get": true,
            "path": "/",
            "accept": true
        },
        {
            "index": 124,
            "method": "GET",
            "get": true,
            "path": "/uploads/employees-avatar/:file",
            "accept": true
        },
        {
            "index": 125,
            "method": "GET",
            "get": true,
            "path": "/uploads/user-avatar/:file",
            "accept": true
        },
    ]
}