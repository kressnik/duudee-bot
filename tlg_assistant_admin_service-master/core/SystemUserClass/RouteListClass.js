'use strict';

const Router = require('url-router');

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require('../../interfaces/basicResponse');

class RouteListClass {
    constructor() {
        this._list = require('./roleRoutesRulesList');
        this._router = {};
    }

    init() {
        //makes routes for all roles
        for (let role in this._list) {
            let list = this._list[role].map(l => {
                return [
                    l.method || 'GET',
                    l.path,
                    () => l
                ];
            })

            this._router[role] = new Router(list);
        }
    }

    checkAccess(role, method, route) {
        try {
            let isNull = [role, method, route].some(a => a == null);

            if (isNull) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Some arguments is null`);
            }

            //filetering last symbol if it '/' - because int route use withot it
            if (route.substr(-1) == '/') {
                route = route.slice(0, route.length - 1);
            }

            const rules = this._router[role];

            if (rules == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Role:[${role}] doenst found in system.`);
            }

            const _route = rules.find(method, route);

            if (_route == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Route:[${route}], Role:[${role}] doenst found in system.`);
            }

            let routeInfo = _route.handler()

            if (routeInfo == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Route:[${route}], Role:[${role}] doenst found in system.`);
            }

            if (routeInfo.accept == false) {
                throw new BasicResponse(ResultStatus.ERROR_ACCESS_DENIED, `Route:[${route}], Methods:[${method}], Role:[${role}] access denied.`);
            }

            return new BasicResponse(ResultStatus.OK, `Route:[${route}], Methods:[${method}], Role:[${role}] access granded.`);
        } catch (e) {
            return new BasicResponseByError(e);
        }
    } //checkAccess
} //RouteListClass

exports.RouteListClass = RouteListClass;