const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const jayson = require('jayson/promise');


const {
    AppLoggerClass
} = __UTILS;

const DEF_RPC_URL = 'rpc';
const DEF_RPC_VER = 'v1';

const {
    RpcStatusCode
} = require('./RpcProcessorStruct')

const {
    RpcMockClientClass
} = require('./rpc_mock_client_bot_class');

class AssistantBotClient {
    constructor({
        host,
        port,
        mocksVersion,
        useMocks = false,
    }) {
        this.logger = new AppLoggerClass('AssistantBotClient');

        this.props = {
            host,
            port,
            useMocks,
            version: DEF_RPC_VER,
            opt: () => {
                return {
                    path: `/${this.props.version}/${DEF_RPC_URL}`,
                    host: this.props.host,
                    port: this.props.port,
                }
            },
            url: () => `http://${this.props.host}:${this.props.port}/${this.props.version}/${DEF_RPC_URL}`
        }

        this.client = jayson.client.http(this.props.opt());

        if (useMocks == false) {
            this.client = jayson.client.http(this.props.opt());
        } else {
            this.client = new RpcMockClientClass(mocksVersion);
        }
    } //constructor

    _reduceErrorRpcToBasic(e) {
        try {
            switch (e.code) {
                case RpcStatusCode.NOT_FOUND:
                    return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, e.message, e.data);
                case RpcStatusCode.SERVER_ERROR_INVALID_METHOD_PARAMETERS:
                    return new BasicResponse(ResultStatus.ERROR_INVALID_DATA, e.message, e.data);
                default:
                    return new BasicResponseByError(e);
            }
        } catch (error) {
            return new BasicResponseByError(error);
        }
    }

    updateConnProps({
        host,
        port
    }) {
        try {
            this.props.host = host || this.props.host;
            this.props.port = port || this.props.port;

            this.client = jayson.client.http(this.props.opt());

            let rsp = new BasicResponse(ResultStatus.OK, 'Update connection props done successfully', {});

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async ping(params) {
        try {
            let request = await this.client.request('ping', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getChannels(params) {
        try {
            let request = await this.client.request('channels.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getUsers(params) {
        try {
            let request = await this.client.request('users.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getSubscribes(params) {
        try {
            let request = await this.client.request('subscribe.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async addSubscribe(params) {
        try {
            let request = await this.client.request('subscribe.create.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getDepositInfo(params) {
        try {
            let request = await this.client.request('deposit.get.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async createNewChannel(params) {
        try {
            let request = await this.client.request('channel.create.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async editChannelInfo(params) {
        try {
            let request = await this.client.request('channel.edit.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    async deleteChannel(params) {
        try {
            let request = await this.client.request('channel.delete.state.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async editStateChannel(params) {
        try {
            let request = await this.client.request('channel.edit.state.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    async getInviteActions(params) {
        try {
            let request = await this.client.request('invite.actions.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getUsersByLink(params) {
        try {
            let request = await this.client.request('users.by.link.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async updateDepositStatus(params) {
        try {
            let request = await this.client.request('deposit.update.status', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getOrders(params) {
        try {
            let request = await this.client.request('subscribe.order.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getOrders = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getOrders);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getDepositStatuses() {
        try {
            let request = await this.client.request('deposit.get.statuses', {});

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, [...request.result.data]);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getOrdersStatuses() {
        try {
            let request = await this.client.request('subscribe.order.get.statuses', {});

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            //reduce
            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, [...request.result.data]);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async editOrderInfo(params) {
        try {
            let request = await this.client.request('order.update.info', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let getStDto = {
                ...request.result.data
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, getStDto);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    /**
     * Refferal 
     */

    /**
     * 
     */
    async getReferalLinksTypes() {
        try {
            let request = await this.client.request('refferal.link.get.types', {});

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            //reduce
            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, [
                ...request.result.data,
            ]);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getReferalLinksTypes

    /**
     * 
     * @param {*} params 
     */
    async getAllReferalLinks(params) {
        try {
            let request = await this.client.request('refferal.link.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAllReferalLinks

    async getReffLinkById(params) {
        try {
            let request = await this.client.request('refferal.link.get.by.id', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAllReferalLinks

    /**
     * 
     * @param {*} params 
     */
    async createReferalLink(params) {
        try {
            let request = await this.client.request('refferal.link.create.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            //reduce
            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //createReferalLink

    /**
     * 
     * @param {*} id link id for delete
     */
    async deleteReferalLink(params) {
        try {
            let request = await this.client.request('refferal.link.delete.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //deleteReferalLink

    /**
     * 
     * @param {*} params 
     */
    async editReferalLink(params) {
        try {
            let request = await this.client.request('refferal.link.edit.one', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }
            //reduce
            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //editReferalLink
    /**
     * 
     * @param {*} params 
     */
    async getAllTracks(params) {
        try {
            let request = await this.client.request('refferal.track.get.all', params);

            if (request.error != null) {
                throw this._reduceErrorRpcToBasic(request.error);
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any data.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, [
                ...request.result.data
            ]);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAllTracks

    async getAllTracksWithPagin(params) {
        try {
            let request = await this.client.request('refferal.track.get.all.with.pagination', params);

            if (request.error != null) {
                throw this._reduceErrorRpcToBasic(request.error);
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any data.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAllTracksWithPagin

    /**
     * Buttons 
     */

    /**
     * 
     * @param {*} params 
     */
    async getTelgramButtonList(params) {
        try {
            let request = await this.client.request('buttons.get.all', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            //reduce
            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, [
                ...request.result.data
            ]);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getTelgramButtonList

    async getWatcherAuthStatus() {
        try {
            let request = await this.client.request('channel.watcher.auth.get.status', {});

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //editReferalLink

    async startWatcherAuth(params) {
        try {
            let request = await this.client.request('channel.watcher.auth.send.start', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //editReferalLink

    async sendWatcherAuthCode(params) {
        try {
            let request = await this.client.request('channel.watcher.auth.send.code', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //editReferalLink

    async sendWatcherUserApiInfo(params) {
        try {
            let request = await this.client.request('channel.watcher.send.api.info', params);

            if (request.error) {
                throw request.error;
            }

            if (request.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any content for parse.');
            }

            if (request.result.status != ResultStatus.OK) {
                throw request.result;
            }

            let rsp = new BasicResponse(ResultStatus.OK, `${request.result.message}`, {
                ...request.result.data
            });

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //editReferalLink
} //AssistantBotClient


exports.AssistantBotClient = AssistantBotClient;