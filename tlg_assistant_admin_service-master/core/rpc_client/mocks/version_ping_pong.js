module.exports = {
    version: 'test_ping_pong',
    methods: {
        "ping": function (params) {
            return {
                jsonrpc: "2.0",
                id: 1586989273,
                result: {
                    status: "OK",
                    message: "Pong",
                    data: {
                        pong: params
                    }
                }
            }
        }
    }
}