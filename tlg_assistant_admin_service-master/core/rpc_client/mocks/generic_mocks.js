module.exports = {
    version: 'generic_mocks',
    methods: {
        'channels.get.all': function (params) {
            return {
                "jsonrpc": "2.0",
                "id": 1587054597,
                "result": {
                    "status": "OK",
                    "message": "Get channels with offset:[null] limit:[null] done successfully.",
                    "data": {
                        "count": 4,
                        "rows": [{
                                "id": 1,
                                "name": "Private test",
                                "description": "for test FREE",
                                "inUse": true,
                                "tlgId": 1416522845,
                                "price": 5500,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 100,
                                "isFree": false,
                                "maxSubcribesQty": 10,
                                "createdAt": "2020-01-05T21:03:23.000Z",
                                "updatedAt": "2020-01-05T21:03:23.000Z"
                            },
                            {
                                "id": 2,
                                "name": "free test",
                                "description": "for test PRIVATE",
                                "inUse": true,
                                "tlgId": 1416522845,
                                "price": 5000,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 100,
                                "isFree": true,
                                "maxSubcribesQty": 10,
                                "createdAt": "2020-01-05T21:03:23.000Z",
                                "updatedAt": "2020-01-05T21:03:23.000Z"
                            },
                            {
                                "id": 3,
                                "name": "simple private",
                                "description": "for test FREE",
                                "inUse": true,
                                "tlgId": 1416522845,
                                "price": 150,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 100,
                                "isFree": false,
                                "maxSubcribesQty": 10,
                                "createdAt": "2020-01-05T21:03:23.000Z",
                                "updatedAt": "2020-01-05T21:03:23.000Z"
                            },
                            {
                                "id": 4,
                                "name": "simple free",
                                "description": "for test FREE",
                                "inUse": true,
                                "tlgId": 1416522845,
                                "price": 100,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 100,
                                "isFree": true,
                                "maxSubcribesQty": 10,
                                "createdAt": "2020-01-05T21:03:23.000Z",
                                "updatedAt": "2020-01-05T21:03:23.000Z"
                            }
                        ]
                    }
                }
            }
        },
        'refferal.track.get.all': function (params = {}) {
            if (params.mock) {
                return params.mock;
            }

            return {
                "jsonrpc": "2.0",
                "id": 1587056750,
                "result": {
                    "status": "OK",
                    "message": "Get tracks from db done done successfully",
                    "data": [{
                        "id": 15,
                        "status": "done",
                        "error": null,
                        "stage": 1,
                        "isNewUser": false,
                        "createdAt": "2020-04-15T17:27:07.000Z",
                        "updatedAt": "2020-04-15T17:27:14.000Z",
                        "userId": 1,
                        "tokenId": 4,
                        "buttonId": 4,
                        "channelId": 1,
                        "sessionId": null,
                        "token": {
                            "id": 4,
                            "link": "without_captcha",
                            "type": "channel",
                            "description": null,
                            "isDelete": false,
                            "isActive": true,
                            "title": "cxcx",
                            "createdAt": "2020-04-08T16:43:27.000Z",
                            "updatedAt": "2020-04-08T16:43:27.000Z",
                            "channelId": 3,
                            "buttonId": 5
                        },
                        "button": {
                            "id": 4,
                            "isActive": false,
                            "isRefferal": null,
                            "name": "📢 SIMPLE | Бесплатно",
                            "description": "1",
                            "userGroup": 1,
                            "handler": "enter_simple_sub_free_channel",
                            "createdAt": "2019-10-01T20:36:49.000Z",
                            "updatedAt": "2019-10-01T20:36:49.000Z"
                        },
                        "session": null,
                        "channel": {
                            "id": 1,
                            "name": "Private test",
                            "description": "for test FREE",
                            "inUse": true,
                            "tlgId": 1416522845,
                            "price": 5500,
                            "currency": "RUB",
                            "color": "",
                            "feeAtOwnExpense": false,
                            "subscriptionDuration": 100,
                            "isFree": false,
                            "maxSubcribesQty": 10,
                            "createdAt": "2020-01-05T21:03:23.000Z",
                            "updatedAt": "2020-01-05T21:03:23.000Z"
                        },
                        "order": {
                            "id": 43,
                            "status": "done",
                            "invoker": "telegram",
                            "sourceId": 1416522845,
                            "feeAtOwnExpense": false,
                            "isFree": false,
                            "price": "5500.00",
                            "currency": "RUB",
                            "fxRate": "0.00",
                            "description": "subscribe 1416522845 free",
                            "timeStamp": 1586971633985,
                            "sn": "uso_5vconmk91lxpoi",
                            "createdAt": "2020-04-15T17:27:13.000Z",
                            "updatedAt": "2020-04-15T17:27:13.000Z",
                            "userId": 1,
                            "depositId": null,
                            "channelId": 1,
                            "trackId": 15
                        }
                    }]
                }
            }
        }
    }
}