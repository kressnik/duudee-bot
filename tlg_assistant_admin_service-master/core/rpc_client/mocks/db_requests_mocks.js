module.exports = {
    version: 'db_requests_mocks',
    methods: {
        'channels.get.all': function (params) {
            return {
                "jsonrpc": "2.0",
                "id": 1587054597,
                "result": {
                    "status": "OK",
                    "message": "Get channels with offset:[null] limit:[null] done successfully.",
                    "data": {
                        "count": 4,
                        "rows": [
                            {
                                "id": 2,
                                "name": "Бадминтон",
                                "description": "cdc",
                                "inUse": true,
                                "tlgId": 1493159989,
                                "price": 0,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 2190,
                                "isFree": true,
                                "maxSubcribesQty": 1,
                                "createdAt": "2019-10-04T20:00:55.000Z",
                                "updatedAt": "2019-10-04T20:00:55.000Z"
                            },
                            {
                                "id": 3,
                                "name": "Солянка",
                                "description": "cdc",
                                "inUse": true,
                                "tlgId": 1479784149,
                                "price": 0,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 2190,
                                "isFree": true,
                                "maxSubcribesQty": 1,
                                "createdAt": "2019-10-04T20:00:55.000Z",
                                "updatedAt": "2019-10-04T20:00:55.000Z"
                            },
                            {
                                "id": 4,
                                "name": "Теннис",
                                "description": "cdc",
                                "inUse": true,
                                "tlgId": 1212245516,
                                "price": 0,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 2190,
                                "isFree": true,
                                "maxSubcribesQty": 1,
                                "createdAt": "2019-10-04T20:00:55.000Z",
                                "updatedAt": "2019-10-04T20:00:55.000Z"
                            },
                            {
                                "id": 5,
                                "name": "Рейтинг Капперов",
                                "description": "cdc",
                                "inUse": true,
                                "tlgId": 1446163591,
                                "price": 0,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 2190,
                                "isFree": true,
                                "maxSubcribesQty": 1,
                                "createdAt": "2019-10-04T20:00:55.000Z",
                                "updatedAt": "2019-10-04T20:00:55.000Z"
                            },
                            {
                                "id": 6,
                                "name": "Футбол",
                                "description": "cdc",
                                "inUse": false,
                                "tlgId": 1337665992,
                                "price": 0,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 2190,
                                "isFree": true,
                                "maxSubcribesQty": 1,
                                "createdAt": "2019-10-04T20:00:55.000Z",
                                "updatedAt": "2019-10-04T20:00:55.000Z"
                            },
                            {
                                "id": 7,
                                "name": "Иван Петрович ЖБ",
                                "description": "cdc",
                                "inUse": true,
                                "tlgId": 1400792953,
                                "price": 0,
                                "currency": "RUB",
                                "color": "",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 2190,
                                "isFree": true,
                                "maxSubcribesQty": 1,
                                "createdAt": "2019-10-04T20:00:55.000Z",
                                "updatedAt": "2019-10-04T20:00:55.000Z"
                            },
                            {
                                "id": 10,
                                "name": "Новости",
                                "description": "",
                                "inUse": true,
                                "tlgId": 1457839633,
                                "price": 0,
                                "currency": "RUB",
                                "color": "rgba(241, 196, 15, 0.2)",
                                "feeAtOwnExpense": false,
                                "subscriptionDuration": 2190,
                                "isFree": true,
                                "maxSubcribesQty": 1,
                                "createdAt": "2020-04-08T23:13:19.000Z",
                                "updatedAt": "2020-04-08T23:13:19.000Z"
                            }
                        ]
                    }
                }
            }
        },
        'refferal.track.get.all': function (params) {
            return {
                "jsonrpc": "2.0",
                "id": 1587056750,
                "result": {
                    "status": "OK",
                    "message": "Get tracks from db done done successfully",
                    "data": [
                        {
                            "id": 385,
                            "status": "done",
                            "error": null,
                            "stage": 0,
                            "isNewUser": true,
                            "createdAt": "2020-04-09T13:54:59.000Z",
                            "updatedAt": "2020-04-09T13:54:59.000Z",
                            "userId": 440,
                            "tokenId": 8,
                            "buttonId": 5,
                            "channelId": 10,
                            "sessionId": null,
                            "token": {},
                            "button": {},
                            "session": null,
                            "channel": {
                                "id": 10,
                                "name": "Новости",
                                "color": "rgba(241, 196, 15, 0.2)",
                            }
                        },
                        {
                            "id": 386,
                            "status": "done",
                            "error": null,
                            "stage": 0,
                            "isNewUser": false,
                            "createdAt": "2020-04-09T14:42:43.000Z",
                            "updatedAt": "2020-04-09T14:42:43.000Z",
                            "userId": 4323,
                            "tokenId": 8,
                            "buttonId": 5,
                            "channelId": 10,
                            "sessionId": null,
                            "token": {},
                            "button": {},
                            "session": null,
                            "channel": {
                                "id": 10,
                                "name": "Новости",
                                "color": "rgba(241, 196, 15, 0.2)"
                            },
                            "order": {}
                        },
                        {
                            "id": 387,
                            "status": "done",
                            "error": null,
                            "stage": 0,
                            "isNewUser": false,
                            "createdAt": "2020-04-09T14:44:38.000Z",
                            "updatedAt": "2020-04-09T14:44:38.000Z",
                            "userId": 7321,
                            "tokenId": 9,
                            "buttonId": 5,
                            "channelId": 10,
                            "sessionId": null,
                            "token": {},
                            "button": {},
                            "session": null,
                            "channel": {
                                "id": 10,
                                "name": "Новости",
                                "color": "rgba(241, 196, 15, 0.2)"
                            },
                            "order": {}
                        },
                        {
                            "id": 389,
                            "status": "done_error",
                            "error": "ERROR_ALREADY_ASSIGNED",
                            "stage": 0,
                            "isNewUser": false,
                            "createdAt": "2020-04-09T14:47:18.000Z",
                            "updatedAt": "2020-04-09T14:47:18.000Z",
                            "userId": 8455,
                            "tokenId": 8,
                            "buttonId": 5,
                            "channelId": 10,
                            "sessionId": null,
                            "token": {},
                            "button": {},
                            "session": null,
                            "channel": {
                                "id": 10,
                                "name": "Новости",
                                "color": "rgba(241, 196, 15, 0.2)"
                            },
                            "order": {}
                        },
                        {
                            "id": 3859,
                            "status": "done_error",
                            "error": "ERROR_ALREADY_ASSIGNED",
                            "stage": 0,
                            "isNewUser": false,
                            "createdAt": "2020-04-09T14:47:18.000Z",
                            "updatedAt": "2020-04-09T14:47:18.000Z",
                            "userId": 440,
                            "tokenId": 8,
                            "buttonId": 5,
                            "channelId": 10,
                            "sessionId": null,
                            "token": {},
                            "button": {},
                            "session": null,
                            "channel": {
                                "id": 10,
                                "name": "Новости",
                                "color": "rgba(241, 196, 15, 0.2)"
                            },
                            "order": {}
                        },
                        {
                            "id": 448,
                            "status": "done_error",
                            "error": "ERROR_INVALID_DATA",
                            "stage": 0,
                            "isNewUser": true,
                            "createdAt": "2020-04-09T20:46:48.000Z",
                            "updatedAt": "2020-04-09T20:46:48.000Z",
                            "userId": 6128,
                            "tokenId": 8,
                            "buttonId": 5,
                            "channelId": null,
                            "sessionId": null,
                            "token": {},
                            "button": {},
                            "session": null,
                            "channel": null,
                            "order": null
                        },
                        {
                            "id": 487,
                            "status": "done",
                            "error": null,
                            "stage": 0,
                            "isNewUser": false,
                            "createdAt": "2020-04-10T13:23:29.000Z",
                            "updatedAt": "2020-04-10T13:23:29.000Z",
                            "userId": 154,
                            "tokenId": 8,
                            "buttonId": 5,
                            "channelId": 6,
                            "sessionId": null,
                            "token": {},
                            "button": {},
                            "session": null,
                            "channel": {
                                "id": 6,
                                "name": "Smth",
                                "color": "rgba(241, 196, 15, 0.2)"
                            },
                            "order": {}
                        }
                    ]
                }
            }
        }
    }
}