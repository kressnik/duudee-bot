module.exports.

RpcStatusCode = Object.freeze({
    PARSE_ERROR_NOT_WELL_FORMED: -32700,  // -32700 ---> parse error. not well formed
    PARSE_ERROR_UNSUPPORTED_ENCODING: -32701,     // -32701 ---> parse error. unsupported encoding
    PARSE_ERROR_INVALID_CHARACTER_FOR_ENCODING:  -32702,   // -32702 ---> parse error. invalid character for encoding
    SERVER_ERROR_INVALID_XML_RPC: -32702,    // -32600 ---> server error. invalid xml-rpc. not conforming to spec.
    SERVER_ERROR_REQUESTED_METHOD_NOT_FOUND: -32601,  // -32601 ---> server error. requested method not found
    SERVER_ERROR_INVALID_METHOD_PARAMETERS: -32602,    // -32602 ---> server error. invalid method parameters
    SERVER_ERROR_INTERNAL_XML_RPC_ERROR: -32603,   // -32603 ---> server error. internal xml-rpc error
    APPLICATION_ERROR:  -32500,  // -32500 ---> application error
    SYSTEM_ERROR:  -32400,  // -32400 ---> system error
    TRANSPORT_ERROR: -32300,   // -32300 ---> transport error
    //-32000 to -32099
    NOT_FOUND: -32000 // Oobject not found
});


