`use strict`

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);

const MOCKS_DIR = path.join(__dirname, 'mocks');

class RpcMockClientClass {
    constructor(version) {
        this._version = version;

        this._mocks = {};

        this._loadMocks();
    }

    _loadMocks() {
        fs.readdirSync(MOCKS_DIR)
            .filter(file => {
                return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
            })
            .forEach(file => {
                const mocks = require(path.join(MOCKS_DIR, file));

                this._mocks[mocks.version] = mocks.methods;
            });
    }

    async request(methods, params) {
        try {
            const mocks = this._mocks[this._version];

            if (mocks == null) {
                throw new Error('Method in this version not found');
            }

            if (mocks[methods] == null) {
                throw new Error('Method in this version not found');
            }

            return mocks[methods](params);
        } catch (e) {
            console.error(e);

            throw e;
        }
    }
}


module.exports.RpcMockClientClass = RpcMockClientClass;