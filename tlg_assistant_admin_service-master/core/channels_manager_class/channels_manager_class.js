'use strict'

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    AppLoggerClass
} = __UTILS;

const {
    AssistantBotClient
} = require('../rpc_client/assistant_bot_client');

class ChannelsManagerClass {
    constructor() {
        this.logger = new AppLoggerClass('ChannelsManagerClass', process.env.CHANNELS_MANAGER_CLASS_LOG_LEVEL);

        this.rpcClient = null;
    }

    async initRpcClient(configs) {
        try {
            this.logger.info('Start');

            this.rpcClient = new AssistantBotClient({
                host: configs.host,
                port: configs.port
            });

            return new BasicResponse(ResultStatus.OK, 'Init AssistantBotClient done successfully', {});
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //initRpcClient

    async getAllChannels(params) {
        try {
            let rpcCallInfo = await this.rpcClient.getChannels(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async deleteChannel(params) {
        try {
            let rpcCallInfo = await this.rpcClient.deleteChannel(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getAllUsers(params) {
        try {

            let rpcCallInfo = await this.rpcClient.getUsers(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getAllSubscribes(params) {
        try {

            let rpcCallInfo = await this.rpcClient.getSubscribes(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async addSubscribe(params) {
        try {
            let rpcCallInfo = await this.rpcClient.addSubscribe(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }



    async getDepositInfo(params) {
        try {
            let rpcCallInfo = await this.rpcClient.getDepositInfo(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async createNewChannel(params) {
        try {

            if (params.type == 'PAYED') {
                params.isFree = 0;
            } else if (params.type == 'FREE') {
                params.isFree = 1;
                params.price = 0;
            }

            let rpcCallInfo = await this.rpcClient.createNewChannel(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async editChannelInfo(params) {
        try {

            if (params.type == 'PAYED') {
                params.isFree = false;
            } else if (params.type == 'FREE') {
                params.isFree = true;
                params.price = 0;
            }

            let rpcCallInfo = await this.rpcClient.editChannelInfo(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async editStateChannel(params) {
        try {

            let rpcCallInfo = await this.rpcClient.editStateChannel(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getInviteActions(params) {
        try {
            let rpcCallInfo = await this.rpcClient.getInviteActions(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getReffLinkTracksByFilter(params) {
        try {
            let rpcCallInfo = await this.rpcClient.getAllTracks(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getReffLinkTracksByFilterWithPagin(params) {
        try {
            let rpcCallInfo = await this.rpcClient.getAllTracksWithPagin(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getUsersByLink(params) {
        try {
            let rpcCallInfo = await this.rpcClient.getUsersByLink(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async updateDepositStatus(params) {
        try {
            let rpcCallInfo = await this.rpcClient.updateDepositStatus(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }
    async getOrders (params) {
        try {
            let rpcCallInfo = await this.rpcClient.getOrders(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getDepositStatuses() {
        try {
            let rpcCallInfo = await this.rpcClient.getDepositStatuses();

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getTelgramButtonList() {
        try {
            let rpcCallInfo = await this.rpcClient.getTelgramButtonList();

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getOrdersStatuses() {
        try {
            let rpcCallInfo = await this.rpcClient.getOrdersStatuses();

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async editOrderInfo(params) {
        try {
            let rpcCallInfo = await this.rpcClient.editOrderInfo(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getReferalLinksTypes() {
        try {
            let rpcCallInfo = await this.rpcClient.getReferalLinksTypes();

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getAllReferalLinks(params) {
        try {
            let rpcCallInfo = await this.rpcClient.getAllReferalLinks(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async createReferalLink(params) {
        try {
            let rpcCallInfo = await this.rpcClient.createReferalLink(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async deleteReferalLink(params) {
        try {
            let rpcCallInfo = await this.rpcClient.deleteReferalLink(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async editReferalLink(params) {
        try {
            let rpcCallInfo = await this.rpcClient.editReferalLink(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async getWatcherAuthStatus() {
        try {
            let rpcCallInfo = await this.rpcClient.getWatcherAuthStatus({});

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async startWatcherAuth(params) {
        try {
            let rpcCallInfo = await this.rpcClient.startWatcherAuth(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async sendWatcherAuthCode(params) {
        try {
            let rpcCallInfo = await this.rpcClient.sendWatcherAuthCode(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    async sendWatcherUserApiInfo(params) {
        try {
            let rpcCallInfo = await this.rpcClient.sendWatcherUserApiInfo(params);

            if (rpcCallInfo.status != ResultStatus.OK) {
                throw rpcCallInfo;
            }

            return rpcCallInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }
}

exports.ChannelsManagerClass = ChannelsManagerClass;