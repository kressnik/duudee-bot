'use strict'

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

const {
    AppLoggerClass
} = __UTILS;

const {
    AssistantBotClient
} = require('../../rpc_client/assistant_bot_client');

const {
    StatisticsReportClass
} = require('./tracks_stats_report_class');

class PublicMethods {
    async getReffStats(args) {
        try {
            this._logger.debug('Call public methods get refferal stats');

            const report = await new StatisticsReportClass(this._rpcClient).build(args);

            this._logger.debug('Retrun response for public methods get refferal stats');

            return report;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(`Catch error when calling public methods get refferal stats: ${error}`);

            return error;
        }
    } //getReffStats
}


class RefferalStatsClass extends PublicMethods {
    constructor() {
        super();

        this._logger = new AppLoggerClass('RefferalStatsClass', process.env.STATS_MANAGER_CLASS_LOG_LEVEL); // wtf with log levels?
        this._rpcClient = null;
    }

    /**
     * 
     * @param {*} configs host and port for connection wiht bot client
     */
    async init(configs) {
        try {
            this._logger.info('Start');

            this._rpcClient = new AssistantBotClient({
                host: configs.host,
                port: configs.port
            });

            return new BasicResponse(ResultStatus.OK, 'Init AssistantBotClient done successfully', {});
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } // initRpcClient
}

exports.RefferalStatsClass = RefferalStatsClass;