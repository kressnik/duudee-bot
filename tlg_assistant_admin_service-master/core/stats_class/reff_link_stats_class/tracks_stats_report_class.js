'use strict'

const {
    AppLoggerClass,
    Loglevel
} = __UTILS;

const {
    reduceByItemToArr,
    calcPersent,
    TRACK,
    ONE_HUNDRED_PERCENT,
    ITEMS_FOR_REDUCE
} = require("../stats_helpers");

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

const {
    waterfall
} = require('async');


class StatisticsReportClass {
    constructor(client, loglevel = Loglevel.ERROR) {
        this._rpcClient = client;
        this._logger = new AppLoggerClass('StatisticsReportClass', loglevel);
    }

    async build(params) {
        try {
            const report = await this._getReffStatsFlow(params);

            report.isOK();

            return report;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(`Catch error when calling public methods get refferal stats: ${error}`);

            return error;
        }
    }

    _reduceToReportShema(values = {}) {
        if (values == null) {
            values = {}
        };

        const {
            newUsers = 0,
                returnUsers = 0,
                totalUsers = 0,
                channelsStats = [],
                channelsTotalStats = {}
        } = values;

        const {
            commingRate = 0,
                lossRate = 0,
                returnSubscribes = 0,
                subscribes = 0,
                uniqUsersSubscribes = 0,
                usersWithRetSubsc = 0,
                usersWithSubsc = 0
        } = channelsTotalStats;

        const shema = {
            newUsers,
            returnUsers,
            totalUsers,
            channelsStats,
            channelsTotalStats: {
                commingRate,
                lossRate,
                returnSubscribes,
                subscribes,
                uniqUsersSubscribes,
                usersWithRetSubsc,
                usersWithSubsc
            }
        };

        return shema;
    }

    /**
     * 
     * @param {*} _params obj of params for get tracks
     */
    async _getReffStatsFlow(_params) {
        return new Promise((resolve, reject) => {
            try {
                waterfall([
                    this._checkParamsFLowFunc.bind(this, _params),
                    this._getReffStatsRawDataFlowFunc.bind(this),
                    this._buidReffStatsFlowFunc.bind(this)
                ], (error, result) => {
                    try {
                        if (error != null &&
                            error.isStatus(ResultStatus.ERROR_NOT_FOUND) == false
                        ) {
                            throw error;
                        }

                        const rsp = new BasicResponse(ResultStatus.OK, 'Getting refferal stats done successfully', this._reduceToReportShema(result));

                        this._logger.info(rsp);

                        resolve(rsp);
                    } catch (e) {
                        let error = new BasicResponseByError(e);

                        this._logger.error(error.toString());

                        resolve(error);
                    }
                });
            } catch (e) {
                let error = new BasicResponseByError(e);

                this._logger.error(error.toString());

                resolve(error);
            }
        })
    } // getReffStatsFlow

    async _checkParamsFLowFunc(_params) {
        try {
            if (_params == null ||
                Array.isArray(_params)
            ) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Incoming parameters should be an object');
            }

            let dateRange = Array.isArray(_params.dateRange) && _params.dateRange.length <= 2 ? _params.dateRange : [];

            let filter = {
                id: [],
                status: [],
                tokenId: [],
                buttonId: [],
                channelId: [],
                orderId: [],
                sessionId: [],
                error: []
            };

            for (let key in _params.filter) {
                filter[key] = Array.isArray(_params.filter[key]) ? _params.filter[key] : [];
            };

            let params = {
                dateRange,
                filter
            };

            return params
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } // _checkParams

    async _getReffStatsRawDataFlowFunc(_params) {
        try {
            const rpcCallInfo = await this._rpcClient.getAllTracks(_params);

            if (rpcCallInfo.status == ResultStatus.ERROR_UNEXP_ERR &&    //need fix!!!!!
                rpcCallInfo.message == 'Not found any tracks'
            ) {
                rpcCallInfo.status = ResultStatus.ERROR_NOT_FOUND;
                throw rpcCallInfo;
            }
            
            rpcCallInfo.isOK();

            return rpcCallInfo.data;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _buidReffStatsFlowFunc(_tracks) {
        try {
            if (_tracks == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Tracks should be an array.');
            }

            if (_tracks.length == 0) {
                return new BasicResponse(ResultStatus.OK, 'Refferal link tracks not found');
            }

            let totalChannelsStats = {};

            const [resCommingStats = {}, statsByChannels] = await Promise.all([
                this._calcCommingStats(_tracks),
                this._calcStatsByChannels(_tracks)
            ]);

            const {
                newUsers = 0,
                    returnUsers = 0,
                    totalUsers = 0
            } = resCommingStats;

            let resTotalChannelsStats = await this._calcTotalChannelsStats(statsByChannels);

            if (resTotalChannelsStats.status == ResultStatus.OK) {
                totalChannelsStats = resTotalChannelsStats.data;
            }


            let resPersentStats = this._calcCommingAndLossRate({
                totalUsers,
                uniqUsersSubscribes: totalChannelsStats.uniqUsersSubscribes || 0
            });

            if (resPersentStats.status == ResultStatus.OK) {
                totalChannelsStats = {
                    ...totalChannelsStats,
                    ...resPersentStats.data
                };
            }

            return {
                newUsers,
                returnUsers,
                totalUsers,
                channelsStats: statsByChannels,
                channelsTotalStats: totalChannelsStats
            };
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _calcCommingStats(_tracks) {
        try {
            let newUsersTracks = [];
            let oldUsersTracks = [];
            let uniqueUsersTracks = [];
            let totalUniqueUsers = 0;
            let countNewUsersTracks = 0;
            let countOldUsersTracks = 0;

            newUsersTracks = _tracks.reduce((recivedArr, currentTrack) => {
                if (currentTrack.isNewUser == false) {
                    oldUsersTracks.push(currentTrack);

                    return recivedArr;
                }
                recivedArr.push(currentTrack);

                return recivedArr;
            }, [])

            newUsersTracks = newUsersTracks.length != 0 ? reduceByItemToArr(newUsersTracks, ITEMS_FOR_REDUCE.USER_ID) : [];
            oldUsersTracks = oldUsersTracks.length != 0 ? reduceByItemToArr(oldUsersTracks, ITEMS_FOR_REDUCE.USER_ID) : [];
            uniqueUsersTracks = _tracks.length != 0 ? reduceByItemToArr(_tracks, ITEMS_FOR_REDUCE.USER_ID) : [];

            countNewUsersTracks = newUsersTracks.length;
            countOldUsersTracks = oldUsersTracks.length;
            totalUniqueUsers = uniqueUsersTracks.length;

            return {
                newUsers: countNewUsersTracks,
                returnUsers: countOldUsersTracks,
                totalUsers: totalUniqueUsers
            }
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async _calcStatsByChannels(_tracks) {
        try {
            const resGetChannels = await this._rpcClient.getChannels({});

            resGetChannels.isOK();

            const {
                rows = []
            } = resGetChannels.data;

            const channelsStatsPromisesBatch = rows.map(async (channel) => {
                let channelId = channel.id;
                let channelObj = {
                    name: channel.name,
                    id: channelId,
                    color: channel.color,
                    tracksStats: {
                        subscribes: 0,
                        returnSubscribes: 0
                    }
                };

                let thisChannelTracks = _tracks.filter((track) => {
                    return track.channelId == channelId;
                });

                channelObj.tracksStats = await this._calcChannelsSubscRequest(thisChannelTracks);

                return channelObj;
            })

            return await Promise.all(channelsStatsPromisesBatch);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.warn(error.toString());

            return [];
        }
    }

    /**
     * 
     * @param {*} _statsByChannels array of stats all channels
     */
    async _calcTotalChannelsStats(_statsByChannels) {
        try {
            let totalSubsc = {
                usersWithRetSubsc: 0,
                usersWithSubsc: 0,
                returnSubscribes: 0,
                subscribes: 0,
                uniqUsersSubscribes: 0
            };

            let usersWithRetSubsc = [];
            let usersWithSubsc = [];

            for (const channel of _statsByChannels) {
                let thisReturnedSubsc = channel.tracksStats.returnSubscribes;
                let thisDoneSubsc = channel.tracksStats.subscribes;
                let thisSubscUsersIds = channel.tracksStats.subscUsersIds;
                let thisReturnSubscUsersIds = channel.tracksStats.returnSubscUsersIds;

                totalSubsc.returnSubscribes += thisReturnedSubsc;
                totalSubsc.subscribes += thisDoneSubsc;

                usersWithRetSubsc = [...usersWithRetSubsc, ...thisReturnSubscUsersIds];
                usersWithSubsc = [...usersWithSubsc, ...thisSubscUsersIds];

                delete channel.tracksStats.subscUsersIds;
                delete channel.tracksStats.returnSubscUsersIds;
            };

            totalSubsc.usersWithRetSubsc = new Set([...usersWithRetSubsc]).size;
            totalSubsc.usersWithSubsc = new Set([...usersWithSubsc]).size;
            totalSubsc.uniqUsersSubscribes = new Set([...usersWithRetSubsc, ...usersWithSubsc]).size;

            return new BasicResponse(ResultStatus.OK, 'Calc total stats by channels done successfully', totalSubsc);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    /**
     * 
     * @param {*} params totalUser (number), uniqUsersSubscribes (number)
     */
    _calcCommingAndLossRate(params = {}) {
        try {
            let totalUsers = params != null ? params.totalUsers || 0 : 0;
            let usersWithReqToSubsc = params != null ? params.uniqUsersSubscribes || 0 : 0;

            let commingRate = calcPersent(totalUsers, usersWithReqToSubsc);

            let resultRates = {
                commingRate,
                lossRate: parseFloat((ONE_HUNDRED_PERCENT - commingRate).toFixed(2))
            };

            return new BasicResponse(ResultStatus.OK, 'Calc comming and loss rate done successfully', resultRates);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    /**
     * 
     * @param {*} _channelTracks array of all tracks by one channels
     */
    async _calcChannelsSubscRequest(_channelTracks) {
        try {
            let tracksStats = {
                subscUsersIds: [],
                returnSubscUsersIds: [],
                subscribes: 0,
                returnSubscribes: 0
            };
            let chanSubscTracks = [];
            let chanReturnSubscTracks = [];

            chanSubscTracks = _channelTracks.filter((track) => {
                return track.status == TRACK.STATUS.DONE; // will add constanta
            });

            chanReturnSubscTracks = _channelTracks.filter((track) => {
                return track.status == TRACK.STATUS.DONE_ERROR && track.error == TRACK.ERROR.ERROR_ALREADY_ASSIGNED;
            });

            tracksStats.subscUsersIds = chanSubscTracks.length != 0 ? reduceByItemToArr(chanSubscTracks, ITEMS_FOR_REDUCE.USER_ID) : [];
            tracksStats.returnSubscUsersIds = chanReturnSubscTracks.length != 0 ? reduceByItemToArr(chanReturnSubscTracks, ITEMS_FOR_REDUCE.USER_ID) : [];

            tracksStats.subscribes = tracksStats.subscUsersIds.length;
            tracksStats.returnSubscribes = tracksStats.returnSubscUsersIds.length;

            return tracksStats;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
} //StatisticsReportClass

module.exports.StatisticsReportClass = StatisticsReportClass;