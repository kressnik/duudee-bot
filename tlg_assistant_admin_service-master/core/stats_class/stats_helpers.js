'use strict'

const ONE_HUNDRED_PERCENT = 100;

const ITEMS_FOR_REDUCE = Object.freeze({
    USER_ID: 'userId',
    TOKEN_ID: 'tokenId',
    ERROR: 'error',
    STATUS: 'status'
});

const TRACK = Object.freeze({
    STATUS: {
        DONE: "done",
        DONE_ERROR: "done_error"
    },
    ERROR: {
        NULL: null,
        ERROR_ALREADY_ASSIGNED: 'ERROR_ALREADY_ASSIGNED',
        ERROR_RECIVE_STOP_SIGNAL: 'ERROR_RECIVE_STOP_SIGNAL',
        ERROR_INVALID_HANDLE: 'ERROR_INVALID_HANDLE',
        ERROR_UNEXP_ERR: 'ERROR_UNEXP_ERR',
        ERROR_INVALID_DATA: 'ERROR_INVALID_DATA',
        ERROR_CANCEL: 'ERROR_CANCEL'
    }
});

function reduceArrToSum(_arr) {
    return _arr.reduce((accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue));
};

function reduceByItemToArr(_rows, _item) {
    let reducedObj = reduceByItem(_rows, _item);

    return Object.keys(reducedObj);
};

function reduceByItem(_rows, _item) {
    return _rows.reduce((recivedObj, currentRow) => {
        let key = currentRow[_item];

        if (key != null) {
            if (recivedObj[key] == null) {
                recivedObj[key] = []
            } ''
            recivedObj[key].push(currentRow);
        };
        return recivedObj;
    }, {})
};

/**
* 
* @param {number} total
* @param {number} part
*/
function calcPersent(total, part) {
    let checkArgs = [total, part].some((a) => a == null || a == 0);

    if (checkArgs == true) {
        return 0;
    }

    let partInPercent = parseFloat(part / total) * ONE_HUNDRED_PERCENT;
    partInPercent = partInPercent.toFixed(2);

    return parseFloat(partInPercent);
};

module.exports.reduceByItemToArr = reduceByItemToArr;
module.exports.reduceByItem = reduceByItem;
module.exports.calcPersent = calcPersent;
module.exports.reduceArrToSum = reduceArrToSum;
module.exports.TRACK = TRACK;
module.exports.ONE_HUNDRED_PERCENT = ONE_HUNDRED_PERCENT;
module.exports.ITEMS_FOR_REDUCE = ITEMS_FOR_REDUCE;