'use strict'

const {
    AppLoggerClass,
    Loglevel
} = __UTILS;

const {
    reduceByItemToArr,
    reduceByItem,
    reduceArrToSum,
    TRACK,
    ITEMS_FOR_REDUCE
} = require("../stats_helpers");

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

const {
    waterfall
} = require('async');

class CnannelReffStatsReportClass {
    constructor(client, loglevel = Loglevel.ERROR) {
        this._rpcClient = client;
        this._logger = new AppLoggerClass('CnannelReffStatsReportClass', loglevel)
    }

    async build(params) {
        try {
            const report = await this._getReffStatsFlow(params);

            report.isOK();

            return report;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(`Catch error when calling public methods get refferal stats: ${error}`);

            return error;
        }
    }

    /**
     * 
     * @param {*} _params obj of params for get tracks
     */
    async _getReffStatsFlow(_params) {
        return new Promise((resolve, reject) => {
            try {
                waterfall([
                    this._checkParamsFLowFunc.bind(this, _params),
                    this._getTracksFlowFunc.bind(this),
                    this._buidChannelReffStatsFlowFunc.bind(this)
                ], (error, result) => {
                    try {
                        if (error != null &&
                            error.isStatus(ResultStatus.ERROR_NOT_FOUND) == false
                        ) {
                            throw error;
                        }

                        const rsp = new BasicResponse(ResultStatus.OK, 'Getting refferal stats done successfully', this._reduceToReportShema(result));

                        this._logger.info(rsp);

                        resolve(rsp);
                    } catch (e) {
                        let error = new BasicResponseByError(e);

                        this._logger.error(error.toString());

                        resolve(error);
                    }
                });
            } catch (e) {
                let error = new BasicResponseByError(e);

                this._logger.error(error.toString());

                resolve(error);
            }
        })
    } // getReffStatsFlow

    async _checkParamsFLowFunc(_params) {
        try {
            if (_params == null ||
                Array.isArray(_params)
            ) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Incoming parameters should be an object');
            }

            let dateRange = Array.isArray(_params.dateRange) && _params.dateRange.length <= 2 ? _params.dateRange : [];

            let filter = {
                id: [],
                status: [],
                tokenId: [],
                buttonId: [],
                channelId: [],
                orderId: [],
                sessionId: [],
                error: []
            };

            for (let key in _params.filter) {
                filter[key] = Array.isArray(_params.filter[key]) ? _params.filter[key] : [];
            };

            let params = {
                dateRange,
                filter
            };

            return params
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } // _checkParams

    async _getTracksFlowFunc(_params) {
        try {
            const rpcCallInfo = await this._rpcClient.getAllTracks(_params);

            if (rpcCallInfo.status == ResultStatus.ERROR_UNEXP_ERR &&    //need fix!!!!!
                rpcCallInfo.message == 'Not found any tracks'
            ) {
                rpcCallInfo.status = ResultStatus.ERROR_NOT_FOUND;
                throw rpcCallInfo;
            }

            rpcCallInfo.isOK();

            return rpcCallInfo.data;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    _reduceToReportShema(values = {}) {
        if (values == null) {
            values = {}
        };

        const {
            statsByReffLink = [],
                totalChannelReffStats = {},
                uniqueChannelReffStats = {}
        } = values;

        const {
            returnSubscribes = 0,
                newSubscribes = 0,
                errorSubscribes = 0,
                stopSubscribes = 0,
                uniqueComeUsers = 0
        } = totalChannelReffStats;

        const shema = {
            statsByReffLink,
            totalChannelReffStats: {
                returnSubscribes,
                newSubscribes,
                errorSubscribes,
                stopSubscribes,
                uniqueComeUsers
            },
            uniqueChannelReffStats: {
                returnSubscribes: uniqueChannelReffStats.returnSubscribes || 0,
                newSubscribes: uniqueChannelReffStats.newSubscribes || 0,
                errorSubscribes: uniqueChannelReffStats.errorSubscribes || 0,
                stopSubscribes: uniqueChannelReffStats.stopSubscribes || 0,
                uniqueComeUsers: uniqueChannelReffStats.uniqueComeUsers || 0
            }
        };

        return shema;
    }

    async _buidChannelReffStatsFlowFunc(_tracks) {
        try {
            const [resStatsByReffLink = {}, uniqueChannelReffStats = {}] = await Promise.all([
                this._getStatsDataByChannelFlow(_tracks),
                this._getStatsDataByUniqueusers(_tracks)
            ]);

            return {
                ...resStatsByReffLink,
                uniqueChannelReffStats
            };

        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _getStatsDataByChannelFlow(_tracks) {
        try {
            return new Promise((resolve, reject) => {
                waterfall([
                    this._reduseTracksByReffLinksFlowFunc.bind(this, _tracks),
                    this._getStatsDataByReffLinksFlowFunc.bind(this),
                    this._getTotalStatsByReffLinkFlowFunc.bind(this),
                    this._completeStatsByLinkBatchFlowFunc.bind(this),
                ], (error, result) => {
                    try {
                        if (error != null) {
                            throw error;
                        };

                        resolve(result);
                    } catch (e) {
                        let error = new BasicResponseByError(e);

                        this._logger.error(error.toString());

                        resolve(error);
                    }
                });
            });
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _getStatsDataByUniqueusers(_tracks) {
        try {
            const [resTracksByStatuses = {}, uniqueComeUsers = {}] = await Promise.all([
                this._sortTracksByStatuses(_tracks),
                this._countUniqueComeUsers(_tracks)
            ]);

            return {
                ...resTracksByStatuses,
                uniqueComeUsers
            };
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _reduseTracksByReffLinksFlowFunc(_tracks) {
        try {
            return reduceByItem(_tracks, ITEMS_FOR_REDUCE.TOKEN_ID);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _getStatsDataByReffLinksFlowFunc(_tracksByLinks) {
        try {
            let resObj = {};

            for (const tokenId in _tracksByLinks) {
                resObj[tokenId] = await this._getStatsDataByUniqueusers(_tracksByLinks[tokenId]);
            };

            return resObj;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _sortTracksByStatuses(_tracksByLink) {
        try {
            let {
                done = [], done_error = []
            } = reduceByItem(_tracksByLink, ITEMS_FOR_REDUCE.STATUS);

            let filterByErorrs = reduceByItem(done_error, ITEMS_FOR_REDUCE.ERROR);

            let tracksWithError = [
                ...filterByErorrs[TRACK.ERROR.ERROR_CANCEL] || [],
                ...filterByErorrs[TRACK.ERROR.ERROR_INVALID_DATA] || [],
                ...filterByErorrs[TRACK.ERROR.ERROR_INVALID_HANDLE] || [],
                ...filterByErorrs[TRACK.ERROR.ERROR_UNEXP_ERR] || []
            ];

            let userDoneSubscribes = reduceByItemToArr(done, ITEMS_FOR_REDUCE.USER_ID);
            let userErrors = reduceByItemToArr(tracksWithError, ITEMS_FOR_REDUCE.USER_ID);
            let userReturnSubscribes = reduceByItemToArr([...filterByErorrs[TRACK.ERROR.ERROR_ALREADY_ASSIGNED] || []], ITEMS_FOR_REDUCE.USER_ID);
            let userStopSignals = reduceByItemToArr([...filterByErorrs[TRACK.ERROR.ERROR_RECIVE_STOP_SIGNAL] || []], ITEMS_FOR_REDUCE.USER_ID);

            return {
                newSubscribes: userDoneSubscribes.length,
                returnSubscribes: userReturnSubscribes.length,
                errorSubscribes: userErrors.length,
                stopSubscribes: userStopSignals.length
            };
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _countUniqueComeUsers(_tracksByLink) {
        try {

            return reduceByItemToArr(_tracksByLink, ITEMS_FOR_REDUCE.USER_ID).length;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _getTotalStatsByReffLinkFlowFunc(_statsByReffLinks) {
        try {
            let sumReturnSubscribes = [];
            let sumNewSubscribes = [];
            let sumErrorSubscribes = [];
            let sumStopSubscribes = [];
            let sumUniqueComeUsers = [];

            for (const tokenId in _statsByReffLinks) {
                let currentTokenTracks = _statsByReffLinks[tokenId];
                let {
                    returnSubscribes,
                    newSubscribes,
                    errorSubscribes,
                    stopSubscribes,
                    uniqueComeUsers
                } = currentTokenTracks;

                sumReturnSubscribes.push(returnSubscribes);
                sumNewSubscribes.push(newSubscribes);
                sumErrorSubscribes.push(errorSubscribes);
                sumStopSubscribes.push(stopSubscribes);
                sumUniqueComeUsers.push(uniqueComeUsers);
            };

            return {
                statsByReffLink: _statsByReffLinks,
                totalChannelReffStats: {
                    returnSubscribes: reduceArrToSum(sumReturnSubscribes),
                    newSubscribes: reduceArrToSum(sumNewSubscribes),
                    errorSubscribes: reduceArrToSum(sumErrorSubscribes),
                    stopSubscribes: reduceArrToSum(sumStopSubscribes),
                    uniqueComeUsers: reduceArrToSum(sumUniqueComeUsers)
                }
            };
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _completeStatsByLinkBatchFlowFunc(_incommingData) {
        try {
            let statsByReffLink = _incommingData.statsByReffLink;
            let res = [];

            for (const tokenId in statsByReffLink) {
                let {
                    id,
                    title = null
                } = await this._getReffLinkInfo({
                    id: tokenId
                });

                res.push({
                    tracksStats: statsByReffLink[tokenId],
                    reffLinkId: id,
                    reffLinkName: title
                });
            };

            return {
                statsByReffLink: res,
                totalChannelReffStats: _incommingData.totalChannelReffStats,
            };
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _getReffLinkInfo(_tokenId) {
        try {
            const rpcCallInfo = await this._rpcClient.getReffLinkById(_tokenId);

            rpcCallInfo.isOK();

            return rpcCallInfo.data;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

} //CnannelReffStatsReportClass

module.exports.CnannelReffStatsReportClass = CnannelReffStatsReportClass;