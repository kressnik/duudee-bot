//cores modules
const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');

const {
    SequelizeClass
} = __UTILS

const {
    AppLoggerClass
} = __UTILS;
const {
    waterfall
} = require('async');

const {
    ChannelsManagerClass
} = require('./channels_manager_class/channels_manager_class');

const {
    RefferalStatsClass
} = require('./stats_class/reff_link_stats_class/stats_class');

const {
    ChannelStatsClass
} = require('./stats_class/channel_stats_class/stats_class');

const {
    LanguageСlass
} = require('./LanguageСlass');

const {
    SystemUserClass
} = require('./SystemUserClass/SystemUserClass');

class Core {
    constructor() {
        this.logger = new AppLoggerClass('CORE', process.env.CORE_LOG_LEVEL);
        this.db = new SequelizeClass('CORE', process.env.CORE_DB_LOG_LEVEL);
        this.channelsManager = new ChannelsManagerClass();
        this.lang = new LanguageСlass();
        this.systemUser = new SystemUserClass();
        this.refferalStatsManager = new RefferalStatsClass();
        this.channelsStatsManager = new ChannelStatsClass();
    }

    async init() {
        try {
            let initFlow = await this.initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //init

    async initFlow() {
        return new Promise((resolve, reject) => {
            waterfall([
                this.initConnnectionToDbFlowFunc.bind(this),
                this.initReffStatsClassFlowFunc.bind(this),
                this.initChannelsStatsClassFlowFunc.bind(this),
                this.initManagerFlowFunc.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this.logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init core done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this.logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    }

    async initConnnectionToDbFlowFunc() {
        try {
            this.logger.info('Start connecting to db');

            //init models
            const configAudit = {
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD || '',
                database: process.env.DB_DATABASE,
                directory: process.env.DB_DIRECTORY,
                useMocks: process.env.DB_USE_MOCKS,
                migration: {
                    version: process.env.DB_MIGRATION_VERSION,
                },
                options: {
                    dialect: 'mysql',
                    host: process.env.DB_HOST,
                    port: process.env.DB_PORT,
                }
            };

            const resIinitDB = await this.db.run(configAudit);

            if (resIinitDB.status != ResultStatus.OK) {
                throw resIinitDB;
            }

            this.logger.info(`${resIinitDB.toString()}`);

            return resIinitDB;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    } //initConnnectionToDbFlowFunc

    async initManagerFlowFunc() {
        try {
            this.logger.info('Start');

            let rpcConfigs = {
                host: process.env.RPC_HOST,
                port: process.env.RPC_PORT
            }

            let resInitMan = await this.channelsManager.initRpcClient(rpcConfigs);

            if (resInitMan.status != ResultStatus.OK) {
                throw resInitMan;
            }

            this.logger.info(`${resInitMan.toString()}`);

            return resInitMan;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    } //CoreClass

    async initReffStatsClassFlowFunc() {
        try {
            this.logger.info('Start');

            let rpcConfigs = {
                host: process.env.RPC_HOST,
                port: process.env.RPC_PORT
            }

            let resInitMan = await this.refferalStatsManager.init(rpcConfigs);

            if (resInitMan.status != ResultStatus.OK) {
                throw resInitMan;
            }

            this.logger.info(`${resInitMan.toString()}`);

            return resInitMan;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    } //CoreClass 

    async initChannelsStatsClassFlowFunc() {
        try {
            this.logger.info('Start');

            let rpcConfigs = {
                host: process.env.RPC_HOST,
                port: process.env.RPC_PORT
            }

            let resInitMan = await this.channelsStatsManager.init(rpcConfigs);

            if (resInitMan.status != ResultStatus.OK) {
                throw resInitMan;
            }

            this.logger.info(`${resInitMan.toString()}`);

            return resInitMan;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            throw error;
        }
    }
}


module.exports.Core = Core;