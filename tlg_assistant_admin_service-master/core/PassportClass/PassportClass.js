//cores modules
const passport = require('passport');

const {
    AppLoggerClass
} = __UTILS;

const {
    Strategy
} = require('passport-local');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    waterfall
} = require('async');

class PassportClass {
    constructor() {
        this._express = null;
        this._db = null;

        this._logger = new AppLoggerClass('PassportClass', process.env.PASSPORT_CLASS_LOG_LEVEL);
        this._passport = passport;
    }

    async init(express, db) {
        try {
            if (express == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Express doesn`t set.');
            }

            this._express = express;

            if (db == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Express doesn`t set.');
            }

            this._db = db;

            let initFlow = await this.initFlow(express);

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //init

    async initFlow() {
        return new Promise((resolve, reject) => {
            waterfall([
                //this.initConnnectionToDbFlowFunc.bind(this),
                this.assignExpressFlowFunc.bind(this),
                this.initStrategyFlowFunc.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this._logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init core done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    }

    async assignExpressFlowFunc() {
        try {
            this._logger.info('Start assign passpport to express');

            this._express.use(this._passport.initialize());

            this._logger.info('Start assign passpport session to express');

            this._express.use(this._passport.session());

            let rsp = new BasicResponse(ResultStatus.OK, 'Assign passport to express done successfully.');

            this._logger.info(rsp.toString());

            return rsp; // persistent login sessions
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //assignExpress


    async initStrategyFlowFunc() {
        try {
            this._localStrategy = new Strategy({
                    // by default, local strategy uses username and password, we will override with email
                    usernameField: 'login',
                    passwordField: 'password',
                    passReqToCallback: true // allows us to pass back the entire request to the callback
                },
                async (_req, _login, _password, _done) => {
                    try {
                        let _auth = await this._db.models.User.authenticate(_login, _password);

                        let user = null;

                        if (_auth.status == ResultStatus.OK) {
                            user = _auth.data;

                            this._logger.info(`Login successfully. STATUS: [${_auth.status}] MESSAGE: [${_auth.message}] LOGIN: [${_login}]`);
                        } else {
                            this._logger.error(`Error when auth. STATUS: [${_auth.status}] MESSAGE: [${_auth.message}] LOGIN: [${_login}]`);
                        }

                        return _done(null, user);
                    } catch (e) {
                        let error = new BasicResponseByError(e);

                        this._logger.error(error.toString());

                        return _done(null, false)
                    }
                });

            this._passport.use(this._localStrategy);

            this._passport.serializeUser(this._db.models.User.serializeUser);
            this._passport.deserializeUser(this._db.models.User.deserializeUser(this._db.models));


            let rsp = new BasicResponse(ResultStatus.OK, ' Init strategy done successfully.');

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //initStrategy

    async initConnnectionToDbFlowFunc() {
        try {
            this._logger.info('Start');
            //init models
            var configAudit = {
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_DATABASE,
                port: process.env.DB_PORT,
                host: process.env.DB_HOST,
            };

            let resIinitDB = await this._db.run(configAudit);

            if (resIinitDB.status != ResultStatus.OK) {
                throw resIinitDB;
            }

            this._logger.info(`${resIinitDB.toString()}`);

            return resIinitDB;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //initConnnectionToDbFlowFunc


    async authenticate(request, strategy) {
        return new Promise(async (resolve, reject) => {
            try {
                this._logger.info(`Try authenticate strategy:[${strategy}] User: `);

                this._passport.authenticate(strategy, function (err, user, info) {
                    if (err) {
                        return resolve(new BasicResponseByError(err));
                    }
                    if (user == null || user == false) {
                        return resolve(new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'User not found or password incorrect.'))
                    }

                    return resolve(new BasicResponse(ResultStatus.OK, "authenticate user done successfully", user));
                })(request);
            } catch (e) {
                let error = new BasicResponseByError(e);

                this.logger.error(error.toString());

                return error;
            }
        })
    }

    serializeUser() {

    }

    deserializeUser() {

    }
} //CoreClass



module.exports.PassportClass = PassportClass;