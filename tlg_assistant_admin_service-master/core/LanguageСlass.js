'use strict'

/**
 * Utils
 */
const path = require('path');
const fs = require('fs');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');

const {
    AppLoggerClass
} = __UTILS;

exports.LanguageСlass = class LanguageСlass {
    constructor() {
        this.logger = new AppLoggerClass("LANGUAGE_СLASS", process.env.LANGUAGE_СLASS_LOG_LEVEL);

        return this.load();
    }

    load() {
        const base = (process.env.ENV_FILE == "build") ? process.execPath : __dirname;
        let langDir = path.join(base, '../langs');

        let langs = [];

        if (!fs.existsSync(langDir)) {
            this.logger.error('Dir with language doesn`t found, take it from internal source.');

            langDir = path.join(__dirname, '../langs');
        } else {
            let countFiles = getCountOfFileInDir(langDir);

            if (countFiles.status != ResultStatus.OK) {

                this.logger.error(`Check files in dir error: ${countFiles.message}`);

                langDir = path.join(__dirname, '../langs');
            } else {
                if (countFiles.data == 0) {
                    this.logger.error('Dir `lang` doesn`t has files, take it from internal source.');

                    langDir = path.join(__dirname, '../langs');
                }
            }
        }


        try {
            let langs = require('require-dir-all')(langDir, { // options
                recursive: false, // recursively go through subdirectories; default value shown
                indexAsParent: false, // add content of index.js/index.json files to parent object, not to parent.index
                includeFiles: /^.*\.(json)$/, // RegExp to select files; default value shown
            });

            return langs;
        } catch (_error) {
            let error = new BasicResponseByError(_error);

            moduleLogger.critical(_error.toString());

            process.exit(1);
        }
    }
};

function getCountOfFileInDir(_dir) {
    try {
        let files = fs.readdirSync(_dir);

        return new BasicResponse(ResultStatus.OK, "File count", files.length);
    } catch (err) {
        return new BasicResponse(ResultStatus.ERROR, err.message, err);
    }
}