`use strict`

const express = require('express');
const router = express.Router();
const Speakeasy = require("speakeasy");

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');

const {
    AppLoggerClass
} = __UTILS;

router.post("/totp-secret", getSecretKeyHandler);
router.post("/totp-generate-secret-key/:systemUser", generateSecretKeyToSystemUserHandler);

router.post("/totp-generate", generateKeyHandler);
router.post("/totp-validate", validateKeyHandler);
router.put("/twoFactor/:state", changeTwoFactorState);
router.put("/user/:id/twoFactor/:state", changeSystemUserTwoFactorState);
router.put("/user/:id/dropTwoFactor", dropUserTwoFactorAuth);

let logger = new AppLoggerClass('twoFactorController', process.env.TWO_FACTOR_CONTROLLER_LOG_LEVEL);

async function getSecretKeyHandler(_req, _res, _next) {
    try {

        let generateInfo = await _req.systemUsers.generateTwoFactorKey(_req.user.id);

        if (generateInfo.status != ResultStatus.OK) {
            throw generateInfo;
        }

        let msg = "Generate two factor code done successfully";

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${generateInfo.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, msg, msg, generateInfo.data.img));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            _next(error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //getSecretKeyHandler

async function generateSecretKeyToSystemUserHandler(_req, _res, _next) {
    try {

        let systemUser = _req.params.systemUser;

        let generateInfo = await _req.systemUsers.generateTwoFactorKey(systemUser);

        if (generateInfo.status != ResultStatus.OK) {
            throw generateInfo;
        }

        let msg = "Generate two factor code done successfully";

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${generateInfo.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, msg, msg, generateInfo.data.img));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            _next(error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //getSecretKeyHandler

async function generateKeyHandler(_req, _res, _next) {
    try {
        _res.send({
            "token": Speakeasy.totp({
                secret: _req.body.secret,
                encoding: "base32"
            }),
            "remaining": (30 - Math.floor((new Date()).getTime() / 1000.0 % 30))
        });
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            _next(error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //generateKeyHandler


async function validateKeyHandler(_req, _res, _next) {
    try {

        _res.send({
            "valid": Speakeasy.totp.verify({
                secret: _req.body.secret,
                encoding: "base32",
                token: _req.body.token,
                window: 0
            })
        });

    } catch (error) {
        if (error instanceof HttpRestResponse) {
            _next(error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //validateKeyHandler

async function changeTwoFactorState(_req, _res, _next) {
    try {

        let state = _req.params.state;

        let changeInfo = await _req.systemUsers.changeTwoFactorState(_req.user.id, state, _req.body.code);

        if (changeInfo.status != ResultStatus.OK) {
            throw changeInfo;
        }

        let msg = "Change two factor state done successfully";

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${changeInfo.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, msg, msg, changeInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            _next(error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //changeTwoFactorState

async function changeSystemUserTwoFactorState(_req, _res, _next) {
    try {

        let state = _req.params.state;
        let id = _req.params.id;

        let changeInfo = await _req.systemUsers.changeSystemUserTwoFactorState(id, state);

        if (changeInfo.status != ResultStatus.OK) {
            throw changeInfo;
        }

        let msg = "Change two factor state done successfully";

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${changeInfo.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, msg, msg, changeInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            _next(error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //changeSystemUserTwoFactorState

async function dropUserTwoFactorAuth(_req, _res, _next) {
    try {

        let id = _req.params.id;

        let dropInfo = await _req.systemUsers.dropUserTwoFactorAuth(id);

        if (dropInfo.status != ResultStatus.OK) {
            throw dropInfo;
        }

        let msg = `Drop two factor auth for user: [${id}] done successfully`;

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${dropInfo.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, msg, msg, dropInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            _next(error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //changeSystemUserTwoFactorState

module.exports = router;