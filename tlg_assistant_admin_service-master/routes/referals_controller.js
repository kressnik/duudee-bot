const express = require('express');
const router = express.Router();
const path = require('path');

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');


const {
    RefferalStatsClass
} = require('../core/stats_class/reff_link_stats_class/stats_class');

const {
    AppLoggerClass
} = __UTILS;

let logger = new AppLoggerClass('referalController', process.env.REFERAL_CONTROLLER_LOG_LEVEL);

router.post('/invite/actions', getAllInviteActionsHandler); // invite actions
router.post('/refferal/:id/users', getUsersByReferalLink);
router.get('/referal/types', getReferalLinksTypes);
router.get('/referals/all', getAllReferalLinks); // all referal links
router.post('/referal', createReferalLink);
router.delete('/referal/:id', deleteReferalLink);
router.put('/referal/:id/edit', editReferalLink);
router.get('/info/create/new/link', getInfoForCreateNewLink);
router.post('/refferal/link/stats/:id', getInfoForStatistic);
router.post('/refferal/link/activity/:id', getInfoForActivity);

async function getInfoForStatistic(req, res, next) {
    try {
        const reqPrms = {
            ...req.body
        };

        let dateRange = Array.isArray(reqPrms.dateRange) && reqPrms.dateRange.length <= 2 ? reqPrms.dateRange : [];
        let filterObj = {
            id: [],
            status: [],
            tokenId: [],
            buttonId: [],
            channelId: [],
            orderId: [],
            sessionId: [],
            error: []
        };

        for (let key in reqPrms.filter) {
            filterObj[key] = Array.isArray(reqPrms.filter[key]) ? reqPrms.filter[key] : [];
        };

        let params = {
            dateRange: [...dateRange],
            filter: {
                ...filterObj
            }
        };

        let {
            data
        } = await req.core.refferalStatsManager.getReffStats(params);

        next(new HttpRestResponse(HttpStatus.OK, 'Getting data for statistic done successfully', 'Getting data for statistic done successfully', data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getInfoForActivity(req, res, next) {
    try {
        const reqPrms = {
            ...req.body
        };

        let dateRange = reqPrms.dateRange != null && reqPrms.dateRange != "" ? reqPrms.dateRange.split(',') : [];
        dateRange = Array.isArray(dateRange) && dateRange.length <= 2 ? dateRange.map(i => Number(i)) : [];
        let filterObj = {
            isNewUser: [],
            tokenId: [],
            userId: [],
            channelId: [],
            error: [],
            status: []
        };

        for (let key in filterObj) {
            filterObj[key] = reqPrms[key] != null && reqPrms[key] != "" ? reqPrms[key].split(',') : [];
            filterObj[key] = filterObj[key].length != null ? filterObj[key].map(i => {
                return isNaN(Number(i)) != true ? Number(i) :
                    i === 'true' ? true :
                    i === 'false' ? false : i;
            }) : [];
        };


        let {
            data
        } = await req.core.channelsManager.getReffLinkTracksByFilterWithPagin({
            dateRange: [...dateRange],
            filter: {
                ...filterObj
            },
            limit: reqPrms.length,
            offset: reqPrms.start
        });

        next(new HttpRestResponse(HttpStatus.OK, 'Getting data for statistic done successfully', 'Getting data for statistic done successfully', data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getAllInviteActionsHandler(req, res, next) { //TODO:
    try {

        let resultBatch = {
            recordsTotal: 0,
            recordsFiltered: 0,
            data: []
        }

        const queryData = req.body;

        let searchValue = (queryData['search[value]'] == null ? "" : queryData['search[value]']);

        let queryParameters = {
            limit: parseInt(queryData.length),
            offset: parseInt(queryData.start),
            order: [],
            whereLike: searchValue
        }

        let listInfo = await req.core.channelsManager.getInviteActions(queryParameters);

        let actions = listInfo.data.data;

        list = Object.values(actions).map(link => {
            return {
                ...link
            }
        });

        var actionList = [];

        for (actionId in list) {
            const actionProps = list[actionId];

            actionList.push({
                referalLink: actionProps.link,
                referalLinkId: actionProps.id,
                count: actionProps.count
            });
        }

        let searchList = [];

        let clearObj = {};
        searchList = actionList;

        let start = parseInt(queryParameters.offset); //расчет старта для вывода с учетом пагинации
        let end = parseInt(start + queryParameters.limit) - 1; //расчет окончания данных для вывода пагинации
        let resultData = searchList;

        resultBatch.recordsTotal = listInfo.data.recordsTotal;
        resultBatch.recordsFiltered = listInfo.data.recordsFiltered;
        resultBatch.data = resultData;

        next(new HttpRestResponse(HttpStatus.OK, 'Getting batch from db done successfully', 'actionList handler', resultBatch));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getUsersByReferalLink(req, res, next) {
    try {
        let id = req.params.id;

        const queryData = req.body;

        let searchValue = (queryData['search[value]'] == null ? "" : queryData['search[value]']);

        let queryParameters = {
            limit: parseInt(queryData.length),
            offset: parseInt(queryData.start),
            order: [],
            whereLike: searchValue
        }

        let result = await req.core.channelsManager.getUsersByLink({
            referalId: id,
            ...queryParameters
        });

        if (result.status != ResultStatus.OK) {
            throw result;
        }

        let batch = [];
        let finalBatch = {};

        for (userId in result.data.finalBatch) {
            batch.push(result.data.finalBatch[userId]);
        }

        finalBatch.data = batch;
        finalBatch.recordsTotal = result.data.recordsTotal;
        finalBatch.recordsFiltered = result.data.recordsFiltered;

        next(new HttpRestResponse(HttpStatus.OK, 'Get users info done successfully', 'Get users info done successfully', finalBatch));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getReferalLinksTypes(req, res, next) {
    try {
        let getReferalLinksTypesInfo = await req.core.channelsManager.getReferalLinksTypes();

        if (getReferalLinksTypesInfo.status != ResultStatus.OK) {
            throw getReferalLinksTypesInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Get referal links done successfully', 'Get referal links done successfully', getReferalLinksTypesInfo.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getInfoForCreateNewLink(req, res, next) {
    try {
        let getReferalLinksTypesInfo = await req.core.channelsManager.getReferalLinksTypes();
        let getAllChannels = await req.core.channelsManager.getAllChannels({});
        let getTelgramButtonList = await req.core.channelsManager.getTelgramButtonList();

        next(new HttpRestResponse(HttpStatus.OK, 'Get info to create new refferal link done successfully', 'Get info to create new refferal link done successfully', {
            linkTypes: getReferalLinksTypesInfo.data,
            channels: getAllChannels.data.rows,
            buttons: getTelgramButtonList.data
        }));

    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getAllReferalLinks(req, res, next) {
    try {
        const queryData = req.query;

        let queryParameters = {
            limit: parseInt(queryData.length),
            offset: parseInt(queryData.start),
            order: [],
            whereLike: (queryData.search == null ? "" : queryData.search.value)
        }

        let listInfo = await req.core.channelsManager.getAllReferalLinks(queryParameters);

        const {
            recordsTotal = 0,
                recordsFiltered = 0,
                limit = 0,
                offset = 0,
                data = []
        } = listInfo.data;

        next(new HttpRestResponse(HttpStatus.OK, 'Getting batch from db done successfully', 'referalList handler', {
            draw: req.query.draw,
            recordsTotal,
            recordsFiltered,
            limit,
            offset,
            data
        }));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function createReferalLink(req, res, next) {
    try {
        let params = req.body;

        let createInfo = await req.core.channelsManager.createReferalLink(params);

        if (createInfo.status != ResultStatus.OK) {
            throw createInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, createInfo.message, 'Create new referal link done successfully', createInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //createReferalLink

async function deleteReferalLink(req, res, next) {
    try {
        let params = req.body;

        params.isSoftMode = true;

        let deleteInfo = await req.core.channelsManager.deleteReferalLink(params);

        if (deleteInfo.status != ResultStatus.OK) {
            throw deleteInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, deleteInfo.message, 'Delete referal link done successfully', deleteInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //deleteReferalLink 

async function editReferalLink(req, res, next) {
    try {
        let params = req.body;

        let editInfo = await req.core.channelsManager.editReferalLink(params);

        if (editInfo.status != ResultStatus.OK) {
            throw editInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, editInfo.message, 'Edit referal link done successfully', editInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
} //editReferalLink 

module.exports = router;