`use strict`

const express = require('express');
const router = express.Router();

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');


const {
    AppLoggerClass
} = __UTILS;

let logger = new AppLoggerClass('ChannelController', process.env.CHANNEL_CONTROLLER_LOG_LEVEL);
router.get('/channels/all', channelsListHandler);
router.get('/channels/all/list', channelsListSimpleHandler);

router.post('/channel', createNewChannelHandler);
router.post('/channel/reff/stats/:id', getChannelreffStatsHandler);
router.put('/channel/:id', editChannelInfoHandler);
router.put('/channel/:id/state/:state', editStateChannelHandler);

router.delete('/channel/:id', deleteChannelHandler);

async function getChannelreffStatsHandler(req, res, next) {
    try {
        const reqPrms = {
            ...req.body
        };

        let dateRange = Array.isArray(reqPrms.dateRange) && reqPrms.dateRange.length <= 2 ? reqPrms.dateRange : [];
        let filterObj = {
            id: [],
            status: [],
            tokenId: [],
            buttonId: [],
            channelId: [],
            orderId: [],
            sessionId: [],
            error: []
        };

        for (let key in reqPrms.filter) {
            filterObj[key] = Array.isArray(reqPrms.filter[key]) ? reqPrms.filter[key] : [];
        };

        let params = {
            dateRange: [...dateRange],
            filter: {
                ...filterObj
            }
        };

        let {
            data
        } = await req.core.channelsStatsManager.getChannelReffStats(params);

        next(new HttpRestResponse(HttpStatus.OK, 'Getting data for statistic done successfully', 'Getting data for statistic done successfully', data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function deleteChannelHandler(req, res, next) {
    try {
        const params = {
            id: parseInt(req.params.id)
        };

        if (isNaN(params.id) || params.id == null) {
            throw new HttpRestResponse(HttpStatus.BAD_REQUEST, 'Null id for delete.', 'Null id for delete.');
        }

        let channelDeleteInfo = await req.core.channelsManager.deleteChannel(params);

        if (channelDeleteInfo.status != ResultStatus.OK) {
            throw channelDeleteInfo;
        }

        logger.info(`USER: [${req.user.username}] with id: [${req.user.id}] - ${channelDeleteInfo.toString()}`);

        next(new HttpRestResponse(HttpStatus.OK, channelDeleteInfo.message, 'Deleting channel done successfully', channelDeleteInfo.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

//get convertre list
async function channelsListHandler(req, res, next) { //TODO:
    try {
        let resultBatch = { //Дефолтный батч для возврата списка сотрудников
            recordsTotal: 0, //Количество записей в БД
            recordsFiltered: 0, //Количество записей после поиска
            draw: req.query.draw, //Временный номер для dataTables
            data: [] //Список сотрудников
        }

        const queryData = req.query; //Параметры запроса
        let queryParameters = { //Параметры для поиска данных в БД
            limit: parseInt(queryData.length), //Количество записей
            offset: parseInt(queryData.start), //Позиция с которой начинается получения данных
            order: [],
            whereLike: (queryData.search == null ? "" : queryData.search.value)
        }

        let listInfo = await req.core.channelsManager.getAllChannels(queryParameters);

        if (listInfo.data.rows != null) {
            var channelList = [];
            var channelQty = listInfo.data.count;
            var data = listInfo.data.rows

            channelList = data.map(channel => {
                return {
                    id: channel.id,
                    name: channel.name,
                    description: channel.description,
                    state: channel.inUse === true ? 1 : channel.inUse === false ? 0 : 2,
                    isFree: channel.isFree,
                    price: channel.price,
                    currency: channel.currency,
                    tlgId: channel.tlgId,
                    subscriptionDuration: channel.subscriptionDuration,
                    color: channel.color
                }
            });

            resultBatch.recordsTotal = channelQty;
            resultBatch.recordsFiltered = channelQty;
            resultBatch.data = channelList;
        }


        next(new HttpRestResponse(HttpStatus.OK, 'Getting batch from db done successfully', 'channelList handler', resultBatch));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function channelsListSimpleHandler(req, res, next) { //TODO:
    try {

        let queryParameters = { //Параметры для поиска данных в БД
            limit: 100, //Количество записей
            offset: 0, //Позиция с которой начинается получения данных
            order: [],
            whereLike: ''
        }

        let listInfo = await req.core.channelsManager.getAllChannels(queryParameters);

        let channelList = [];

        let resultBatch = {};

        if (listInfo.data.rows != null) {
            var channelQty = listInfo.data.count;
            var data = listInfo.data.rows

            channelList = data.map(channel => {
                return {
                    id: channel.id,
                    name: channel.name,
                    description: channel.description,
                    state: channel.inUse,
                    isFree: channel.isFree,
                    price: channel.price,
                    currency: channel.currency,
                    tlgId: channel.tlgId,
                    subscriptionDuration: channel.subscriptionDuration,
                    color: channel.color
                }
            });

            resultBatch.recordsTotal = channelQty;
            resultBatch.recordsFiltered = channelQty;
            resultBatch.data = channelList;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Getting batch from db done successfully', 'channelList handler', channelList));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function createNewChannelHandler(req, res, next) {
    try {
        let params = req.body;

        let createInfo = await req.core.channelsManager.createNewChannel(params);

        if (createInfo.status != ResultStatus.OK) {
            throw createInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, createInfo.message, 'Create new channel done successfully', createInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function editChannelInfoHandler(req, res, next) {
    try {
        let params = req.body;

        let editInfo = await req.core.channelsManager.editChannelInfo(params);

        if (editInfo.status != ResultStatus.OK) {
            throw editInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, editInfo.message, 'Edit channel info done successfully', editInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function editStateChannelHandler(req, res, next) {
    try {
        let params = req.params;

        let editState = await req.core.channelsManager.editStateChannel(params);

        if (editState.status != ResultStatus.OK) {
            throw editState;
        }

        next(new HttpRestResponse(HttpStatus.OK, editState.message, 'Edit channel info done successfully', editState));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

module.exports = router;