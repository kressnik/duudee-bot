const express = require('express');
const router = express.Router();

router.post('/:version/rpc', RpcProcessorHandler);


async function RpcProcessorHandler(req, res, next) {
    // prepare a context object passed into the JSON-RPC method
    const context = {
        headers: req.headers
    };

    let response = await  req.core.EventProcessor.RpcProcessor.call(req.body, context);

    res.send(response);
}

module.exports = router;
