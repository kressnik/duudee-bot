var express = require('express');
var router = express.Router();
var path = require('path');

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');


const MODULE_NAME = "LANG";

router.get('/langs/:langName', geLangHandler);
router.get('/langs', geLangListHandler);


function geLangHandler(_req, _res, _next) {
    try {
        let target = _req.params.langName;

        if (target == null) {
            throw new HttpRestResponse(HttpStatus.NOT_FOUND, 'Target doenst exist', 'MESSAGE_KEY');
        }

        let lang = _req.core.lang[target];

        if (lang == null) {
            throw new HttpRestResponse(HttpStatus.NOT_FOUND, 'Language doenst exist', 'MESSAGE_KEY');
        }

        _next(new HttpRestResponse(HttpStatus.OK, 'Language is ok', 'MESSAGE_KEY', lang));
    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
}

function geLangListHandler(_req, _res, _next) {
    try {
        let langList = [];

        for (lang in _req.core.lang) {
            langList.push(lang)
        }

        _next(new HttpRestResponse(HttpStatus.OK, 'Get lang list done successfully.', 'MESSAGE_KEY', langList));
    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
}

module.exports = router;