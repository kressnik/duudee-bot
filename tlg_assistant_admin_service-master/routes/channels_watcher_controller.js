const express = require('express');
const router = express.Router();

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');

const {
    AppLoggerClass
} = __UTILS;

const logger = new AppLoggerClass('ChannelController', process.env.CHANNEL_CONTROLLER_LOG_LEVEL);

router.get('/channel/watcher/auth/status', getWatcherAuthStatusHandler);
router.post('/channel/watcher/auth/start', startWatcherAuthHandler);
router.post('/channel/watcher/auth/code', sendWatchetAuthCodeHandler);
router.post('/channel/watcher/api/info', sendWatcherUserApiInfoHandler);

async function getWatcherAuthStatusHandler(req, res, next) {
    try {
        const resWatcherAuthStatus = await req.core.channelsManager.getWatcherAuthStatus();

        if (resWatcherAuthStatus.status != ResultStatus.OK) {
            throw resWatcherAuthStatus;
        }

        const {
            status = 'get status done with erorr'
        } = resWatcherAuthStatus.data;

        logger.info(`Get watcher auth status. STATUS: [${status}]`);

        next(new HttpRestResponse(HttpStatus.OK, resWatcherAuthStatus.message, 'Geting watcher auth status done successfully', resWatcherAuthStatus.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function startWatcherAuthHandler(req, res, next) {
    try {
        const params = req.body;

        const resStartWatcherAuth = await req.core.channelsManager.startWatcherAuth(params);

        if (resStartWatcherAuth.status != ResultStatus.OK) {
            throw resStartWatcherAuth;
        }

        logger.info(`Watcher auth start. PHONE: [${params.phone}]`);

        next(new HttpRestResponse(HttpStatus.OK, resStartWatcherAuth.message, 'Geting watcher auth status done successfully', resStartWatcherAuth.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function sendWatchetAuthCodeHandler(req, res, next) {
    try {
        const params = req.body;

        const resSendWatcherAuthCode = await req.core.channelsManager.sendWatcherAuthCode(params);

        if (resSendWatcherAuthCode.status != ResultStatus.OK) {
            throw resSendWatcherAuthCode;
        }

        logger.info(`Sending user auth code foe channels watcher`);

        next(new HttpRestResponse(HttpStatus.OK, resSendWatcherAuthCode.message, 'Geting watcher auth status done successfully', resSendWatcherAuthCode.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function sendWatcherUserApiInfoHandler(req, res, next) {
    try {
        const params = req.body;

        const resSendWatcherUserApiInfo = await req.core.channelsManager.sendWatcherUserApiInfo(params);

        if (resSendWatcherUserApiInfo.status != ResultStatus.OK) {
            throw resSendWatcherUserApiInfo;
        }

        logger.info(`Sending user API info for channels watcher`);

        next(new HttpRestResponse(HttpStatus.OK, resSendWatcherUserApiInfo.message, 'Geting watcher auth status done successfully', resSendWatcherUserApiInfo.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

module.exports = router;