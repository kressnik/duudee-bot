`use strict`

const express = require('express');
const router = express.Router();

const {
    AppLoggerClass
} = __UTILS;

const administratorMenu = {
    monitoring: {
        icon: '<i class="fe fe-home"></i>',
        href: '/index.html'
    },
    channelsManagement: {
        icon: '<i class="fe fe-file"></i>',
        href: '/channels-management.html'
    },
    // usersManagement: {
    //     icon: '<i class="fe fe-file"></i>',
    //     href: '/users-management.html'
    // },
    // subscribesManagement: {
    //     icon: '<i class="fe fe-file"></i>',
    //     href: '/subscribes-management.html'
    // },
    referalManagement: {
        icon: '<i class="fe fe-file"></i>',
        href: '/refferals-management.html'
    },
    ordersManagement: {
        icon: '<i class="fe fe-file"></i>',
        href: '/orders-managment.html'
    }
};

const managerMenu = {
    monitoring: {
        icon: '<i class="fe fe-home"></i>',
        href: '/index.html'
    },
    channelsManagement: {
        icon: '<i class="fe fe-file"></i>',
        href: '/channels-management.html'
    },
    // usersManagement: {
    //     icon: '<i class="fe fe-file"></i>',
    //     href: '/users-management.html'
    // },
    referalManagement: {
        icon: '<i class="fe fe-file"></i>',
        href: '/refferals-management.html'
    },
};


const defaultGroupsList = {
    'administrator': {
        name: 'administrator',
        menu: administratorMenu
    },
    'manager': {
        name: 'manager',
        menu: managerMenu
    }
}

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');

let logger = new AppLoggerClass('userController', process.env.USER_CONTROLLER_LOG_LEVEL);

router.get('/user/language', getLanguageHandler);
router.get('/user/menu', getMenuHandler);

router.put('/user/password', changePasswordHandler);
router.put('/user/info', changeUserInfoHandler);
router.get('/user', getUserInfoHandler); //Получение информации об авторизированном пользователе

router.get('/users', getUsersHandler);
router.get('/user/:id', getSpecificUserHandler);
router.put('/user/:id', changeSpecificUserHandler);
router.post('/user', addNewSystemUserHandler);
router.delete('/user/:id', deleteSystemUserHandler);

async function changeSpecificUserHandler(_req, _res, _next) {
    try {

        let idSystemUser = _req.params.id;
        var userInfo = _req.body;

        let changeInfo = await _req.systemUsers.changeUserProps(idSystemUser, userInfo, _req.user);

        if (changeInfo.status != ResultStatus.OK) {
            throw changeInfo;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${changeInfo.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, changeInfo.message, changeInfo.name, changeInfo.data));

    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
}

async function getSpecificUserHandler(_req, _res, _next) {
    try {

        let idSystemUser = _req.params.id;

        let resultGet = await _req.systemUsers.getUserProps(idSystemUser);

        if (resultGet.status != ResultStatus.OK) {
            throw resultGet;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${resultGet.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, resultGet.message, resultGet.mes, resultGet.data));

    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
}

async function deleteSystemUserHandler(_req, _res, _next) {
    try {
        let idSystemUser = _req.params.id;

        if (idSystemUser == _req.user.id) {
            throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `You cant delete yourself`, {});
        }

        let resultDelete = await _req.systemUsers.deleteOneUser(idSystemUser);

        if (resultDelete.status != ResultStatus.OK) {
            throw resultDelete;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${resultDelete.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, resultDelete.message, resultDelete.message, resultDelete.data));
    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, _error.message, _error.data));
        }
    }
}

async function addNewSystemUserHandler(_req, _res, _next) {

    try {

        let addingInfo = await _req.systemUsers.createNewUser(_req);

        if (addingInfo.status != ResultStatus.OK) {
            throw addingInfo;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${addingInfo.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, addingInfo.message, 'Add new user done successfully', addingInfo.data));

    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'Cant add new user to system', _error.data));
        }
    }
}

async function getUsersHandler(_req, _res, _next) { //Получения список сотрудников

    try {

        let usersInfo = await _req.systemUsers.getUsers(_req);

        if (usersInfo.status != ResultStatus.OK &&
            usersInfo.status != ResultStatus.ERROR_NOT_FOUND
        ) {
            throw usersInfo;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${usersInfo.toString()}`);

        // _next(new HttpRestResponse(HttpStatus.OK, usersInfo.message, 'Success when getting users', usersInfo.data));

        _res.status(200).send(usersInfo.data);
    } catch (_error) {

        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'Error when getting users', _error.data));
        }
    }
}

async function getMenuHandler(_req, _res, _next) {
    try {
        if (defaultGroupsList[_req.user.role] == null) {
            throw new new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, `Role:[${_req.user.role}] doens't exist in system.`, 'MESSAGE_KEY');
        }

        _next(new HttpRestResponse(HttpStatus.OK, 'Get menu done successfully.', 'MESSAGE_KEY', defaultGroupsList[_req.user.role]));
    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
}

async function getLanguageHandler(_req, _res, _next) {

    var tmpResponse = new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, 'Unknown state');

    try {
        tmpResponse.assign(__SystemMessage.LANG_USER_GET_OK);
        var stub = {
            lang: (_req.user) ? _req.user.lang : undefined
        }
        tmpResponse.data = stub;

        if (tmpResponse.code != 200) {
            __ResponseAction.systemLog(tmpResponse);
        }
        tmpResponse = new HttpRestResponse(HttpStatus.OK, tmpResponse.message, 'MESSAGE_KEY', tmpResponse.data);

        _next(tmpResponse);
    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
}

async function changePasswordHandler(_req, _res, _next) {

    try {

        var password = _req.body.password;

        let resUpdate = await _req.systemUsers.changePassword(password, _req.user.id);
        //reducer

        if (resUpdate.status != ResultStatus.OK) {
            throw resUpdate;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${resUpdate.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, resUpdate.message, resUpdate.message, resUpdate.data));

    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
}


async function changeUserInfoHandler(_req, _res, _next) {
    try {

        var userInfo = _req.body;

        let resUpdate = await _req.systemUsers.changeUserInfo(userInfo, _req.user);
        //reducer

        if (resUpdate.status != ResultStatus.OK) {
            throw resUpdate;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${resUpdate.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, resUpdate.message, resUpdate.message, resUpdate.data));

    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, _error.message, _error.data));
        }
    }
}


async function getUserInfoHandler(_req, _res, _next) {
    try {

        var stub = {
            user: (_req.user.username) ? _req.user.username : null,
            fullName: (_req.user.firstName) ? _req.user.firstName : null,
            shortName: (_req.user.shortName) ? _req.user.shortName : null,
            email: (_req.user.email) ? _req.user.email : null,
            imgDist: (_req.user.avatarUserLink) ? _req.user.avatarUserLink : null,
            role: (_req.user.role) ? _req.user.role : null,
            isUseTwoFa: (_req.user.isUseTwoFa) ? _req.user.isUseTwoFa : null
        }

        // get flag does the user have 2fa button
        let checkForShow = await _req.systemUsers.checkTwoFactorFlag(_req.user.id);

        if (checkForShow.status == ResultStatus.OK) {
            stub.twoFactorFlag = true;
        } else {
            stub.twoFactorFlag = false;
        }

        logger.info(`USER: [${_req.user.username}] with id: [${_req.user.id}] - ${checkForShow.toString()}`);

        _next(new HttpRestResponse(HttpStatus.OK, 'Get user info.', 'Get user info done successfully', stub));
    } catch (_error) {
        if (_error instanceof HttpRestResponse) {
            _next(_error);
        } else {
            _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'MESSAGE_KEY', _error.data));
        }
    }
} //getUserInfoHandler


module.exports = router;