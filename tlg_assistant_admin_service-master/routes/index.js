`use strict`

const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass("ROUTES", process.env["ROUTES_LOG_LEVEL"]);
const app = new(require('express').Router)();

const {
    BasicResponse,
    BasicResponseFromError,
    ResultStatus
} = require('../interfaces/basicResponse');

const path = require('path');
const fileSystem = require('../utils/fileSystem');

moduleLogger.verbose(__dirname);

let routes = require('require-dir-all')(__dirname, { // options
    recursive: true, // recursively go through subdirectories; default value shown
    indexAsParent: false, // add content of index.js/index.json files to parent object, not to parent.index
    includeFiles: /^.*\.(js)$/, // RegExp to select files; default value shown
});

moduleLogger.info("Start assign routes to system.");

async function useAll(_routes) {
    try {
        let routesList = [];

        //let use all routes 
        let use = (_routes) => {
            for (let i in _routes) {
                //routeList
                let route = _routes[i];

                if (typeof route == "function") {

                    for (let r of route.stack) {
                        routesList.push({
                            ...r.route.methods,
                            path: r.route.path
                        });
                    }

                    moduleLogger.verbose('Router require: ', i);

                    app.use(route);
                } else {
                    use(route);
                }
            } //for
        }

        use(_routes);

        if (process.env.ENV_FILE != 'build') {
            let pathFile = path.join(__dirname, 'routes_list.json');

            let getMethod = (o) => {
                if (o.get == true) {
                    return 'GET'
                } else if (o.post == true) {
                    return 'POST'
                } else if (o.delete == true) {
                    return 'DELETE'
                } else if (o.put == true) {
                    return 'PUT'
                } else if (o.options == true) {
                    return 'OPTIONS'
                }

                console.error(o);
            }

            routesList = routesList.map((r, index) => {
                return {
                    index: index,
                    method: getMethod(r),
                    accept: true,
                    ...r
                }
            });

            let body = JSON.stringify(routesList);

            let create = await fileSystem.createFile(body, pathFile);

            if (create.status != ResultStatus.OK) {
                console.error(create.message);
            } else {
                console.info(create.message);
            }
        }
    } catch (e) {
        console.error(e);
    }
}



useAll(routes);

moduleLogger.info("End assign routes to system.")

module.exports = app;