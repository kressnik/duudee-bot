'use strict'

const express = require('express');
const router = express.Router();

const {
    ResultStatus,
    BasicResponse
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');

const MODULE_NAME = "AUTH";

router.get("/logout", logoutHandler);
//router.post("/register", regiserHandle);
router.post("/login", loginHandler);
router.options("/login", loginHandler);


function logoutHandler(_req, _res, _next) {
    _req.logOut();
    _req.session.destroy();
    _res.redirect("/login.html");
}

async function regiserHandle(_req, _res, _next) {

    var tmpResponse = __ResponseAction.makeByProps('DEFAULT_MESSAGE', {
        module: MODULE_NAME,
        subModule: 'REGISTER',
        user: 'core',
        data: {}
    });

    try {
        const user = _req.body;

        let addRes = await models.User.add(user);

        switch (addRes.status) {
            case ResultStatus.ERROR_INVALID_DATA:
                tmpResponse.assign(__SystemMessage.SYSTEM_USER_REGISTER_ERROR_INVALID_DATA);
                tmpResponse.updateMessage(addRes.message);
                break;
            case ResultStatus.ERROR:
                tmpResponse.assign(__SystemMessage.SYSTEM_USER_REGISTER_ERROR);
                tmpResponse.updateMessage(addRes.message);
                break;
            case ResultStatus.OK:
                tmpResponse.assign(__SystemMessage.SYSTEM_USER_REGISTER_OK);
                break;
            default:
                tmpResponse.updateMessage(addRes.message);
                break;
        }


        if (tmpResponse.code != 200) {
            __ResponseAction.systemLog(tmpResponse);
        }

        _next(new HttpRestResponse(tmpResponse.code, tmpResponse.message, tmpResponse.name, {}));
    } catch (_error) {
        tmpResponse.assign(__SystemMessage.REGISTER_UNEXPT_ERR);
        tmpResponse.updateMessage(_error.message);
        __ResponseAction.systemLog(tmpResponse);
        _next(new HttpRestResponse(tmpResponse.code, _error.message, tmpResponse.name, _error));
    }
} //regiserHandle

async function loginHandler(_req, _res, _next) {
    try {
        let auth = await _req.systemUsers.authenticateRequestFlow(_req, 'local');

        if (auth.status != ResultStatus.OK) {
            throw auth;
        }

        let response = new BasicResponse(ResultStatus.OK, 'Login done successfully', {});

        return _next(new HttpRestResponse(HttpStatus.OK, `User with login :[${auth.data.login}] loged in`, 'LOGIN_OK', response));

    } catch (_error) {
        switch (_error.status) {
            case ResultStatus.SUCCESS_NO_CONTENT:
                return _next(new HttpRestResponse(HttpStatus.OK, _error.message, 'Need to enter 2FA code', _error));

            default:
                return _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, '2FA code is invalid', {}));
        }
    } //
}


module.exports = router;