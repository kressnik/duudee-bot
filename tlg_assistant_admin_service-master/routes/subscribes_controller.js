`use strict`

const express = require('express');
const router = express.Router();

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');

const {
    searchData
} = require('../utils/helpers');

const {
    AppLoggerClass
} = __UTILS;

let logger = new AppLoggerClass('poolController', process.env.POOL_CONTROLLER_LOG_LEVEL);

router.post('/subscribes', getAllSubscribesHandler);
router.post('/subscribe', addNewSubscribeHandler);
router.get('/subscribe/:id/deposit', getDepositInfoBySubscribeHandler);
router.put('/order/:id/deposit', editDepositStatusHandler);
router.get('/deposit/statuses', getDepositStatusesHandler);
router.put('/order/:id/edit', editOrderInfoHandler);
router.get('/orders/get/filtered', getAndRenderOrdersByFilter);
router.get('/orders/statuses', getOrdersStatuses);

async function getOrdersStatuses(req, res, next) {
    try {
        let resGetOrdersStatuses = await req.core.channelsManager.getOrdersStatuses();

        if (resGetOrdersStatuses.status != ResultStatus.OK) {
            throw resGetOrdersStatuses;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Get orders statuses done successfully', 'Get orders statuses done successfully', resGetOrdersStatuses.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getAndRenderOrdersByFilter(req, res, next) {
    try {
        const queryData = req.query;
        let resultBatch = {
            recordsTotal: 0,
            recordsFiltered: 0,
            data: []
        }

        let {
            selectedSNsArr = null, selectedStatusesArr = null, isIncludeFree = true, isManualOnly = false, selectedStatusesDepo = null, start = null, length = null, search = null
        } = queryData;

        let queryParameters = {};

        if (start != null && length != null) {
            queryParameters = {
                limit: parseInt(length),
                offset: parseInt(start),
                filter: {
                    order: null,
                    deposit: null
                },
                whereLike: (search == null ? "" : search.value)
            }
        } else {
            queryParameters = {
                limit: 10000,
                offset: 0,
                filter: {
                    order: null,
                    deposit: null
                }
            }
        }


        selectedSNsArr = selectedSNsArr != null ? selectedSNsArr.split(',') : selectedSNsArr;
        selectedStatusesArr = selectedStatusesArr != null ? selectedStatusesArr.split(',') : selectedStatusesArr;
        selectedStatusesDepo = selectedStatusesDepo != null ? selectedStatusesDepo.split(',') : selectedStatusesDepo;
        isManualOnly = isManualOnly === false ? false : true;
        isIncludeFree = isIncludeFree === true ? true : false;

        if (selectedStatusesDepo != null) {
            queryParameters.filter.deposit = {
                status: selectedStatusesDepo
            }
        }

        if (isManualOnly == true) {
            if (queryParameters.filter.deposit == null) {
                queryParameters.filter.deposit = {};
            }
            queryParameters.filter.deposit.isManual = isManualOnly;
        }

        if (isIncludeFree == false) {
            if (queryParameters.filter.order == null) {
                queryParameters.filter.order = {};
            }
            queryParameters.filter.order.isFree = isIncludeFree;
        }

        if (selectedSNsArr != null &&
            selectedSNsArr.length != 0) {
            if (queryParameters.filter.order == null) {
                queryParameters.filter.order = {};
            }

            queryParameters.filter.order.sn = selectedSNsArr;
        }

        if (selectedStatusesArr != null &&
            selectedStatusesArr.length != 0) {
            if (queryParameters.filter.order == null) {
                queryParameters.filter.order = {};
            }

            queryParameters.filter.order.status = selectedStatusesArr;
        }

        let resGetOrders = await req.core.channelsManager.getOrders(queryParameters);

        if (resGetOrders.status != ResultStatus.OK) {
            throw resGetOrders;
        }

        resultBatch.recordsTotal = resGetOrders.data.recordsTotal;
        resultBatch.recordsFiltered = resGetOrders.data.recordsFiltered;
        resultBatch.data = resGetOrders.data.orders;

        next(new HttpRestResponse(HttpStatus.OK, 'Get orders done successfully', 'Get orders done successfully', resultBatch));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getAllSubscribesHandler(req, res, next) { //TODO:
    try {

        let resultBatch = {
            recordsTotal: 0,
            recordsFiltered: 0,
            data: []
        }

        const queryData = req.body;

        let searchValue = (queryData['search[value]'] == null ? "" : queryData['search[value]']);

        let queryParameters = {
            limit: parseInt(queryData.length),
            offset: parseInt(queryData.start),
            order: [],
            whereLike: searchValue
        }

        let listInfo = await req.core.channelsManager.getAllSubscribes(queryParameters);

        let subscribes = listInfo.data.data;

        list = Object.values(subscribes).map(subscribe => {
            return {
                ...subscribe
            }
        });

        var subscribesList = [];

        for (subscribeId in list) {
            const subscribeProps = list[subscribeId];

            subscribesList.push({
                id: subscribeProps.id,
                channelId: subscribeProps.channelId,
                orderId: subscribeProps.orderId,
                userId: subscribeProps.userId,
                status: subscribeProps.status,
                isFree: subscribeProps.isFree,
                userInfo: subscribeProps.userInfo
            });
        }

        let resultData = subscribesList;

        resultBatch.recordsTotal = listInfo.data.recordsTotal;
        resultBatch.recordsFiltered = listInfo.data.recordsFiltered;
        resultBatch.data = resultData;

        next(new HttpRestResponse(HttpStatus.OK, 'Getting batch from db done successfully', 'subscribesList handler', resultBatch));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function addNewSubscribeHandler(req, res, next) {
    try {
        let params = req.body;

        params.invoker = 'admin_panel';

        let addSubInfo = await req.core.channelsManager.addSubscribe(params);

        if (addSubInfo.status != ResultStatus.OK) {
            throw addSubInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Add new subscribe done successfully', 'Add new subscribe done successfully', addSubInfo));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getDepositInfoBySubscribeHandler(req, res, next) {
    try {
        let id = req.params.id;

        let getDepoInfo = await req.core.channelsManager.getDepositInfo({
            id: id
        });

        if (getDepoInfo.status != ResultStatus.OK) {
            throw getDepoInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Get deposit done successfully', 'Get deposit info done successfully', getDepoInfo.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function editDepositStatusHandler(req, res, next) {
    try {
        let id = req.params.id;

        let status = req.body.status;

        if (status == null) {
            throw new HttpRestResponse(HttpStatus.BAD_REQUEST, 'Status in null', 'Status in null');
        }

        let params = {
            orderId: id,
            status: status
        }

        let updateDepoInfo = await req.core.channelsManager.updateDepositStatus(params);

        if (updateDepoInfo.status != ResultStatus.OK) {
            throw updateDepoInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Edit deposit status done successfully', 'Edit deposit status done successfully', updateDepoInfo.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function getDepositStatusesHandler(req, res, next) {
    try {
        let getDepositStatusesInfo = await req.core.channelsManager.getDepositStatuses();

        if (getDepositStatusesInfo.status != ResultStatus.OK) {
            throw getDepositStatusesInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Get deposit statuses done successfully', 'Get deposit statuses done successfully', getDepositStatusesInfo.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

async function editOrderInfoHandler(req, res, next) {
    try {
        let params = req.body;

        params.orderId = req.params.id;

        let editOrderInfo = await req.core.channelsManager.editOrderInfo(params);

        if (editOrderInfo.status != ResultStatus.OK) {
            throw editOrderInfo;
        }

        next(new HttpRestResponse(HttpStatus.OK, 'Edit deposit info done successfully', 'Edit deposit info done successfully', editOrderInfo.data));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

module.exports = router;