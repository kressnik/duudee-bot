'use strict'

const express = require('express');
const router = express.Router();

const { BasicResponse, ResultStatus, BasicResponseByError } = require("../interfaces/basicResponse");
const { HttpRestResponse, HttpStatus } = require('../interfaces/httpRestResponse');

router.get("/helth-check", reportHandler);

async function reportHandler(_req, _res, _next) {
    try {
        const reportDTO = {
            uptime: process.uptime()
        }

        _next(new HttpRestResponse(HttpStatus.OK, "helth-check", "helth-check", reportDTO));
    } catch (e) {
        let response = e instanceof HttpRestResponse ? e : new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.message, "", e);

        _next(response);
    }
}//reportHandler

module.exports = router;
