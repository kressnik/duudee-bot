`use strict`

const express = require('express');
const router = express.Router();
const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("../interfaces/basicResponse");

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');

const {
    AppLoggerClass
} = __UTILS;

let logger = new AppLoggerClass('User controller', process.env.USER_CONTRROLLER_LOG_LEVEL);

router.post('/users', getAllUsersHandler);

async function getAllUsersHandler(req, res, next) { //TODO:
    try {
        const {
            length,
            start = 0,
        } = req.body;

        let searchValue = (req.body['search[value]'] == null ? "" : req.body['search[value]']);

        let queryParameters = {
            limit: parseInt(length),
            offset: parseInt(start),
            order: [],
            whereLike: searchValue
        }

        let listInfo = await req.core.channelsManager.getAllUsers(queryParameters);

        let users = listInfo.data.data;

        list = Object.values(users).map(user => {
            return {
                ...user
            }
        });

        var userList = [];

        for (userId in list) {
            const userProps = list[userId];

            let fullName = `${userProps.firstName} ${userProps.lastName}`;

            userList.push({
                id: userProps.id,
                name: fullName,
                username: userProps.username,
                tlgUserId: userProps.tlgUserId,
                tlgInviteLink: userProps.inviteLink,
                role: userProps.role,
                state: userProps.state,
                channels: userProps.channels
            });
        };

        next(new HttpRestResponse(HttpStatus.OK, 'Getting batch from db done successfully', 'userList handler', {
            recordsTotal: listInfo.data.recordsTotal,
            recordsFiltered: listInfo.data.recordsFiltered,
            data: userList
        }));
    } catch (error) {
        if (error instanceof HttpRestResponse) {
            next(error);
        } else {
            next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message, error.message, error.data));
        }
    }
}

module.exports = router;