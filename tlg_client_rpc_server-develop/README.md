## How to start:
1. **Strongly** recommended to use `virtualenv`
2. Install requirements: `pip install -r ./requirements.txt` or `pip install -r ./requirements.txt` for developing
3. Set environment variables:
    + *Server* related variables should start with prefix `RPC_SERVER_`
    + List of supported variables: https://www.uvicorn.org/settings/
    + Script will automatically truncate server variables' prefix before pass them to `uvicorn`
    + *Service* related variables define paths for mapping file (see below) and session files:
        + `RPC_SERVICE_CONF_PATH` - directory for service mapping file (default `./`)
        + `RPC_SERVICE_SESS_PATH` - directory for Telethon session files (default `./sessions/`)
4. Start service. Entry point: *launch.py* (For example: `python ./launch.py`)

## Entry points (URLs):
1. `http://{host}:{port}/v1/jsonrpc`
2. `http://{host}:{port}/v1/rpc`

## Notes:
+ **URL ALWAYS MUST END WITH `/`**
+ For some reason when you request admin logs first time you **have to** specify **channel id** or **invite URL**. After that channel name also can be used.
+ All Telethon related data stored in *session* files (i.e. `foobar.session`).
Default path to folder with session files: `./sessions/`
+ Service has preliminary support for multi-user operation.
Currently it just creates mapping file (`tgc-conf.json`) between **API_ID**, **API_HASH** and session files.
+ To completely remove all sensitive information remove session files and `tgc-conf.json`
+ Packets instalation: `python -m pip install -r requirements.txt`. In case of errors like:
`...fatal error: Python.h: No such file or directory...`
Install linux package: `apt install python3.x-dev` where `x` - current python minor version

##Docker

**build container**
```docker build -t d2/tlg_client_rpc_server .```

**run container** Run container by docker-compose for set env, cashes and other things
```docker-compose -f dco_main.yaml up```