import os

import uvicorn

if __name__ == "__main__":
    opts = {}

    for k, v in os.environ._data.items():
        if isinstance(k, bytes):
            k = k.decode('utf-8')
        if not k.startswith('RPC_SERVER_'):
            continue

        if isinstance(v, bytes):
            v = v.decode('utf-8')
        opts[k.split('RPC_SERVER_')[1].lower()] = v

    if opts.get('port', None):
        opts['port'] = int(opts['port'])

    uvicorn.run('service:app', **opts)
