import logging

import uvicorn
import json

from starlette.applications import Starlette
from starlette_jsonrpc import dispatcher
from starlette_jsonrpc.endpoint import JSONRPCEndpoint
from starlette_jsonrpc.exceptions import JSONRPCUserException
#from tlg_client import ApiException
try:
    from tlg_client import ApiException
    from tlg_client import TlgClient
except ModuleNotFoundError:
    from service.tlg_client  import ApiException
    from service.tlg_client import TlgClient


logger = logging.getLogger(__name__)

app = Starlette(debug=True)
_tele_client = TlgClient()


def tlg_err2json_rps(tlg_ecode: int):
    try:
        return (-tlg_ecode) - 32000
    except Exception as e:
        logger.exception(e)
        logger.exception('tlg_err2json_rps:')
        logger.exception(tlg_ecode)

        return -32603



@dispatcher.add_method(name='user.send.ApiInfo')
async def api_info(params):
    api_id = params['api_id']
    api_hash = params['api_hash']
    try:
        return await _tele_client.set_api_info(api_id, api_hash)
    except Exception as e:
        logger.exception(e)
        # re-raising KeyError as workaround because starlette_jsonrpc currently catch only this error type
        raise KeyError(e) from e


@dispatcher.add_method(name='user.send.StartAuth')
async def start_auth(params):
    phone = params['phone']
    try:
        return await _tele_client.start_auth(phone)
    except Exception as e:
        logger.exception(e)
        # re-raising KeyError as workaround because starlette_jsonrpc currently catch only this error type
        raise KeyError(e) from e


@dispatcher.add_method(name='user.send.AuthCode')
async def auth_code(params):
    code = params['code']
    try:
        return await _tele_client.send_auth_code(code)
    except Exception as e:
        logger.exception(e)
        # re-raising KeyError as workaround because starlette_jsonrpc currently catch only this error type
        raise KeyError(e) from e


@dispatcher.add_method(name='channels.get.adminLog')
async def admin_logs(params):
    channel = params.pop('channel')
    logger.info(f'Start fetching logs for channel:[{channel}]')

    try:
        return await _tele_client.get_admin_logs(channel=channel, **params)
    except ApiException as e:
        logger.warning('ApiException')
        logger.warning(e)
        
        code = 500
        message = f'{e}'

        if hasattr(e, 'code'):
            logger.exception('code lost')
            code = tlg_err2json_rps(e.code)
        if hasattr(e, 'message'):
            message = e.message

        raise JSONRPCUserException(code, message, error=f'{e}') from e

    except Exception as e:
        logger.warning('Exception')
        logger.warning(e)

        code = 500
        message = f'{e}'

        if hasattr(e, 'code'):
            logger.exception('code lost')
            code = tlg_err2json_rps(e.code)
        if hasattr(e, 'message'):
            message = e.message

        raise JSONRPCUserException(code, message, error=f'{e}') from e
        # re-raising KeyError as workaround because starlette_jsonrpc currently catch only this error type


@dispatcher.add_method(name='auth.get.Status')
async def auth_status(params):
    try:
        return await _tele_client.get_auth_status()
    except Exception as e:
        logger.exception(e)
        # re-raising KeyError as workaround because starlette_jsonrpc currently catch only this error type
        raise KeyError(e) from e

app.mount('/v1/jsonrpc', JSONRPCEndpoint)
app.mount('/v1/rpc', JSONRPCEndpoint)

if __name__ == '__main__':
    uvicorn.run(app, port=5000, debug=True, log_level='debug')
