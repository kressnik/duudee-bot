import random
import itertools
import json
import logging
from pathlib import Path
import os
import shutil
from types import SimpleNamespace
from typing import Optional
import copy
import re
import datetime
from functools import partial
from enum import IntEnum

from telethon import TelegramClient


logger = logging.getLogger(__name__)


class AuthStatus(IntEnum):
    EMPTY = 0,
    PENDING = 1,
    DONE = 2,
    NOT_INIT = 3,
    ERROR = -1

    def __str__(self):
        return self.name


class TlgError(IntEnum):
    UNAUTHORIZED = 401,
    SEE_OTHER = 303,
    BAD_REQUEST = 400,
    FORBIDDEN = 403,
    NOT_ACCEPTABLE = 406,
    FLOOD = 420,
    INTERNAL = 500


class ApiException(Exception):
    pass


class TlgClient:
    _CONF_NAME = 'tgc-conf.json'
    DEF_CONF_PATH = './'
    DEF_SESSIONS_PATH = './sessions/'

    CONF_PATH = Path(os.getenv('RPC_SERVICE_CONF_PATH', './')).joinpath(_CONF_NAME)
    SESSIONS_PATH = os.getenv('RPC_SERVICE_SESS_PATH', './sessions/')

    def __init__(self):
        self.api_info = SimpleNamespace(
            id=0, hash='', sess_id='', phone='', authorized=False)
        self.conf = self._load_config()

        self.client = None
        self._auth_status = AuthStatus.ERROR

        if not os.path.exists(self.SESSIONS_PATH):
            os.makedirs(self.SESSIONS_PATH)

    def _word_generator(self):
        vowels = 'aiueo'
        consonants = 'bcdfgjklmnprstvwxz'
        while True:
            yield random.choice(consonants)
            yield random.choice(vowels)

    def _generate_name(self, len_=12) -> str:
        name = ''.join(itertools.islice(self._word_generator(), len_))
        logger.info(f'New name was generated: {name}')
        return name

    def _load_config(self) -> dict:
        logger.info(f'Loading config from {self.CONF_PATH}')
        try:
            with open(self.CONF_PATH, 'rt') as fconf:
                conf = json.load(fconf)
        except FileNotFoundError:
            logger.warning(
                f'Config file not found. Creating empty config file: {self.CONF_PATH}')
            conf = []
            self._save_config(conf)
        return conf

    def _save_config(self, config):
        logger.info(f'Saving config to {self.CONF_PATH}')
        with open(self.CONF_PATH, 'wt') as fconf:
            json.dump(config, fconf)

    def _get_conf_entry_id(self, id_: int, hash_: str) -> Optional[int]:
        entry_id = None

        for i, item in enumerate(self.conf):
            id_ = item.get('id', None)
            hash_ = item.get('hash', None)

            logger.info(f'id:[{id_}] hash:[{hash_}]')

            if self.api_info.id == id_ and self.api_info.hash == hash_:
                entry_id = i
                break

        return entry_id

    def _update_phone(self, phone: str):
        entry_id = self._get_conf_entry_id(self.api_info.id, self.api_info.hash)

        if entry_id is None:
            raise ValueError((f"Can't find configuration for API_ID:{self.api_info.id}"
                              f" and API_HASH:{self.api_info.hash}"))
        self.conf[entry_id]['phone'] = phone
        self._save_config(self.conf)

    def _get_client_session(self, create=True) -> Path:
        entry_id = self._get_conf_entry_id(self.api_info.id, self.api_info.hash)

        self.api_info.sess_id = self.conf[entry_id].get('session_id') if entry_id is not None else None

        if not self.api_info.sess_id and create:
            logger.info(
                f"Name for id:{self.api_info.id}, hash:{self.api_info.hash} wasn't found in config. Generating new one")

            self.api_info.sess_id = self._generate_name()
            self.conf.insert(0,
                             {'id': self.api_info.id, 'hash': self.api_info.hash, 'session_id': self.api_info.sess_id})
            self._save_config(self.conf)

        return Path(self.SESSIONS_PATH).joinpath(self.api_info.sess_id).with_suffix('.session')

    def _logs2json(self, logs: str):
        patt = re.compile((r'(?P<func>(?:^|(?<==|,))[\w]*?(?:(?=\()))|(?P<time>datetime\.\w+\(.+?\))|'
                           r'(?P<none>None)|(?P<bool>True|False)|(?P<eq>=)|(?P<rbr>\))|(?P<key>\w+(?:(?==)))|'
                           r'(?P<lbr>\()|(?P<text>\'.+?\'(?=,))'))

        qout = ('"', '\\"')
        s_qout = ("\\'", "'")
        handlers = {
            'func': lambda v: f'{{"type":"{v}",',
            'eq': lambda v: ':',
            'time': lambda v: str(int(datetime.datetime.timestamp(
                eval('datetime.datetime(2019, 11, 28, 21, 10, 25,\
                     tzinfo=datetime.timezone.utc)')))),
            'bool': lambda v: v.lower(),
            'rbr': lambda v: '}',
            'lbr': lambda v: '',
            'none': lambda v: '"null"',
            'key': lambda v: f'"{v}"',
            'text': lambda v: f'"{v[1:-1].replace(qout[0], qout[1]).replace(s_qout[0], s_qout[1])}"'
        }

        def replace_(handlers, matchobj):
            # re.sub call `repl` with only one group at time
            dct = [(k, v)
                   for k, v in matchobj.groupdict().items() if v is not None][0]
            return handlers[dct[0]](dct[1])

        repl = partial(replace_, handlers)

        logger.debug(f'Raw logs:\n{logs}')

        res = re.sub(patt, repl, logs)
        res = res.replace(',}', '}')
        dst = ['[', res, ']']
        return json.loads(''.join(dst))

    def _map_filters(self, filters: Optional[dict]) -> dict:
        if not filters:
            return {}
        tmp = {
            'restrict': filters.pop('ban', None),
            'unrestrict': filters.pop('unban', None),
            'ban': filters.pop('kick', None),
            'unban': filters.pop('unkick', None)
        }

        telethon_filters = copy.copy(filters)
        telethon_filters.update(tmp)
        return telethon_filters

    async def _connect(self, session_path=None):
        try:
            self.api_info.id = self.api_info.id if self.api_info.id else self.conf[0].get('id', 0)
            self.api_info.hash = self.api_info.hash if self.api_info.hash else self.conf[0].get('hash', '')
        except IndexError as e:
            raise ApiException('Unable to connect - API info not present') from e

        try:
            self.api_info.phone = self.api_info.phone if self.api_info.phone else self.conf[0].get('phone', '')
        except IndexError:
            pass

        sess = Path(session_path) if session_path else self._get_client_session()

        logger.info(f'Creating client with API: id:{self.api_info.id}, hash:{self.api_info.hash}')
        self.client = TelegramClient(str(sess), self.api_info.id, self.api_info.hash)
        await self.client.connect()
        self._auth_status = AuthStatus.EMPTY if not await self.client.is_user_authorized() else AuthStatus.DONE

    async def set_api_info(self, api_id: int, api_hash: str):
        self.api_info.id = api_id
        self.api_info.hash = api_hash

        await self._connect()

        logger.info((f'Creating TelegramClient instance for id:{self.api_info.id},'
                     f'hash:{self.api_info.hash} with session ID:"{self.api_info.sess_id}"'))

        logger.info(f'Auth status: {self._auth_status}')

        return {'status': 'OK'}

    async def start_auth(self, phone: str):
        self.api_info.phone = phone
        res = SimpleNamespace(status='UNEXPECTED_ERROR')
        if not await self.client.is_user_authorized():
            self.phone_hash = await self.client.send_code_request(phone)
            res.status = 'WAITING_CODE'
            self._auth_status = AuthStatus.PENDING
        else:
            res.status = 'OK'
        self._update_phone(phone)
        return res.__dict__

    async def send_auth_code(self, code):
        await self.client.sign_in(self.api_info.phone,
                                  code, phone_code_hash=self.phone_hash.phone_code_hash)
        self._auth_status = AuthStatus.DONE
        try:
            sess = self._get_client_session()
            backup_file = sess.with_suffix(sess.suffix + '.bak')
            if backup_file.exists():
                backup_file.unlink()
            shutil.copy2(sess, backup_file)
        except Exception as e:
            logger.warning(f'Exception "{str(e)}" ocurred on creating backup file')
        return {'status': 'OK'}

    async def get_auth_status(self):
        res = SimpleNamespace(status=str(AuthStatus.ERROR), phone='')

        try:
            if not self.client:
                await self._connect()
        except ApiException:
            self._auth_status = AuthStatus.NOT_INIT
        else:
            res.phone = self.api_info.phone

        res.status = str(self._auth_status)

        return res.__dict__


    async def get_admin_logs(self, channel: str, limit: int = None,
                             max_id: int = 0, min_id: int = 0, q: str = None, filters: dict = None):
        '''Arguments
            channel     InputChannel Channel
            limit       int Maximum number of results to return, see pagination
            max_id      long Maximum ID of message to return (see pagination)
            min_id      long Minimum ID of message to return (see pagination)
            q           string Search query, can be empty
            filters     Optional. Set of boolean events’ filters
                join    Bool
                leave   Bool
                invite  Bool
                ban     Bool
                unban   Bool
                kick    Bool
                unkick  Bool
                promote Bool
                demote  Bool
                info    Bool
                settings    Bool
                pinned  Bool
                edit    Bool
                delete  Bool
        '''
        if not self.client:
            await self._connect()

        from telethon.tl.types import PeerChannel

        entity = PeerChannel(channel) if isinstance(channel, int) else channel

        ch_entity = await self.client.get_entity(entity)

        logs = await self.client.get_admin_log(
            ch_entity, limit, max_id=max_id, min_id=min_id, search=q, **self._map_filters(filters))

        return self._logs2json(','.join((str(log) for log in logs)))
