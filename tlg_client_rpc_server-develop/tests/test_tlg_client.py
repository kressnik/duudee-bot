import os
from pathlib import Path
import shutil
import json
from types import SimpleNamespace as SN

import pytest
from pytest_mock import mocker


from service.tlg_client import TlgClient


# def spy_decorator(func):
#     m = mocker.MagicMock()

#     def wrapper(self, *args, **kwargs):
#         m(*args, **kwargs)
#         return func(self, *args, **kwargs)
#     wrapper.mock = m
#     return wrapper


class BaseTlg:
    @pytest.fixture
    def tclient(self):
        restore_conf = False
        bak_path = f'{TlgClient.CONF_PATH}.tbak'
        if os.path.exists(TlgClient.CONF_PATH):
            shutil.move(TlgClient.CONF_PATH, bak_path)
            restore_conf = True

        yield

        try:
            os.remove(TlgClient.CONF_PATH)
        except OSError:
            pass
        if restore_conf:
            shutil.move(bak_path, TlgClient.CONF_PATH)

    @pytest.fixture
    def new_conf(self, tclient):
        with open(TlgClient.CONF_PATH, 'wt') as f_conf:
            f_conf.write('[]')
        yield
        with open(TlgClient.CONF_PATH, 'rt') as f_conf:
            conf = json.load(f_conf, object_hook=lambda d: SN(**d))
            try:
                s_path = Path(TlgClient.SESSIONS_PATH).joinpath(
                    conf[0].session_id).with_suffix('.session.bak')
                os.remove(s_path)
                s_path = s_path.with_suffix('')
                os.remove(s_path)
            except (IOError, IndexError):
                pass

    @pytest.fixture
    def telethon(self, mocker):
        mocker.patch('tlg_client.TelegramClient')
        TlgClient.TelegramClient.connect.return_value = True


class TestInit(BaseTlg):
    def test_conf_file_created(self, tclient, mocker):
        mocker.spy(TlgClient, '_save_config')
        tc = TlgClient()
        assert tc._save_config.call_count == 1 and os.path.exists(
            TlgClient.CONF_PATH)
        with open(TlgClient.CONF_PATH, 'rt') as cfg:
            data = cfg.read()
            assert data == '[]'

    def test_conf_file_not_created(self, new_conf, mocker):
        mocker.spy(TlgClient, '_save_config')
        tc = TlgClient()
        assert tc._save_config.call_count == 0


class TestApiInfo(BaseTlg):
    def test_api_added_to_cfg(self, new_conf, mocker, telethon):
        tc = TlgClient()

        mocker.patch.object(TlgClient, '_generate_name')
        sess_id = 'foobar'
        id_ = 12345
        hash_ = '0123456789abcdef0123456789abcdef'

        tc._generate_name.return_value = sess_id

        res = tc.set_api_info(id_, hash_)

        assert res == {'status': 'OK'}

        with open(tc.CONF_PATH, 'rt') as f_conf:
            conf = json.load(f_conf, object_hook=lambda d: SN(**d))
            assert conf[0].id == id_ and conf[0].hash == hash_ and conf[0].session_id == sess_id

    def test_api_session_files_created(self, new_conf, mocker):
        tc = TlgClient()

        mocker.patch.object(TlgClient, '_generate_name')
        sess_id = 'foobar'
        id_ = 12345
        hash_ = '0123456789abcdef0123456789abcdef'

        tc._generate_name.return_value = sess_id

        res = tc.set_api_info(id_, hash_)

        sess_bak_path = Path(TlgClient.SESSIONS_PATH).joinpath(
            conf[0].session_id).with_suffix('.session.bak')
        sess_path = sess_bak_path.with_suffix('')
        assert os.path.exists(sess_path) and os.path.exists(sess_bak_path)

class TestAuthStatus(BaseTlg):
    @pytest.mark.asyncio
    async def test_uninit(self):
        tc = TlgClient()

        res = await tc.get_auth_status()
        assert res['status'] == 'NOT_INIT' and res['phone'] == ''
