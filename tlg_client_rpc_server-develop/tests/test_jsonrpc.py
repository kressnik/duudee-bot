# import copy
# import random

# import pytest
# from pytest_mock import mocker

# from service.tlg_client import TlgClient


# def rpc_with_id(id: str = None):
#     return {
#         'jsonrpc': '2.0',
#         'id': id if id else str(random.randint(1, 1000)),
#         'params': {}
#     }


# ENTRY_POINT = '/v1/jsonrpc'


# class BaseRPC:
#     def add_params(self, **kwargs):
#         self._data['params'].update(**kwargs)

#     def check_id(self, res):
#         return res['id'] == self._data['id']

#     def check_error(self, res, expect=False, code=None):
#         err_rec = ('error' in res.keys()) == expect
#         code_res = True
#         if expect and code:
#             code_res = res['error']['code'] == code
#         return err_rec and code_res


# class TestApiInfo(BaseRPC):
#     @pytest.fixture(autouse=True)
#     def _mock_api(self, monkeypatch):
#         monkeypatch.setattr(TlgClient, 'set_api_info',
#                             lambda self_, *args, **kwargs: '{"status":"OK"}')

#     @pytest.fixture(autouse=True)
#     def _make_rpc_data(self):
#         self._data = rpc_with_id()
#         self._data['method'] = 'user.send.ApiInfo'

#     def test_empty_api_id(self, client):
#         self.add_params(api_hash='0123456789abcdef0123456789abcdef')
#         res = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(
#             res, expect=True, code=-32602)

#     def test_empty_api_hash(self, client):
#         self.add_params(api_id=12345)
#         res = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(
#             res, expect=True, code=-32602)

#     def test_valid_info(self, client):
#         self.add_params(
#             api_id=12345, api_hash='0123456789abcdef0123456789abcdef')
#         res = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(res)


# class TestStartAuth(BaseRPC):
#     @pytest.fixture(autouse=True)
#     def _mock_api(self, monkeypatch):
#         monkeypatch.setattr(TlgClient, 'start_auth',
#                             lambda self_, *args, **kwargs: '{"status":"OK"}')

#     @pytest.fixture(autouse=True)
#     def _make_rpc_data(self):
#         self._data = rpc_with_id()
#         self._data['method'] = 'user.send.StartAuth'

#     def test_empty_phone(self, client):
#         res = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(
#             res, expect=True, code=-32602)

#     def test_valid_phone(self, client):
#         self.add_params(phone='+380900000000')
#         res: dict = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(res)


# class TestAuthCode(BaseRPC):
#     @pytest.fixture(autouse=True)
#     def _mock_api(self, monkeypatch):
#         monkeypatch.setattr(TlgClient, 'send_auth_code',
#                             lambda self_, *args, **kwargs: '{"status":"OK"}')

#     @pytest.fixture(autouse=True)
#     def _make_rpc_data(self):
#         self._data = rpc_with_id()
#         self._data['method'] = 'user.send.AuthCode'

#     def test_empty_code(self, client):
#         res: dict = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(
#             res, expect=True, code=-32602)

#     def test_valid_code(self, client):
#         self.add_params(code='12345')
#         res: dict = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(res)


# class TestAdminLog(BaseRPC):
#     @pytest.fixture(autouse=True)
#     def _mock_api(self, monkeypatch):
#         self._answ = '[{"type":"ChannelAdminLogEvent","id":550223416412,"date":1574975425,"user_id":519435845,"action":{"type":"ChannelAdminLogEventActionParticipantInvite","participant":{"type":"ChannelParticipantSelf","user_id":418568144,"inviter_id":519435845,"date":1574975425}}}]'
#         monkeypatch.setattr(TlgClient, 'get_admin_logs',
#                             lambda self_, *args, **kwargs: self._answ)

#     @pytest.fixture(autouse=True)
#     def _make_rpc_data(self):
#         self._data = rpc_with_id()
#         self._data['method'] = 'channels.get.adminLog'

#     def test_empty_params(self, client):
#         res: dict = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(
#             res, expect=True, code=-32602)

#     def test_valid_channel(self, client):
#         self.add_params(channel='t.me/test')
#         res: dict = client.post(ENTRY_POINT, json=self._data).json
#         assert self.check_id(res) and self.check_error(
#             res) and res['result'] == self._answ


# def test_hello(client):
#     res = client.get('/hello')
#     assert res.status_code == 200
#     assert res.data == b"Hello, World!"
