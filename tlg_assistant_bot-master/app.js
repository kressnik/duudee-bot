const express = require('express');
const cors = require("cors");
const session = require('express-session');
const helmet = require('helmet');
const path = require('path');

require('express-async-errors');

const Routes = require("./routes");

console.time('Reqire core');
const {
    Core
} = require('./core');
console.timeEnd('Reqire core');

//reqire to set DI utils
require('./utils');

/**
 * utils
 */
const {
    AppLoggerClass
} = __UTILS;
/**
 * interfaces
 */
const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = require("./interfaces/basicResponse");
const {
    HttpRestResponse,
    HttpStatus
} = require('./interfaces/httpRestResponse');


/**
 * Class
 */
class App {
    constructor() {
        this._express = express();
        this._logger = new AppLoggerClass('App', process.env.APP_LOG_LEVEL);

    }

    async create() {
        try {
            this._logger.info(`NODE_ENV: ${process.env.NODE_ENV}`);

            //create core
            this.core = new Core();

            let initResult = await this.core.init();

            if (initResult.status != ResultStatus.OK) {
                throw new Error(`${initResult.message} - ${initResult.status}`);
            }

            this._logger.info(`Init Core ${initResult}`);

            // let db_dir = parsePathDb(process.env.DB_DIR);

            // await models.init(db_dir);

            //link json parser middleware
            this._express.use(express.json({
                limit: '50mb'
            }));

            this._express.use(helmet());

            //ACCEPT all request source by cors
            this._express.use(cors({
                orign: "*"
            }));

            //link urlencoded parser middleware
            this._express.use(express.urlencoded({
                extended: false
            }));



            this._express.use(session({
                secret: "knvljdsfnvlbnefvlscvskjvkvkj",
                resave: true,
                saveUninitialized: true
            }));

            //add load static files from public dir
            //app.use(express.static(path.join(__dirname, 'public')));

            // Register '.mustache' extension with The Mustache Express

            //this._express.engine('html', mustacheExpress())
            //this._express.set('view engine', 'html');
            //
            //this._express.set('views', (path.join(__dirname, 'public')));

            this._express.use('/facebook', express.static(path.join(__dirname, '/public/facebook')));

            /**
             * App midleware, for assign local class
             */
            this._express.use((_req, _res, _next) => {
                _req.core = this.core;

                _next();
            });

            /**
             * routers
             */

            this._express.use(Routes);

            this._express.use(this.errorHandlerMiddle.bind(this));
            this._express.use(this.defaultRoute.bind(this));

            let rsp = new BasicResponse(ResultStatus.OK, 'Create done new App successfully.');

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    /**
     * Handle function
     */

    errorHandlerMiddle(_outBatch, _req, _res, _next) {
        let code = HttpStatus.INTERNAL_SERVER_ERROR;

        if (_outBatch.code == null) {
            console.error(_outBatch);
            _outBatch = {
                time: Date.now(),
                code: HttpStatus.INTERNAL_SERVER_ERROR,
                message: _outBatch.message,
                meassgeCode: "SYSTEM_ERROR",
                method: _req.method,
            };
        } else {
            code = _outBatch.code;
        }


        _res.status(code).send(_outBatch);
    };

    defaultRoute(_req, _res, _next) {
        const respStub = {
            time: Date.now(),
            code: HttpStatus.NOT_FOUND,
            message: `Route doesn't found.  ${_req.path}`,
            meassgeCode: "SYSTEM_ROUTE_NOT_FOUND",
            method: _req.method,
        };

        console.warn(respStub.code + ":" + respStub.message);

        _res.status(HttpStatus.NOT_FOUND).send(respStub);
    };

} //class App

// function parsePathDb(_path) {
//     try {

//         if (_path.split('\\').length < 2) {
//             return process.env.ENV_FILE == "build" ? path.join(path.dirname(process.execPath), _path) : path.join(__dirname, _path);
//         } else {
//             return _path;
//         }


//     } catch (_error) {
//         console.error(_error);
//     }
// }

module.exports.App = App;