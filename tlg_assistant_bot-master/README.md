# tlg_assistant_bot
**ENV**
```
    "HTTP_PORT": "9001",
    "TELEGRAM_TOKEN": ".....:......",

    "DB_USERNAME": "root",
    "DB_PASSWORD": "",
    "DB_DATABASE": "tlg_assistant_bot",
    "DB_PORT": "3306",
    "DB_HOST": "localhost",

    "KUNA_PUBLIC_TOKEN": ".....",
    "KUNA_PRIVATE_TOKEN": ".....",

    "DEPOSIT_SDK_UAH_PUBLIC_KEY": "...",
    "DEPOSIT_SDK_USD_PUBLIC_KEY": "....",
    "DEPOSIT_SDK_RUB_PUBLIC_KEY": "...",
    "DEPOSIT_SDK_UAH_WORLDWIDE_PUBLIC_KEY": ".."

    //use fake deposit data
    "USE_FAKE": "true",

    //migrate "UP"/"DOWN"
    "DB_DIRECTORY": "db"
    "MIGRATION_VERSION": "last",

    **create data base**

    CREATE DATABASE `tlg_assistant_bot` DEFAULT CHARACTER SET = `utf8mb4` DEFAULT COLLATE = `utf8mb4_general_ci`;

    **add channels**
   INSERT INTO `tlg_assistant_bot`.`channels` (`id`, `name`, `description`, `in_use`, `tlg_id`, `price`, `currency`, `fee_at_own_expense`, `subscription_duration`, `is_free`, `max_subcribes_qty`, `created_at`, `updated_at`) VALUES ('1', 'Test', 'for test', '1', '1329485750', '100', 'RUB', '1', '100', '1', '10', NOW(), NOW());


    **add buttons**
    INSERT INTO `assistant_buttons` (`id`, `is_active`, `name`, `description`, `user_group`, `created_at`, `updated_at`) VALUES ('2', '0', '📢 Подписки | Бесплатно', '1', '1', '2019-10-01 20:36:49', '2019-10-01 20:36:49');

    INSERT INTO `assistant_buttons` (`id`, `is_active`, `name`, `description`, `user_group`, `created_at`, `updated_at`) VALUES ('1', '1', '📢 Подписки | Приват', '1', '1', '2019-10-01 20:36:49', '2019-10-01 20:36:49');

    // all invites save into invite_action_model
    // inviteId is STRING(55)

    // start bot with invite link looks like

    telegram.me/<BOT_NAME>?start=<INVITE_ID_HERE>
 
 telegram.me/@testBotEvryThingsBot?start=invite-3-2

    // 
INSERT INTO `kuna_depo_channels` (`id`, `name`, `settings`, `is_active`, `is_force`, `created_at`, `updated_at`) VALUES ('1', 'deposit_sdk_uah_worldwide', '{\"services\": {\"payment_card_uah_hpp\": {\"enabled\": false}}}', '1', '1', '2020-01-27 19:28:12', '2020-01-27 19:28:12');
INSERT INTO `kuna_depo_channels` (`id`, `name`, `settings`, `is_active`, `is_force`, `created_at`, `updated_at`) VALUES ('2', 'deposit_sdk_rub', '{\"services\": {\"qiwi_rub_hpp\": {\"enabled\": false}}}', '1', '1', '2020-01-27 19:28:12', '2020-01-27 19:28:12');
INSERT INTO `kuna_depo_channels` (`id`, `name`, `settings`, `is_active`, `is_force`, `created_at`, `updated_at`) VALUES ('3', 'deposit_sdk_uah', '{\"services\": {\"qiwi_rub_hpp\": {\"enabled\": false}, \"payment_card_uah_hpp\": {\"enabled\": true}}}', '1', '1', '2020-01-27 19:28:12', '2020-01-27 19:28:12');

```


RPC
```
{
	"jsonrpc": "2.0", 
	"method": "channel.watcher.auth.send.code", 
	"params": {
		"code": "34601"
	}, 
	"id": {{$timestamp}}
}

{
    "jsonrpc": "2.0",
    "id": 1588775999,
    "result": {
        "status": "OK",
        "message": "Send auth code:[34601] returned status:[OK]",
        "data": {
            "status": "OK"
        }
    }
}
```

```
{
	"jsonrpc": "2.0", 
	"method": "channel.watcher.auth.send.start", 
	"params": {
		"phone": "+380502947272"
	}, 
	"id": {{$timestamp}}
}

{
    "jsonrpc": "2.0",
    "id": 1588775827,
    "result": {
        "status": "OK",
        "message": "Send start auth for phone:[+380502947272] return status:[WAITING_CODE]",
        "data": {
            "status": "WAITING_CODE"
        }
    }
}
```

```
{
	"jsonrpc": "2.0", 
	"method": "channel.watcher.send.api.info", 
	"params": {
		"apiId": "831856",
		"apiHash": "70e49300dc784e41b9f3f73f013500c3"
	}, 
	"id": {{$timestamp}}
}

{
    "jsonrpc": "2.0",
    "id": 1588775774,
    "result": {
        "status": "OK",
        "message": "Send api info auth apiId:[831856], apiHash:[70e49300dc784e41b9f3f73f013500c3] return status:[OK]",
        "data": {
            "status": "OK"
        }
    }
}
```


```
{
	"jsonrpc": "2.0", 
	"method": "channel.watcher.auth.get.status", 
	"params": {
		
	}, 
	"id": {{$timestamp}}
}
 
 status:   
    EMPTY
    PENDING
    DONE
    NOT_INIT
    ERROR
{
    "jsonrpc": "2.0",
    "id": 1588775813,
    "result": {
        "status": "OK",
        "message": "Get auth status return status:[EMPTY], phone:[]",
        "data": {
            "status": "EMPTY",
            "phone": ""
        }
    }
}
```

