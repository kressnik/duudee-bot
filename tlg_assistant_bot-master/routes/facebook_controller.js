'use strict';

const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
var ejs = require('ejs');

const {
    BasicResponse,
    ResultStatus
} = require('../interfaces/basicResponse');

const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');

const tamplate = fs.readFileSync(path.join(__dirname, '../public/facebook/preloader.ejs'));

router.get('/facebook/:token', facebookPreloaderHandler);

async function facebookPreloaderHandler(_req, _res, _next) {
    try {
        console.log(`recive facebook promotion by token:[${_req.params.token}]`);

        const result = await _req.core.assistantProcessor.promotionRequestHandler(_req.params.token);

        result.isOK();

        console.log(result);

        const html = ejs.render(tamplate.toString(), {
            pixel_id: result.data.pixelId,
            bot_username: result.data.botUsername,
            token: result.data.token
        });

        _res.send(html);
    } catch (_error) {
        _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'UNEXPT_ERR', _error));
    }
} //buildInfoHandler

module.exports = router;