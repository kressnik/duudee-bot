let app = new(require('express').Router)();

const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass("ROUTES", process.env.ROUTES_LOG_LEVEL);

moduleLogger.verbose(__dirname);

let routes = require('require-dir-all')(__dirname, { // options
    recursive: true, // recursively go through subdirectories; default value shown
    indexAsParent: false, // add content of index.js/index.json files to parent object, not to parent.index
    includeFiles: /^.*\.(js)$/, // RegExp to select files; default value shown
});

moduleLogger.info("Start assign routes to system.")
//let use all routes 
function use(_routes) {
    for (let i in _routes) {
        //routeList
        let route = _routes[i];

        if (typeof route == "function") {

            moduleLogger.verbose('Router require: ', i);

            app.use(route);
        } else {
            use(route);
        }
    } //for
}

use(routes);

moduleLogger.info("End assign routes to system.")

module.exports = app;