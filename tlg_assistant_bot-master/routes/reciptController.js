'use strict';

const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const mustache = require('mustache');


const {
    BasicResponse,
    ResultStatus
} = require('../interfaces/basicResponse');
const {
    HttpRestResponse,
    HttpStatus
} = require('../interfaces/httpRestResponse');



router.get('/any-cash', getKunaCodeHandler_);


async function getKunaCodeHandler(_req, _res, _next) {
    try {
        if (_req.query.locale == null) {
            _next(new HttpRestResponse(HttpStatus.BAD_REQUEST, 'Locale doesn`t set. ?locale=en_EN', 'GET_KUNA_CODE_HTML_ERROR'));
        } else {
            let langs = _req.core.EmailPdfSenderClass.serviceies.langs['kunacodes-pdf'][_req.query.locale];

            if (langs == null) {
                _next(new HttpRestResponse(HttpStatus.BAD_REQUEST, 'Langs doesn`t set', 'GET_KUNA_CODE_HTML_ERROR'));
            } else {
                //make html
                let pathToHtml = path.join(__dirname, '../htmlPatterns/kunacodes-pdf/index.html');

                var html = fs.readFileSync(pathToHtml, 'utf8');

                let getFileUrl = function (str) {
                    //return encodeURI('file://' + path.join(__dirname, '../htmlPatterns/kunacodes-pdf/'));
                    return encodeURI(`http://localhost:${process.env.HTTP_PORT}/kunacodes-pdf/`);
                };

                html = html.replaceAll('./', getFileUrl);

                let data = {
                    locale: _req.query.locale,
                    code_amount_currency: _req.query.currency,
                    code_string_text: _req.query.code,
                    code_amount_value: _req.query.amount,
                    non_refundable_before_value: _req.query.non_refundable_before,
                    cretated_at_value: _req.query.created_at,
                    activated_at_value: _req.query.redeemed_at,
                    addressse_value: (_req.query.addressee == 'all' ? langs.public : (_req.query.addressee || '-')),
                    qr_code: `/qr/${_req.query.code}`
                }

                let nullValues = [];

                let checkNullfunc = function (obj) {
                    for (let prop in obj) {
                        if (obj[prop] == null) {
                            nullValues.push(prop);
                        }
                    }

                    return nullValues.length > 0
                };

                if (checkNullfunc(data)) {
                    throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Some data is null. ${nullValues.join(',')}.`, data);
                }

                html = mustache.to_html(html, {
                    ...data,
                    ...langs
                });

                _res.send(html);
            }
        } //check locale
    } catch (_error) {
        _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'KUNACODE_UNEXPT_ERR', _error));
    }
} //buildInfoHandler

async function getKunaCodeHandler_(_req, _res, _next) {
    try {
        console.log(_req.query.path);

        _res.render('any_cash.html', _req.query);
    } catch (_error) {
        _next(new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, _error.message, 'KUNACODE_UNEXPT_ERR', _error));
    }
} //buildInfoHandler

module.exports = router;