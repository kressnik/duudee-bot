const chai = require('chai');
const should = chai.should();
const {
    expect
} = require('chai');
const env = require('env-var');

require('../utils');

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = __UTILS.INTERFACES;

const {
    AppLoggerClass,
} = __UTILS;

const {
    TRACK_STATE
} = require('../core/wizzards_processor/wizzards_types');

const logger = new AppLoggerClass('TestChannelWatcherClass');

const bizSdk = require('facebook-nodejs-business-sdk');

const {
    FacebookAdsApi,
    ServerEvent,
    EventRequest,
    UserData,
    CustomData,
    Content
} = bizSdk;


const {
    FacebookPixelServiceClass,
    FacebookPixelServicePropsClass
} = require('../core/facebook_pixel_service/facebook_pixel_service_class');


async function buildCFacebookPixelService(args = {}) {
    try {
        const props = {
            parallelLimit: 1,
            accessToken: process.env.FACEBOOK_ACCSESS_TOKEN || "EAAgrd6IO1qkBACMx7zrnh9HGDnuMOkMZAFY3rZAtUVq2GUVE3WoQ4nqDgfYSH1xat1GjxVgZAmQkAEed3ZCnsRiMDqPFuhCOt06z2TS7CIZBCSisdBLlolmSaYI0RNlwvN4ZBFDdEPZArlUQjlVOO99V8AjfFe0oUYL2ZB1z6CW7olDpEr22bcFQ"
        }

        const facebookService = new FacebookPixelServiceClass();

        const init = await facebookService.init(props);

        init.isOK;

        logger.info(init);

        return facebookService;
    } catch (e) {
        logger.error(e);

        throw e;
    }
}

describe('FacebookPixelServicePropsClass units test', () => {
    before(async () => {

    });

    describe('test private methods', () => {
        describe('test FacebookPixelServicePropsClass', () => {
            it('should apply config', () => {
                const propsInstance = new FacebookPixelServicePropsClass();

                const watcherConfigs = {
                    parallelLimit: 199,
                    accessToken: 'access_token'
                }

                propsInstance.apply(watcherConfigs);

                propsInstance.values.should.be.a('object');

                expect(propsInstance.values).to.deep.equal(watcherConfigs);
            });
        });

        describe('test method:[_reduceTrackEventToPixelServerEvent]', () => {
            it('should return server event from TRACK_STATE.CREATED', () => {
                const track = {
                    id: 0,
                    status: TRACK_STATE.CREATED,
                    stage: 3,
                    error: null,
                    userId: 1,
                    tokenId: 2,
                    buttonId: 3,
                    subscribeId: 4,
                    sessionId: 5,
                    isNewUser: true,
                    channelId: 6
                };

                const serverEvent = new FacebookPixelServiceClass()._reduceTrackEventToPixelServerEvent(track);

                expect(serverEvent instanceof ServerEvent).to.equal(true);
                serverEvent.should.have.property('_event_name', TRACK_STATE.CREATED);
                serverEvent.should.have.property('_custom_data');
                expect(serverEvent._custom_data._custom_properties).to.deep.equal(track);
            })
        })

        describe('test method[_queuHandlerByCb]', async () => {
            it('should return ERROR_INVALID_DATA if track undef', async () => {
                const service = await buildCFacebookPixelService();

                const res = await service._queuHandlerByCb({
                    cb: (res) => {
                        res.should.be.a('object');
                        res.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                    }
                });
            });

            it('should return OK after recive track', async () => {
                try {
                    const track = {
                        id: 0,
                        status: TRACK_STATE.CREATED,
                        stage: 3,
                        error: null,
                        userId: 1,
                        tokenId: 2,
                        buttonId: 3,
                        subscribeId: 4,
                        sessionId: 5,
                        isNewUser: true,
                        channelId: 6,
                        pixelId: 1177707395902556
                    };

                    const service = await buildCFacebookPixelService();

                    const res = await service._queuFrontHandlerByCb(
                        track
                    );

                    console.log(res);
                } catch (e) {
                    console.error(e);

                    throw e;
                }

            }).timeout(5000);
        });
    });

    describe('test public methods', () => {
        describe('test method[tracksHandler]', () => {
            it('should return ERROR_INVALID_DATA if track undef', async () => {
                const service = await buildCFacebookPixelService();

                const res = service.tracksHandler();

                res.should.be.a('object');
                res.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
            });

        });
    });

});


process.on('unhandledRejection', (e) => {
    console.log('you forgot to return a Promise! Check your tests!' + e.message)
})