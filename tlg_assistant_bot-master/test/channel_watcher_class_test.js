const chai = require('chai');
const should = chai.should();
const {
    expect
} = require('chai');
const env = require('env-var');

require('../utils');

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = __UTILS.INTERFACES;

const {
    AppLoggerClass,
    SequelizeClass
} = __UTILS;

const {
    RpcStatusCode
} = require('../utils/telegram_rpc_client/rpc_status_code');


const logger = new AppLoggerClass('TestChannelWatcherClass');

const {
    ChannelWatcherClass,
    ChannelWatcherPropsClass
} = require('../core/channel_watcher/channel_watcher_class');

async function buildConnToDbFlowFunc(configs) {
    try {
        logger.info('Start init db connection');
        //init models

        const db = new SequelizeClass('ChannelWatcherClass', process.env.CHANNEL_WATCHERCLASS_DB_LOG_LEVEL);

        const resIinitDb = await db.run(configs);

        resIinitDb.isOK();

        logger.info(resIinitDb);

        return db;
    } catch (e) {
        const error = new BasicResponseByError(e);

        logger.error(error.toString());

        throw error;
    }
}

async function buildChannelWatcher(args = {}) {
    try {
        const {
            mocksVersion,
            useMocks = false,
        } = args;

        const watcherConfigs = {
            state: 0,
            client: {
                host: process.env.RPC_HOST || 'localhost',
                port: process.env.RPC_PORT || 9001,
                mocksVersion,
                useMocks
            }
        }

        const dbConfigs = {
            username: env.get('DB_USERNAME').default('root').asString(),
            password: env.get('DB_PASSWORD').default('').asString(),
            database: env.get('DB_DATABASE').default('tlg_assistant_bot_test').asString(),
            directory: env.get('DB_DIRECTORY').default('db').asString(),
            migration: {
                version: env.get('MIGRATION_VERSION').default('v_1_4').asString(),
            },
            options: {
                dialect: 'mysql',
                port: env.get('DB_PORT').default('3306').asIntPositive(),
                host: env.get('DB_HOST').default('localhost').asString(),
            },
        }

        const db = await buildConnToDbFlowFunc(dbConfigs);

        const watcherInstance = new ChannelWatcherClass();

        const init = await watcherInstance.init({
            db,
            props: watcherConfigs
        });

        init.isOK;

        logger.info(init);

        return watcherInstance;
    } catch (e) {
        logger.error(e);

        throw e;
    }
}

describe('ChannelWatcherClass units test', () => {
    before(async () => {

    });

    describe('test private methods', () => {
        describe('test ChannelWatcherPropsClass', () => {
            it('should apply config', () => {
                const propsInstance = new ChannelWatcherPropsClass();

                const watcherConfigs = {
                    state: 1,
                    interval: 999,
                    client: {
                        mocksVersion: 'mocksVersionTest',
                        useMocks: false,
                        host: 'localhost',
                        port: 9001,
                        path: 'rpc_test',
                        version: 'v1'
                    }
                }

                propsInstance.apply(watcherConfigs);

                propsInstance.values.should.be.a('object');

                expect(propsInstance.values).to.deep.equal(watcherConfigs);
            })
        });

        describe('test method[_reduceTelegramLogToModel]', () => {
            it(`should return model from [ChannelAdminLogEventActionParticipantToggleBan]`, () => {
                const watcherInstance = new ChannelWatcherClass();

                const model = watcherInstance._reduceTelegramLogToModel({
                    channelId: 12345,
                    log: {
                        "type": "ChannelAdminLogEvent",
                        "id": 832999903696,
                        "date": 1574975425,
                        "user_id": 264387813,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantToggleBan",
                            "prev_participant": {
                                "type": "ChannelParticipant",
                                "user_id": 829759382,
                                "date": 1574975425
                            },
                            "new_participant": {
                                "type": "ChannelParticipantBanned",
                                "user_id": 829759382,
                                "kicked_by": 264387813,
                                "date": 1574975425,
                                "banned_rights": {
                                    "type": "ChatBannedRights",
                                    "until_date": 1574975425,
                                    "view_messages": true,
                                    "send_messages": true,
                                    "send_media": true,
                                    "send_stickers": true,
                                    "send_gifs": true,
                                    "send_games": true,
                                    "send_inline": true,
                                    "embed_links": true,
                                    "send_polls": true,
                                    "change_info": true,
                                    "invite_users": true,
                                    "pin_messages": true
                                },
                                "left": true
                            }
                        }
                    }
                });

                model.should.have.property('channelId', 12345);
                model.should.have.property('logId', 832999903696);
                model.should.have.property('userId', 264387813);

                model.should.have.property('date', 1574975425);
                model.should.have.property('type', 'ChannelAdminLogEventActionParticipantToggleBan');
                model.should.have.property('userId', 264387813);
                model.should.have.property('json');
            });

            it(`should return model from [ChannelAdminLogEventActionParticipantLeave]`, () => {
                const watcherInstance = new ChannelWatcherClass();

                const model = watcherInstance._reduceTelegramLogToModel({
                    channelId: 12345,
                    log: {
                        "type": "ChannelAdminLogEvent",
                        "id": 833004749312,
                        "date": 1574975425,
                        "user_id": 829759382,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantLeave"
                        }
                    }
                });

                model.should.have.property('channelId', 12345);
                model.should.have.property('logId', 833004749312);
                model.should.have.property('userId', 829759382);

                model.should.have.property('date', 1574975425);
                model.should.have.property('type', 'ChannelAdminLogEventActionParticipantLeave');
                model.should.have.property('json');
            });

            it(`should return model from [ChannelAdminLogEventActionParticipantJoin]`, () => {
                const watcherInstance = new ChannelWatcherClass();

                const model = watcherInstance._reduceTelegramLogToModel({
                    channelId: 12345,
                    log: {
                        "type": "ChannelAdminLogEvent",
                        "id": 833004067240,
                        "date": 1574975425,
                        "user_id": 829759382,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantJoin"
                        }
                    }
                });

                model.should.have.property('channelId', 12345);
                model.should.have.property('logId', 833004067240);
                model.should.have.property('userId', 829759382);

                model.should.have.property('date', 1574975425);
                model.should.have.property('type', 'ChannelAdminLogEventActionParticipantJoin');
                model.should.have.property('json');
            });
        });

        describe('test method:[_fetchChannelAdminLogsFlowFucn]', () => {
            it('should throw `status=ERROR_INVALID_DATA`', async () => {
                try {
                    const watcher = await buildChannelWatcher();

                    const fetchAdminLogsRes = await watcher._fetchChannelAdminLogsFlowFucn();

                    await watcher._db.sequelize.close();

                    throw new Error('should throw `status=ERROR_INVALID_DATA`');
                } catch (fetchAdminLogsRes) {
                    fetchAdminLogsRes.should.be.a('object');
                    fetchAdminLogsRes.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                }

            });

            it('should return `logs and channelId`', async () => {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'get_channels_admin_logs_mock'
                    });

                    const channelTlgId = '123344556';
                    const channelId = '1';

                    const fetchAdminLogsRes = await watcher._fetchChannelAdminLogsFlowFucn({
                        channelTlgId,
                        channelId
                    });

                    await watcher._db.sequelize.close();

                    console.log(fetchAdminLogsRes);

                    fetchAdminLogsRes.should.be.a('object');
                    fetchAdminLogsRes.should.have.property('channelId', channelId);
                    fetchAdminLogsRes.should.have.property('channelTlgId', channelTlgId);
                    fetchAdminLogsRes.should.have.property('logs');
                    fetchAdminLogsRes.logs.should.to.be.an('array');
                } catch (e) {
                    logger.error(e);

                    throw e;
                }
            })
        });

        describe('test method:[_reduceAdminLogsToModelFlowFunc]', function () {
            const logsMocks = [{
                    "type": "ChannelAdminLogEvent",
                    "id": 822190385296,
                    "date": 1574975425,
                    "user_id": 829759382,
                    "action": {
                        "type": "ChannelAdminLogEventActionParticipantJoin"
                    }
                },
                {
                    "type": "ChannelAdminLogEvent",
                    "id": 822185979736,
                    "date": 1574975425,
                    "user_id": 877375623,
                    "action": {
                        "type": "ChannelAdminLogEventActionParticipantLeave"
                    }
                }, {
                    "type": "ChannelAdminLogEvent",
                    "id": 822185696320,
                    "date": 1574975425,
                    "user_id": 877375623,
                    "action": {
                        "type": "ChannelAdminLogEventActionParticipantJoin"
                    }
                }
            ];

            it('should return channel and models', async () => {
                const watcherInstance = new ChannelWatcherClass();

                const models = await watcherInstance._reduceAdminLogsToModelFlowFunc({
                    channelId: 12345,
                    logs: logsMocks
                });

                //console.log(models.logs);

                models.should.be.a('object');
                models.should.have.property('channelId', 12345);
                models.should.have.property('logs');
                models.logs.should.to.be.an('array');

                for (const log of models.logs) {
                    log.should.have.property('channelId', 12345);
                    log.should.have.property('logId');
                    log.should.have.property('userId');
                    log.should.have.property('date');
                    log.should.have.property('type');
                    log.should.have.property('userId');
                    log.should.have.property('json');
                }
            });
        });

        describe('test method:[_fetchChannelLastIdAdminLogFlowFucn]', function () {
            it('should return last log', async function () {
                let logsBath = [{
                        channelId: 12345,
                        logId: 822190385296,
                        userId: 829759382,
                        kickedBy: null,
                        date: 1574975425,
                        type: 'ChannelAdminLogEventActionParticipantJoin',
                        json: {
                            type: 'ChannelAdminLogEvent',
                            id: 822190385296,
                            date: 1574975425,
                            user_id: 829759382,
                            action: {}
                        }
                    },
                    {
                        channelId: 12345,
                        logId: 822185979736,
                        userId: 877375623,
                        kickedBy: null,
                        date: 1574975425,
                        type: 'ChannelAdminLogEventActionParticipantLeave',
                        json: {
                            type: 'ChannelAdminLogEvent',
                            id: 822185979736,
                            date: 1574975425,
                            user_id: 877375623,
                            action: {}
                        }
                    },
                    {
                        channelId: 12345,
                        logId: 822185696320,
                        userId: 877375623,
                        kickedBy: null,
                        date: 1574975425,
                        type: 'ChannelAdminLogEventActionParticipantJoin',
                        json: {
                            type: 'ChannelAdminLogEvent',
                            id: 822185696320,
                            date: 1574975425,
                            user_id: 877375623,
                            action: {}
                        }
                    }
                ];

                const watcher = await buildChannelWatcher({});

                console.log('Destroy values in db');
                //clear all db
                await watcher._db.sequelize.query("SET FOREIGN_KEY_CHECKS = 0");
                await watcher._db.models.Channel.destroy({
                    where: {}
                });
                await watcher._db.models.TelegramAdminsLogs.destroy({
                    where: {}
                });
                await watcher._db.sequelize.query("SET FOREIGN_KEY_CHECKS = 1");

                const addChannel = await watcher._db.models.Channel.createOne({
                    name: 'test',
                    tlgId: 39383948,
                    description: 'test mock',
                    inUse: true,
                    price: 1,
                    currency: 'RUB',
                    feeAtOwnExpense: false,
                    isFree: true,
                    maxSubcribesQty: 1,
                    subscriptionDuration: 30,
                    color: '[#0,#0,#0]'
                });

                const channelId = addChannel.data.id;
                const channelTlgId = addChannel.data.tlgId;

                console.log(`Add channel with id:[${channelId}] to db`, addChannel);

                logsBath = logsBath.map(log => {
                    return {
                        ...log,
                        channelId,
                    }
                });

                //push logs to  dbConfigs
                const addReuslt = await watcher._db.models.TelegramAdminsLogs.createOrUpdateBatch(logsBath);

                console.log(`Add logs to db: `, addReuslt);

                //run function
                const fetchAdminLogsRes = await watcher._fetchChannelLastIdAdminLogFlowFucn({
                    channelId,
                    channelTlgId
                });

                await watcher._db.sequelize.close();

                fetchAdminLogsRes.should.be.a('object');
                fetchAdminLogsRes.should.have.property('channelId', channelId);
                fetchAdminLogsRes.should.have.property('channelTlgId', channelTlgId);
                fetchAdminLogsRes.should.have.property('lastLogId', 822190385296);
            })
        });

        describe('test method:[_savingAdminLogsToDbFlowFunc]', function () {
            it('should save batch and return logsModel`s', async function () {
                try {
                    let logsBath = [{
                            channelId: 12345,
                            logId: 822190385296,
                            userId: 829759382,
                            kickedBy: null,
                            date: 1574975425,
                            type: 'ChannelAdminLogEventActionParticipantJoin',
                            json: {
                                type: 'ChannelAdminLogEvent',
                                id: 822190385296,
                                date: 1574975425,
                                user_id: 829759382,
                                action: {}
                            }
                        },
                        {
                            channelId: 12345,
                            logId: 822185979736,
                            userId: 877375623,
                            kickedBy: null,
                            date: 1574975425,
                            type: 'ChannelAdminLogEventActionParticipantLeave',
                            json: {
                                type: 'ChannelAdminLogEvent',
                                id: 822185979736,
                                date: 1574975425,
                                user_id: 877375623,
                                action: {}
                            }
                        },
                        {
                            channelId: 12345,
                            logId: 822185696320,
                            userId: 877375623,
                            kickedBy: null,
                            date: 1574975425,
                            type: 'ChannelAdminLogEventActionParticipantJoin',
                            json: {
                                type: 'ChannelAdminLogEvent',
                                id: 822185696320,
                                date: 1574975425,
                                user_id: 877375623,
                                action: {}
                            }
                        }
                    ];

                    const additionalLog = {
                        channelId: 12345,
                        logId: 800000000,
                        userId: 829759382,
                        kickedBy: null,
                        date: 1574975425,
                        type: 'ChannelAdminLogEventActionParticipantJoin',
                        json: {
                            type: 'ChannelAdminLogEvent',
                            id: 822190385296,
                            date: 1574975425,
                            user_id: 829759382,
                            action: {}
                        }
                    }

                    const watcher = await buildChannelWatcher({});

                    console.log('Destroy values in db');
                    //clear all db
                    await watcher._db.sequelize.query("SET FOREIGN_KEY_CHECKS = 0");
                    await watcher._db.models.Channel.destroy({
                        where: {}
                    });
                    await watcher._db.models.TelegramAdminsLogs.destroy({
                        where: {}
                    });
                    await watcher._db.sequelize.query("SET FOREIGN_KEY_CHECKS = 1");

                    const addChannel = await watcher._db.models.Channel.createOne({
                        name: 'test',
                        tlgId: 39383948,
                        description: 'test mock',
                        inUse: true,
                        price: 1,
                        currency: 'RUB',
                        feeAtOwnExpense: false,
                        isFree: true,
                        maxSubcribesQty: 1,
                        subscriptionDuration: 30,
                        color: '[#0,#0,#0]'
                    });

                    const channelId = addChannel.data.id;
                    console.log(`Add channel with id:[${channelId}] to db`, addChannel);

                    logsBath = logsBath.map(log => {
                        return {
                            ...log,
                            channelId,
                        }
                    });

                    //push logs to  dbConfigs
                    const addReuslt = await watcher._savingAdminLogsToDbFlowFunc({
                        channelId,
                        logs: logsBath
                    });

                    await watcher._db.sequelize.close();

                    addReuslt.should.be.a('object');
                    addReuslt.should.have.property('channelId', channelId);
                    addReuslt.should.have.property('logs');
                    addReuslt.logs.should.to.be.an('array');
                    addReuslt.logs.should.have.property('length', logsBath.length);

                    for (const log of addReuslt.logs) {
                        log.should.have.property('channelId', channelId);
                        log.should.have.property('logId');
                        log.should.have.property('userId');
                        log.should.have.property('date');
                        log.should.have.property('type');
                        log.should.have.property('userId');
                        log.should.have.property('json');
                    }
                } catch (e) {
                    logger.error(e);

                    throw e;
                }
            });

            it('should save batch with additional log and return logsModel`s', async function () {
                try {
                    let logsBath = [{
                            channelId: 12345,
                            logId: 822190385296,
                            userId: 829759382,
                            kickedBy: null,
                            date: 1574975425,
                            type: 'ChannelAdminLogEventActionParticipantJoin',
                            json: {
                                type: 'ChannelAdminLogEvent',
                                id: 822190385296,
                                date: 1574975425,
                                user_id: 829759382,
                                action: {}
                            }
                        },
                        {
                            channelId: 12345,
                            logId: 822185979736,
                            userId: 877375623,
                            kickedBy: null,
                            date: 1574975425,
                            type: 'ChannelAdminLogEventActionParticipantLeave',
                            json: {
                                type: 'ChannelAdminLogEvent',
                                id: 822185979736,
                                date: 1574975425,
                                user_id: 877375623,
                                action: {}
                            }
                        },
                        {
                            channelId: 12345,
                            logId: 822185696320,
                            userId: 877375623,
                            kickedBy: null,
                            date: 1574975425,
                            type: 'ChannelAdminLogEventActionParticipantJoin',
                            json: {
                                type: 'ChannelAdminLogEvent',
                                id: 822185696320,
                                date: 1574975425,
                                user_id: 877375623,
                                action: {}
                            }
                        }
                    ];

                    const watcher = await buildChannelWatcher({});

                    console.log('Destroy values in db');
                    //clear all db
                    await watcher._db.sequelize.query("SET FOREIGN_KEY_CHECKS = 0");
                    await watcher._db.models.Channel.destroy({
                        where: {}
                    });
                    await watcher._db.models.TelegramAdminsLogs.destroy({
                        where: {}
                    });
                    await watcher._db.sequelize.query("SET FOREIGN_KEY_CHECKS = 1");

                    const addChannel = await watcher._db.models.Channel.createOne({
                        name: 'test',
                        tlgId: 39383948,
                        description: 'test mock',
                        inUse: true,
                        price: 1,
                        currency: 'RUB',
                        feeAtOwnExpense: false,
                        isFree: true,
                        maxSubcribesQty: 1,
                        subscriptionDuration: 30,
                        color: '[#0,#0,#0]'
                    });

                    const channelId = addChannel.data.id;
                    console.log(`Add channel with id:[${channelId}] to db`, addChannel);

                    logsBath = logsBath.map(log => {
                        return {
                            ...log,
                            channelId,
                        }
                    });

                    //push logs to  dbConfigs
                    const addReuslt = await watcher._savingAdminLogsToDbFlowFunc({
                        channelId,
                        logs: logsBath
                    });


                    addReuslt.should.be.a('object');
                    addReuslt.should.have.property('channelId', channelId);
                    addReuslt.should.have.property('logs');
                    addReuslt.logs.should.to.be.an('array');
                    addReuslt.logs.should.have.property('length', logsBath.length);

                    for (const log of addReuslt.logs) {
                        log.should.have.property('channelId', channelId);
                        log.should.have.property('logId');
                        log.should.have.property('userId');
                        log.should.have.property('date');
                        log.should.have.property('type');
                        log.should.have.property('userId');
                        log.should.have.property('json');
                    }

                    const additionalLog = {
                        channelId,
                        logId: 800000000,
                        userId: 829759382,
                        kickedBy: null,
                        date: 1574975425,
                        type: 'ChannelAdminLogEventActionParticipantJoin',
                        json: {
                            type: 'ChannelAdminLogEvent',
                            id: 822190385296,
                            date: 1574975425,
                            user_id: 829759382,
                            action: {}
                        }
                    }

                    const logsBathWithAdditional = logsBath.concat([additionalLog]);

                    //push logs to with additional log dbConfigs
                    const addBathWithAdditional = await watcher._savingAdminLogsToDbFlowFunc({
                        channelId,
                        logs: logsBathWithAdditional
                    });

                    addBathWithAdditional.should.be.a('object');
                    addBathWithAdditional.should.have.property('channelId', channelId);
                    addBathWithAdditional.should.have.property('logs');
                    addBathWithAdditional.should.have.property('saved');
                    addBathWithAdditional.logs.should.to.be.an('array');
                    addBathWithAdditional.logs.should.have.property('length', logsBathWithAdditional.length);

                    for (const log of addBathWithAdditional.logs) {
                        log.should.have.property('channelId', channelId);
                        log.should.have.property('logId');
                        log.should.have.property('userId');
                        log.should.have.property('date');
                        log.should.have.property('type');
                        log.should.have.property('userId');
                        log.should.have.property('json');
                    }


                    //chek updte shuol be 3(update) + 1(add) = 4

                    const logsfromDb = await watcher._db.models.TelegramAdminsLogs.findAll();

                    logsfromDb.should.have.property('length', logsBathWithAdditional.length);

                    await watcher._db.sequelize.close();
                } catch (e) {
                    logger.error(e);

                    throw e;
                }
            });
        })
    });


    describe('test public methods by mocks', () => {
        describe('method:[authGetStatus]', () => {
            it('should return OK if auth setuped', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'get_auth_status_ok_mock'
                    });

                    const response = await watcher.authGetStatus();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('status', 'DONE');
                    response.data.should.have.property('phone');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return OK and NOT_INIT', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'get_auth_status_not_init_mock'
                    });

                    const response = await watcher.authGetStatus();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('status', 'NOT_INIT');
                    response.data.should.have.property('phone');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return OK and PENDING', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'get_auth_status_pending_mock'
                    });

                    const response = await watcher.authGetStatus();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('status', 'PENDING');
                    response.data.should.have.property('phone');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

        });

        describe('method:[userSendApiInfo]', () => {
            it('should return `ERROR_INVALID_DATA` any params provided atrgs should be an object', async function () {
                try {
                    const watcher = await buildChannelWatcher();

                    console.log(watcher.userSendApiInfo);

                    const response = await watcher.userSendApiInfo();

                    console.log(response);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` any params provided', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_api_info_mock'
                    });

                    const response = await watcher.userSendApiInfo({});

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` api_hash not provided', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_api_info_mock'
                    });

                    const response = await watcher.userSendApiInfo({
                        apiId: 1234
                    });


                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `OK` all params provided', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_api_info_mock'
                    });

                    const response = await watcher.userSendApiInfo({
                        apiId: 1234,
                        apiHash: 'cdcdcdcd'
                    });

                    console.log(response);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('method:[userSendStartAuth]', () => {
            it('should return `ERROR_INVALID_DATA` if phone not provided', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    });

                    const response = await watcher.userSendStartAuth();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if phone provide empty string', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    });

                    const response = await watcher.userSendStartAuth('');

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if phone provide object', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    });

                    const response = await watcher.userSendStartAuth({
                        phone: ''
                    });

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `OK` and `WAITING_CODE` if phone provide object', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    });

                    const response = await watcher.userSendStartAuth('+380502947272');

                    console.log(response);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.data.should.have.property('status', 'WAITING_CODE');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('method:[userSendAuthCode]', () => {
            it('should return `ERROR_INVALID_DATA` if code not provided', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    });

                    const response = await watcher.userSendAuthCode();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if code provide empty string', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    });

                    const response = await watcher.userSendAuthCode('');

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if phone provide object', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    });

                    const response = await watcher.userSendAuthCode({
                        code: ''
                    });

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `OK` if code provide object', async function () {
                try {
                    const watcher = await buildChannelWatcher({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    });

                    const response = await watcher.userSendAuthCode('123456');

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.data.should.have.property('status', 'OK');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });
    });
});


process.on('unhandledRejection', (e) => {
    console.log('you forgot to return a Promise! Check your tests!' + e.message)
})