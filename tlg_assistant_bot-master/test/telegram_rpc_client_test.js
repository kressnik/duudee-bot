const chai = require('chai');
const should = chai.should();

require('../utils');

const {
    ResultStatus,
    BasicResponse,
    BasicResponseByError
} = require('../interfaces/basicResponse');

const {
    RpcStatusCode
} = require('../utils/telegram_rpc_client/rpc_status_code');


const {
    TelegramRpcClentClass,
    TelegramRpcClientStatusEnum
} = require('../utils/telegram_rpc_client/telegram_rpc_client_class');


function buildClient(args = {}) {
    try {
        const {
            mocksVersion,
            useMocks = false,
        } = args;

        let rpcConfigs = {
            host: process.env.RPC_HOST || 'localhost',
            port: process.env.RPC_PORT || 9001,
            mocksVersion,
            useMocks
        }

        return new TelegramRpcClentClass(rpcConfigs);
    } catch (e) {
        console.error(e);

        throw e;
    }
}

describe('TelegramRpcClentClass units test.', () => {
    before(async () => {

    });

    describe('test private methods', () => {
        describe('test _reduceErrorRpcToBasic method', () => {
            it('_reduceErrorRpcToBasic should return package ERRO_NOT_FOUND', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.NOT_FOUND,
                    message: "Not found error",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_NOT_FOUND);
                result.should.have.property('data', 'test');
            });

            it('_reduceErrorRpcToBasic should return package ERROR_INVALID_DATA', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.SERVER_ERROR_INVALID_METHOD_PARAMETERS,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                result.should.have.property('data', 'test');
            });


            it('_reduceErrorRpcToBasic should return package UNAUTHORIZED', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.UNAUTHORIZED,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_UNAUTHORIZED);
                result.should.have.property('data', 'test');
            });

            it('_reduceErrorRpcToBasic should return package ERROR_SEE_OTHER', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.SEE_OTHER,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_SEE_OTHER);
                result.should.have.property('data', 'test');
            });

            it('_reduceErrorRpcToBasic should return package ERROR_BAD_REQUEST', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.BAD_REQUEST,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_BAD_REQUEST);
                result.should.have.property('data', 'test');
            });

            it('_reduceErrorRpcToBasic should return package ERROR_FORBIDDEN', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.FORBIDDEN,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_FORBIDDEN);
                result.should.have.property('data', 'test');
            });


            it('_reduceErrorRpcToBasic should return package ERROR_NOT_ACCEPTABLE', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.NOT_ACCEPTABLE,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_NOT_ACCEPTABLE);
                result.should.have.property('data', 'test');
            });

            it('_reduceErrorRpcToBasic should return package ERROR_FLOOD', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.FLOOD,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_FLOOD);
                result.should.have.property('data', 'test');
            });

            it('_reduceErrorRpcToBasic should return package ERROR_INTERNAL', async () => {
                const rpcErrorProperty = {
                    code: RpcStatusCode.INTERNAL,
                    message: "",
                    data: 'test'
                }

                let result = await buildClient()._reduceErrorRpcToBasic(rpcErrorProperty);

                result.should.be.a('object');
                result.should.have.property('status', ResultStatus.ERROR_INTERNAL);
                result.should.have.property('data', 'test');
            });
        });
    });


    describe('test public methods by mocks', () => {
        describe('method:[authGetStatus]', () => {
            it('should return OK if auth setuped', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_auth_status_ok_mock'
                    }).authGetStatus();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('status', 'DONE');
                    response.data.should.have.property('phone');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return OK and NOT_INIT', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_auth_status_not_init_mock'
                    }).authGetStatus();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('status', 'NOT_INIT');
                    response.data.should.have.property('phone');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return OK and PENDING', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_auth_status_pending_mock'
                    }).authGetStatus();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.should.have.property('data');

                    response.data.should.have.property('status', 'PENDING');
                    response.data.should.have.property('phone');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

        });

        describe('method:[userSendApiInfo]', () => {
            it('should return `ERROR_INVALID_DATA` any params provided atrgs should be an object', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_api_info_mock'
                    }).userSendApiInfo();

                    console.log(response);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` any params provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_api_info_mock'
                    }).userSendApiInfo({});

                    console.log(response);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` api_hash not provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_api_info_mock'
                    }).userSendApiInfo({
                        apiId: 1234
                    });

                    console.log(response);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `OK` all params provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_api_info_mock'
                    }).userSendApiInfo({
                        apiId: 1234,
                        apiHash: 'cdcdcdcd'
                    });

                    console.log(response);

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });


        describe('method:[userSendStartAuth]', () => {
            it('should return `ERROR_INVALID_DATA` if phone not provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    }).userSendStartAuth();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if phone provide empty string', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    }).userSendStartAuth('');

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if phone provide object', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    }).userSendStartAuth({
                        phone: ''
                    });

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `OK` and `WAITING_CODE` if phone provide object', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_start_auth_mock'
                    }).userSendStartAuth('+380502947272');

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.data.should.have.property('status', 'WAITING_CODE');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('method:[userSendAuthCode]', () => {
            it('should return `ERROR_INVALID_DATA` if code not provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    }).userSendAuthCode();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if code provide empty string', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    }).userSendAuthCode('');

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if phone provide object', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    }).userSendAuthCode({
                        code: ''
                    });

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `OK` if code provide object', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'send_auth_code_mock'
                    }).userSendAuthCode('123456');

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.data.should.have.property('status', 'OK');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });

        describe('method:[channelsGetAdminLog]', () => {
            it('should return `ERROR_INVALID_DATA` if params not provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_channels_admin_logs_mock'
                    }).channelsGetAdminLog();

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
            it('should return `ERROR_INVALID_DATA` if channel not provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_channels_admin_logs_mock'
                    }).channelsGetAdminLog({});

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return `ERROR_INVALID_DATA` if cahnnel provide empty string', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_channels_admin_logs_mock'
                    }).channelsGetAdminLog({
                        channel: ''
                    });

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_INVALID_DATA);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });

            it('should return BAD_REQUEST', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_admin_logs_bad_request_mock'
                    }).channelsGetAdminLog({
                        channel: '123344556'
                    });

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.ERROR_BAD_REQUEST);
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
            it('should return `OK` and array if cahnnel provided', async function () {
                try {
                    const response = await buildClient({
                        useMocks: true,
                        mocksVersion: 'get_channels_admin_logs_mock'
                    }).channelsGetAdminLog({
                        channel: '123344556'
                    });

                    response.should.be.a('object');
                    response.should.have.property('status', ResultStatus.OK);
                    response.data.should.to.be.an('array');
                } catch (e) {
                    console.error(e);

                    throw e;
                }
            });
        });
    });
});


process.on('unhandledRejection', (e) => {
    console.log('you forgot to handle a Promise! Check your tests!\n' + e.message)
})