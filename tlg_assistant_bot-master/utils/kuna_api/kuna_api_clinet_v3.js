'use strict';

const crypto = require('crypto');

const {
    AppLoggerClass,
    Loglevel
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const axios = require('axios');

const uinqueid = require('uniqid');

class KunaApiClientV3 {
    constructor(logLevel, baseURL) {
        this._logger = new AppLoggerClass('KunaApiClientV3', logLevel || Loglevel.INFO);
        this.baseURL = baseURL || 'https://api.kuna.io/v3';
        this.props = {};
        this._client = axios.create({
            baseURL: this.baseURL,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    async init({
        kunaPublicToken,
        kunaPrivateToken
    }) {
        try {
            let isNull = [
                kunaPrivateToken,
                kunaPrivateToken
            ].some(a => a == null);

            if (isNull == true) {
                let error = new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Wrong incomin parameters');
                
                this._logger.error(error);
                
                return error;
            }

            this.axios = axios;

            this.props.kunaPublicToken = kunaPublicToken;
            this.props.kunaPrivateToken = kunaPrivateToken;

            let rsp = new BasicResponse(ResultStatus.OK, 'Init client done successfully');

            this._logger.info(rsp.toString());

            return rsp;
        } catch (_error) {
            const {
                response,
            } = _error;

            if (response == null) {
                let errBody = _error.toJSON();

                throw new BasicResponse(ResultStatus.ERROR, errBody.message, errBody);
            }

            let err = this._reduceResponse(response || error);

            this._logger.error(err.toString());

            return err;
        }
    }

    async getPaymentPrerequest({
        currency,
        public_key
    }) {
        try {
            let body = {
                currency,
                public_key
            }
            //POST https: //pay.kuna.io/public-api/payment-prereques

            let response = await axios.post('https://com.paycore.io/public-api/payment-prerequest', body, {
                headers: {
                    'Content-Type': 'application/json'
                },

            });

            response = this._reduceResponse(response);

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payment Prerequest done successfully', response.data);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (_error) {
            const {
                response,
            } = _error;

            if (response == null) {
                let errBody = _error.toJSON();

                throw new BasicResponse(ResultStatus.ERROR, errBody.message, errBody);
            }

            let err = this._reduceResponse(response || error);

            this._logger.error(err.toString());

            return err;
        }
    }

    async getDepositDetailsFake({

    }) {
        return {
            amount: 101.4,
            comment: null,
            created_at: "2019-09-22T15:38:02+03:00",
            currency: 1,
            destination: "537541******4404",
            id: 1230945,
            provider: "4bill",
            sn: "Tzg3aksEpUCk",
            status: "done",
            txid: "cpi_blkNuWkSN6eHfTq2",
        }
    }

    async makePaymentInvoiceFake({
        depositId,
        service
    }) {

        switch (service) {
            case 'payment_card_uah_hpp':
                return new BasicResponse(ResultStatus.OK, 'fake', {
                    data: {
                        active_payment: {
                            payload: {
                                action: "https://mapi.xpay.com.ua/ru/frame/widget/5f32e716-df91-4521-8c5f-1e7b009b4028",
                                method: "GET",
                                metadata: [],
                                params: [],
                                resolution: "OK",
                                status: "invoked"
                            },
                        },
                        amount: 101.4,
                        created: 1569139933,
                        currency: "UAH",
                        description: null,
                        id: "cpi_Tzg3aksEpUCk",
                        metadata: [],
                        payment_amount: 101.4,
                        processed_amount: null,
                        reference_id: depositId || 'Tzg3aksEpUCk',
                        refunded_amount: null,
                        resolution: "OK",
                        return_url: null,
                        serial_number: "Tzg3aksEpUCk",
                        service: "payment_card_uah_hpp",
                        service_currency: "UAH",
                        service_flow: "hpp",
                        service_method: "payment_card",
                        status: "process_pending",
                        test_mode: false
                    }
                });

            case 'qiwi_rub_hpp':
                return new BasicResponse(ResultStatus.OK, 'Fake qiwi', {
                    active_payment: {
                        payload: {
                            action: "https://api.any.cash/_handler/eapi",
                            metadata: [],
                            method: "POST",
                            params: {
                                amount: 5498.46,
                                callback_url: "https://psp-ext.paycore.io/anycash/callback",
                                externalid: "pay_YvHDtUncrLQMeoph79iD4EXi",
                                in_currency: "RUB",
                                merchant: 404,
                                method: "payin_sci",
                                payway: "qiwi",
                                redirect_url: "https://psp-ext.paycore.io/anycash/return?payment_id=pay_YvHDtUncrLQMeoph79iD4EXi",
                                sign: "cdbf3a29b1fedb01f0e9498f571a65546d766fa1223a245cfad29868c4bb0f97ffc07d0f4648da0bc504b96a3475b90393a95be49f9f3ea1bcdc776a0357f380",
                                userdata: '{"payer ":"380502947272 ","contact ":"380502947272 "}',
                            }
                        }
                    },
                    amount: 1913.3,
                    created: 1569162607,
                    currency: "UAH",
                    description: null,
                    id: "cpi_7uUr3WYKVNw7wSDN",
                    metadata: {
                        switch_method: "uah"
                    },
                    payment_amount: 1913.3,
                    processed_amount: null,
                    reference_id: "bHZBk9XJMYnF",
                    refunded_amount: null,
                    resolution: "OK",
                    return_url: null,
                    serial_number: "7uUr3WYKVNw7wSDN",
                    service: "qiwi_rub_hpp",
                    service_currency: "RUB",
                    service_flow: "hpp",
                    service_method: "qiwi",
                    status: "process_pending",
                    test_mode: false,
                })
        }
    }

    async makePaymentInvoice({
        service,
        currency,
        amount,
        publicKey,
        depositId,
        serviceFields,
        swithMethod,
        customer,
    }) {
        try {
            let body = {
                public_key: publicKey,
                reference_id: depositId,
                service: service,
                service_fields: serviceFields,
                customer,
                metadata: {
                    switch_method: swithMethod
                },
                currency: currency,
                amount: amount
            }

            let response = await axios.post('https://com.paycore.io/public-api/payment-invoices', body, {
                headers: {
                    'Content-Type': 'application/json'
                },

            });

            //response.data.data.active_payment.payload

            response = this._reduceResponse(response);

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payment Prerequest done successfully', response.data);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (_error) {
            const {
                response,
            } = _error;

            if (response == null) {
                let errBody = _error.toJSON();

                throw new BasicResponse(ResultStatus.ERROR, errBody.message, errBody);
            }

            let err = this._reduceResponse(response || error);

            this._logger.error(err.toString());

            return err;
        }
    }

    async makeDepositFake() {
        return new BasicResponse(ResultStatus.OK, "Fake", {
            deposit_id: process.env.FAKE_DEPO_REF_ID || uinqueid()//'Tzg3aksEpUCk' //uinqueid()
        });
    }

    async makeDeposit(info) {
        try {
            const timestamp = Date.now();
            let body = {
                ...info
            };

            let hash = crypto.createHmac('sha384', this.props.kunaPrivateToken)
                .update(`/v3/auth/deposit${timestamp}${JSON.stringify(body)}`)
                .digest('hex');

            let response = await this._client.post('/auth/deposit', body, {
                headers: {
                    'kun-apikey': this.props.kunaPublicToken,
                    'kun-nonce': timestamp,
                    'kun-signature': hash,
                }
            });

            response = this._reduceResponse(response);

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payment Prerequest done successfully', response.data);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (_error) {
            const {
                response,
            } = _error;

            if (response == null) {
                let errBody = _error.toJSON();

                throw new BasicResponse(ResultStatus.ERROR, errBody.message, errBody);
            }

            let err = this._reduceResponse(response || error);

            this._logger.error(err.toString());

            return err;
        }
    }

    async getDepositDetails({
        id
    }) {
        try {
            const timestamp = Date.now();
            let body = {
                id
            };

            let hash = crypto.createHmac('sha384', this.props.kunaPrivateToken)
                .update(`/v3/auth/deposit/details${timestamp}${JSON.stringify(body)}`)
                .digest('hex');

            let response = await this._client.post('/auth/deposit/details', body, {
                headers: {
                    'kun-apikey': this.props.kunaPublicToken,
                    'kun-nonce': timestamp,
                    'kun-signature': hash,
                }
            });

            response = this._reduceResponse(response);

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payment Prerequest done successfully', response.data);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (_error) {
            const {
                response,
            } = _error;

            if (response == null) {
                let errBody = _error.toJSON();

                throw new BasicResponse(ResultStatus.ERROR, errBody.message, errBody);
            }

            let err = this._reduceResponse(response || error);

            this._logger.error(err.toString());

            return err;
        }
    }

    async getCurrencies() {
        try {
            let response = await this._client.get('/currencies');

            response = this._reduceResponse(response);

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payment Prerequest done successfully', response.data);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (_error) {
            const {
                response,
            } = _error;

            if (response == null) {
                let errBody = _error.toJSON();

                throw new BasicResponse(ResultStatus.ERROR, errBody.message, errBody);
            }

            let err = this._reduceResponse(response || error);

            this._logger.error(err.toString());

            return err;
        }
    }

    _reduceResponse(response) {
        switch (response.status) {
            case 200:
            case 201:
                return new BasicResponse(ResultStatus.OK, response.statusText, response.data);
            case 400:
            case 401:
                return new BasicResponse(ResultStatus.ERROR_INVALID_ACCESS, response.statusText, response.data);
            case 422:
                return new BasicResponse(ResultStatus.ERROR_INVALID_ACCESS, response.statusText, response.data);
            case 404:
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, response.statusText, response.data);
            default:
                console.log(response.status, response.data);

                return new BasicResponse(ResultStatus.ERROR_UNEXP_ERR, `Unknown status:[${response.status}]`, response.data);
        }
    }
}


exports.KunaApiClientV3 = KunaApiClientV3;