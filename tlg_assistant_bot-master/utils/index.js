/**
 * Module for DI utils modulse
 */
global.__UTILS = {};

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse.js');

__UTILS.INTERFACES = {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
};

const {
    AppLoggerClass,
    Loglevel
} = require('./logger/AppLoggerClass');

__UTILS.AppLoggerClass = AppLoggerClass;
__UTILS.Loglevel = Loglevel;

const {
    RpcProcessorClass
} = require('./rpc_processor/RpcProcessorClass');

__UTILS.RpcProcessorClass = RpcProcessorClass;

const {
    SequelizeClass
} = require('./sequelize/sequelize_class');

__UTILS.SequelizeClass = SequelizeClass;

const {
    KunaApiClientV3
} = require('./kuna_api/kuna_api_clinet_v3');

__UTILS.KunaApiClientV3 = KunaApiClientV3;


const {
    TelegramRpcClentClass,
    TelegramRpcClientStatusEnum
} = require('./telegram_rpc_client/telegram_rpc_client_class');

__UTILS.TelegramRpcClentClass = TelegramRpcClentClass;
__UTILS.TelegramRpcClientStatusEnum = TelegramRpcClientStatusEnum;


const helpers = require('./helpers/helpers');

__UTILS.helpers = helpers;

__UTILS.svg2png = require('svg-png-converter').svg2png;