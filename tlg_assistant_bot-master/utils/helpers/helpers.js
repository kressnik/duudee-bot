'use strict';

module.exports.delay = function delay(delayMs = 0) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(true)
        }, delayMs);
    });
}


global.__UTILS.delay = module.exports.delay;