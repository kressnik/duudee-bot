`use strict`

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);

const MOCKS_DIR = path.join(__dirname, 'mocks');

class RpcMockClientClass {
    constructor(version) {
        this._version = version;

        this._mocks = {};

        this._loadMocks();
    }

    _loadMocks() {
        const mocks = require('require-dir-all')(MOCKS_DIR, { // options
            recursive: true, // recursively go through subdirectories; default value shown
            indexAsParent: false, // add content of index.js/index.json files to parent object, not to parent.index
            includeFiles: /^.*\.(js)$/, // RegExp to select files; default value shown
        });

        const setMocks = (_moks) => {
            if (_moks.version == null) {
                for (const version in _moks) {
                    setMocks(_moks[version]);
                }
            } else {
                this._mocks[_moks.version] = _moks.methods;
            }
        }

        for (const version in mocks) {
            setMocks(mocks[version]);
        }
    }

    async request(packet) {
        try {
            const {
                method,
                params
            } = packet;

            const mocks = this._mocks[this._version];

            if (mocks == null) {
                throw new Error('Method in this version not found');
            }

            if (mocks[method] == null) {
                throw new Error('Method in this version not found');
            }

            return mocks[method](params);
        } catch (e) {
            console.error(e);

            throw e;
        }
    }
}


module.exports.RpcMockClientClass = RpcMockClientClass;