module.exports = {
    version: 'send_api_info_mock',
    methods: {
        "user.send.ApiInfo": function (params) {
            if (params.api_id == null) {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587908346",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: 'api_id'"
                        }
                    }
                }
            }

            if (params.api_hash == null) {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587908346",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: 'api_hash'"
                        }
                    }
                }
            }


            return {
                "jsonrpc": "2.0",
                "id": 1587917176,
                "result": {
                    "status": "WAITING_CODE"
                }
            }
        }
    }
}