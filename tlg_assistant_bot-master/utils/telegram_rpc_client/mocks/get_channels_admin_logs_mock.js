module.exports = {
    version: 'get_channels_admin_logs_mock',
    methods: {
        "channels.get.adminLog": function (params) {
            if (params.channel == null) {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587908346",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: 'channel'"
                        }
                    }
                }
            }

            if (params.channel == '') {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587917132",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: TypeError(\"bytes or str expected, not <class 'NoneType'>\")"
                        }
                    }
                }
            }


            return {
                "jsonrpc": "2.0",
                "id": 1587923686,
                "result": [{
                        "type": "ChannelAdminLogEvent",
                        "id": 833004749312,
                        "date": 1574975425,
                        "user_id": 829759382,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantLeave"
                        }
                    },
                    {
                        "type": "ChannelAdminLogEvent",
                        "id": 833004067240,
                        "date": 1574975425,
                        "user_id": 829759382,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantJoin"
                        }
                    },
                    {
                        "type": "ChannelAdminLogEvent",
                        "id": 833003276280,
                        "date": 1574975425,
                        "user_id": 264387813,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantToggleBan",
                            "prev_participant": {
                                "type": "ChannelParticipantBanned",
                                "user_id": 829759382,
                                "kicked_by": 264387813,
                                "date": 1574975425,
                                "banned_rights": {
                                    "type": "ChatBannedRights",
                                    "until_date": 1574975425,
                                    "view_messages": true,
                                    "send_messages": true,
                                    "send_media": true,
                                    "send_stickers": true,
                                    "send_gifs": true,
                                    "send_games": true,
                                    "send_inline": true,
                                    "embed_links": true,
                                    "send_polls": true,
                                    "change_info": true,
                                    "invite_users": true,
                                    "pin_messages": true
                                },
                                "left": true
                            },
                            "new_participant": {
                                "type": "ChannelParticipant",
                                "user_id": 829759382,
                                "date": 1574975425
                            }
                        }
                    },
                    {
                        "type": "ChannelAdminLogEvent",
                        "id": 832999903696,
                        "date": 1574975425,
                        "user_id": 264387813,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantToggleBan",
                            "prev_participant": {
                                "type": "ChannelParticipant",
                                "user_id": 829759382,
                                "date": 1574975425
                            },
                            "new_participant": {
                                "type": "ChannelParticipantBanned",
                                "user_id": 829759382,
                                "kicked_by": 264387813,
                                "date": 1574975425,
                                "banned_rights": {
                                    "type": "ChatBannedRights",
                                    "until_date": 1574975425,
                                    "view_messages": true,
                                    "send_messages": true,
                                    "send_media": true,
                                    "send_stickers": true,
                                    "send_gifs": true,
                                    "send_games": true,
                                    "send_inline": true,
                                    "embed_links": true,
                                    "send_polls": true,
                                    "change_info": true,
                                    "invite_users": true,
                                    "pin_messages": true
                                },
                                "left": true
                            }
                        }
                    },
                    {
                        "type": "ChannelAdminLogEvent",
                        "id": 832997776180,
                        "date": 1574975425,
                        "user_id": 877375623,
                        "action": {
                            "type": "ChannelAdminLogEventActionParticipantJoin"
                        }
                    }
                ]
            }
        }
    }
}