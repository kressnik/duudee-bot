module.exports = {
    version: 'get_auth_status_not_init_mock',
    methods: {
        "auth.get.Status": function (params) {
            return {
                "jsonrpc": "2.0",
                "id": "rid_1587904392103_v2",
                "result": {
                    "status": "NOT_INIT",
                    "phone": ""
                }
            }
        }
    }
}