module.exports = {
    version: 'get_auth_status_ok_mock',
    methods: {
        "auth.get.Status": function (params) {
            return {
                "jsonrpc": "2.0",
                "id": "rid_1587904392103_v2",
                "result": {
                    "status": "DONE",
                    "phone": "+38050999999999"
                }
            }
        }
    }
}