module.exports = {
    version: 'send_auth_code_mock',
    methods: {
        "user.send.AuthCode": function (params) {
            if (params.code == null) {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587908346",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: 'code'"
                        }
                    }
                }
            }

            if (params.code == '') {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587917132",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: TypeError(\"bytes or str expected, not <class 'NoneType'>\")"
                        }
                    }
                }
            }


            return {
                "jsonrpc": "2.0",
                "id": 1587917248,
                "result": {
                    "status": "OK"
                }
            }
        }
    }
}