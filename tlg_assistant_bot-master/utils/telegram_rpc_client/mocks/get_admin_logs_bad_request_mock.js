module.exports = {
    version: 'get_admin_logs_bad_request_mock',
    methods: {
        "channels.get.adminLog": function (params) {
            return {
                "jsonrpc": "2.0",
                "id": "1588432985",
                "error": {
                    "code": -32400,
                    "message": "BAD_REQUEST",
                    "data": {
                        "foo": "bar"
                    }
                }
            }

        }
    }
}