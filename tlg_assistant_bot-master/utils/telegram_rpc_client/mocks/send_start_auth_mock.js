module.exports = {
    version: 'send_start_auth_mock',
    methods: {
        "user.send.StartAuth": function (params) {
            if (params.phone == null) {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587908346",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: 'phone'"
                        }
                    }
                }
            }

            if (params.phone == '') {
                return {
                    "jsonrpc": "2.0",
                    "id": "1587917132",
                    "error": {
                        "code": -32602,
                        "message": "Invalid params.",
                        "data": {
                            "params": "Required param: TypeError(\"bytes or str expected, not <class 'NoneType'>\")"
                        }
                    }
                }
            }


            return {
                "jsonrpc": "2.0",
                "id": 1587917176,
                "result": {
                    "status": "WAITING_CODE"
                }
            }
        }
    }
}