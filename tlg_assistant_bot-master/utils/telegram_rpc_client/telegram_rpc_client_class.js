'use strict'
const Many = require('extends-classes');
const rp = require('request-promise');

const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    BasicResponseByError,
    ResultStatus
} = __UTILS.INTERFACES;


const {
    RpcStatusCode
} = require('./rpc_status_code')

const {
    RpcMockClientClass
} = require('./rpc_mock_telegram_client_class');

const DEF_RPC_PATH = 'jsonrpc';
const DEF_RPC_VER = 'v1';

const TelegramRpcClientStatusEnum = Object.freeze({
    IDLE: 'idle',
    DIASBLED: 'diasbled',
    AUTH_REQIRED: 'auth_reqired',
    AUTH_WAITNG_CODE: 'auth_reqired',
    WORKING: 'working',
    ERROR: 'error'
});


class TelegramRpcClientState {
    constructor(state = TelegramRpcClientStatusEnum.IDLE) {
        this._value = this.switch(state);
    }

    get value() {
        return this._value;
    }

    isWorking() {
        return this._value == TelegramRpcClientStatusEnum.WORKING;
    }

    switch (state) {
        if (Object.values(TelegramRpcClientStatusEnum).some(_state => _state === state)) {
            return this._value = state;
        }
    }
} //TelegramRpcClientState


class TelegramRpcClentPropsClass {
    constructor() {
        this._values = {
            host: '127.0.0.1',
            port: 5000,
            path: DEF_RPC_PATH,
            version: DEF_RPC_VER,
            useMocks: false
        };
    } //constuctor

    apply(args = {}) {
        for (const key in args) {
            if (this._values.hasOwnProperty(key) == true) {
                this._values[key] = args[key];
            }
        }

        return this;
    }

    get useMocks() {
        return this._values.useMocks;
    }

    get opt() {
        return {
            path: `/${this._values.version}/${this._values.path}/`,
            host: this._values.host,
            port: this._values.port,
        }
    }

    get url() {
        return `http://${this._values.host}:${this._values.port}/${this._values.version}/${this._values.path}/`;
    }
} //TelegramRpcClentPropsClass


class RpcRequsetPaketClass {
    constructor(args = {}) {
        const {
            jsonrpc = '2.0',
                method = '',
                params = {},
                id = `rid_${Date.now()}_v2`
        } = args;

        this._value = {
            jsonrpc,
            method,
            params,
            id
        }
    }

    toJSON() {
        return JSON.stringify(this._value);
    }

    get raw() {
        return this._value;
    }

    get id() {
        return this._value.id;
    }

    get json() {
        return JSON.stringify(this._value);
    }
} //RpcRequsetPaketClass


class TelegramRpcClentClassPublicMethods {
    /**
     * Get auth status
     * @param void
     */
    async authGetStatus() {
        try {
            this._logger.info('Get auth status');

            const request = await this._rcpCall('auth.get.Status');

            request.isOK();

            const {
                status,
                phone
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Get auth status return status:[${status}], phone:[${phone}]`, {
                status,
                phone
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`Get auth status return: ${error}`);

            return error;
        }
    } //authGetStatus

    /**
     * Send and seup api information
     * @param {*} apiId
     * @param {*} apiHash
     */
    async userSendApiInfo(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'args is null, should be an object');
            }

            const {
                apiId,
                apiHash
            } = args;


            this._logger.info(`Send api info auth apiId:[${apiId}], apiHash:[${apiHash}]`);

            const request = await this._rcpCall('user.send.ApiInfo', {
                api_id: apiId,
                api_hash: apiHash
            });

            request.isOK();

            /** {
                "status": "DONE",
                "phone": "+380502947272"
            } */
            const {
                status
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Send api info auth apiId:[${apiId}], apiHash:[${apiHash}] return status:[${status}]`, {
                status
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`Get userSendApiInfo return: ${error}`);

            return error;
        }
    } //userSendApiInfo

    /**
     * Start initiate auth process.
     * @param {*} phone phone for recived code
     */
    async userSendStartAuth(phone) {
        try {
            if (phone == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'phone is null, should be an strting');
            }

            if (typeof phone != 'string') {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'phone should be an strting', typeof phone);
            }


            this._logger.info(`Send start auth for phone:[${phone}]`);

            const request = await this._rcpCall('user.send.StartAuth', {
                phone
            });

            request.isOK();

            const {
                status
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Send start auth for phone:[${phone}] return status:[${status}]`, {
                status
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`userSendStartAuth return: ${error}`);

            return error;
        }
    } //userSendStartAuth


    /**
     * Send auth code for auth process.
     * @param {*} phone phone for recived code
     */
    async userSendAuthCode(code) {
        try {
            if (code == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'code is null, should be an strting');
            }

            if (typeof code != 'string') {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'code should be an strting');
            }


            this._logger.info(`Send auth code:[${code}]`);

            const request = await this._rcpCall('user.send.AuthCode', {
                code
            });

            request.isOK();

            const {
                status
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Send auth code:[${code}] returned status:[${status}]`, {
                status
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`userSendAuthCode catch error: ${error}`);

            return error;
        }
    } //userSendAuthCode


    /**
     * Get the admin log of a channel / supergroup
     * @params channel Username, chat ID, Update, Message or InputChannel Channel Optional
     * @params q string Search query, can be empty Yes
     * @params eventsFilter ChannelAdminLogEventsFilter Event filter Optional
     * @params admins Array of Username, chat ID, Update, Message or InputUser Fetch only actions from these admins Optional
     * @params maxId long Maximum ID of message to return (see pagination) Yes
     * @params minId long Minimum ID of message to return (see pagination) Yes
     * @params limit int Maximum number of results to return, see pagination
     */
    async channelsGetAdminLog(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'args is null, should be an object');
            }

            const {
                channel,
                q,
                eventsFilter,
                admins,
                maxId,
                minId,
                limit
            } = args;

            const srtArgs = [
                channel ? `channel:[${channel}]` : null,
                maxId ? `maxId:[${maxId}]` : null,
                minId ? `minId:[${minId}]` : null,
                limit ? `limit:[${limit}]` : null,
                q ? `q:[${q}]` : null,
            ].filter(e => e != null).join(',');

            this._logger.info(`Get channel admin logs. ${srtArgs}`);

            const request = await this._rcpCall('channels.get.adminLog', {
                channel,
                max_id: maxId,
                min_id: minId,
                limit,
                q,
                admins,
                events_filter: eventsFilter,
            });

            request.isOK();

            const rsp = new BasicResponse(ResultStatus.OK, `Get channel admin logs. ${srtArgs} done successfully`, request.data);

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`channelsGetAdminLog catch error: ${error}`);

            return error;
        }
    } //userSendAuthCode
} //TelegramRpcClentClassPublicMethods


class TelegramRpcClentClass extends TelegramRpcClentClassPublicMethods {
    constructor(args = {}) {
        super();

        const {
            host,
            port,
            mocksVersion,
            useMocks = false,
        } = args;

        this._logger = new AppLoggerClass('TelegramRpcClentClass');

        this._props = new TelegramRpcClentPropsClass().apply(args);
        this._state = new TelegramRpcClientState(TelegramRpcClientStatusEnum.WORKING); //temporary

        if (useMocks == true) {
            this._mockClient = new RpcMockClientClass(mocksVersion);
        }
    } //constructor

    async init(configs) {
        try {
            if (configs == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'configs is null, should be an object');
            }

            this._props.apply(configs);

            if (configs.useMocks == true) {
                this._mockClient = new RpcMockClientClass(configs.mocksVersion);
            }

            this._logger.info(`Setup props:[${JSON.stringify(configs)}]`);

            const rsp = new BasicResponse(ResultStatus.OK, `Init RPC client done successfully`);

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }


    _reduceErrorRpcToBasic(e) {
        try {
            switch (e.code) {
                case RpcStatusCode.NOT_FOUND:
                    return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, e.message, e.data);
                case RpcStatusCode.SERVER_ERROR_INVALID_METHOD_PARAMETERS:
                    return new BasicResponse(ResultStatus.ERROR_INVALID_DATA, e.message, e.data);
                case RpcStatusCode.SERVER_ERROR_INVALID_XML_RPC:
                    return new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, e.message, e.data);
                case RpcStatusCode.NOT_FOUND:
                    return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, e.message, e.data);
                case RpcStatusCode.UNAUTHORIZED:
                    return new BasicResponse(ResultStatus.ERROR_UNAUTHORIZED, e.message, e.data);
                case RpcStatusCode.SEE_OTHER:
                    return new BasicResponse(ResultStatus.ERROR_SEE_OTHER, e.message, e.data);
                case RpcStatusCode.BAD_REQUEST:
                    return new BasicResponse(ResultStatus.ERROR_BAD_REQUEST, e.message, e.data);
                case RpcStatusCode.FORBIDDEN:
                    return new BasicResponse(ResultStatus.ERROR_FORBIDDEN, e.message, e.data);
                case RpcStatusCode.NOT_ACCEPTABLE:
                    return new BasicResponse(ResultStatus.ERROR_NOT_ACCEPTABLE, e.message, e.data);
                case RpcStatusCode.FLOOD:
                    return new BasicResponse(ResultStatus.ERROR_FLOOD, e.message, e.data);
                case RpcStatusCode.INTERNAL:
                    return new BasicResponse(ResultStatus.ERROR_INTERNAL, e.message, e.data);

                default:
                    return new BasicResponseByError(e);
            }
        } catch (error) {
            return new BasicResponseByError(error);
        }
    }

    async _request(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'args should be an object');
            }

            if (this._props.useMocks == true) {
                return this._mockClient.request(args.body);
            }

            return await rp({
                method: 'POST',
                uri: args.url,
                body: args.body,
                json: true // Automatically stringifies the body to JSON
            });
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async _rcpCall(method, params = {}) {
        try {
            if (this._state.isWorking() == false) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_READY, `Telrgram RPC clinetn not ready, state:[${this._state.value}]`)
            }

            if (method == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Method should be specified');
            }

            const startTime = Date.now();

            const packet = new RpcRequsetPaketClass({
                method,
                params
            });

            this._logger.info(`Make request id:[${packet.id}] method:[${method}], start time:[${startTime}]`);

            this._logger.debug(`RPC request url: ${this._props.url}`);
            this._logger.debug(`RPC request packet: ${packet.json}`);

            const response = await this._request({
                url: this._props.url,
                body: packet.raw
            });

            this._logger.debug(`RPC response packet: ${JSON.stringify(response)}`);

            this._logger.info(`Recive response request id:[${packet.id}] method:[${method}], end time:[${Date.now()}], duration:[${Date.now() - startTime} ms]`);

            if (response.error) {
                throw response.error;
            }

            if (response.result == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Not found any result property for parse.');
            }

            const rsp = new BasicResponse(ResultStatus.OK, `Call method: [${method}] done successfully`, response.result);


            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = this._reduceErrorRpcToBasic(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    updateConnProps({
        host,
        port
    }) {
        try {
            this.props.host = host || this.props.host;
            this.props.port = port || this.props.port;

            this._client = jayson.client.http(this.props.opt());

            let rsp = new BasicResponse(ResultStatus.OK, 'Update connection props done successfully', {});

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
}


exports.TelegramRpcClentClass = TelegramRpcClentClass;
exports.TelegramRpcClientStatusEnum = TelegramRpcClientStatusEnum;