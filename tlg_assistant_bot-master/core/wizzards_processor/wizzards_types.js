exports.CHANNEL_OP_BUTTONS = Object.freeze({
    STOP: 'Отменить ❌',
    BACK: 'Назад 🔙'
});

exports.TRACK_STATE = Object.freeze({
    CREATED: 'created',
    WAITING_ORDER: 'waiting_order',
    DONE_OK: 'done',
    DONE_ERROR: 'done_error'
});