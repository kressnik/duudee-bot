'use strict'

const {
    AppLoggerClass
} = __UTILS;


const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;


const {
    TRACK_STATE
} = require('./wizzards_types');

class WizardSceneTrackerClass {
    constructor(db) {
        this._logger = new AppLoggerClass('WizardSceneTrackerClass', process.env.WIZARD_SCENE_TRACKER_CLASS_LOG_LEVEL);
        this._db = db;
    }

    /**
     * 
     * @param {*} db link to db connection 
     */
    async init(db) {
        try {
            if (db == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'DB link is null');
            }

            this._db = db;

            let rsp = new BasicResponse(ResultStatus.OK, "Init wizzard tracker done successfully");

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    } //init

    /**
     * 
     * @param {*} buttonId button invoker
     * @param {*} userId user who started
     * @param {*} tokenId token of link
     * @param {*} sessionId session of track that now is active
     * @param {*} isNewUser true if user first time in system
     * @param {*} channelId channel id 
     */
    async start(args) {
        try {
            const {
                buttonId, //button invoker
                userId, //user who started
                tokenId, //token of link 
                sessionId, //token active session id,
                isNewUser,
                channelId
            } = args;

            this._logger.info([
                `Start track for `,
                `user id:[${userId}]`,
                `token id:[${tokenId}]`,
                `button id:[${buttonId}]`,
                `session id:[${sessionId}]`,
                `isNewUser:[${isNewUser}]`,
                `channel id:[${channelId}]`
            ].join(','));

            let created = await this._db.models.WizzardTrack.createOne({
                status: TRACK_STATE.CREATED,
                buttonId, //button invoker
                userId, //user who started
                tokenId, //token of link
                sessionId, //token active session id,
                isNewUser,
                channelId
            });

            created.isOK();

            this._logger.info(created);

            return created;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    } //start

    /**
     * 
     * @param {*} id
     * @param {*} stage
     * @param {*} orderId
     * @param {*} error
     * @param {*} channelId
     *  
     */
    async updateStage(args) {
        try {
            const {
                id,
                stage,
                orderId,
                error,
                channelId
            } = args;

            this._logger.info([
                `Update track `,
                `id:[${id}]`,
                `stage:[${stage}]`,
                `order id:[${orderId}]`,
                `error:[${error}]`,
                `channel id:[${channelId}]`
            ].join(','));

            let updteResult = await this._db.models.WizzardTrack.updateOne({
                id,
                stage,
                orderId,
                error,
                channelId
            });

            updteResult.isOK();

            this._logger.info(updteResult);

            return updteResult;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    } //updateStage

    /**
     * Super fucntion
     * @param {*} id
     *  
     */
    async wait(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments should be an object');
            }

            const {
                id,
                channelId
            } = args;

            let updteResult = await this._db.models.WizzardTrack.updateOne({
                id,
                status: TRACK_STATE.WAITING_ORDER,
                channelId
            });

            updteResult.isOK();

            this._logger.info(updteResult);

            let rsp = new BasicResponse(ResultStatus.OK, `Set track id:[${id}] waitng status:[${TRACK_STATE.WAITING_ORDER}] done successfully`)

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    }

    /**
     * 
     * @param {*} id
     * @param {*} error
     *  
     */
    async done(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments should be an object');
            }

            const {
                id,
                subscribeId,
                error,
                channelId
            } = args;

            if (error != null) {
                this._logger.warn(`Set done track id:[${id}] with error:[${error}]`)
            } else {
                this._logger.info(`Set done track id:[${id}]`);
            }

            let status = error != null ? TRACK_STATE.DONE_ERROR : TRACK_STATE.DONE_OK;

            let updteResult = await this._db.models.WizzardTrack.updateOne({
                id,
                subscribeId,
                status,
                error,
                channelId
            });

            updteResult.isOK();

            let rsp = new BasicResponse(ResultStatus.OK, `Set track id:[${id}] finalized status:[${status}] with error:[${error}] done successfully`)

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    }


    /**
     * 
     * @param {Object} filter [tokenId<Array>, buttonId<Array>, channelId<Array>, orderId<Array>, sessionId<Array>, error<Array>, status<Array>, id<Array>] 
     * @param {Array} timeRange: [start, end],
     */
    async getAllByFilter(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Args is null, should be an object');
            }

            if (Array.isArray(args) == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Args should be an object');
            }

            this._logger.info(`Start get all tracks by filter`);

            let tracksRequest = await this._db.models.WizzardTrack.getAllByFilter(args);

            this._logger.info(tracksRequest);

            return tracksRequest;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getAllByFilterAndPagination(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Args is null, should be an object');
            }

            if (Array.isArray(args) == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Args should be an object');
            }

            this._logger.info(`Start get all tracks by filter with pagination`);

            let tracksRequest = await this._db.models.WizzardTrack.getAllByFilterAndPagination(args);

            this._logger.info(tracksRequest);

            return tracksRequest;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
}

exports.WizardSceneTrackerClass = WizardSceneTrackerClass;