const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;

const WizardScene = require("telegraf/scenes/wizard");
const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const svgCaptcha = require('svg-captcha');

const {
    CHANNEL_OP_BUTTONS
} = require('../wizzards_types');


const {
    svg2png
} = __UTILS

const humanizeDuration = require('humanize-duration');

const DECLINE_KEYBOARD = Extra
    .HTML()
    .markup((m) => m.keyboard([
        [
            //CHANNEL_OP_BUTTONS.BACK,
            CHANNEL_OP_BUTTONS.STOP
        ],
    ]).resize());

const {
    WizardBasicFuctionsClass
} = require('./wizzard_basic_fucntions');

class ChannelsFreeWizardClass extends WizardBasicFuctionsClass {
    constructor(name = "foo bar", core) {
        super();

        this._logger = new AppLoggerClass(`ChannelsWizardClass:${name}`, process.env.WIZARD_LOG_LEVEL);

        this.name = name;
        this.core = core;
    }

    build(name, keyboards, core) {
        this.keyboards = keyboards;
        this.core = core;

        const create = new WizardScene(
            this.name || name, {
                leaveHandlers: [this._leaveMiddlware.bind(this.core)]
            },

            this._getChannelListScence.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //1

            this._makeSubscribeRequestChAfterSelectScene.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //2

            this._checkAntiBotInputFields.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //3
        );

        return create;
    }

    async _getChannelListScence(checkBefore, errorHandler, ctx) {
        try {
            //\warn first scene dont use checkBefore!
            let token = ctx.session.user.getLastToken();

            ctx.session.track = {};

            let startTrack = await ctx.session.tracker.start({
                userId: ctx.session.user.id,
                buttonId: ctx.session.button.id,
                tokenId: token && token.tokenId,
                sessionId: token && token.sessionId,
                isNewUser: ctx.session.user.isNew
            });

            startTrack.isOK();

            this._logger.info(startTrack);

            ctx.session.track = {
                ...startTrack.data
            }
            //end prepare info for tracking

            //delete start messages
            if (ctx.session && ctx.session.startMessage) {
                let message = ctx.session.startMessage;

                let delRes = await this._deleteChatMessage({
                    chatId: message.chat.id,
                    messageId: message.message_id
                });

                if (delRes.status != ResultStatus.OK) {
                    this._logger.error(delRes);
                } else {
                    this._logger.info(delRes);
                }

                ctx.session.startMessage = null;
            }

            let pChannels = await this._userSubsProc.getChannels({
                isFree: true
            });

            if (
                pChannels.status == ResultStatus.ERROR_NOT_FOUND ||
                pChannels.data.length == 0
            ) {
                ctx.session.error = pChannels;

                await ctx.reply('К сожалению сейчас нет активных каналов 😱');

                await ctx.scene.leave();

                return pChannels; //next scene
            }

            pChannels.isOK();

            let channelsList = pChannels.data.slice();

            let descrMsg = await ctx.reply(`Вы выбрали ${ctx.match || ctx.session.button.name}`, DECLINE_KEYBOARD);

            this._logger.debug(`Send to telegram descrMsg:[${descrMsg.message_id}]`);

            //if channel id not set send choise

            let msg = Extra.HTML()
                .markup((m) => {
                    return m.inlineKeyboard(channelsList.map(ch => {
                        let subscriptionDuration = null;

                        if (process.env.USE_HUMANIZE_DURATION != null) {
                            subscriptionDuration = humanizeDuration(ch.subscriptionDuration * 3600 * 1000, {
                                language: 'ru',
                                units: ['mo'],
                                round: true
                            }); //get seconds from hours
                        }

                        subscriptionDuration = ch.subscriptionDuration > 0 ? subscriptionDuration : '';


                        let name = [
                            `${ch.name}`,
                            subscriptionDuration || null
                        ].filter(a => a != null).join(' | ');

                        return [m.callbackButton(name, `${ch.tlgId}`)];
                    }));
                });

            let listMsg = await ctx.reply('Выберите канал на который хотите подписаться 🤳', msg);

            ctx.session.startMessages = [listMsg];

            this._logger.debug(`Send to telegram listMsg:[${listMsg.message_id}]`);


            return ctx.wizard.next(); //next scene
        } catch (e) {
            errorHandler(e, ctx);
        }
    }

    async _makeSubscribeRequestChAfterSelectScene(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == false) {
                return;
            }

            //tracking 
            let updateTrackRes = await ctx.session.tracker.updateStage({
                id: ctx.session.track.id,
                stage: ctx.session.__scenes.cursor
            });

            updateTrackRes.isOK();

            this._logger.info(updateTrackRes);
            //end tracking 

            //delete start messages
            if (Array.isArray(ctx.session.startMessages) == true) {
                for (let message of ctx.session.startMessages) {
                    this._logger.info(`Delete message_id:[${message.message_id}] for chat_id:[${message.chat.id}]`);

                    let delRes = await this._deleteChatMessage({
                        chatId: message.chat.id,
                        messageId: message.message_id
                    });

                    if (delRes.status != ResultStatus.OK) {
                        this._logger.error(delRes);
                    }

                    this._logger.info(delRes);
                }

                ctx.session.startMessages = null;
            }


            const channelTlgId = JSON.parse(ctx.update.callback_query.data);

            const checkSubscribe = await this._userSubsProc.chekcSubcribeByTlg({
                channelTlgId: channelTlgId,
                userId: ctx.session.user.id
            });

            ctx.session.channelId = checkSubscribe.data.channel.id;

            //tracking 
            const updateTracChannelIdRes = await ctx.session.tracker.updateStage({
                id: ctx.session.track.id,
                stage: ctx.session.__scenes.cursor,
                channelId: ctx.session.channelId
            });

            if (updateTracChannelIdRes.isStatus(ResultStatus.OK)) {
                this._logger.info(updateTracChannelIdRes);
            } else {
                this._logger.warn(updateTracChannelIdRes);
            }

            if (checkSubscribe.status == ResultStatus.ERROR_ALREADY_ASSIGNED) {
                this._logger.warn(checkSubscribe);

                const subscribe = checkSubscribe.data.subscribe;

                //set erro for tracking
                ctx.session.error = checkSubscribe;
                ctx.session.orderId = subscribe.orderId;


                if (subscribe.isGranded == true) {
                    const msg = [
                        `У вас уже оформлена подписка на этот канал.`,
                        `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                    ].join('\n');

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);

                    this._logger.info(`Start export invite link for channel tlgId:[${channelTlgId}]`);

                    const inviteLink = await this._exportChatInviteLink(channelTlgId);

                    inviteLink.isOK();

                    const linkBtn = Markup.urlButton('Вернуться в канал', inviteLink.data);

                    const sendUserRes2 = await ctx.reply('Для перехода в канал нажми на кнопку 👇🏻', Markup.inlineKeyboard([linkBtn]).extra());

                    this._logger.info(`Send 'already' messge id ${sendUserRes2.message_id}`);
                } else {
                    const msg = [
                        `У вас начат процесс подписки на этот канал. Ждите финального стататуса.`,
                        `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                    ].join('\n');

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);
                }

                return await ctx.scene.leave();
            }

            // const captcha = svgCaptcha.createMathExpr({
            //     color: true, // characters will have distinct colors instead of grey, true if background option is set
            //     background: '#FFFFFF', // background color of the svg image
            //     mathMin: 1, // the minimum value the math expression can be
            //     mathMax: 9, // the maximum value the math expression can be
            //     mathOperator: '+', // The operator to use, +, - or +- (for random + or -)
            // });

            const captcha = svgCaptcha.create({
                size: process.env.CAPTCHA_SIZE || 4, // size of random string
                charPreset: '1234567890',
                ignoreChars: '', // filter out some characters like 0o1i
                noise: process.env.CAPTCHA_NOISE || 1, // number of noise lines
                color: true, // characters will have distinct colors instead of grey, true if background option is set
                background: '#FFFFFF', // background color of the svg image
            });

            const outputBuffer = await svg2png({
                input: captcha.data,
                encoding: 'buffer',
                format: 'jpeg',
            });


            await ctx.reply('Введите цифры указанные на изображении');
            await ctx.replyWithPhoto({
                source: outputBuffer,
            });


            ctx.session.bill = {
                captcha,
                channelTlgId
            };

            return ctx.wizard.next(); //next scene
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_makeSubscribeRequestChScene

    async _checkAntiBotInputFields(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx, true) == false) {
                return
            }

            //tracking 
            let updateTrackRes = await ctx.session.tracker.updateStage({
                id: ctx.session.track.id,
                stage: ctx.session.__scenes.cursor
            });

            updateTrackRes.isOK();

            this._logger.info(updateTrackRes);
            //end tracking 

            let answer = ctx.update.message ? ctx.update.message.text : '';

            let bill = ctx.session.bill;

            if (answer == bill.captcha.text) {
                let bill = await this._userSubsProc.makeFreeSubscribeRequestChannel({
                    channelTlgId: ctx.session.bill.channelTlgId,
                    userId: ctx.session.user.id
                });

                if (bill.status == ResultStatus.OK) {
                    this._logger.info(bill.toString());

                    bill = bill.data;

                    let setWait = await ctx.session.tracker.wait({
                        id: ctx.session.track.id,
                        channelId: bill.channelId
                    });

                    if (setWait.status != ResultStatus.OK) {
                        this._logger.warn(setWait);
                    } else {
                        this._logger.info(setWait);
                    }


                    let subsrcribeResult = await this._userSubsProc.makeFreeSubscribeOrder({
                        ...bill,
                        trackId: ctx.session.track.id
                    });

                    if (subsrcribeResult.status != ResultStatus.OK) {
                        throw subsrcribeResult;
                    }

                    subsrcribeResult = subsrcribeResult.data;

                    //set for tracking
                    ctx.session.orderId = subsrcribeResult.orderInfo.id;

                    let msg = [
                        `Запрос на подписку оформлен`,
                        `Вы ближайшее время вам прийдет инвайт ссылка`,
                        `Если у вас появились вопросы или что то пошло не так вы можете написать в саппорт @duudee_sport_support 🆘`,
                    ].join('\n');

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);

                    return await ctx.scene.leave();
                }
            }

            let msg = 'Ответ не верный. Повторите ввод';

            await ctx.reply(msg);
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_checkPhoneInputFields


} //ChannelsWizardClass


exports.ChannelsFreeWizardClass = ChannelsFreeWizardClass;
exports.CHANNEL_OP_BUTTONS = CHANNEL_OP_BUTTONS;