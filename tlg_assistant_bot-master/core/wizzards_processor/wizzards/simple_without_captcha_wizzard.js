const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

const WizardScene = require("telegraf/scenes/wizard");

const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const CHANNEL_OP_BUTTONS = Object.freeze({
    STOP: 'Отменить ❌',
    BACK: 'Назад 🔙'
});

const DECLINE_KEYBOARD = Extra
    .HTML()
    .markup((m) => m.keyboard([
        [
            //CHANNEL_OP_BUTTONS.BACK,
            CHANNEL_OP_BUTTONS.STOP
        ],
    ]).resize());

const {
    WizardBasicFuctionsClass
} = require('./wizzard_basic_fucntions');

class SimpleFreeWithoutCaptchaWizardClass extends WizardBasicFuctionsClass {
    constructor(name = "foo bar", core) {
        super();

        this._logger = new AppLoggerClass(`SimpleFreeWizardClass:${name}`, process.env.WIZARD_LOG_LEVEL);

        this.name = name;
        this.core = core;
    }

    build(name, keyboards, core) {
        this.keyboards = keyboards;
        this.core = core;

        const create = new WizardScene(
            this.name || name, {
                leaveHandlers: [this._leaveMiddlware.bind(this.core)]
            },
            this._makeSubscribeRequestChAfterSelectScene.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //1
        );

        return create;
    }

    async _makeSubscribeRequestChAfterSelectScene(checkBefore, errorHandler, ctx) {
        try {
            //update user information

            //prrepare info for tracking
            const token = ctx.session.user.getLastToken();

            ctx.session.track = {};

            const startTrack = await ctx.session.tracker.start({
                userId: ctx.session.user.id,
                buttonId: ctx.session.button.id,
                tokenId: token && token.tokenId,
                sessionId: token && token.sessionId,
                isNewUser: ctx.session.user.isNew,
                channelId: ctx.session.channelId
            });


            startTrack.isOK();

            this._logger.info(startTrack);

            ctx.session.track = {
                ...startTrack.data
            }
            //end prepare info for tracking

            const channelInfo = await this._userSubsProc.getChannel({
                id: ctx.session.channelId
            });

            if (
                channelInfo.status == ResultStatus.ERROR_NOT_FOUND
            ) {
                this._logger.warn(channelInfo);

                await ctx.reply('К сожалению сейчас нет активных каналов 😱');

                ctx.session.error = channelInfo;

                return await ctx.scene.leave();
            }

            channelInfo.isOK();

            const channelId = channelInfo.data.id;

            if (channelInfo.data.inUse == false) {
                let error = new BasicResponse(ResultStatus.ERROR_RESOURCE_IS_NOT_ACTIVE, `Channel id:[${channelId}] is not active`, channelInfo.data);

                this._logger.warn(error);

                await ctx.reply('К сожалению сейчас нет активных каналов 😱');

                ctx.session.error = error;

                return await ctx.scene.leave();
            }

            const channelTlgId = channelInfo.data.tlgId;

            ctx.reply(`Вы хотите оформить подписку на канал: ${channelInfo.data.name}`, DECLINE_KEYBOARD);

            //delete start messages
            if (Array.isArray(ctx.session.startMessages) == true) {
                for (let message of ctx.session.startMessages) {
                    this._logger.info(`Delete message_id:[${message.message_id}] for chat_id:[${message.chat.id}]`);

                    let delRes = await this._deleteChatMessage({
                        chatId: message.chat.id,
                        messageId: message.message_id
                    });

                    if (delRes.status != ResultStatus.OK) {
                        this._logger.error(delRes);
                    }

                    this._logger.info(delRes);
                }

                ctx.session.startMessages = null;
            }

            const checkSubscribe = await this._userSubsProc.chekcSubcribeByTlg({
                channelTlgId: channelTlgId,
                userId: ctx.session.user.id
            });

            ctx.session.channelId = checkSubscribe.data.channelId;

            if (checkSubscribe.status == ResultStatus.ERROR_ALREADY_ASSIGNED) {
                this._logger.warn(checkSubscribe);

                const subscribe = checkSubscribe.data.subscribe;

                //set erro for tracking
                ctx.session.error = checkSubscribe;
                ctx.session.orderId = subscribe.orderId;

                if (subscribe.isGranded == true) {
                    const msg = [
                        `У вас уже оформлена подписка на этот канал.`,
                        `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                    ].join('\n');

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);

                    this._logger.info(`Start export invite link for channel tlgId:[${channelTlgId}]`);

                    const inviteLink = await this._exportChatInviteLink(channelTlgId);

                    inviteLink.isOK();

                    const linkBtn = Markup.urlButton('Вернуться в канал', inviteLink.data);

                    const sendUserRes2 = await ctx.reply('Для перехода в канал нажми на кнопку👇🏻', Markup.inlineKeyboard([linkBtn]).extra());

                    this._logger.info(`Send already messge id ${sendUserRes2.message_id}`);
                } else {
                    const msg = [
                        `У вас начат процесс подписки на этот канал. Ждите финального стататуса.`,
                        `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                    ].join('\n');

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);
                }

                this._logger.warn(checkSubscribe.toString());

                return await ctx.scene.leave();
            } // ccheck already assign


            let bill = await this._userSubsProc.makeFreeSubscribeRequestChannel({
                channelTlgId,
                userId: ctx.session.user.id
            });

            bill.isOK();

            this._logger.info(bill.toString());

            bill = bill.data;

            const setWait = await ctx.session.tracker.wait({
                id: ctx.session.track.id,
                channelId: bill.channelId
            });

            if (setWait.status != ResultStatus.OK) {
                this._logger.warn(setWait);
            } else {
                this._logger.info(setWait);
            }

            let subsrcribeResult = await this._userSubsProc.makeFreeSubscribeOrder({
                ...bill,
                trackId: ctx.session.track.id
            });

            if (subsrcribeResult.status != ResultStatus.OK) {
                throw subsrcribeResult;
            }

            subsrcribeResult = subsrcribeResult.data;

            //set for tracking
            ctx.session.orderId = subsrcribeResult.orderInfo.id;

            const msg = [
                `Запрос на подписку оформлен`,
                `Вы ближайшее время вам прийдет инвайт ссылка`,
                `Если у вас появились вопросы или что то пошло не так вы можете написать в саппорт @duudee_sport_support 🆘`,
            ].join('\n');

            await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);

            return await ctx.scene.leave();
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_makeSubscribeRequestChScene
} //SimpleFreeWithoutCaptchaWizardClass


exports.SimpleFreeWithoutCaptchaWizardClass = SimpleFreeWithoutCaptchaWizardClass;
exports.CHANNEL_OP_BUTTONS = CHANNEL_OP_BUTTONS;