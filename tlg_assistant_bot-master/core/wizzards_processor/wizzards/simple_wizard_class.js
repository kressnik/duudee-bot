const {
    AppLoggerClass
} = __UTILS;


const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

const WizardScene = require("telegraf/scenes/wizard");

const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const CHANNEL_OP_BUTTONS = Object.freeze({
    STOP: 'Отменить ❌',
    BACK: 'Назад 🔙'
});

const DECLINE_KEYBOARD = Extra.HTML()
    .markup((m) => m.keyboard([
        [
            //CHANNEL_OP_BUTTONS.BACK,
            CHANNEL_OP_BUTTONS.STOP
        ],
    ]).resize());

const {
    WizardBasicFuctionsClass
} = require('./wizzard_basic_fucntions');


class SimpleWizardClass extends WizardBasicFuctionsClass {
    constructor(name = "foo bar", core, db) {
        super();

        this._logger = new AppLoggerClass(`SimpleWizardClass:${name}`, process.env.WIZARD_LOG_LEVEL);

        this.name = name;
        this.core = core;
        this._db = db;
    }

    build(name, keyboards) {
        this.keyboards = keyboards;

        const create = new WizardScene(
            this.name || name, {
                leaveHandlers: [this._leaveMiddlware.bind(this.core)]
            },
            this._getPaymentsCurrenciesListScence.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //1
            this._getPaymentsListScence.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //2

            this._makeSubscribeRequestChScene.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //3

            this._checkPhoneInputFields.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //4

            this._finalScene.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)) //5
        );

        return create;
    }

    async _getPaymentsCurrenciesListScence(checkBefore, errorHandler, ctx) {
        try {
            //\warn first scene dont use checkBefore!

            //prrepare info for tracking
            let token = ctx.session.user.getLastToken();

            ctx.session.track = {};

            let startTrack = await ctx.session.tracker.start({
                userId: ctx.session.user.id,
                buttonId: ctx.session.button.id,
                tokenId: token && token.tokenId,
                sessionId: token && token.sessionId,
                isNewUser: ctx.session.user.isNew,
                channelId: ctx.session.channelId
            });

            startTrack.isOK();

            this._logger.info(startTrack);

            ctx.session.track = {
                ...startTrack.data
            }
            //end prepare info for tracking

            const channelInfo = await this._userSubsProc.getChannel({
                id: ctx.session.channelId
            });

            if (
                channelInfo.status == ResultStatus.ERROR_NOT_FOUND
            ) {
                this._logger.warn(channelInfo);

                await ctx.reply('К сожалению сейчас нет активных каналов 😱');

                ctx.session.error = channelInfo;

                await ctx.scene.leave();

                return channelInfo; //next scene
            }

            channelInfo.isOK();

            if (channelInfo.data.inUse == false) {
                const error = new BasicResponse(ResultStatus.ERROR_RESOURCE_IS_NOT_ACTIVE, `Channel id:[${channelInfo.data.id}] is not active`, channelInfo.data);

                this._logger.warn(error);

                await ctx.reply('К сожалению сейчас нет активных каналов 😱');

                ctx.session.error = error;

                return await ctx.scene.leave();
            }

            const channelTlgId = channelInfo.data.tlgId;

            ctx.reply(`Вы хотите оформить подписку на канал: ${channelInfo.data.name}`, DECLINE_KEYBOARD);

            //delete prev messages
            if (Array.isArray(ctx.session.startMessages) == true) {
                for (const message of ctx.session.startMessages) {
                    this._logger.info(`Delete message_id:[${message.message_id}] for chat_id:[${message.chat.id}]`);

                    let delRes = await this._deleteChatMessage({
                        chatId: message.chat.id,
                        messageId: message.message_id
                    });

                    if (delRes.status != ResultStatus.OK) {
                        this._logger.error(delRes);
                    }

                    this._logger.info(delRes);
                }
            }

            ctx.session.startMessages = null;

            const checkSubscribe = await this._userSubsProc.chekcSubcribeByTlg({
                channelTlgId: channelTlgId,
                userId: ctx.session.user.id
            });

            if (checkSubscribe.status == ResultStatus.ERROR_ALREADY_ASSIGNED) {
                const subscribe = checkSubscribe.data.subscribe;

                //set erro for tracking
                ctx.session.error = checkSubscribe;
                ctx.session.orderId = subscribe.orderId;

                this._logger.warn(checkSubscribe.toString());

                if (subscribe.isGranded == true) {
                    const msg = [
                        `У вас уже оформлена подписка на этот канал.`,
                        `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                    ].join('\n');

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);

                    if (process.env.SEND_INVITE_IF_ALREADY_EXIST_FOR_PAUED_WIZZ != null) {
                        this._logger.info(`Start export invite link for channel tlgId:[${channelTlgId}]`);

                        const inviteLink = await this._exportChatInviteLink(channelTlgId);

                        inviteLink.isOK();

                        const linkBtn = Markup.urlButton('Вернуться в канал', inviteLink.data);

                        const sendUserRes2 = await ctx.reply('Для перехода в канал нажми на кнопку👇🏻', Markup.inlineKeyboard([linkBtn]).extra());

                        this._logger.info(`Send already messge id ${sendUserRes2.message_id}`);
                    } //SEND_INVITE_IF_ALREADY_EXIST_FOR_PAUED_WIZZ
                } else {
                    const msg = [
                        `У вас начат процесс подписки на этот канал. Ждите финального стататуса.`,
                        `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                    ].join('\n');

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);
                } //isGranded

                return await ctx.scene.leave();
            } //ERROR_ALREADY_ASSIGNED

            const currenciesList = await this._userSubsProc.getPaymentCurrencies();
            //if it have not callback_query skip

            if (currenciesList.status != ResultStatus.OK) {
                throw currenciesList;
            }

            if (currenciesList.data.length == 0) {
                await ctx.reply('К сожалению сейчас нет активных способов оплаты 😱');

                ctx.session.error = new BasicResponse(ResultStatus.ERRROR_HAS_NOT_ACTIVE_PAYMENT_CURRENCY, `Not found any active payment methods trackId:[${ctx.session.track.id}]`);

                ctx.wizard.leave(); //next scene
            } else {
                await ctx.reply('Выберите валюту оплаты 💰',
                    Extra.HTML()
                    .markup((m) => {
                        return m.inlineKeyboard(currenciesList.data.map(currency => {
                            return m.callbackButton(currency, JSON.stringify([channelTlgId, currency]));
                        }))
                    }));
            }

            return ctx.wizard.next(); //next scene
            // }
        } catch (e) {
            errorHandler(e, ctx);
        }
    }

    async _getPaymentsListScence(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                let [channelTlgId, currency] = JSON.parse(ctx.update.callback_query.data);

                let activeMethodsREq = await this._userSubsProc.getActiveMethods(currency);

                if (activeMethodsREq.status != ResultStatus.OK) {
                    throw activeMethodsREq;
                }

                if (activeMethodsREq.data.length == 0) {
                    await ctx.reply(`К сожалению сейчас нет активных методов для '${currency}' 😱`);

                    ctx.session.error = new BasicResponse(ResultStatus.ERRROR_HAS_NOT_ACTIVE_PAYMENT_CURRENCY, `Not found any active payment methods trackId:[${ctx.session.track.id}]`);

                    ctx.wizard.leave(); //next scene
                } else {
                    await ctx.reply('Выберите способ оплаты 💳',
                        Extra.HTML()
                        .markup((m) => {
                            return m.inlineKeyboard(activeMethodsREq.data.map(ch => {
                                let chString = JSON.stringify([channelTlgId, currency, ch.alias]);
                                return m.callbackButton(ch.name, chString);
                            }))
                        }));
                }

                return ctx.wizard.next(); //next scene
            }
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_getPaymentsListScence


    async _makeSubscribeRequestChScene(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                let paymentRequest = JSON.parse(ctx.update.callback_query.data);

                let [
                    channelTlgId,
                    currency,
                    serviceAlias,
                ] = paymentRequest;

                console.log(channelTlgId);

                //const decline = JSON.stringify([...paymentRequest, false]);

                console.log(ctx.session.user.id);

                let subscribe = await this._userSubsProc.makeSubscribeRequestChannnel({
                    channelTlgId,
                    currency,
                    serviceAlias,
                    userId: ctx.session.user.id
                });

                if (subscribe.status != ResultStatus.OK) {
                    throw subscribe;
                }

                ctx.session.invoce = subscribe.data;

                let inputFields = ctx.session.invoce.fieldsList;

                //if not set phone skip twice
                if (inputFields.phone != null) {
                    let msg = 'Введите номер телефона 📲 Формат +7...., +380....'; //inputFields.phone.hint[ctx.session.user.languageCode];

                    await ctx.reply(msg);
                    // return ctx.wizard.next(); //next scene
                } else {

                    //looks like shits but its one way to fix it (.)
                    //inrease index
                    await ctx.wizard.next();
                    //4 its number in arryay steps 
                    await ctx.wizard.steps[ctx.wizard.cursor](ctx);
                }
            }
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_makeSubscribeRequestChScene


    async _checkPhoneInputFields(checkBefore, errorHandler, ctx) {
        try {
            const softMode = true;

            if (await checkBefore(ctx, softMode) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                let inputFields = ctx.session.invoce.fieldsList;

                let makeReciept = async (phone) => {
                    const accept = JSON.stringify([
                        true
                    ]);

                    let msgBody = [
                        ctx.session.invoce.disclamer,
                        ctx.session.invoce.descr,
                        'Проверьте и подтвердите реквизиты',
                        (phone ? `Номер телефона: <b>${phone}</b>` : null)
                    ].filter(e => e != null).join('\n');

                    await ctx.reply(msgBody,
                        Extra.HTML()
                        .markup((m) => {
                            return m.inlineKeyboard([
                                m.callbackButton('Подтверждаю ✅', accept),
                                // m.callbackButton('Отменить ❌', decline)
                            ])
                        }));

                    ctx.session.invoce.serviceFields = {
                        phone
                    };

                    ctx.wizard.next();
                }

                //if we hve some to propmt from user
                if (inputFields.phone == null) {
                    return await makeReciept();
                }

                let phone = ctx.update.message ? ctx.update.message.text : '';

                //const regex = new RegExp(/^((8|\+[1-9])[\-]?)?(\(?\d{3}\)?[\-]?)?[\d\-]{7,10}$/);
                const regex = new RegExp(/^\+[1-9]{1}[0-9]{10,11}$/);

                //const regex = new RegExp(inputFields.phone.regexp);

                if (regex.test(phone) == true) {
                    return await makeReciept(phone);
                }

                let msg = 'Номер телефона не верный. Повторите ввод';

                await ctx.reply(msg);
            } //checkBefore
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_checkPhoneInputFields


    async _finalScene(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                let [
                    accept
                ] = JSON.parse(ctx.update.callback_query.data);

                if (accept == true) {
                    await ctx.reply(`Подготавливаем платежку, еще немного времени и все готово 🧘‍`);

                    let setWait = await ctx.session.tracker.wait({
                        id: ctx.session.track.id,
                        channelId: ctx.session.invoce.channelId,
                    });

                    if (setWait.status != ResultStatus.OK) {
                        this._logger.warn(setWait);
                    } else {
                        this._logger.info(setWait);
                    }

                    //make deposit 
                    let subsrcribe = await this._userSubsProc.makeSubscribeOrder({
                        ...ctx.session.invoce,
                        trackId: ctx.session.track.id
                    });

                    if (subsrcribe.status != ResultStatus.OK) {
                        throw subsrcribe;
                    }

                    subsrcribe = subsrcribe.data;

                    //set for tracking
                    ctx.session.orderId = subsrcribe.orderInfo.id;

                    let sn = subsrcribe.orderInfo.sn;
                    let msg = '🙇';

                    if (ctx.session.invoce.isManual === true) {
                        msg = [
                            `Номер заявки для администратора SN:<b>${sn}</b>`,
                            `Если у вас появились вопросы или что то пошло не так вы можете написать в саппорт @duudee_sport_support 🆘`,
                        ].join('\n');
                    } else {
                        let recieptUrl = subsrcribe.depositInfo.action;

                        if (recieptUrl == null || recieptUrl == '') {
                            throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Recived null or empty invoce url');
                        }

                        let payBtn = Markup.urlButton('Платежка', recieptUrl);

                        await ctx.reply('Перейдите на форму оплаты по ссылке:', Markup.inlineKeyboard([payBtn]).extra());

                        msg = [
                            `Если у вас появились вопросы или что то пошло не так вы можете написать в саппорт @duudee_sport_support 🆘`,
                            `Ожидаем поддверждения платежа SN:<b>${sn}</b> 🏁 🏃`
                        ].join('\n');
                    }


                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);
                } else {
                    await ctx.reply('Вы отмнеили заявку ✋');
                }
            }
        } catch (e) {
            errorHandler(e, ctx);
        }

        return ctx.scene.leave();
    }
} //SimpleWizardClass


exports.SimpleWizardClass = SimpleWizardClass;
exports.CHANNEL_OP_BUTTONS = CHANNEL_OP_BUTTONS;