const {
    AppLoggerClass
} = __UTILS;


const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;

const WizardScene = require("telegraf/scenes/wizard");

const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const humanizeDuration = require('humanize-duration');

const {
    CHANNEL_OP_BUTTONS
} = require('../wizzards_types');

const DECLINE_KEYBOARD = Extra.HTML()
    .markup((m) => m.keyboard([
        [
            //CHANNEL_OP_BUTTONS.BACK,
            CHANNEL_OP_BUTTONS.STOP
        ],
    ]).resize());

const {
    WizardBasicFuctionsClass
} = require('./wizzard_basic_fucntions');

class ChannelsWizardClass extends WizardBasicFuctionsClass {
    constructor(name = "foo bar", core) {
        super();

        this._logger = new AppLoggerClass(`ChannelsWizardClass:${name}`, process.env.WIZARD_LOG_LEVEL);

        this.name = name;
        this.core = core;
    }

    build(name, keyboards) {
        this.keyboards = keyboards;

        const create = new WizardScene(
            this.name || name, {
                leaveHandlers: [this._leaveMiddlware.bind(this.core)]
            },
            this._getChannelListScence.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //0

            this._getPaymentsCurrenciesListScence.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //1

            this._getPaymentsListScence.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //2

            this._makeSubscribeRequestChScene.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //3

            this._checkPhoneInputFields.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)), //4

            this._finalScene.bind(this.core, this._checkBefore.bind(this), this._errorhandler.bind(this)) //5
        );

        return create;
    }

    async _getChannelListScence(checkBefore, errorHandler, ctx) {
        try {
            //\warn first scene dont use checkBefore!
            //prrepare info for tracking
            let token = ctx.session.user.getLastToken();

            ctx.session.track = {};

            let startTrack = await ctx.session.tracker.start({
                userId: ctx.session.user.id,
                buttonId: ctx.session.button.id,
                tokenId: token && token.tokenId,
                sessionId: token && token.sessionId,
                isNewUser: ctx.session.user.isNew
            });


            startTrack.isOK();

            this._logger.info(startTrack);

            ctx.session.track = {
                ...startTrack.data
            }
            //end prepare info for tracking

            //delete start messages
            if (ctx.session && ctx.session.startMessage) {
                let message = ctx.session.startMessage;

                let delRes = await this._deleteChatMessage({
                    chatId: message.chat.id,
                    messageId: message.message_id
                });

                if (delRes.status != ResultStatus.OK) {
                    this._logger.error(delRes);
                } else {
                    this._logger.info(delRes);
                }

                ctx.session.startMessage = null;
            }

            let pChannels = await this._userSubsProc.getChannels({
                isFree: false
            });

            if (
                pChannels.status == ResultStatus.ERROR_NOT_FOUND ||
                pChannels.data.length == 0
            ) {
                ctx.session.error = pChannels;

                await ctx.reply('К сожалению сейчас нет активных каналов 😱');

                await ctx.scene.leave();

                return pChannels; //next scene
            }

            pChannels.isOK();

            let channelsList = pChannels.data.slice();


            let descrMsg = await ctx.reply(`Вы выбрали ${ctx.match || ctx.session.button.name}`, DECLINE_KEYBOARD);

            this._logger.debug(`Send to telegram descrMsg:[${descrMsg.message_id}]`);

            //if channel id not set send choise

            let msg = Extra.HTML()
                .markup((m) => {
                    return m.inlineKeyboard(channelsList.map(ch => {
                        let subscriptionDuration = null;

                        if (process.env.USE_HUMANIZE_DURATION != null) {
                            subscriptionDuration = humanizeDuration(ch.subscriptionDuration * 3600 * 1000, {
                                language: 'ru',
                                units: ['mo'],
                                round: true
                            }); //get seconds from hours
                        }

                        subscriptionDuration = ch.subscriptionDuration > 0 ? subscriptionDuration : '';

                        const price = `${ch.price} ${ch.currency}`;

                        let name = [
                            `${ch.name}`,
                            price,
                            subscriptionDuration || null
                        ].filter(a => a != null).join(' | ');

                        return [m.callbackButton(name, `${ch.tlgId}`)];
                    }));
                });

            let listMsg = await ctx.reply('Выберите канал на который хотите подписаться 🤳', msg);

            ctx.session.startMessages = [listMsg];

            this._logger.debug(`Send to telegram listMsg:[${listMsg.message_id}]`);


            return ctx.wizard.next(); //next scene
        } catch (e) {
            errorHandler(e, ctx);
        }
    }

    async _getPaymentsCurrenciesListScence(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                //delete start messages
                if (Array.isArray(ctx.session.startMessages) == true) {
                    for (let message of ctx.session.startMessages) {
                        this._logger.info(`Delete message_id:[${message.message_id}] for chat_id:[${message.chat.id}]`);

                        let delRes = await this._deleteChatMessage({
                            chatId: message.chat.id,
                            messageId: message.message_id
                        });

                        if (delRes.status != ResultStatus.OK) {
                            this._logger.error(delRes);
                        }

                        this._logger.info(delRes);
                    }
                }

                ctx.session.startMessages = null;

                const channelTlgId = ctx.update.callback_query.data;

                const checkSubscribe = await this._userSubsProc.chekcSubcribeByTlg({
                    channelTlgId: channelTlgId,
                    userId: ctx.session.user.id
                });

                ctx.session.channelId = checkSubscribe.data.channel.id;

                //tracking 
                const updateTracChannelIdRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.session.__scenes.cursor,
                    channelId: ctx.session.channelId
                });

                if (updateTracChannelIdRes.isStatus(ResultStatus.OK)) {
                    this._logger.info(updateTracChannelIdRes);
                } else {
                    this._logger.warn(updateTracChannelIdRes);
                }

                if (checkSubscribe.status == ResultStatus.ERROR_ALREADY_ASSIGNED) {
                    const subscribe = checkSubscribe.data.subscribe;

                    //set erro for tracking
                    ctx.session.error = checkSubscribe;
                    ctx.session.orderId = subscribe.orderId;

                    this._logger.warn(checkSubscribe.toString());

                    if (subscribe.isGranded == true) {
                        const msg = [
                            `У вас уже оформлена подписка на этот канал.`,
                            `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                        ].join('\n');

                        await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);

                        if (process.env.SEND_INVITE_IF_ALREADY_EXIST_FOR_PAYED_WIZZARD != null) {
                            this._logger.info(`Start export invite link for channel tlgId:[${channelTlgId}]`);

                            const inviteLink = await this._exportChatInviteLink(channelTlgId);

                            inviteLink.isOK();

                            const linkBtn = Markup.urlButton('Вернуться в канал', inviteLink.data);

                            const sendUserRes2 = await ctx.reply('Для перехода в канал нажми на кнопку👇🏻', Markup.inlineKeyboard([linkBtn]).extra());

                            this._logger.info(`Send already messge id ${sendUserRes2.message_id}`);
                        } //SEND_INVITE_IF_ALREADY_EXIST_FOR_PAUED_WIZZ
                    } else {
                        let msg = [
                            `У вас начат процесс подписки на этот канал. Ждите финального стататуса.`,
                            `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`,
                        ].join('\n');

                        await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);
                    }

                    return await ctx.scene.leave();
                } //ERROR_ALREADY_ASSIGNED


                let currenciesList = await this._userSubsProc.getActiveSourceCurrencyList();
                //if it have not callback_query skip

                if (currenciesList.status != ResultStatus.OK) {
                    throw currenciesList;
                }

                if (currenciesList.data.length == 0) {
                    await ctx.reply('К сожалению сейчас нет активных способов оплаты 😱');

                    ctx.session.error = new BasicResponse(ResultStatus.ERRROR_HAS_NOT_ACTIVE_PAYMENT_CURRENCY, `Not found any active payment methods trackId:[${ctx.session.track.id}]`);

                    return ctx.wizard.leave();
                }

                await ctx.reply('Выберите валюту оплаты 💰',
                    Extra.HTML()
                    .markup((m) => {
                        return m.inlineKeyboard(currenciesList.data.map(currency => {
                            return m.callbackButton(currency, JSON.stringify([channelTlgId, currency]));
                        }))
                    }));

                return ctx.wizard.next(); //next scene
            }
        } catch (e) {
            errorHandler(e, ctx);
        }
    }

    async _getPaymentsListScence(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                let [channelTlgId, currency] = JSON.parse(ctx.update.callback_query.data);

                let activeMethods = await this._userSubsProc.getActiveMethods(currency);

                if (activeMethods.status != ResultStatus.OK) {
                    throw activeMethods;
                }

                if (activeMethods.data.length == 0) {
                    await ctx.reply(`К сожалению сейчас нет активных методов для '${currency}' 😱`);

                    ctx.session.error = new BasicResponse(ResultStatus.ERRROR_HAS_NOT_ACTIVE_PAYMENT_CURRENCY, `Not found any active payment methods trackId:[${ctx.session.track.id}]`);

                    return await ctx.scene.leave(); //next scene
                }

                await ctx.reply('Выберите способ оплаты 💳',
                    Extra.HTML()
                    .markup((m) => {
                        return m.inlineKeyboard(activeMethods.data.map(ch => {
                            let chString = JSON.stringify([channelTlgId, currency, ch.alias]);
                            return m.callbackButton(ch.name, chString);
                        }))
                    }));

                return ctx.wizard.next(); //next scene
            }
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_getPaymentsListScence

    async _makeSubscribeRequestChScene(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                let paymentRequest = JSON.parse(ctx.update.callback_query.data);

                let [
                    channelTlgId,
                    currency,
                    serviceAlias,
                ] = paymentRequest;

                this._logger.info(`User id:[${ctx.session.user.id}] channel id:[${channelTlgId}]`)

                let subscribe = await this._userSubsProc.makeSubscribeRequestChannnel({
                    channelTlgId,
                    currency,
                    serviceAlias,
                    userId: ctx.session.user.id
                });

                if (subscribe.status != ResultStatus.OK) {
                    throw subscribe;
                }

                ctx.session.invoce = subscribe.data;

                let inputFields = ctx.session.invoce.fieldsList;

                //if not set phone skip twice
                if (inputFields.phone != null) {
                    let msg = 'Введите номер телефона 📲 Формат +7...., +380....'; //inputFields.phone.hint[ctx.session.user.languageCode];

                    await ctx.reply(msg);

                    return ctx.wizard.next(); //next scene
                }

                //looks like shits but its one way to fix it (.)
                //inrease index
                await ctx.wizard.next();
                //4 its number in arryay steps 
                await ctx.wizard.steps[ctx.wizard.cursor](ctx);
            }
        } catch (e) {
            errorHandler(e, ctx);
        }
    } //_makeSubscribeRequestChScene

    async _checkPhoneInputFields(checkBefore, errorHandler, ctx) {
        const softMode = true;

        if (await checkBefore(ctx, softMode) == true) {
            //tracking 
            let updateTrackRes = await ctx.session.tracker.updateStage({
                id: ctx.session.track.id,
                stage: ctx.wizard.cursor
            });

            updateTrackRes.isOK();

            this._logger.info(updateTrackRes);
            //end tracking

            let inputFields = ctx.session.invoce.fieldsList;

            let makeReciept = async (phone) => {
                const accept = JSON.stringify([
                    true
                ]);

                let msgBody = [
                    ctx.session.invoce.disclamer,
                    ctx.session.invoce.descr,
                    'Проверьте и подтвердите реквизиты',
                    (phone ? `Номер телефона: <b>${phone}</b>` : null)
                ].filter(e => e != null).join('\n');

                await ctx.reply(msgBody,
                    Extra.HTML()
                    .markup((m) => {
                        return m.inlineKeyboard([
                            m.callbackButton('Подтверждаю ✅', accept),
                            // m.callbackButton('Отменить ❌', decline)
                        ])
                    }));

                ctx.session.invoce.serviceFields = {
                    phone
                };

                ctx.wizard.next();
            }

            //if we hve some to propmt from user
            if (inputFields.phone == null) {
                return await makeReciept();
            }

            let phone = ctx.update.message ? ctx.update.message.text : '';

            //const regex = new RegExp(/^((8|\+[1-9])[\-]?)?(\(?\d{3}\)?[\-]?)?[\d\-]{7,10}$/);
            const regex = new RegExp(/^\+[1-9]{1}[0-9]{10,11}$/);

            //const regex = new RegExp(inputFields.phone.regexp);

            if (regex.test(phone) == true) {
                return await makeReciept(phone);
            }

            let msg = 'Номер телефона не верный. Повторите ввод';

            await ctx.reply(msg);
        }
    } //_checkPhoneInputFields

    async _finalScene(checkBefore, errorHandler, ctx) {
        try {
            if (await checkBefore(ctx) == true) {
                //tracking 
                let updateTrackRes = await ctx.session.tracker.updateStage({
                    id: ctx.session.track.id,
                    stage: ctx.wizard.cursor
                });

                updateTrackRes.isOK();

                this._logger.info(updateTrackRes);
                //end tracking

                let [
                    accept
                ] = JSON.parse(ctx.update.callback_query.data);

                if (accept == true) {
                    let msg = '🙇';

                    let setWait = await ctx.session.tracker.wait({
                        id: ctx.session.track.id,
                        channelId: ctx.session.invoce.channelId,
                    });

                    if (setWait.status != ResultStatus.OK) {
                        this._logger.warn(setWait);
                    } else {
                        this._logger.info(setWait);
                    }

                    //make order 
                    let orderRequest = await this._userSubsProc.makeSubscribeOrder({
                        ...ctx.session.invoce,
                        trackId: ctx.session.track.id
                    });

                    if (orderRequest.status != ResultStatus.OK) {
                        throw orderRequest;
                    }

                    orderRequest = orderRequest.data;

                    //set for tracking
                    ctx.session.orderId = orderRequest.orderInfo.id;

                    let sn = orderRequest.orderInfo.sn;

                    if (ctx.session.invoce.isManual === true) {
                        msg = [
                            `Номер заявки для администратора SN:<b>${sn}</b>`,
                            `Если у вас появились вопросы или что то пошло не так вы можете написать в саппорт @duudee_sport_support 🆘`,
                        ].join('\n');
                    } else {
                        await ctx.reply(`Подготавливаем платежку, еще немного времени и все готово 🧘‍`);

                        let recieptUrl = orderRequest.depositInfo.action;

                        if (recieptUrl == null || recieptUrl == '') {
                            throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Recived null or empty invoce url');
                        }

                        let payBtn = Markup.urlButton('Платежка', recieptUrl);

                        await ctx.reply('Перейдите на форму оплаты по ссылке:', Markup.inlineKeyboard([payBtn]).extra());

                        msg = [
                            `Если у вас появились вопросы или что то пошло не так вы можете написать в саппорт @duudee_sport_support 🆘`,
                            `Ожидаем поддверждения платежа SN:<b>${sn}</b> 🏁 🏃`
                        ].join('\n');
                    }

                    await ctx.reply(msg, this.keyboards.MAIN_KEYBOARD);


                } else {
                    await ctx.reply('Вы отмнеили заявку ✋');
                }
            }
        } catch (e) {
            errorHandler(e, ctx);
        }

        return ctx.scene.leave();
    }
} //ChannelsWizardClass


exports.ChannelsWizardClass = ChannelsWizardClass;
exports.CHANNEL_OP_BUTTONS = CHANNEL_OP_BUTTONS;