'use strict';

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;


const {
    CHANNEL_OP_BUTTONS
} = require('../wizzards_types');

const {
    TelegramMethodsClass
} = require('../../assistant_processor/telegram_methods_class');

class WizardBasicFuctionsClass {
    async _errorhandler(e, ctx) {
        const error = TelegramMethodsClass.reduceTelegramErrorToBasic(e);

        ctx.session.error = error;

        let errMSg = [
            'Упс у нас произошла ошибка 🙇',
            'Попробуйте еще раз',
            'В ближайшее время мы уcтраним проблему'
        ].join('\n');

        await ctx.reply(errMSg);

        this._logger.error(error);

        await ctx.scene.leave();
    }

    async _checkBefore(ctx, softMode = false) {
        if (ctx.update.callback_query == null) {
            //maybe it back or stop
            let cmd = ctx.message.text;

            switch (cmd) {
                case CHANNEL_OP_BUTTONS.STOP: {
                    ctx.session.error = new BasicResponse(ResultStatus.ERROR_RECIVE_STOP_SIGNAL, `Recive stop requset user:[${ctx.session.user.id}] stage ??`);

                    this._logger.warn(ctx.session.error);

                    await ctx.scene.leave();
                    return false;
                }

                case CHANNEL_OP_BUTTONS.BACK: {
                    this._logger.warn('Recive back requset');

                    await ctx.wizard.back();
                    return false
                }

                default:
                    let error = new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Recive requset without callback_query user:[${ctx.session.user.id}] stage ??`);

                    this._logger.warn(error);

                    if (softMode == false) {
                        ctx.session.error = error;

                        await ctx.scene.leave();

                        return false;
                    }
                    return true;
            } //switch
        } else {
            return true;
        }
    } //checkBefore

    async _leaveMiddlware(ctx, next) {
        if (
            ctx.message &&
            ctx.message.text == CHANNEL_OP_BUTTONS.STOP
        ) {
            await ctx.reply('Процесс прерван ❌', this.keyboards.MAIN_KEYBOARD);
        } else {
            await ctx.reply('Процесс подписки закончен', this.keyboards.MAIN_KEYBOARD);
        }

        if (
            Array.isArray(ctx.session.startMessages) == true &&
            ctx.session.startMessages.length > 0
        ) {
            for (let message of ctx.session.startMessages) {
                this._logger.info(`Delete message_id:[${message.message_id}] for chat_id:[${message.chat.id}]`);

                let delRes = await this._deleteChatMessage({
                    chatId: message.chat.id,
                    messageId: message.message_id
                });

                if (delRes.status != ResultStatus.OK) {
                    this._logger.error(delRes);
                }

                this._logger.info(delRes);
            }

            ctx.session.startMessages = null;
        }

        if (ctx.session.button != null) {
            this._logger.info(`Leave wizzard for button name:[${ctx.session.button.name}] handler:[${ctx.session.button.handler}]`)
        } else {
            this._logger.info(`Leave wizzard`);
        }


        if (ctx.session.track != null && ctx.session.error != null) {
            //tracking 
            let doneTrackRes = await ctx.session.tracker.done({
                id: ctx.session.track.id,
                error: ctx.session.error != null ? ctx.session.error.status : null,
                channelId: ctx.session.channelId,
            });

            doneTrackRes.isOK();

            this._logger.info(doneTrackRes);
        }

        //end tracking 

        //clear some session variables
        ctx.session.channelId = null;
        ctx.session.orderId = null;
        ctx.session.button = null;
        ctx.session.error = null;

        await next();
    }

    //_exportChatInviteLink
    async _exportChatInviteLink(channelTlgId) {
        try {
            const chatInfo = await this._getChatInfo(`-100${channelTlgId}`);

            chatInfo.isOK();

            if (chatInfo.invite_link == null) {
                this._logger.warn(`Recieve null invite_link for channel:[${channelTlgId}]`);

                const inviteLink = await this.telegraf.telegram.exportChatInviteLink(channelTlgId);

                inviteLink.isOK();

                return inviteLink;
            }

            const inviteLink = new BasicResponse(ResultStatus.OK, `Export invite link:[${chatInfo.invite_link}] from chat:[${channelTlgId}] info done successfully`, chatInfo.invite_link);

            this._logger.debug(inviteLink);

            return inviteLink;
        } catch (e) {
            const error = new makeBasicResponseFromError(e);

            this._logger.error(error);

            return error;
        }
    }
}

exports.WizardBasicFuctionsClass = WizardBasicFuctionsClass;