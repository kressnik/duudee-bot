'use strict';

//cores modules
const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;

const {
    AppLoggerClass
} = __UTILS;

const {
    waterfall,
    queue
} = require('async');

const {
    TelegramRpcClentClass,
    TelegramRpcClientStatusEnum
} = __UTILS;

const {
    ChannelProcessorClass
} = require('../channels_processor_class');

const EventEmitter = require('events');
const Many = require('extends-classes');

const WATCHER_INTERVAL_DEFAULT_MS = 5000;
const WATCHER_STATE = Object.freeze({
    ACTIVE: 1,
    DISACTIVE: 0
});

// channelAdminLogEventActionChangeTitle Channel / supergroup title was changed
// channelAdminLogEventActionChangeAbout The description was changed
// channelAdminLogEventActionChangeUsername Channel / supergroup username was changed
// channelAdminLogEventActionChangePhoto The channel / supergroup 's picture was changed
// channelAdminLogEventActionToggleInvites Invites were enabled / disabled
// channelAdminLogEventActionToggleSignatures Channel signatures were enabled / disabled
// channelAdminLogEventActionUpdatePinned A message was pinned
// channelAdminLogEventActionEditMessage A message was edited
// channelAdminLogEventActionDeleteMessage A message was deleted
// channelAdminLogEventActionParticipantJoin A user has joined the group( in the case ofbig groups, info of the user that has joined isn 't shown)
// channelAdminLogEventActionParticipantLeave A user left the channel / supergroup( in thecase ofbig groups, info of the user that has joined isn 't shown)
// channelAdminLogEventActionParticipantInvite A user was invited to the group channelAdminLogEventActionParticipantToggleBan The banned rights of a user were changed channelAdminLogEventActionParticipantToggleAdmin The admin rights of a user were changed channelAdminLogEventActionChangeStickerSet The supergroup 's stickerset was changed
// channelAdminLogEventActionTogglePreHistoryHidden The hidden prehistory setting was changed channelAdminLogEventActionDefaultBannedRights Thedefault banned rights were modified channelAdminLogEventActionStopPoll A poll was stopped channelAdminLogEventActionChangeLinkedChat The linked chat was changed channelAdminLogEventActionChangeLocation The geogroup location was changed channelAdminLogEventActionToggleSlowMode Slow mode settingfor supergroups was changed
// ChannelAdminLogEventActionParticipantToggleBan

class ChannelWatcherPropsClass {
    constructor() {
        this._values = {
            client: {
                host: null,
                port: null,
                path: null,
                useMocks: null,
                mocksVersion: null,
                version: null,
            },
            state: WATCHER_STATE.DISACTIVE,
            interval: WATCHER_INTERVAL_DEFAULT_MS
        };

    }

    get state() {
        return this._values.state;
    }

    get interval() {
        return this._values.interval;
    }

    set interval(interval) {
        return this._values.interval = interval;
    }

    get values() {
        return this._values;
    }

    get client() {
        return this._values.client;
    }

    apply(props = {}) {
        const {
            client,
            state,
            interval
        } = props;

        if (client != null) {
            for (const key in client) {
                if (this._values.client.hasOwnProperty(key) == true) {
                    this._values.client[key] = client[key];
                }
            }
        }

        console.log(this._values.client)

        if (state != null) {
            this._values.state = state;
        }

        if (interval != null) {
            this._values.interval = interval;
        }

        return this._values;
    }
}

class ChannelPublicMethods {
    constructor() {}
    /**
     * Get auth status
     * @param void
     */
    async authGetStatus() {
        try {
            this._logger.info('Get auth status');

            const request = await this._client.authGetStatus();

            request.isOK();

            const {
                status,
                phone
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Get auth status return status:[${status}], phone:[${phone}]`, {
                status,
                phone
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`Get auth status return: ${error}`);

            return error;
        }
    } //authGetStatus

    /**
     * Send and seup api information
     * @param {*} apiId
     * @param {*} apiHash
     */
    async userSendApiInfo(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'args is null, should be an object');
            }

            const {
                apiId,
                apiHash
            } = args;


            this._logger.info(`Send api info auth apiId:[${apiId}], apiHash:[${apiHash}]`);

            const request = await this._client.userSendApiInfo({
                apiId,
                apiHash
            });

            request.isOK();

            /** {
                "status": "DONE",
                "phone": "+380502947272"
            } */
            const {
                status
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Send api info auth apiId:[${apiId}], apiHash:[${apiHash}] return status:[${status}]`, {
                status
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`Get userSendApiInfo return: ${error}`);

            return error;
        }
    } //userSendApiInfo

    /**
     * Start initiate auth process.
     * @param {*} phone phone for recived code
     */
    async userSendStartAuth(phone) {
        try {
            if (phone == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'phone is null, should be an strting');
            }

            if (typeof phone != 'string') {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'phone should be an strting', typeof phone);
            }

            this._logger.info(`Send start auth for phone:[${phone}]`);

            const request = await this._client.userSendStartAuth(phone);

            request.isOK();

            const {
                status
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Send start auth for phone:[${phone}] return status:[${status}]`, {
                status
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`userSendStartAuth return: ${error}`);

            return error;
        }
    } //userSendStartAuth

    /**
     * Send auth code for auth process.
     * @param {*} code code recived after auth
     */
    async userSendAuthCode(code) {
        try {
            if (code == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'code is null, should be an strting');
            }

            if (typeof code != 'string') {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'code should be an strting');
            }

            this._logger.info(`Send auth code:[${code}]`);

            const request = await this._client.userSendAuthCode(code);

            request.isOK();

            const {
                status
            } = request.data;

            const rsp = new BasicResponse(ResultStatus.OK, `Send auth code:[${code}] returned status:[${status}]`, {
                status
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`userSendAuthCode catch error: ${error}`);

            return error;
        }
    } //userSendAuthCode
    // filter,
    //  filter,
    // dateRange,
    // limit,
    // offset
    // async getAllByFilterLimitOffset(args) {
    //     try {
    //         cosnt requset  =await this._db.models.tele
    //     } catch (e) {
    //         const error = new BasicResponseByError(e);

    //         this._logger.error(`userSendAuthCode catch error: ${error}`);

    //         return error;
    //     }
    // }
}

class ChannelWatcherProcessorClass extends ChannelPublicMethods {
    constructor() {
        super();
    }

    _reduceTelegramLogToModel({
        channelId = null,
        log = {}
    }) {
        const {
            type,
            action
        } = log;

        this._logger.info(`Reduce telegram log type:[${type}] action type:[${action.type}]`);

        switch (action.type) {
            case 'ChannelAdminLogEventActionParticipantToggleBan':
                return {
                    channelId,
                    logId: log.id,
                        userId: log.user_id,
                        kickedBy: log.action.new_participant.kicked_by,
                        date: log.date,
                        type: action.type,
                        json: log,
                }

                case 'ChannelAdminLogEventActionChangeTitle':
                case 'ChannelAdminLogEventActionChangeAbout':
                case 'ChannelAdminLogEventActionChangeUsername':
                case 'ChannelAdminLogEventActionChangePhoto':
                case 'ChannelAdminLogEventActionChangePhoto':
                case 'ChannelAdminLogEventActionToggleInvites':
                case 'ChannelAdminLogEventActionToggleSignatures':
                case 'ChannelAdminLogEventActionUpdatePinned':
                case 'ChannelAdminLogEventActionEditMessage':
                case 'ChannelAdminLogEventActionDeleteMessage':
                case 'ChannelAdminLogEventActionParticipantJoin':
                case 'ChannelAdminLogEventActionParticipantLeave':
                case 'ChannelAdminLogEventActionParticipantInvite':
                case 'ChannelAdminLogEventActionTogglePreHistoryHidden':
                    return {
                        channelId,
                        logId: log.id,
                            userId: log.user_id,
                            kickedBy: null,
                            date: log.date,
                            type: action.type,
                            json: log,
                    };

                default:
                    this._logger.warn(`Unknown teleggram logg type:[${action.type}]`);

                    return null;
        }
    }

    async _fetchChannelLastIdAdminLogFlowFucn(args) {
        try {
            const {
                channelId,
                channelTlgId
            } = args;

            if (channelTlgId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelTlgId should be provided');
            }

            if (channelId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be provided');
            }

            if (isNaN(parseInt(channelId))) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be an integer');
            }

            this._logger.info(`Fetch batch of telegram logs for the channel:[${channelId}]`);

            const lastLogIdRes = await this._db.models.TelegramAdminsLogs.getLastChannelLogId(channelId);

            lastLogIdRes.isOK();

            this._logger.info(lastLogIdRes);

            return {
                channelId,
                channelTlgId,
                lastLogId: lastLogIdRes.data
            };
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //fetchChannelAdminLLogsFlowFucn
    /**
     * Fetch admin logs by MtProto channelsGetAdminLog method
     * @param {*} channelId id of channel
     * @param {*} lastLogId id of last log for fetch next 
     */
    async _fetchChannelAdminLogsFlowFucn(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'args should be an object');
            }

            let {
                channelId,
                channelTlgId,
                lastLogId = 0,
            } = args;

            if (channelTlgId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelTlgId should be provided');
            }

            if (channelId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be provided');
            }

            if (isNaN(parseInt(channelId))) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be an integer');
            }

            if (lastLogId == null || isNaN(lastLogId)) {
                lastLogId = 0;
            }

            this._logger.info(`Fetch batch of telegram logs for the channel:[${channelId}]  channelTlgId:[${channelTlgId}] from lastLogId:[${lastLogId}]`);

            const adminslogs = await this._client.channelsGetAdminLog({
                channel: channelTlgId,
                minId: lastLogId
            });

            adminslogs.isOK();

            this._logger.info(adminslogs);

            return {
                channelId,
                channelTlgId,
                logs: adminslogs.data
            };
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            throw error;
        }
    } //fetchChannelAdminLLogsFlowFucn

    async _reduceAdminLogsToModelFlowFunc(args) {
        try {
            const {
                channelId,
                logs = []
            } = args;

            if (channelId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be provided');
            }

            if (isNaN(parseInt(channelId))) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be an integer');
            }
            this._logger.info(`Reduce batch of logs:[${logs.length}] ffor the channel:[${channelId}]`);

            const logsReduced = logs.map(log => this._reduceTelegramLogToModel({
                channelId,
                log
            })).filter(log => log != null);;

            this._logger.info(`=>Reduce batch of logs:[${logs.length}] ffor the channel:[${channelId}]`);

            return {
                channelId,
                logs: logsReduced
            }
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            throw error;
        }
    } //reduceAdminLogsToModel

    async _savingAdminLogsToDbFlowFunc(args = {}) {
        try {
            const {
                channelId,
                logs = []
            } = args;

            if (isNaN(parseInt(channelId))) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be an integer');
            }

            this._logger.info(`Saving batch of logs:[${logs.length}] for the channel:[${channelId}]`);

            const saveBatchRes = await this._db.models.TelegramAdminsLogs.createOrUpdateBatch(logs);

            saveBatchRes.isOK();

            this._logger.info(saveBatchRes);

            return {
                channelId,
                logs: saveBatchRes.data.logs,
                saved: saveBatchRes.data.saved
            };
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            throw error;
        }
    } //reduceAdminLogsToModel

    // async _publishAdminLogEventFlowFunc(args) {
    //     try {
    //         const {
    //             cahnnelId,
    //             logs = []
    //         } = args;

    //         this._emmiter.emit(TELEGRAM_WATCHER_EVENTS.LOGS);
    //     } catch (e) {
    //         const error = new BasicResponseByError(e);

    //         this._logger.error(error.toString());

    //         return error;
    //     }
    // } //reduceAdminLogsToModel

    _watcherFlow(args) {
        return new Promise(async (resolve, reject) => {
            try {
                waterfall([
                    this._fetchChannelLastIdAdminLogFlowFucn.bind(this, args),
                    this._fetchChannelAdminLogsFlowFucn.bind(this),
                    this._reduceAdminLogsToModelFlowFunc.bind(this),
                    this._savingAdminLogsToDbFlowFunc.bind(this),
                    //this._publishAdminLogEventFlowFunc.bind(this)
                ], (error, result) => {
                    try {
                        if (error) {
                            throw error;
                        }

                        const rsp = new BasicResponse(ResultStatus.OK, 'Channel watcher done successfully.', result);

                        this._logger.info(rsp);

                        resolve(rsp);
                    } catch (e) {
                        const error = new BasicResponseByError(e);

                        this._logger.error(error);

                        resolve(error);
                    }
                });
            } catch (e) {
                const error = new BasicResponseByError(e);

                this._logger.error(error);

                resolve(error);
            }
        })
    }


    async _watcherHandler() {
        try {
            this._logger.info('Start new watching cycle');

            const channelsList = await this._channelProcessor.getActiveList({});

            channelsList.isOK();

            this._logger.info(channelsList);

            for (const channel of channelsList.data) {
                this._logger.info(`Start wachning chanel name:[${channel.name}] tlgId:[${channel.tlgId}]`);

                const watchRes = await this._watcherFlow({
                    channelId: channel.id,
                    channelTlgId: channel.tlgId
                });

                if (watchRes.isStatus(ResultStatus.OK)) {
                    this._logger.info(watchRes);
                } else {
                    this._logger.warn(watchRes);
                }
            }

            this._startNextCycle();

            const rsp = new BasicResponse(ResultStatus.OK, 'Watcher cycle done successfully');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            this._startNextCycle();

            return error;
        }
    }
}

class ChannelWatcherClass extends ChannelWatcherProcessorClass {
    constructor() {
        super();

        this._watchertimer = null;

        this._logger = new AppLoggerClass('ChannelWatcherClass', process.env.CHANNEL_WATCHER_CLASS_LOG_LELVEL);

        this.emttier = new EventEmitter();
        this._db = null;
        this._props = new ChannelWatcherPropsClass();

        this._client = new TelegramRpcClentClass();
        this._channelProcessor = new ChannelProcessorClass();
    }

    __call(method, args) {
        console.error(`'${method}()' is missing!`, args);
    }

    async init(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'args should be an object');
            }

            const {
                db,
                props
            } = args;

            if (db == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'db conection should be provided');
            }

            if (props == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'props should be provided');
            }

            this._db = db;

            this._props.apply(props);

            const initFlow = await this._initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            const rsp = new BasicResponse(ResultStatus.OK, 'Init Channel Watcher done successfully.');

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    } //init

    _initFlow() {
        return new Promise((resolve) => {
            waterfall([
                this._initChannelListFlowFunc.bind(this),
                this._initTelegramRpcClientFlowFunc.bind(this),
                this._initWatcherFlowFunc.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    const rsp = new BasicResponse(ResultStatus.OK, 'Init channel watcher done successfully');

                    this._logger.info(rsp);

                    return resolve(rsp);
                } catch (e) {
                    const error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    } //initFlow

    async _initChannelListFlowFunc() {
        try {
            this._logger.info('Start init channel list');
            //init models

            let resInit = await this._channelProcessor.init(this._db);

            if (resInit.status != ResultStatus.OK) {
                throw resInit;
            }

            this._logger.info(`${resInit.toString()}`);

            return resInit;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initChannelListFlowFunc

    async _initTelegramRpcClientFlowFunc() {
        try {
            this._logger.info('Start Init Telegram RPC service Client');

            const initRes = await this._client.init(this._props.client);

            if (initRes.status != ResultStatus.OK) {
                throw initRes;
            }

            this._logger.info(initRes.toString());

            return initRes;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initKunaClientFlowFunc

    _startNextCycle() {
        try {
            if (this._props.state == WATCHER_STATE.ACTIVE) {
                this._logger.info(`Start timer for channel watcher with timeout:[${this._props.interval}`);

                this._watchertimer = setTimeout(this._watcherHandler.bind(this), this._props.interval)
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Init and Start with state:[${this._props.state}] cycle.`);

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }

    }

    async _initWatcherFlowFunc() {
        try {
            this._startNextCycle();

            const rsp = new BasicResponse(ResultStatus.OK, 'Init watcher cylcle done successfully');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initWatcherFlowFunc

} //ChannelWatcherClass

exports.ChannelWatcherClass = ChannelWatcherClass;
exports.ChannelWatcherProcessorClass = ChannelWatcherProcessorClass;
exports.ChannelWatcherPropsClass = ChannelWatcherPropsClass;