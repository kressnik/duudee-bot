//cores modules
const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');

const {
    AppLoggerClass
} = __UTILS;

const {
    waterfall,
    queue
} = require('async');

const {
    KunaApiClientV3
} = require('../utils/kuna_api/kuna_api_clinet_v3');

const {
    DepositWatcherClass,
    EVENTS_LIST,
    DEPOSIT_STAUTUS
} = require('./deposit_watcher_class');

const PUBLIC_KEYS = Object.freeze({
    DEPOSIT_SDK_UAH_PUBLIC_KEY: process.env.DEPOSIT_SDK_UAH_PUBLIC_KEY,
    DEPOSIT_SDK_USD_PUBLIC_KEY: process.env.DEPOSIT_SDK_USD_PUBLIC_KEY,
    DEPOSIT_SDK_RUB_PUBLIC_KEY: process.env.DEPOSIT_SDK_RUB_PUBLIC_KEY,
    DEPOSIT_SDK_UAH_WORLDWIDE_PUBLIC_KEY: process.env.DEPOSIT_SDK_UAH_WORLDWIDE_PUBLIC_KEY
});

const CURRENCY_LIST = Object.freeze({
    UAH: 'UAH',
    RUB: 'RUB'
});

const {
    URL
} = require('url');

const Browser = require('zombie');
const uinqueid = require('uniqid');

class DepositUAHInputListClass {
    constructor() {
        this._logger = new AppLoggerClass('DepositUAHInputListClass');

        this._depositInputList = {
            deposit_sdk_uah: {
                name: 'deposit_sdk_uah',
                publicKey: PUBLIC_KEYS.DEPOSIT_SDK_UAH_PUBLIC_KEY,
                services: {
                    payment_card_uah_hpp: {
                        alias: 'p_card_uah',
                        name: 'VISA MasterCard Ukraine',
                        code: "payment_card_uah_hpp",
                        enabled: true,
                        //terminal
                        terminalFee: 0.015,
                        terminalStaticFee: 5,
                        decimal: 2,
                        //currency of teterminal
                        fields: [],
                        terminalCurrency: CURRENCY_LIST.UAH,
                        currency: [CURRENCY_LIST.UAH],
                        recieptCurrency: CURRENCY_LIST.UAH,
                        method: "payment_card",
                        switchMethod: 'uah',
                        disclamer: function (feeAtOwnExpense) {
                            let statcFeeDescr = this.terminalStaticFee > 0 ? ` + ${this.terminalStaticFee}` : '';
                            let fee = Number((this.terminalFee * 100).toFixed(this.decimal));
                            return [
                                `Оплата будет произведена через ${this.name}.`,
                                feeAtOwnExpense == false ? `Комисия составит ${fee}% ${statcFeeDescr} ${this.currency}` : null,
                                `При покупке используйте только украинские 🇺🇦 карты Visa и MasterCard с валютой гривна, иначе платеж вернется обратно.`
                            ].filter(e => e != null).join('\n');
                        },
                        consist: function (cur) {
                            return this.currency.some(lcur => lcur == cur);
                        }
                    },
                    qiwi_rub_hpp: {
                        enabled: false,
                        terminalFee: 0.03,
                        terminalStaticFee: 0,
                        method: "qiwi",
                        decimal: 2,
                        alias: "qiwi_rub_uah",
                        code: "qiwi_rub_hpp",
                        name: 'QIWI',
                        currency: [CURRENCY_LIST.RUB],
                        terminalCurrency: CURRENCY_LIST.UAH,
                        recieptCurrency: CURRENCY_LIST.RUB,
                        flow: "hpp",
                        switchMethod: 'uah',
                        fields: [{
                            key: "phone",
                            type: "string",
                            label: {
                                "ru": "Номер телефона",
                                "en": "Phone number",
                                "uk": "Номер телефону"
                            },
                            hint: {
                                "ru": "Введите номер телефона",
                                "en": "Enter phone number",
                                "uk": "Введіть номер телефону"
                            },
                            "regexp": "^\\\\+(9[976]\\\\d|8[987530]\\\\d|6[987]\\\\d|5[90]\\\\d|42\\\\d|3[875]\\\\d|2[98654321]\\\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\\\d{9,14}$",
                            "required": true,
                            "position": 0
                        }],
                        disclamer: function (feeAtOwnExpense) {
                            let statcFeeDescr = this.terminalStaticFee > 0 ? ` + ${this.terminalStaticFee}` : '';
                            let fee = Number((this.terminalFee * 100).toFixed(this.decimal));

                            return [
                                `Оплата будет произведена через электронный кошелек QIWI.`,
                                feeAtOwnExpense == false ? `Комисия составит ${fee}% ${statcFeeDescr} ${this.currency}` : null
                            ].filter(e => e != null).join('\n');
                        },
                        consist: function (cur) {
                            return this.currency.some(lcur => lcur == cur);
                        }
                    },
                }
            },
            deposit_sdk_uah_worldwide: {
                name: 'deposit_sdk_uah_worldwide',
                publicKey: PUBLIC_KEYS.DEPOSIT_SDK_UAH_WORLDWIDE_PUBLIC_KEY,
                services: {
                    payment_card_uah_hpp: {
                        enabled: false,
                        terminalStaticFee: 0,
                        terminalFee: 0.035,
                        terminalCurrency: CURRENCY_LIST.UAH,
                        alias: "p_card_uah_world",
                        code: "payment_card_uah_hpp",
                        name: 'VISA/MC Worldwide',
                        method: "payment_card",
                        decimal: 2,
                        currency: [CURRENCY_LIST.RUB],
                        switchMethod: 'worldwide',
                        getCustomer: function (referenceId) {
                            return {
                                reference_id: `${referenceId}`,
                                //email: `${referenceId}@dduudeesport.com`
                            }
                        },
                        disclamer: function (feeAtOwnExpense) {
                            let statcFeeDescr = this.terminalStaticFee > 0 ? ` + ${this.terminalStaticFee}` : '';
                            let fee = Number((this.terminalFee * 100).toFixed(this.decimal));

                            return [
                                `Оплата будет произведена через Международные карты 🌏 ${this.name}.`,
                                feeAtOwnExpense == false ? `Комисия составит ${fee}% ${statcFeeDescr} ${this.currency}` : null,
                                `Вы можете совершить покупку с помощью любой карты, выпущенной VISA / Mastercard, за исключением карт, выпущенных в США, Англии, Германии, Японии, стран из санкционного списка FATF, OFAC и ЕС`,
                                `Вы приобретаете средства пополнения XPAY в эквиваленте 1к1, которые потом будут использованы для покупки подписки`
                            ].filter(e => e != null).join('\n');
                        },
                        consist: function (cur) {
                            return this.currency.some(lcur => lcur == cur);
                        }
                    }
                }
            },
            deposit_sdk_rub: {
                name: 'deposit_sdk_rub',
                publicKey: PUBLIC_KEYS.DEPOSIT_SDK_RUB_PUBLIC_KEY,
                services: {
                    qiwi_rub_hpp: {
                        enabled: true,
                        code: 'qiwi_rub_hpp',
                        method: 'qiwi',
                        decimal: 2,
                        alias: "qiwi_rub_rub",
                        code: "qiwi_rub_hpp",
                        name: 'QIWI',
                        flow: 'hpp',
                        currency: [CURRENCY_LIST.RUB],
                        terminalCurrency: CURRENCY_LIST.RUB,
                        terminalFee: 0.015,
                        recieptCurrency: CURRENCY_LIST.RUB,
                        switchMethod: 'rub',
                        fields: [{
                            key: "phone",
                            type: "string",
                            label: {
                                "ru": "Номер телефона",
                                "en": "Phone number",
                                "uk": "Номер телефону"
                            },
                            hint: {
                                "ru": "Введите номер телефона",
                                "en": "Enter phone number",
                                "uk": "Введіть номер телефону"
                            },
                            "regexp": "^\\\\+(9[976]\\\\d|8[987530]\\\\d|6[987]\\\\d|5[90]\\\\d|42\\\\d|3[875]\\\\d|2[98654321]\\\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\\\d{9,14}$",
                            "required": true,
                            "position": 0
                        }],
                        disclamer: function (feeAtOwnExpense) {
                            let statcFeeDescr = this.terminalStaticFee > 0 ? ` + ${this.terminalStaticFee}` : '';
                            let fee = Number((this.terminalFee * 100).toFixed(this.decimal));

                            return [
                                `Оплата будет произведена через электронный кошелек QIWI.`,
                                feeAtOwnExpense == false ? `Комисия составит ${fee}% ${statcFeeDescr} ${this.currency}` : null
                            ].filter(e => e != null).join('\n');
                        },
                        consist: function (cur) {
                            return this.currency.some(lcur => lcur == cur);
                        }
                    }
                }
            },
            manualy_deposit: {
                name: 'manualy_deposit',
                publicKey: null,
                services: {
                    manualy_deposit_rub_hpp: {
                        enabled: true,
                        isManual: true,
                        terminalStaticFee: 0,
                        terminalFee: 0,
                        terminalCurrency: CURRENCY_LIST.RUB,
                        currency: [CURRENCY_LIST.RUB],
                        alias: "manualy_deposit_rub_alias",
                        code: "manualy_deposit_rub_hpp",
                        name: 'Ручной',
                        method: "manualy_deposit_rub_method",
                        decimal: 2,
                        switchMethod: null,
                        getCustomer: function (referenceId) {
                            return {
                                reference_id: `${referenceId}`,
                                //email: `${referenceId}@dduudeesport.com`
                            }
                        },
                        disclamer: function (feeAtOwnExpense) {
                            let statcFeeDescr = this.terminalStaticFee > 0 ? ` + ${this.terminalStaticFee}` : '';
                            let fee = Number((this.terminalFee * 100).toFixed(this.decimal));

                            return [
                                `Оплата будет произведена через Администратора карты.`,
                                `После сосздания заявки с вами свяжеться Администратор и уточнит следущие действия`
                            ].filter(e => e != null).join('\n');
                        },
                        consist: function (cur) {
                            return this.currency.some(lcur => lcur == cur);
                        }
                    }
                }
            }
        }
    }

    async init(db) {
        try {
            this._db = db;

            let listRequest = await this._db.models.KunaDepoChannel.getActiveMethods();

            listRequest.isOK();

            listRequest = listRequest.data;

            for (let methodName in this._depositInputList) {
                let method = this._depositInputList[methodName];

                this._logger.info(`check method:[${methodName}]`);

                if (listRequest[methodName] != null) {
                    for (let serviceName in method.services) {
                        this._logger.info(`have to apply settings for service: ${serviceName}`);

                        let service = method.services[serviceName];

                        method.services[serviceName] = {
                            ...service,
                            ...listRequest[methodName].settings.services[serviceName]
                        }
                    } //services

                    method = {
                        ...method,
                        ...listRequest[methodName]
                    }
                } //
            } //for _depositInputList

            let rsp = new BasicResponse(ResultStatus.OK, 'Init methods list done successfully', {});

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    getMethodsBySourceCurrency(currency) {
        let out = new Set();

        for (let input of Object.values(this._depositInputList)) {
            this._logger.debug(`method name:[${input.name}]`);

            for (let serv of Object.values(input.services)) {
                this._logger.debug(`alias:[${serv.alias}], state:[${serv.enabled}]`);

                if (
                    serv.enabled == true &&
                    serv.consist(currency)
                ) {
                    out.add(serv);
                }
            }
        }

        return Array.from(out);
    }

    getService(alias) {
        for (let input of Object.values(this._depositInputList)) {
            for (let serv of Object.values(input.services)) {
                if (serv.alias == alias) {
                    return {
                        ...serv,
                        publicKey: input.publicKey
                    };
                }
            }
        }
    }

    getSourceCurrencyList() {
        //crutch
        return [CURRENCY_LIST.RUB, CURRENCY_LIST.UAH];
    }

    getActiveSourceCurrencyList() {
        let out = new Set();

        for (let input of Object.values(this._depositInputList)) {
            this._logger.debug(`method name:[${input.name}]`);

            for (let serv of Object.values(input.services)) {
                this._logger.debug(`alias:[${serv.alias}], state:[${serv.enabled}]`);

                if (
                    serv.enabled == true
                ) {
                    for (let currecny of serv.currency) {
                        out.add(currecny);
                    }
                }
            }
        }

        return Array.from(out);
    }
} //depositInputListClass

class PaymentSystemFucntionClass {
    constructor() {}

    async getPaymentCurrencies() {
        try {
            let list = this._uahDepositInputList.getSourceCurrencyList();

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payment Currnecies list', list);


            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getActiveSourceCurrencyList() {
        try {
            let list = this._uahDepositInputList.getActiveSourceCurrencyList();

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Active Get Payment Currnecies list', list);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getSourceCurrencyList() {
        try {
            let list = this._uahDepositInputList.getSourceCurrencyList();

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payments Methods list', list);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    getService(servCode) {
        try {
            let service = this._uahDepositInputList.getService(servCode);

            if (service == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, 'Service not found');
            }

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payments Methods list', service);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getActiveMethods(curency) {
        try {
            // let getPaymentPrerequest = await this._kunaClient.getPaymentPrerequest({
            //     currency: 'RUB',
            //     public_key: PUBLIC_KEYS.DEPOSIT_SDK_RUB_PUBLIC_KEY
            // });

            // console.log(getPaymentPrerequest);

            let list = this._uahDepositInputList.getMethodsBySourceCurrency(curency);

            let rsp = new BasicResponse(ResultStatus.OK, 'Get Payments Methods list', list);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async makeManualDeposit(depoBody) {
        try {
            if (depoBody == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Depo body have to be an Object');
            }

            const {
                channelId,
                gateWay,
                terminalCurrency,
                serviceCode,
                serviceSwitchMethod,
                serviceMethod,
                serviceFields,
                publicKey,
                serviceAlias,
                amount,
                fee,
            } = depoBody;

            let paymentId = uinqueid('manualy');

            this._logger.info(`paymentId:[${paymentId}]`);
            this._logger.info(`channelId:[${channelId}]`);
            this._logger.info(`gateWay:[${gateWay}]`);
            this._logger.info(`serviceAlias:[${serviceAlias}]`);
            this._logger.info(`amount:[${amount}]`);
            this._logger.info(`fee:[${fee}]`);


            //get deep payment reciept action

            let depBodyDb = {
                status: DEPOSIT_STAUTUS.MANUAL_PENDING,
                amount: amount,
                currency: terminalCurrency,
                referenceId: paymentId,
                serviceAlias: serviceAlias,
                serviceCode: serviceCode,
                serviceMethod: serviceMethod,
                resolution: 'OK', //its legacy from paycore
                paymentId: paymentId,
                paymentAmount: amount,
                created: Math.round(Date.now() / 1000), //its legacy from paycore - have to be a seconds
                sn: paymentId,
                serviceCurrency: terminalCurrency,
                action: '',
                fee: fee,
                isManual: true
            };

            this._logger.info(`referenceId:[${depBodyDb.referenceId}]`);
            this._logger.info(`resolution:[${depBodyDb.resolution}]`);
            this._logger.info(`paymentId:[${depBodyDb.paymentId}]`);
            this._logger.info(`serviceCurrency:[${depBodyDb.serviceCurrency}]`);
            this._logger.info(`action:[${depBodyDb.action}]`);

            this._logger.info(`Sart save deposit in db.`);

            let saveDeposit = await this._db.models.OrderDeposit.createOne(depBodyDb);

            if (saveDeposit.status != ResultStatus.OK) {
                throw saveDeposit;
            }

            let rsp = new BasicResponse(ResultStatus.OK, 'Create deposit and add it to system done succseffuly.', saveDeposit.data);

            this._logger.info(rsp.toString());

            return rsp;


        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async makeDeposit(info) {
        try {
            const {
                channelId,
                gateWay,
                terminalCurrency,
                serviceCode,
                serviceSwitchMethod,
                serviceFields,
                publicKey,
                serviceAlias,
                amount,
                fee,
            } = info;

            //first step create deposit on kuna
            let depBody = {
                amount: amount, //inputAmount,
                currency: terminalCurrency.toLowerCase(),
            };


            this._logger.info(`channelId:[${channelId}]`);
            this._logger.info(`gateWay:[${gateWay}]`);
            this._logger.info(`serviceAlias:[${serviceAlias}]`);
            this._logger.info(`amount:[${amount}]`);
            this._logger.info(`fee:[${fee}]`);


            this._logger.info(`Start make deposit on kuna.`);

            /**
             * Make depoist on KUNA makeDepositFake
             */
            let deposit = null;

            if (process.env.USE_FAKE != null) {
                deposit = await this._kunaClient.makeDepositFake(depBody);
            } else {
                deposit = await this._kunaClient.makeDeposit(depBody);
            }


            if (deposit.status != ResultStatus.OK) {
                throw deposit;
            }

            let depositId = deposit.data.deposit_id;

            this._logger.info(`Get deposit_id:[${depositId}] make deposit on kuna.`);

            let serviceInfo = this.getService(serviceAlias);

            if (serviceInfo.status != ResultStatus.OK) {
                throw serviceInfo;
            }

            serviceInfo = serviceInfo.data;


            //after succsess create payment - invoice
            let payInvoiceBody = {
                service: serviceCode,
                currency: terminalCurrency.toUpperCase(),
                amount,
                publicKey,
                depositId,
                swithMethod: serviceSwitchMethod,
                serviceFields
            };

            if (serviceInfo.getCustomer != null) {
                payInvoiceBody.customer = serviceInfo.getCustomer(depositId);
            }

            this._logger.info(`Start make payment invoice on kuna.`);


            let payInvoice = null;

            if (process.env.USE_FAKE != null) {
                payInvoice = await this._kunaClient.makePaymentInvoiceFake(payInvoiceBody);
            } else {
                payInvoice = await this._kunaClient.makePaymentInvoice(payInvoiceBody);
            }

            if (payInvoice.status != ResultStatus.OK) {
                throw payInvoice;
            }

            payInvoice = payInvoice.data.data;

            let actionUrl = "";

            console.log(payInvoice);



            if (payInvoice.active_payment.payload.method == "POST") {
                let payload = payInvoice.active_payment.payload;

                const myURL = new URL('http://localhost:9001/any-cash');

                for (let param in payload.params) {
                    myURL.searchParams.set(param, payload.params[param]);
                }

                actionUrl = myURL.href;

                //promisifiy get url by browser
                let getUrl = (actionUrl) => {
                    return new Promise(async resolve => {
                        var browser = new Browser();

                        browser.visit(actionUrl, function () {
                            console.log(browser.location.href);

                            resolve(browser.location.href);
                        });

                        //await browser.destroy();
                    })
                }

                //set urls
                actionUrl = await getUrl(myURL.href);
            } else if (payInvoice.active_payment.payload.method == "GET") {
                actionUrl = payInvoice.active_payment ? payInvoice.active_payment : {};
                actionUrl = actionUrl.payload ? actionUrl.payload : {};
                actionUrl = actionUrl.action ? new URL(actionUrl.action) : null;


                let params = payInvoice.active_payment.payload.params;

                if (params) {
                    for (let param in params) {
                        console.log(`${param}: ${params[param]}`);

                        actionUrl.searchParams.append(param, params[param]);
                    }
                }

                //cratch
                if (serviceFields.phone) {
                    actionUrl.searchParams.append('phone', serviceFields.phone);
                }


                actionUrl = actionUrl.href;
            } else {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Unknown method ${payInvoice.active_payment.payload.method}`);
            }

            console.log(actionUrl);
            console.log(payInvoice);


            //get deep payment reciept action

            let depBodyDb = {
                status: payInvoice.status,
                amount: amount,
                currency: payInvoice.currency,
                status: payInvoice.status,
                referenceId: payInvoice.reference_id,
                serviceAlias: serviceAlias,
                serviceCode: payInvoice.service,
                serviceMethod: payInvoice.service_method,
                resolution: payInvoice.resolution,
                paymentId: payInvoice.id,
                paymentAmount: payInvoice.payment_amount,
                created: payInvoice.created,
                sn: payInvoice.serial_number,
                serviceCurrency: payInvoice.service_currency,
                action: actionUrl,
                fee: fee,
                isManual: false
            };

            this._logger.info(`Get payment invoice on kuna.`);

            this._logger.info(`referenceId:[${depBodyDb.referenceId}]`);
            this._logger.info(`resolution:[${depBodyDb.resolution}]`);
            this._logger.info(`paymentId:[${depBodyDb.paymentId}]`);
            this._logger.info(`serviceCurrency:[${depBodyDb.serviceCurrency}]`);
            this._logger.info(`action:[${depBodyDb.action}]`);

            this._logger.info(`Sart save deposit in db.`);

            let saveDeposit = await this._db.models.OrderDeposit.createOne(depBodyDb);

            if (saveDeposit.status != ResultStatus.OK) {
                throw saveDeposit;
            }

            let rsp = new BasicResponse(ResultStatus.OK, 'Create deposit and add it to system done succseffuly.', saveDeposit.data);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async updateDepositStatus(data) {
        try {
            let updres = await this._depositWatcher.updateDepositStatus(data);

            updres.isOK();

            this._logger.info(updres);

            return updres;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    }
} //PaymentSystemFucntionClass

class PaymentsSystemClass extends PaymentSystemFucntionClass {
    constructor() {
        super();

        this._logger = new AppLoggerClass('PaymentsSystemClass', process.env.PAYMENTS_SYSTEM_CLASS_LOG_LEVEL);
        this._db = null;
        this._kunaClient = new KunaApiClientV3();
        this._uahDepositInputList = new DepositUAHInputListClass();
        this._depositWatcher = new DepositWatcherClass();

    } //deps


    async init(db) {
        try {
            this._db = db;

            let initFlow = await this._initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            if (process.env.CHECK_LOST_ACTIONS != null) {
                this.checkLostActionsTask();
            }

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //init

    async _initFlow() {
        return new Promise((resolve) => {
            waterfall([
                this._initDepoInputListFlowFunc.bind(this),
                this._initKunaClientFlowFunc.bind(this),
                this._initWatcherFlowFunc.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this._logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init core done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    } //initFlow

    async _initDepoInputListFlowFunc() {
        try {
            this._logger.info('Start Kuna Client');

            let initRes = await this._uahDepositInputList.init(this._db);

            if (initRes.status != ResultStatus.OK) {
                throw initRes;
            }

            this._logger.info(initRes.toString());

            return initRes;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initKunaClientFlowFunc

    async _initKunaClientFlowFunc() {
        try {
            this._logger.info('Start Kuna Client');

            let initRes = await this._kunaClient.init({
                kunaPublicToken: process.env.KUNA_PUBLIC_TOKEN,
                kunaPrivateToken: process.env.KUNA_PRIVATE_TOKEN
            });

            if (initRes.status != ResultStatus.OK) {
                throw initRes;
            }

            this._logger.info(initRes.toString());

            return initRes;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initKunaClientFlowFunc

    async _initWatcherFlowFunc() {
        try {
            this._logger.info('Start init watcher Client');

            let initRes = await this._depositWatcher.init(this._db, this._kunaClient, {
                interval: process.env.WATCH_DEPOSIT_INTERVAL,
                state: process.env.WATCH_DEPOSIT_STATE
            });

            if (initRes.status != ResultStatus.OK) {
                throw initRes;
            }

            this._logger.info(initRes.toString());

            return initRes;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initWatcherFlowFunc

    _subscribeOnDepositUpdateEvent(handler) {
        try {
            if (handler == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Handler have to be set.');
            }

            this._depositWatcher.on(EVENTS_LIST.UPDATE, handler);

            let rsp = new BasicResponse(ResultStatus.OK, 'Subscrib to updte event done sucssfully.');

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //subscribeDepositUpdateEvents

} //PaymentsSystemClass


exports.PaymentsSystemClass = PaymentsSystemClass;
exports.CURRENCY_LIST = CURRENCY_LIST;
exports.DEPOSIT_EVENTS_LIST = EVENTS_LIST;
exports.DEPOSIT_STATUS = DEPOSIT_STAUTUS;