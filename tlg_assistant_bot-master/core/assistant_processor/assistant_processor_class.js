`use strict`

const Many = require('extends-classes');

//cores modules
const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    SequelizeClass
} = __UTILS;

const {
    AppLoggerClass
} = __UTILS;

const {
    BtnHandlerProcessorClass
} = require('../buttons/btn_handler_processor_class');

const {
    waterfall,
    queue
} = require('async');

//tegraf 
const session = require("telegraf/session");
const Telegraf = require('telegraf');
const Stage = require("telegraf/stage");
//tegraf 


const StatsD = require('hot-shots');

const {
    ChannelsWizardClass
} = require('../wizzards_processor/wizzards/channels_wizard_class');

const {
    ChannelsFreeWizardClass
} = require('../wizzards_processor/wizzards/channel_free_wizzard');

const {
    SimpleFreeWizardClass
} = require('../wizzards_processor/wizzards/simple_free_wizard_class');

const {
    SimpleWizardClass
} = require('../wizzards_processor/wizzards/simple_wizard_class');

const {
    SimpleFreeWithoutCaptchaWizardClass
} = require('../wizzards_processor/wizzards/simple_without_captcha_wizzard');

const {
    UserSubscribeProcessorClass,
    SUBSCRIBE_REQUEST_EVENTS,
    SUBSCRIBE_ORDER_STATUS,
    SUBSCRIBE_STATUS_TRANSLATE
} = require('../subscribe_processor/user_subscribe_processor_class');

const {
    SystemUserProcessorClass
} = require('../system_user_processor_class');


const {
    AssistantsHandlersClass
} = require('./assistants_handlers_class');

const {
    AssistantPublicMethods
} = require('./assistant_public_methods_class');

const {
    WizardSceneTrackerClass
} = require('../wizzards_processor/wazzard_tracker_class');

const {
    TelegramMethodsClass
} = require('../assistant_processor/telegram_methods_class');



const {
    ChannelWatcherClass
} = require('../channel_watcher/channel_watcher_class')

class AssistantProcessorClass extends Many(AssistantsHandlersClass, AssistantPublicMethods, TelegramMethodsClass) {
    constructor() {
        super();

        this._logger = new AppLoggerClass('AssistantProcessorClass', process.env.ACTIONS_PROCESSOR_LOG_LEVEL);
        this._db = new SequelizeClass('AssistantProcessorClass', process.env.ACTIONS_PROCESSOR_DB_LOG_LEVEL);

        this.telegraf = null;
        this._ddClient = new StatsD();

        //services
        this._userSubsProc = new UserSubscribeProcessorClass();
        this._btnHandlerProcessor = new BtnHandlerProcessorClass();
        this._systemUserProcessor = new SystemUserProcessorClass();
        this._tracker = new WizardSceneTrackerClass();
        this._channelWatcher = new ChannelWatcherClass();
        //services end

        this._props = {};
        this.info = {};

        this._buttons = {};
        this.keyboards = {

        };

        //this.queue = null; //have to be an async queue
    }

    async init(configs) {
        try {
            if (configs == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Wrong incoming configs.');
            }

            if (configs.db == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Wrong incoming configs for db.');
            }

            this._props.db = {
                ...configs.db
            }


            this._props.channelSubscribeWatcher = {
                ...configs.channelSubscribeWatcher
            }

            this._props.channelWatcher = {
                ...configs.channelWatcher
            }

            //this._logger.info(`DEF_PARALLEL_LIMIT: ${configs.parallelLimit}`);
            //this._props.parallelLimit = configs.parallelLimit || DEF_PARALLEL_LIMIT;

            let initFlow = await this._initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            if (process.env.CHECK_LOST_ACTIONS != null) {
                this.checkLostActionsTask();
            }

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //init

    /**
     * init handlers 
     */

    async _initFlow() {
        return new Promise((resolve) => {
            waterfall([
                this._initConnnectionToDbFlowFunc.bind(this),
                this._initChannelWatcherFlowFunc.bind(this),
                this._initUserProsessorFlowFunc.bind(this),
                this._initSubscribeProcSytemFlowFunc.bind(this),
                this._initFlowTelgramFunc.bind(this),
                this._initTrackerFlowFunc.bind(this),
                this._assignTegramButtonsToTelegraf.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this._logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init core done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    } //initFlow

    async _initConnnectionToDbFlowFunc() {
        try {
            this._logger.info('Start');
            //init models

            let {
                modelsPath,
                username,
                password = "",
                database,
                host,
                dialect
            } = this._props.db;

            this._logger.info(`modelsPath:[${modelsPath}]`);
            this._logger.info(`username:[${username}]`);
            this._logger.info(`password:[${password.slice(0, 4)}]`);
            this._logger.info(`database:[${database}]`);
            this._logger.info(`host:[${host}]`);
            this._logger.info(`dialect:[${dialect}]`);


            let resIinitDb = await this._db.run(this._props.db);

            if (resIinitDb.status != ResultStatus.OK) {
                throw resIinitDb;
            }

            this._logger.info(`${resIinitDb.toString()}`);

            return resIinitDb;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _initChannelWatcherFlowFunc() {
        try {
            this._logger.info('Start init Channel Watcher');

            const initRes = await this._channelWatcher.init({
                db: this._db,
                props: this._props.channelWatcher
            });

            if (initRes.status != ResultStatus.OK) {
                throw initRes;
            }

            this._logger.info('End init Channel Watcher');

            this._logger.info(initRes);

            return initRes;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            throw error;
        }
    } //_initChannelWatcherFlowFunc


    async _initUserProsessorFlowFunc() {
        try {
            this._logger.info('Start init system user processor');


            let initFlow = this._systemUserProcessor.init(this._db);

            if (initFlow.status != ResultStatus.OK) {
                throw runWatcher;
            }

            this._logger.info(initFlow);

            return initFlow;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initChannelWatcherFlowFunc

    async _initSubscribeProcSytemFlowFunc() {
        try {
            this._logger.info('Start init Subscribe System');

            let resInit = await this._userSubsProc.init(this._db, {
                state: process.env.SUBSCRIBE_PROC_WATCH_STATE,
                intreval: process.env.SUBSCRIBE_PROC_WATCH_INTERVAL,
                channelSubscribeWatcher: this._props.channelSubscribeWatcher
            });

            if (resInit.status != ResultStatus.OK) {
                throw resInit;
            }

            //subscribe order event

            this._userSubsProc.on(SUBSCRIBE_REQUEST_EVENTS.CREATED, this._subscribeEventHandler.bind(this));
            this._userSubsProc.on(SUBSCRIBE_REQUEST_EVENTS.ORDER_DONE, this._subscribeEventHandler.bind(this));
            this._userSubsProc.on(SUBSCRIBE_REQUEST_EVENTS.ERROR, this._subscribeEventHandler.bind(this));
            this._userSubsProc.on(SUBSCRIBE_REQUEST_EVENTS.EXPIRED, this._subscribeEventHandler.bind(this));
            this._userSubsProc.on(SUBSCRIBE_REQUEST_EVENTS.ORDER_CANCEL, this._subscribeEventHandler.bind(this));
            this._userSubsProc.on(SUBSCRIBE_REQUEST_EVENTS.GRANDED, this._subscribeGrandedEventHandler.bind(this));

            this._logger.info(`${resInit.toString()}`);

            return resInit;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initPyamentsSytemFlowFunc

    async _initFlowTelgramFunc() {
        try {
            this._logger.info('Start');

            const options = {
                channelMode: true
            };

            // Создаем менеджера сцен
            const stage = new Stage();

            this.telegraf = new Telegraf(process.env.TELEGRAM_TOKEN, options);

            this.telegraf.catch(_erro => {
                console.error(_erro);
            });

            let rsp = new BasicResponse(ResultStatus.OK, 'Init Telegraf done successfully');

            //create wizzard for pro
            let ChannelsWizzard = new ChannelsWizardClass('enter_sub_private_channel', this).build(null, {
                MAIN_KEYBOARD: this.keyboards.MAIN_KEYBOARD
            }, this);

            let ChannelsFreeWizard = new ChannelsFreeWizardClass('enter_sub_free_channel', this).build(null, {
                MAIN_KEYBOARD: this.keyboards.MAIN_KEYBOARD
            }, this);

            let SimpleFreeWizard = new SimpleFreeWizardClass('enter_simple_sub_free_channel', this).build(null, {
                MAIN_KEYBOARD: this.keyboards.MAIN_KEYBOARD
            }, this);

            let SimpleWizard = new SimpleWizardClass('enter_simple_sub_private_channel', this).build(null, {
                MAIN_KEYBOARD: this.keyboards.MAIN_KEYBOARD
            }, this);

            let SimpleFreeWithoutCaptchaWizard = new SimpleFreeWithoutCaptchaWizardClass('simple_free_without_captcha_wizard_class', this).build(null, {
                MAIN_KEYBOARD: this.keyboards.MAIN_KEYBOARD
            }, this);



            stage.register(ChannelsWizzard);
            stage.register(ChannelsFreeWizard);
            stage.register(SimpleFreeWizard);
            stage.register(SimpleWizard);
            stage.register(SimpleFreeWithoutCaptchaWizard);


            this.telegraf.use(session());

            this.telegraf.use(stage.middleware());

            //auth middlware
            this.telegraf.use(this._tlgAuthUserMiddleWareHandler.bind(this));

            this.telegraf.start(this._tlgStartUserHandler.bind(this));

            this.telegraf.use(this._tlgMaintenanceModeMiddleWareHandler.bind(this));


            this.telegraf.hears('Отменить ❌', async (ctx) => {
                await ctx.reply('Отмененно', this.keyboards.MAIN_KEYBOARD);

                ctx.scene.leave();
            });

            this.telegraf.launch();

            this._logger.info(rsp.toString());

            const bot = await this.telegraf.telegram.getMe();

            console.log(bot);

            this.info = {
                ...bot
            }

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //

    async _initTrackerFlowFunc() {
        try {
            this._logger.info('Start init tracker flow');

            let initTrackerRes = await this._tracker.init(this._db);

            initTrackerRes.isOK();

            this._logger.info(initTrackerRes);

            return initTrackerRes;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }
    async _assignTegramButtonsToTelegraf() {
        try {
            this._logger.info('Start init processor button processor.');

            let initBtnHandlerInfo = await this._btnHandlerProcessor.init(this._db, this._ddClient, this._tracker);

            if (initBtnHandlerInfo.status != ResultStatus.OK) {
                throw initBtnHandlerInfo;
            }

            this._logger.info(initBtnHandlerInfo);

            this._logger.info('Start load buttons and assign it to Telegraf.');

            let buttonsList = await this._db.models.AssistantButton.getAllActive();

            this.keyboards.MAIN_KEYBOARD = [];

            if (
                buttonsList.status != ResultStatus.OK &&
                buttonsList.status != ResultStatus.ERROR_NOT_FOUND
            ) {
                throw buttonsList;
            }

            if (Array.isArray(buttonsList.data) == false) {
                buttonsList.data = [{
                    name: '🤖'
                }];
            }



            for (let button of buttonsList.data) {
                this._buttons[button.id] = {
                    ...button
                };

                this.telegraf.hears(button.name, (ctx) => {
                    this._logger.info(`Assig hears button:[${button.name}] to Telegraf by handler:[${button.handler}]`);

                    this._btnHandlerProcessor.callButtonHandlerById({
                        context: this,
                        buttonId: button.id,
                        ctx
                    });
                });

                this.telegraf.action(button.name, (ctx, next) => {
                    this._logger.info(`Assig action button:[${button.name}] to Telegraf by handler:[${button.handler}]`);

                    this._btnHandlerProcessor.callButtonHandlerById({
                        context: this,
                        buttonId: button.id,
                        ctx
                    });
                });
            }

            let buttons = buttonsList.data.map(button => button.name);

            this.keyboards.MAIN_KEYBOARD = Telegraf.Extra
                .HTML()
                .markup((m) => m.keyboard([
                    [
                        ...buttons
                    ],
                ]).resize());

            this.keyboardsButtons = buttons;

            let rsp = new BasicResponse(ResultStatus.OK, 'Buttons and processer init and assign successfully.', this.keyboards.MAIN_KEYBOARD);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }
} //

exports.AssistantProcessorClass = AssistantProcessorClass;