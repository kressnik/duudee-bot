const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');


const LINK_TYPES = Object.freeze({
    START: 'start',
    CHANNEL: 'channel',
    check: function (status = '') {
        status.toLowerCase();

        for (let key in this) {
            if (this[key] === status) {
                return status;
            }
        }

        return null;
    },
    list: function () {
        return Object.values(this).filter(a => typeof a == 'string');
    }
});

class AssistantPublicMethods {
    /**
     * 
     * @param {Object} filter [tokenId<Array>, buttonId<Array>, channelId<Array>, orderId<Array>, sessionId<Array>, error<Array>, status<Array>, id<Array>] 
     * @param {Array} timeRange: [start, end],
     */
    async getAllTracksByFilter(args) {
        try {
            this._logger.info(`Start get all tracks by filter`);

            let tracksRequest = await this._tracker.getAllByFilter(args);

            tracksRequest.isOK();

            this._logger.info(tracksRequest);

            return tracksRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getAllTracksByFilterWithPagin(args) {
        try {
            this._logger.info(`Start get all tracks by filter with pagination`);

            let tracksRequest = await this._tracker.getAllByFilterAndPagination(args);

            tracksRequest.isOK();

            this._logger.info(tracksRequest);

            return tracksRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getLinkSessions(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            const {
                tokenId
            } = params;

            this._logger.info(`Start get all sessions for token:[${tokenId}]`);

            let sessionRequest = await this._db.models.TokenSession.getAllByTokenId(tokenId);

            sessionRequest.isOK();

            this._logger.info(sessionRequest);

            return sessionRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllSubscribesOrdres

    async checkPeriod(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            const {
                tokenId,
                startDate,
                endDate,
            } = params;

            this._logger.info(`Check period cretate sessions for token:[${tokenId}] start date:[${startDate}] end date:[${endDate}]`);

            let sessionRequest = await this._db.models.TokenSession.checkPeriod({
                tokenId,
                startDate,
                endDate,
            });

            sessionRequest.isOK();

            this._logger.info(sessionRequest);

            return sessionRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //checkPeriod

    async createLinkSession(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            const {
                tokenId,
                startDate,
                endDate,
                title,
            } = params;

            this._logger.info(`Start cretate sessions for token:[${tokenId}] start date:[${startDate}] end date:[${endDate}] title:[${title}]`);

            let sessionRequest = await this._db.models.TokenSession.createOne({
                tokenId,
                startDate,
                endDate,
                title,
            });

            sessionRequest.isOK();

            this._logger.info(sessionRequest);

            return sessionRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllSubscribesOrdres

    async deleteLinkSession(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            const {
                id
            } = params;

            this._logger.info(`Start delete sessions by id:[${id}]`);

            let sessionRequest = await this._db.models.TokenSession.deleteOne(id);

            sessionRequest.isOK();

            this._logger.info(sessionRequest);

            return sessionRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllSubscribesOrdres

    /**
     * Wrapper getAllSubscribesOrdres
     * @param {array} filter filtering by status
     * @param {int} offset 
     * @param {int} limit 
     */
    async getAllSubscribesOrdres(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //fetch data from db
            let subscribesOrdresInfo = await this._userSubsProc.getAllSubscribesOrdres(params);

            return subscribesOrdresInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllSubscribesOrdres

    async getSubscribeOrderStatuses(params) {
        try {
            let depoStatusesRequest = await this._userSubsProc.getSubscribesStatuses();

            depoStatusesRequest.isOK();

            return depoStatusesRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getSubscribesStatuses

    /**
     * @param {int} offset 
     * @param {int} limit 
     */
    async getAllSubscribes(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //fetch data from db

            let subscribesInfo = await this._db.models.Subcribe.getAll(params);

            if (subscribesInfo.status != ResultStatus.OK) {
                throw subscribesInfo;
            }

            for (let sub of subscribesInfo.data.data) {

                let subOrderInfo = await this._db.models.SubscribeOrder.getOrderInfo({
                    id: sub.orderId
                });

                if (subOrderInfo.message == 'This subscribe is free') {
                    sub.isFree = true;
                } else {
                    sub.isFree = subOrderInfo.data.isFree;
                }

                let userInfo = await this._db.models.User.getOneById({
                    id: sub.userId
                });

                sub.userInfo = userInfo.data;
            }

            return subscribesInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllSubscribes

    async getAllUsers(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //fetch data from db

            let usersInfo = await this._db.models.User.getAll(params);

            if (usersInfo.status != ResultStatus.OK) {
                throw usersInfo;
            }

            let users = usersInfo.data.data;

            let finalUsersBatch = {
                users: null,
                info: null
            }

            for (let user of users) {
                ///на хрена этот код
                let inviteLink = await this._db.models.LinkAction.getInviteActionByUserId(user.id)

                if (inviteLink.status == ResultStatus.OK) {
                    user.inviteLink = inviteLink.data.inviteId;
                }

                let subscribesInfo = await this._db.models.Subcribe.getAllWithUser({
                    userId: user.id
                });

                user.channels = [];

                if (subscribesInfo.status == ResultStatus.OK) {
                    let subscribesArr = subscribesInfo.data;

                    for (let subscribe of subscribesArr) {

                        let channelInfo = subscribe.channelInfo;

                        user.channels.push(channelInfo);
                    }
                }
            }

            finalUsersBatch.users = usersInfo.data;
            finalUsersBatch.info = usersInfo.data.total;

            return usersInfo;

            // return new BasicResponse(ResultStatus.OK, usersInfo.message, finalUsersBatch);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllUsers

    async getAllChannels(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //fetch data from db

            let channelsInfo = await this._db.models.Channel.getAll(params);

            if (channelsInfo.status != ResultStatus.OK) {
                throw channelsInfo;
            }

            return channelsInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllChannels

    async createSubscribe(params) {
        try {
            let checkSubscribe = await this._userSubsProc.chekcSubcribeByTlg({
                channelTlgId: parseInt(params.channelTlgId),
                userId: parseInt(params.userId)
            });

            if (checkSubscribe.status == ResultStatus.ERROR_ALREADY_ASSIGNED) {
                throw checkSubscribe;
            }

            let bill = await this._userSubsProc.makeFreeSubscribeRequestChannel({
                channelTlgId: params.channelTlgId,
                userId: params.userId
            });

            if (bill.status != ResultStatus.OK) {
                throw bill;
            }

            this._logger.info(bill.toString());

            bill = bill.data;

            bill.gateWay = params.invoker;

            let order = await this._userSubsProc.makeFreeSubscribeOrder(bill);

            if (order.status != ResultStatus.OK) {
                throw order;
            }

            return order;

        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getDepoInfo(params) {
        try {
            let subInfo = await this._db.models.Subcribe.getOneById(params);

            if (subInfo.status != ResultStatus.OK) {
                throw subInfo;
            }

            let orderInfo = await this._db.models.SubscribeOrder.getOrderInfo({
                id: subInfo.data.orderId
            })

            if (orderInfo.status != ResultStatus.OK) {
                throw orderInfo;
            }

            if (orderInfo.data == null) {
                return orderInfo;
            }

            let depoInfo = await this._db.models.OrderDeposit.getDepoInfoById({
                id: orderInfo.data.depositId
            })

            return depoInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getDepoInfo


    async getDepositStatuses() {
        try {
            let depoStatusesRequest = await this._userSubsProc.getDepositStatuses();

            depoStatusesRequest.isOK();

            return depoStatusesRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getDepositStatuses

    async updateOrderInfo(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //call fucntion from subscribe proccessor

            let updRequest = await this._userSubsProc.updateOrderInfo(params);

            updRequest.isOK();

            return updRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //updateDepoStatus

    async updateDepoStatus(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //call fucntion from subscribe proccessor

            const {
                orderId
            } = params;

            this._logger.info(`Update deposit stutus by order id: [${orderId}]`);

            let updRequest = await this._userSubsProc.updateDepositStatus(params);

            updRequest.isOK();

            return updRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //updateDepoStatus

    async createChannel(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let createInfo = await this._db.models.Channel.createOne(params);

            if (createInfo.status != ResultStatus.OK) {
                throw createInfo;
            }

            return createInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //createChannel

    async editChannelInfo(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let editInfo = await this._db.models.Channel.editOne(params);

            if (editInfo.status != ResultStatus.OK) {
                throw editInfo;
            }

            return editInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //editChannelInfo

    async deleteChannel(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let editInfo = await this._db.models.Channel.deleteOne(params);

            if (editInfo.status != ResultStatus.OK) {
                throw editInfo;
            }

            return editInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //deleteChannel

    async editStateChannel(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let editInfo = await this._db.models.Channel.editStateOne(params);

            if (editInfo.status != ResultStatus.OK) {
                throw editInfo;
            }

            return editInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //editStateChannel

    async getUsersByLink(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //fetch data from db

            let linkInfo = await this._db.models.LinkAction.getUsersIdByLink(params);

            if (linkInfo.status != ResultStatus.OK) {
                throw linkInfo;
            }

            let links = linkInfo.data.orders;

            let finalBatch = [];

            for (let link of links) {

                let user = await this._db.models.User.getOneById({
                    id: link.userId
                })

                let subscribesInfo = await this._db.models.Subcribe.getAllWithUser({
                    userId: link.userId
                });

                user.data.channels = [];

                if (subscribesInfo.status == ResultStatus.OK) {
                    let subscribesArr = subscribesInfo.data;

                    for (let subscribe of subscribesArr) {

                        let channelInfo = subscribe.channelInfo;

                        user.data.channels.push(channelInfo);
                    }
                }

                finalBatch.push(user.data);
            }

            // finalUsersBatch.users = usersInfo.data;
            // finalUsersBatch.info = usersInfo.data.total;

            // return usersInfo;

            return new BasicResponse(ResultStatus.OK, linkInfo.message, {
                finalBatch,
                recordsTotal: linkInfo.data.recordsTotal,
                recordsFiltered: linkInfo.data.recordsFiltered
            });

        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getUsersByLinkMethod

    async getLinkTypes() {
        try {
            return new BasicResponse(ResultStatus.OK, 'Available Links types', LINK_TYPES.list());
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    } // getLinkTypes

    async createReferalLink(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let checkStatus = LINK_TYPES.check(params.type);

            if (checkStatus == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Invalid link type:[${params.type}]`, LINK_TYPES.list());
            }

            let createInfo = await this._db.models.ReferalLink.createOne(params);

            if (createInfo.status != ResultStatus.OK) {
                throw createInfo;
            }

            return createInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //createReferalLink

    async softDeleteReferalLink(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let deleteInfo = await this._db.models.ReferalLink.deleteOne({
                id: params.id
            });

            if (deleteInfo.status != ResultStatus.OK) {
                throw deleteInfo;
            }

            return deleteInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //softDeleteReferalLink

    async editReferalLink(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let editInfo = await this._db.models.ReferalLink.editOne(params);

            if (editInfo.status != ResultStatus.OK) {
                throw editInfo;
            }

            return editInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //editReferalLink

    async getAllReferalLinks(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let links = await this._db.models.ReferalLink.getAll(params);

            if (links.status != ResultStatus.OK) {
                throw links;
            }

            return links;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllReferalLinks  

    async getreffLinkById(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let links = await this._db.models.ReferalLink.findOneById(params);

            if (links.status != ResultStatus.OK) {
                throw links;
            }

            return links;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getreffLinkById

    async getAllTelegramButtons() {
        try {
            let buttonsList = await this._db.models.AssistantButton.getAll();

            buttonsList.isOK();

            return buttonsList;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    /**
     * Get auth status
     * @param void
     */
    async watcherAuthGetStatus() {
        try {
            const request = await this._channelWatcher.authGetStatus();

            request.isOK();

            return request;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`Get auth status return: ${error}`);

            return error;
        }
    } //authGetStatus

    /**
     * Send and seup api information
     * @param {*} apiId
     * @param {*} apiHash
     */
    async watcherUserSendApiInfo(args) {
        try {
            const request = await this._channelWatcher.userSendApiInfo(args);

            request.isOK();

            this._logger.info(request);

            return request;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`Get userSendApiInfo return: ${error}`);

            return error;
        }
    } //userSendApiInfo

    /**
     * Start initiate auth process.
     * @param {*} phone phone for recived code
     */
    async watcherUserSendStartAuth(phone) {
        try {
            const request = await this._channelWatcher.userSendStartAuth(phone);

            request.isOK();

            this._logger.info(request);

            return request;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`userSendStartAuth return: ${error}`);

            return error;
        }
    } //watcherUserSendStartAuth

    /**
     * Send auth code for auth process.
     * @param {*} phone phone for recived code
     */
    async watcherUserSendAuthCode(code) {
        try {
            const request = await this._channelWatcher.userSendAuthCode(code);

            request.isOK();

            this._logger.info(request);

            return request;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(`userSendAuthCode catch error: ${error}`);

            return error;
        }
    } //watcherUserSendAuthCode
}

module.exports.AssistantPublicMethods = AssistantPublicMethods;