'use strict'

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;

const Extra = require('telegraf/extra');

class TelegramMethodsClass {
    /**
     * Reduce teleggram rresponse to BasicResponse
     * @param {*} error 
     */
    static reduceTelegramErrorToBasic(error) {
        if (error.constructor.name !== 'TelegramError') {
            return new BasicResponseByError(error);
        }

        switch (error.code) {
            case 400:
                return new BasicResponse(ResultStatus.ERROR_BAD_REQUEST, error.message, error.response);
            case 403:
                return new BasicResponse(ResultStatus.ERROR_FORBIDDEN, error.message, error.response);
            default:
                return new BasicResponse(ResultStatus.ERROR_UNEXP_ERR, error.message, error.response);
        }
    }

    /**
     * #Wrrapper
     * Get tegram chat info 
     * @param {*} tlgChaId Telegram chat id in both format(-100 supported)
     */
    async _getChatInfo(tlgChaId) {
        try {
            if (tlgChaId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Telegram chat id should be provided.');
            }

            this._logger.debug(`Start request telegram chat:[${tlgChaId}] info`);

            if (tlgChaId > 0) {
                tlgChaId = `-100${tlgChaId}`
            }

            const chatInfo = await this.telegraf.telegram.getChat(tlgChaId);

            const rsp = new BasicResponse(ResultStatus.OK, `Get telegram chat:[${tlgChaId}] info done successfully`, chatInfo);

            this._logger.debug(rsp);

            return rsp;
        } catch (e) {
            const error = TelegramMethodsClass.reduceTelegramErrorToBasic(e);

            this._logger.error(error);

            return error;
        }
    }

    //telegram facade function
    async _exportChatInviteLink(tlgChaId) {
        try {
            if (tlgChaId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Telegram chat id should be provided.');
            }

            this._logger.debug(`Start request telegram chat:[${tlgChaId}] invite link`);

            if (tlgChaId > 0) {
                tlgChaId = `-100${tlgChaId}`
            }

            const inviteLink = await this.telegraf.telegram.exportChatInviteLink(tlgChaId);

            const rsp = new BasicResponse(ResultStatus.OK, `Get telegram chat:[${tlgChaId}] invite link:[${inviteLink}] info done successfully`, inviteLink);

            this._logger.debug(rsp);

            return rsp;
        } catch (e) {
            const error = TelegramMethodsClass.reduceTelegramErrorToBasic(e);

            this._logger.error(error);

            return error;
        }
    }

    async _sendMessageToChat(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments should be an object');
            }

            const {
                chatId,
                message,
                extra = Extra.HTML()
            } = args;

            this._logger.debug(`Send to chatId:[${chatId}]`);

            const msgTlg = await this.telegraf.telegram.sendMessage(chatId, message, extra);

            const rsp = new BasicResponse(ResultStatus.OK, `Sended to chatId:[${chatId}] message_id:[${msgTlg.message_id}]`, msgTlg);

            this._logger.debug(rsp);

            return rsp;
        } catch (e) {
            const error = TelegramMethodsClass.reduceTelegramErrorToBasic(e);

            this._logger.error(error);

            return error;
        }
    } //_publishToAdminss

    async _unbanChatMember(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'arguments should be an object');
            }

            const {
                userTlgID,
                channelTlgID
            } = args;

            this._logger.debug(`Unban Chat Member:[${userTlgID}] channelTlgID:[${channelTlgID}]`);

            const unbanRes = await this.telegraf.telegram.unbanChatMember(`-100${channelTlgID}`, userTlgID);

            this._logger.debug(`unbanRes:[${unbanRes}]`);

            const rsp = new BasicResponse(ResultStatus.OK, `Unban Chat Member:[${userTlgID}] channelTlgID:[${channelTlgID}] done successfully.`, unbanRes);

            this._logger.debug(rsp);

            return rsp;
        } catch (e) {
            const error = TelegramMethodsClass.reduceTelegramErrorToBasic(e);

            this._logger.error(error);

            return error;
        }
    } //_unbanChatMember

    async _deleteChatMessage(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'args should be object');
            }

            const {
                chatId,
                messageId
            } = args;

            if (chatId === null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'chatId is null');
            }

            if (messageId === null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'messageId is null');
            }

            const delRes = await this.telegraf.telegram.deleteMessage(chatId, messageId);

            if (delRes != true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Delete message:[${messageId}] chat:[${chatId}] return false `);
            }

            const rsp = new BasicResponse(ResultStatus.OK, `Delete message:[${messageId}] chat:[${chatId}] done successfully`);

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = TelegramMethodsClass.reduceTelegramErrorToBasic(e);

            this._logger.error(error);

            return error;
        }
    } // _deleteChatMessage
}


exports.TelegramMethodsClass = TelegramMethodsClass;