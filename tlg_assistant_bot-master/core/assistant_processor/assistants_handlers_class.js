const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;

const USER_ROLE = Object.freeze({
    ADMIN: 0,
    SUBSRIBER: 1
});

const {
    SUBSCRIBE_REQUEST_EVENTS,
    SUBSCRIBE_STATUS_TRANSLATE
} = require('../subscribe_processor/user_subscribe_processor_class');

const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const adminDescr = '👨‍💻<i>Admin[report]</i>👨‍💻';

const humanizeDuration = require('humanize-duration');


class AssistantsHandlersClass {
    constructor() {}

    async promotionRequestHandler(link) {
        try {
            const referalLinkInfo = await this._db.models.ReferalLink.findOneByLink({
                link
            });

            referalLinkInfo.isOK();

            const rsp = new BasicResponse(ResultStatus.OK, 'Handle promotion request', {
                botUsername: process.env.BOT_USERNAME || this.info.username,
                pixelId: referalLinkInfo.data.facebookPixelId,
                token: referalLinkInfo.data.link
            });

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    }

    async _tlgStartUserHandler(ctx) {
        try {
            const {
                first_name,
                id,
                is_bot,
                language_code,
                last_name,
                username,
            } = ctx.from;

            const descr = [`Recive start event id[${id}]`,
                `username:[${username}]`,
                `language_code:[${language_code}]`,
                `last_name:[${last_name}]`,
                `first_name:[${first_name}]`,
                `is_bot:[${is_bot}]`
            ].join(' ');

            this._logger.info(descr);

            ctx.startTime = new Date().getTime();

            let referalLink = ctx.startPayload;

            if (referalLink != null && referalLink != "") {
                this._logger.info(`Recive start with token:[${referalLink}] from username:[${username}] tlgid:[${id}]`);

                let inviteResult = await ctx.session.user.assignInviteLink({
                    userId: ctx.session.user.id,
                    referalLink
                });

                if (inviteResult.isStatuses(ResultStatus.OK, ResultStatus.ERROR_ALREADY_ASSIGNED)) {
                    this._logger.info(inviteResult);

                    const {
                        buttonId,
                        channelId,
                        link
                    } = inviteResult.data;

                    this._ddClient.increment(`${link}`);

                    let linkReq = await this._btnHandlerProcessor.callButtonHandlerById({
                        params: {
                            channelId,

                        },
                        buttonId,
                        ctx
                    });

                    if (linkReq.status != ResultStatus.OK) {
                        this._logger.error(linkReq);
                    } else {
                        this._logger.info(linkReq);
                    }
                } else {
                    this._logger.warn(inviteResult);
                } //check assign link to user
            } else {
                await ctx.reply([
                    `Привет 👋 ${first_name || ''} ${last_name || ''}`,
                    `▪️ Для того чтобы подписаться на наши каналы жми "Список каналов".`,
                    `▪️ Для того чтобы приобрести доступ в приватный канал по теннису жми "VIP каналы".`,
                    `Если у вас появились вопросы или что то пошло не так. Вы можете написать в саппорт @duudee_sport_support 🆘`
                ].join('\n'), this.keyboards.MAIN_KEYBOARD);

                let msg = Extra.HTML()
                    .markup((m) => {
                        return m.inlineKeyboard(this.keyboardsButtons.map(buttonName => {
                            return [m.callbackButton(buttonName, buttonName)];
                        }))
                    });
                //await ctx.reply(`<code>${checkin.accessHash}</code>`, Extra.HTML());

                ctx.session.startMessage = await ctx.reply('Выберите категорию ниже', msg);
            }

            ctx.endTime = new Date().getTime();

            let startDuration = ctx.endTime - ctx.startTime;

            this._ddClient.histogram('start_response_time_ms', startDuration);

            const startResult = new BasicResponse(ResultStatus.OK, `Start process done successfully. ${descr}`);

            return startResult;
        } catch (e) {
            ctx.reply(`Упс... Произошла техническая неполадка. Попробуйте чуть позже.`);

            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    }

    async _tlgMaintenanceModeMiddleWareHandler(ctx, next) {
        try {
            if (ctx.updateType == 'channel_post') {
                return //await next();
            }

            if (ctx.updateType == 'edited_channel_post') {
                return //await next();
            }

            //checkin use
            if (ctx.session.user.role == USER_ROLE.ADMIN) {
                return await next();
            }

            let maintenanceMode = await this._db.models.CoreSetting.getOneByName({
                name: 'maintenanceMode'
            });

            if (maintenanceMode.status != ResultStatus.OK) {
                throw maintenanceMode;
            }

            maintenanceMode = maintenanceMode.data;

            if (maintenanceMode.value == false) {
                ctx.responseTimeEnd = new Date().getTime();

                let duration = ctx.responseTimeEnd - ctx.responseTimeStart;

                this._ddClient.histogram('response_time_ms', duration);

                await next();
            } else {
                let msgTechMode = [
                    `👷‍ Мы производим обслуживание сервиса 👷‍`,
                    `Приносим извенения за доставленные неудобства, скоро все заработает 🙇`,
                    'Если у вас появились вопросы вы можете написать в саппорт @duudee_sport_support 🆘',
                ].join('\n');

                ctx.responseTimeEnd = new Date().getTime();

                let duration = ctx.responseTimeEnd - ctx.responseTimeStart;

                this._ddClient.histogram('response_time_ms', duration);

                await ctx.reply(msgTechMode);
            }
        } catch (e) {
            await ctx.reply(`Упс... Произошла техническая неполадка. Попробуйте чуть позже.`);

            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async _tlgAuthUserMiddleWareHandler(ctx, next) {
        try {
            this._logger.info('Start');

            ctx.responseTimeStart = new Date().getTime();
            //checkin user
            if (ctx.updateType == 'channel_post') {
                return //await next();
            }

            if (ctx.updateType == 'edited_channel_post') {
                return //await next();
            }


            let chUser = {
                firstName: ctx.from.first_name || '',
                lastName: ctx.from.last_name || '',
                username: ctx.from.username || '',
                tlgUserId: ctx.from.id,
                languageCode: ctx.from.language_code || 'not_set'
            };

            let checkin = await this._systemUserProcessor.checkIn(chUser);

            if (checkin.status != ResultStatus.OK) {
                throw checkin;
            }

            this._logger.info(checkin.toString());

            checkin = checkin.data;

            ctx.session.user = checkin;

            await next();
        } catch (e) {
            ctx.reply(`Упс... Произошла техническая неполадка. Попробуйте чуть позже.`);

            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async _subscribeGrandedEventHandler(event) {
        try {
            const {
                type,
                data,
            } = event;

            if (type != SUBSCRIBE_REQUEST_EVENTS.GRANDED) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Recive wrong type:[${type}]. Have to be:[${SUBSCRIBE_REQUEST_EVENTS.GRANDED}]`);
            }

            const {
                expireTimeUtc,
                sn,
                userId,
                channelInfo,
                subscriptionDuration
            } = data;

            const hDuration = humanizeDuration(subscriptionDuration * 3600 * 1000, {
                language: 'ru'
            }); //get seconds from hours

            //todo: refactor get from user processor class
            let user = await this._db.models.User.getOneById({
                id: userId
            });

            if (user.status != ResultStatus.OK) {
                throw user;
            }

            user = user.data;

            //add to channell

            // let chatInfo = await this.telegraf.telegram.getChat(`-100${channelInfo.tlgId}`);
            const chatInfoRequest = await this._getChatInfo(channelInfo.tlgId);

            //assert if not ok
            chatInfoRequest.isOK();

            const chatInfo = chatInfoRequest.data;

            //check for send only free channel
            if (channelInfo.isFree == true) {
                if (chatInfo.invite_link == null) {
                    this._logger.warn(`recieve null invite_link for channel:[${channelInfo.tlgId}], have to export new`);

                    const inviteLinkRequest = await this._exportChatInviteLink(channelInfo.tlgId);

                    inviteLinkRequest.isOK();

                    chatInfo.invite_link = inviteLinkRequest.data;
                }

                this._logger.info(`telegram channel id:[${channelInfo.tlgId}] invite_lik:[${chatInfo.invite_link}]`);

                let linkBtn = Markup.urlButton('Вам сюда', chatInfo.invite_link);

                let unban = await this._unbanChatMember({
                    channelTlgID: channelInfo.tlgId,
                    userTlgID: user.tlgUserId
                });

                if (unban.status != ResultStatus.OK) {
                    this._logger.warn(unban.toString());
                } else {
                    this._logger.info(unban.toString());
                }

                //promt to user sub info
                // let msgForSubcribrer = [
                //     `Вам успешно офрмлена подписка серийный номер: <b>${sn}</b>`,
                //     //`Сыылка на канал <i>${chatInfo.invite_link}</i>`,
                //     `Канал <b>${chatInfo.title}</b> на срок <i>${hDuration}</i>`,
                //     `Время прекращения подписки ${new Date(expireTimeUtc).toISOString()}`
                // ].filter(e => e != null).join('\n');

                //let sendUserRes = await this._sendMessageToChat(user, msgForSubcribrer, Extra.HTML());;
                //this._logger.info(sendUserRes.toString());

                this._logger.debug(`Start Send welcome invite messge to user:[${user.username}:${user.tlgUserId}]`);

                const sendUserRes2 = await this._sendMessageToChat({
                    chatId: user.tlgUserId,
                    message: `Для перехода в канал ${chatInfo.title} нажми на кнопку `,
                    extra: Markup.inlineKeyboard([linkBtn]).extra()
                });

                if (sendUserRes2.status != ResultStatus.OK) {
                    this._logger.warn(sendUserRes2);
                } else {
                    this._logger.info(sendUserRes2);
                }
            } //check for send only free channel

            if (process.env.SEND_TO_ADMIN_SUBSCRIBE_GRANDED != null) {
                const userDescr = [
                    `Пользователь: ${user.tlgUserId}:@${user.username}`,
                    `ФИО:${user.firstName}:${user.lastName}`
                ].join('\n');

                //promt to admins
                const msgForAdmin = [
                    adminDescr,
                    `Выдана подписка, серийный номер: <b> ${sn} </b>`,
                    `Канал <b>${chatInfo.title}</b> на срок ${hDuration}`,
                    `Время прекращения подписки ${new Date(expireTimeUtc).toISOString()}`,
                    userDescr
                ].filter(e => e != null).join('\n');

                const sendAdminRes = await this._publishToAdmins(msgForAdmin);

                if (sendAdminRes.status != ResultStatus.OK) {
                    throw sendAdminRes;
                }
            } //SEND_TO_ADMIN_SUBSCRIBE_GRANDED
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    } //_subscribeGrandedEventHandler

    async _subscribeEventHandler(event) {
        try {
            if (event == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Recived null event');
            }

            const {
                type,
                data,
            } = event;

            const {
                depositInfo,
                orderInfo,
                channelInfo, //?
                cause
            } = data;

            this._logger.info(`Recived event type:[${type}]`);

            let user = await this._db.models.User.getOneById({
                id: orderInfo.userId
            });

            let userDescr = [];

            if (user.status != ResultStatus.OK) {
                this._logger.warn(user.toString());
            } else {
                user = user.data

                userDescr = [
                    `Пользователь: ${user.tlgUserId}:@${user.username}`,
                    `ФИО: ${user.firstName}:${user.lastName}`
                ].join('\n');
            }

            let chDescr = [
                `Канал: [${channelInfo.name}]`
            ].join('\n');



            switch (type) {
                case SUBSCRIBE_REQUEST_EVENTS.CREATED: {
                    const sn = data.orderInfo.sn;

                    if (data.orderInfo.status == 'manual_pending' || data.orderInfo.isFree == false) {
                        const msg = [
                            adminDescr,
                            `Выполнен запрос серийный номер: <b>${sn}</b>`,
                            depositInfo != null ? `Время: ${depositInfo.createdAt.toString()}` : null,
                            depositInfo != null ? `Платежка/сумма: ${depositInfo.serviceAlias}/${depositInfo.amount} ${depositInfo.currency}` : null,
                            userDescr
                        ].filter(e => e != null).join('\n');



                        const sendRes = await this._publishToAdmins(msg);

                        if (sendRes.status != ResultStatus.OK) {
                            throw sendRes;
                        }

                        this._logger.info(sendRes.toString());
                    }
                }
                break;

            case SUBSCRIBE_REQUEST_EVENTS.ORDER_DONE:
            case SUBSCRIBE_REQUEST_EVENTS.ORDER_CANCEL: {
                this._logger.info(`Recived event type:[${type}] for order id:[${orderInfo.id}] with status:[${orderInfo.status}]`);

                const error = null;

                if (type == SUBSCRIBE_REQUEST_EVENTS.ORDER_CANCEL) {
                    error = ResultStatus.ERROR_CANCEL;
                }

                //finalize track 
                const finilizeTrackResult = await this._tracker.done({
                    id: orderInfo.trackId,
                    error
                });

                if (finilizeTrackResult.isStatus(ResultStatus.OK) == false) {
                    this._logger.warn(finilizeTrackResult);
                } else {
                    this._logger.info(finilizeTrackResult);
                }

                const st = SUBSCRIBE_STATUS_TRANSLATE.get('ru', orderInfo.status);

                const msgForAdmin = [
                    adminDescr,
                    `Запросу присвоен статус <b>'${st}'</b> серийный номер: <b>${orderInfo.sn}</b>`,
                    depositInfo != null ? `Время начала: ${depositInfo.createdAt.toString()}` : null,
                    depositInfo != null ? `Время окончания: ${depositInfo.updatedAt.toString()}` : null,
                    cause ? `Причина: ${cause}` : null,
                    depositInfo != null ? `Платежка/сумма: ${depositInfo.serviceAlias}/${depositInfo.amount} ${depositInfo.currency}` : null,
                    chDescr,
                    userDescr
                ].filter(e => e != null).join('\n');

                const sendAdminRes = await this._publishToAdmins(msgForAdmin);

                if (sendAdminRes.status != ResultStatus.OK) {
                    throw sendAdminRes;
                }

                this._logger.info(`Sent to admin, ${sendAdminRes}. ${type}`);

                //let descr = orderInfo.isFree ? `Запросу присвоен статус <b>'${st}'</b> серийный номер: <b>${orderInfo.sn}</b>`;

                const msgForSubcribrer = [
                    chDescr,
                    // `Запросу присвоен статус <b>'${st}'</b> серийный номер: <b>${orderInfo.sn}</b>`,
                    `Запросу присвоен статус <b>'${st}'</b>`,
                    depositInfo != null ? `Cумма: ${depositInfo.amount} ${depositInfo.currency}` : null,
                    type == SUBSCRIBE_REQUEST_EVENTS.DONE ? `Ожидайте в ближайшее время с вами свяжутся` : null
                ].filter(e => e != null).join('\n');

                this._logger.debug(`Start order desscription messge to user:[${user.username}:${user.tlgUserId}]`);

                const sendUserRes = await this._sendMessageToChat({
                    chatId: user.tlgUserId,
                    message: msgForSubcribrer
                });

                this._logger.info(sendUserRes.toString());
            }
            break;

            case SUBSCRIBE_REQUEST_EVENTS.ERROR:
                this._logger.error(`SUBSCRIBE_REQUEST_EVENTS.ERROR: error${orderInfo}`);

                //finalize track
                const finilizeTrackResult = await this._tracker.done({
                    id: orderInfo.trackId,
                    error: orderInfo.status
                });

                if (finilizeTrackResult.isStatus(ResultStatus.OK) == false) {
                    this._logger.warn(finilizeTrackResult);
                } else {
                    this._logger.info(finilizeTrackResult);
                }

                break;
            case SUBSCRIBE_REQUEST_EVENTS.EXPIRED:

            default:
                this._logger.warn(`Unknown type ${type} processing subscribe`);

            } //switch 

            this._logger.info('Start processing subscribe');

            const rsp = new BasicResponse(ResultStatus.OK, `Process subscrebe event done sucssfully`)

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //_orderEventHandler

    async _publishToAdmins(msg) {
        try {
            const admins = await this._db.models.User.getUsersIdByRole(USER_ROLE.ADMIN);

            if (admins.status != ResultStatus.OK) {
                throw admins;
            }

            let promises = [];

            for (const admin of admins.data) {
                promises.push((async () => {
                    this._logger.info(`Send to admin:[${admin.username}:${admin.tlgUserId}]`);

                    const msgTlg = await this._sendMessageToChat({
                        chatId: admin.tlgUserId,
                        message: msg,
                        extra: Extra.HTML()
                    });

                    this._logger.info(`Sended to admin:[${admin.username}:${admin.tlgUserId}] msg:${msgTlg.id}`);
                })())
            }

            const all = await Promise.all(promises);

            const rsp = new BasicResponse(ResultStatus.OK, 'Publish message for all admins done succsfully.', all);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //_publishToAdminss

    async _showUsersSubcribes(userInfo) {
        try {
            const {
                id
            } = userInfo;


            const subscribes = await this._db.models.Subcribe.getAllWithUser({
                userId: id
            });

            if (subscribes.status != ResultStatus.OK) {
                throw subscribes;
            }

            const msgSybscribe = subscribes.data.map(info => {
                const hDuration = humanizeDuration(info.subscriptionDuration * 3600 * 1000, {
                    language: 'ru'
                }); //get seconds from hours

                return [
                    `Канал <b>${info.channelInfo.name}</b> на срок <i>${hDuration}</i>`,
                    `Время прекращения подписки ${new Date(info.expireTimeUtc).toISOString()}`
                ].filter(e => e != null).join('\n');
            }).join('\n----\n');

            const sendUserRes = await this._sendMessageToChat({
                chatId: userInfo.tlgId,
                message: msgSybscribe
            });

            this._logger.info(sendUserRes.toString());

        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
} //AssistantsHandlersClass

module.exports.AssistantsHandlersClass = AssistantsHandlersClass;