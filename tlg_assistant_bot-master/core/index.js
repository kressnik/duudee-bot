require('../utils');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');

const {
    RpcProcessorClass,
    AppLoggerClass
} = __UTILS;

const {
    waterfall
} = require('async');

const path = require('path');

const {
    AssistantProcessorClass
} = require('./assistant_processor/assistant_processor_class');

class Core {
    constructor() {
        this._logger = new AppLoggerClass('CORE', process.env.CORE_LOG_LEVEL);

        let rpcConf = {
            methodsPath: path.join(__dirname, '../rpc_methods'),
            core: this
        }

        this.rpcProcessor = new RpcProcessorClass(rpcConf);
        this.assistantProcessor = new AssistantProcessorClass();
    }

    async init() {
        try {
            let initFlow = await this._initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //init

    async _initFlow() {
        return new Promise((resolve) => {
            waterfall([
                //----
                this._initAssistantBotFlowFunc.bind(this),
                this.rpcProcessor.initFlowFunc.bind(this.rpcProcessor),
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    let rsp = new BasicResponse(ResultStatus.OK, 'Initilization Flow done successfully');

                    this._logger.info(rsp.toString());

                    resolve(rsp);
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    }

    async _initAssistantBotFlowFunc() {
        try {
            this._logger.info('Start init assistant');
            //init models
            const configs = {
                db: {
                    username: process.env.DB_USERNAME,
                    password: process.env.DB_PASSWORD || '',
                    database: process.env.DB_DATABASE,
                    directory: process.env.DB_DIRECTORY,
                    migration: {
                        version: process.env.MIGRATION_VERSION,
                    },
                    options: {
                        dialect: 'mysql',
                        port: process.env.DB_PORT,
                        host: process.env.DB_HOST,
                    },
                },
                channelSubscribeWatcher: {
                    phone: process.env.CHANNEL_SUBSCRIBE_WATCHER_PHONE,
                    state: process.env.CHANNEL_SUBSCRIBE_WATCHER_STATE,
                    interval: process.env.CHANNEL_SUBSCRIBE_WATCHER_INTERVAL,
                },
                channelWatcher: {
                    client: {
                        host: process.env.CHANNEL_WATCHER_CLIENT_HOST,
                        port: process.env.CHANNEL_WATCHER_CLIENT_PORT,
                        mocksVersion: process.env.CHANNEL_WATCHER_CLIENT_MOCKVERSION,
                        useMocks: process.env.CHANNEL_WATCHER_CLIENT_USE_MOCKS,
                        path: process.env.CHANNEL_WATCHER_CLIENT_PATH,
                        version: process.env.CHANNEL_WATCHER_CLIENT_VERSION
                    },
                    state: process.env.CHANNEL_WATCHER_STATE,
                    interval: process.env.CHANNEL_WATCHER_INTERVAL
                }
            };

            let resIinitProc = await this.assistantProcessor.init(configs);

            if (resIinitProc.status != ResultStatus.OK) {
                throw resIinitProc;
            }

            this._logger.info(`${resIinitProc.toString()}`);

            return resIinitProc;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //initProcessorFlowFunc
}


module.exports.Core = Core;