//cores module
'use strict';


const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    AppLoggerClass
} = __UTILS;

const {
    waterfall,
    queue
} = require('async');
const Many = require('extends-classes');

const {
    PaymentsSystemClass,
    DEPOSIT_STATUS,
    CURRENCY_LIST
} = require('../payments_system_class');

const math = require('mathjs');
const uniqid = require('uniqid');
const moment = require('moment');

const FX_RATE = {
    "rub": {
        "currency": "rub",
        "usd": 0.015571497308466692,
        "uah": 0.38266,
        "eur": 0.01397,
        "rub": 1.0
    },
    "qiwi-rub": {
        "currency": "rub",
        "usd": 0.015571497308466692,
        //"uah": 0.3561444,
        "uah": 0.38266,
        "eur": 0.01397,
        "rub": 1.0
    },
    "uah": {
        "currency": "uah",
        "usd": 0.04092889267945007,
        "uah": 1.0,
        "btc": 3.81e-06,
        "eur": 0.037106883185824814,
        "rub": 2.6132859457481836
    },
}

const SUBSCRIBE_ORDER_STATUS = Object.freeze({
    PENDING: 'pending',
    ERROR: 'error',
    CANCEL: 'cancel',
    DONE: 'done'
});

const SUBSCRIBE_STATUS_TRANSLATE = Object.freeze({
    ru: {
        pnding: 'В процессе 🕕',
        error: 'Ошибка ⚠️',
        cancel: 'Отменен ❌',
        done: 'Выполнен ✅'
    },
    get: function (code, key) {
        let lang = this[code];

        if (lang) {
            return lang[key];
        }
    }
});

const SUBSCRIBE_REQUEST_EVENTS = Object.freeze({
    CREATED: 'created',
    PENDING: 'pending',
    ERROR: 'error',
    ORDER_CANCEL: 'cancel',
    ORDER_DONE: 'done',
    EXPIRED: 'expired',
    GRANDED: 'granded',
    check: function (status = '') {
        status.toLowerCase();

        for (let key in this) {
            if (this[key] === status) {
                return status;
            }
        }

        return null;
    },
    list: function () {
        return Object.values(this).filter(a => typeof a == 'string');
    }
});

const SUBSCRIBE_STATUS = Object.freeze({
    CREATED: 'created',
    WAITING_ORDER: 'waiting_order',
    DONE_ORDER: 'done_order',
    GRANDED: 'granded',
    //ready to new
    ERROR: 'error',
    CANCELED: 'canceled',
    EXPIRED: 'expired',
});

const WATCHER_STATE = Object.freeze({
    ACTIVE: 1,
    DISACTIVE: 0
});

const DEFAULT_INTEVAL_VALUE_MS = 5000;

const EvnetEmiter = require('events');


class SubcribeChannelClass {
    constructor() {
        this._db = null;
        this._logger = new AppLoggerClass('SubcribeChannelClass', process.env.USER_SUBSCRIBE_PROCESSOR_CLASS_LOG_LEVEL);
    }

    async init(db) {
        try {
            if (db == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'DB link not set');
            }

            this._db = db;

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }

    }

    async getActiveList(filter) {
        try {
            let list = await this._db.models.Channel.getAllActive(filter);

            if (list.status != ResultStatus.OK) {
                throw list;
            }

            return list;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getActiveList

    async getOneByTlgId({
        tlgId
    }) {
        try {
            let channel = await this._db.models.Channel.getOneByTlgId({
                tlgId
            });

            if (channel.status != ResultStatus.OK) {
                throw channel;
            }

            return channel;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getActiveList

    async getOneById({
        id
    }) {
        try {
            let channel = await this._db.models.Channel.getOneById({
                id
            });

            if (channel.status != ResultStatus.OK) {
                throw channel;
            }

            return channel;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getActiveList
}

class GrandSubscribeWatcherClass {
    constructor() {
        this._db = null;

        this._processor = null;
        this._watchTimer = null;
        this._logger = new AppLoggerClass('GrandSubscribeWatcherClass', process.env.GRAND_SUBSCRIBE_WATCHER_LOG_LEVEL);

        this.props = {
            interval: DEFAULT_INTEVAL_VALUE_MS,
            state: WATCHER_STATE.ACTIVE
        }
    }

    async init(processor, {
        interval,
        state
    }) {
        try {
            if (processor == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'HAve to set processor link');
            }

            this._db = processor._db;

            this._processor = processor;

            this.props.interval = interval || this.props.interval;
            this.props.state = state || this.props.state;


            if (this.props.state == WATCHER_STATE.ACTIVE) {
                this._startNextCycle();
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Init and Start with state:[${this.props.state}] cycle.`);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initPyamentsSytemFlowFunc

    async _startNextCycle() {
        this._logger.debug('Start timer');
        this._watchTimer = setTimeout(this._watcherHandler.bind(this), this.props.interval)
    }

    async _watcherHandler() {
        try {
            let snaphot = Date.now();

            this._logger.debug(`Start watch cycle: [${snaphot}]`);

            //read all deposit with staus pending;
            let subscribes = await this._db.models.Subcribe.getWithStatus([
                SUBSCRIBE_STATUS.CREATED
            ]);

            if (subscribes.status == ResultStatus.ERROR_NOT_FOUND) {
                this._logger.debug(subscribes.toString());
            } else if (subscribes.status != ResultStatus.OK) {
                throw subscribes;
            } else {
                //check status 
                subscribes = subscribes.data;

                this._logger.info(`Pull [${subscribes.length}] subscribes to processing`);

                for (let subcribe of subscribes) {
                    //get fep staus on kuna
                    this._logger.info(`Update subcribe status for id:[${subcribe.id}] channelId[${subcribe.channelId}]`);

                    let updBody = {
                        status: SUBSCRIBE_STATUS.GRANDED,
                        id: subcribe.id
                    }

                    let updSt = await this._db.models.Subcribe.updateOne(updBody);

                    if (updSt.status != ResultStatus.OK) {
                        throw updSt;
                    }

                    this._logger.info(updSt.toString());

                    let channelInfo = await this._processor._channelsList.getOneById({
                        id: subcribe.channelId
                    });

                    if (channelInfo.status != ResultStatus.OK) {
                        throw channelInfo;
                    }



                    //emit update
                    let event = {
                        type: SUBSCRIBE_REQUEST_EVENTS.GRANDED,
                        data: {
                            ...updSt.data,
                            channelInfo: channelInfo.data
                        }
                    };

                    this._processor._makeEvent(event);
                    //get next
                } //if new status
            }

            this._startNextCycle();

            this._logger.debug(`End watch cycle: [${snaphot}]`);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            this._startNextCycle();
        }
    }
}


class PublicMethodsClass {
    async getChannels(filter) {
        try {
            let list = await this._channelsList.getActiveList(filter);

            if (list.status != ResultStatus.OK) {
                throw list;
            }

            this._logger.info(list.toString());


            return list;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getChannel(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Argement is null');
            }

            const {
                id
            } = args;

            let channelRequest = await this._channelsList.getOneById({
                id
            });

            if (channelRequest.status != ResultStatus.OK) {
                throw channelRequest;
            }

            this._logger.info(channelRequest.toString());

            return channelRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    //wrapper 
    async getPaymentCurrencies() {
        try {
            let paymentCurrencies = await this._paymentsSystem.getSourceCurrencyList();

            if (paymentCurrencies.status != ResultStatus.OK) {
                throw paymentCurrencies;
            }

            return paymentCurrencies;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getPaymentCurrencies

    async getActiveSourceCurrencyList() {
        try {
            let paymentCurrencies = await this._paymentsSystem.getActiveSourceCurrencyList();

            if (paymentCurrencies.status != ResultStatus.OK) {
                throw paymentCurrencies;
            }

            return paymentCurrencies;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getPaymentCurrencies

    async getActiveMethods(currency) {
        try {
            let methList = await this._paymentsSystem.getActiveMethods(currency);

            if (methList.status != ResultStatus.OK) {
                throw methList;
            }

            return methList;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getActiveMethods

    async chekcSubcribeByTlg(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args should not be an object');
            }

            const {
                userId,
                channelTlgId
            } = args;

            let chInfo = await this._channelsList.getOneByTlgId({
                tlgId: channelTlgId
            });

            if (chInfo.status != ResultStatus.OK) {
                throw chInfo;
            }

            let checkInSubscibe = await this._db.models.Subcribe.getByUserAndChannelWithStatuses({
                userId,
                channelId: chInfo.data.id,
                statuses: [SUBSCRIBE_STATUS.CREATED,
                    SUBSCRIBE_STATUS.GRANDED,
                    SUBSCRIBE_STATUS.WAITING_ORDER,
                    SUBSCRIBE_STATUS.DONE_ORDER
                ]
            });

            if (checkInSubscibe.status != ResultStatus.OK) {
                throw checkInSubscibe;
            }



            if (checkInSubscibe.data.length > 0) {
                const subscribe = checkInSubscibe.data[0];

                subscribe.isGranded = subscribe.status == SUBSCRIBE_STATUS.GRANDED;

                const rsp = new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED,
                    `User userId:[${userId}] already have subscribe:[${subscribe.id}] with status:[${subscribe.status}]`, {
                        channel: chInfo.data,
                        subscribe
                    });

                return rsp;
            }

            let rsp = new BasicResponse(ResultStatus.OK, 'Subsribe`s check done sucsessfully.', {
                channel: chInfo.data,
                subscribe: checkInSubscibe.data
            });

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async getDepositStatuses() {
        try {
            return new BasicResponse(ResultStatus.OK, 'Deposit available statuses', DEPOSIT_STATUS.list());
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async updateOrderInfo(updateBody) {
        try {
            if (updateBody == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'update body is null, have to be an object');
            }

            const {
                orderId,
                description,
                feeAtOwnExpense,
                isFree,
                price,
                currency,
                status
            } = updateBody;

            this._logger.info(`Try update order:[${orderId}] description to [${description}]`);

            //get order info from _db
            let updateOrderReq = await this._db.models.SubscribeOrder.updateOneById({
                id: orderId,
                description,
                feeAtOwnExpense,
                isFree,
                price,
                currency,
                status
            });

            updateOrderReq.isOK();

            this._logger.info(updateOrderReq);

            return updateOrderReq;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async updateDepositStatus(updateBody) {
        try {
            if (updateBody == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'update body is null, have to be an object');
            }

            let {
                status,
                orderId,
                depositId,
                silent,
            } = updateBody;

            let checkStatus = DEPOSIT_STATUS.check(status);

            if (checkStatus == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Invalid status value:[${status}`, DEPOSIT_STATUS);
            }

            if (depositId == null) {
                this._logger.info(`Try update deposit status to [${status}], for odrder id [${orderId}]`);

                //get order info from _db
                let orderInfo = await this._db.models.SubscribeOrder.getOrderInfo({
                    id: orderId
                });

                if (orderInfo.status != ResultStatus.OK) {
                    throw orderInfo;
                }

                depositId = orderInfo.data.depositId;

                if (orderInfo.data.isFree == true) {
                    throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Order with id:[${orderId}] has isFree flag set true. In this case it hasn't deposit.`);
                }
            }

            this._logger.info(`Try update deposit id:[${depositId}] status to [${status}]`);

            //update status
            let depoInfo = await this._paymentsSystem.updateDepositStatus({
                depositId,
                status,
                silent
            });

            depoInfo.isOK();

            this._logger.info(depoInfo);

            //return depo info
            return depoInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
    /**
     * 
     * subscribe ordres  
     */

    async getSubscribesStatuses() {
        try {
            return new BasicResponse(ResultStatus.OK, 'Get subscriber orders available statuses', SUBSCRIBE_REQUEST_EVENTS.list());
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    /**
     * @param {array} filter filtering by status
     * @param {int} offset 
     * @param {int} limit 
     */
    async getAllSubscribesOrdres(params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            //fetch data from db

            let subscribesInfo = await this._db.models.SubscribeOrder.getAll(params);

            if (subscribesInfo.status != ResultStatus.OK) {
                throw subscribesInfo;
            }

            return subscribesInfo;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getAllSubscribes

    /**
     * 
     * @param {*} param0 
     */
    //preapring data for make subscribe
    async makeFreeSubscribeRequestChannel({
        userId,
        channelTlgId
    }) {
        try {
            let isNull = [
                channelTlgId,
                userId
            ].some(a => a == null);

            if (isNull == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Invalid incoming values');
            }

            let chInfo = await this._channelsList.getOneByTlgId({
                tlgId: channelTlgId
            });

            if (chInfo.status != ResultStatus.OK) {
                throw chInfo;
            }


            // \todo
            //check max subscribes fo limit

            chInfo = chInfo.data;

            this._logger.info(`Make subscribe request for channnel:[${channelTlgId}]`);
            this._logger.info(`currency:[free]`);
            this._logger.info(`serviceAlias:[free]`);


            let invoice = {
                channelId: chInfo.id,
                channelTlgId,
                userId,
                gateWay: 'telegram',
                priceCurrency: chInfo.currency, //price currecny
                feeAtOwnExpense: chInfo.feeAtOwnExpense,
                isFree: chInfo.isFree,
                amount: chInfo.price,
                price: chInfo.price,
                fee: 0,
                fxRate: 0,
                paymentCurrency: null,
                serviceMethod: null,
                serviceAlias: null,
                serviceCode: null,
                serviceSwitchMethod: null,
                recieptCurrency: null,
                publicKey: null,
                fieldsList: null,
                terminalCurrency: null,
            };

            this._logger.info(`Chanel payment currency:[${chInfo.currency}], method currency:[free]`);

            let rsp = new BasicResponse(ResultStatus.OK, 'Create invoice done sucsessfully.', invoice);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //

    async makeSubscribeRequestChannnel({
        currency,
        serviceAlias,
        channelTlgId,
        userId
    }) {
        try {
            let isNull = [
                currency,
                serviceAlias,
                channelTlgId
            ].some(a => a == null);

            if (isNull == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Invalid incoming values');
            }

            let chInfo = await this._channelsList.getOneByTlgId({
                tlgId: channelTlgId
            });

            if (chInfo.status != ResultStatus.OK) {
                throw chInfo;
            }

            chInfo = chInfo.data;

            this._logger.info(`Make subscribe request for channnel:[${channelTlgId}]`);
            this._logger.info(`currency:[${currency}]`);
            this._logger.info(`serviceAlias:[${serviceAlias}]`);

            let serviceInfo = this._paymentsSystem.getService(serviceAlias);

            if (serviceInfo.status != ResultStatus.OK) {
                throw serviceInfo;
            }

            serviceInfo = serviceInfo.data;

            const {
                isManual,
                terminalFee,
                terminalStaticFee,
                publicKey,
                decimal,
                terminalCurrency,
                recieptCurrency,
                switchMethod,
                fields = []
            } = serviceInfo;
            // //make list for check

            let fieldsList = fields.reduce((obj, item) => {
                obj[item.key] = item
                return obj
            }, {});

            let invoice = {
                isManual,
                channelId: chInfo.id,
                channelTlgId,
                userId,
                gateWay: 'telegram',
                paymentCurrency: currency,
                priceCurrency: chInfo.currency, //price currecny
                feeAtOwnExpense: chInfo.feeAtOwnExpense,
                isFree: chInfo.isFree,
                price: chInfo.price,
                serviceMethod: serviceInfo.method,
                serviceAlias,
                serviceCode: serviceInfo.code,
                serviceSwitchMethod: switchMethod,
                recieptCurrency,
                publicKey: publicKey,
                fieldsList,
                terminalCurrency,
                fee: 0,
                amount: chInfo.price,
                fxRate: 0
            };

            this._logger.info(`Pull payment service info for:[${serviceAlias}]`);
            this._logger.info(`terminalFee:[${terminalFee}]`);
            this._logger.info(`terminalStaticFee:[${terminalStaticFee}]`);
            this._logger.info(`isManual:[${isManual}]`);


            //if we have same currncy havent convert to uah

            this._logger.info(`Chanel payment currency:[${chInfo.currency}], method currency:[${currency}]`);

            /** 
             * make total amount
             */
            //get fx_rate
            let priceCurrency_ = invoice.serviceMethod == "qiwi" ? `qiwi-${invoice.priceCurrency.toLowerCase()}` : invoice.priceCurrency.toLowerCase();
            let terminalCurrency_ = invoice.terminalCurrency.toLowerCase();

            invoice.fxRate = FX_RATE[priceCurrency_][terminalCurrency_];

            //convert by fx rate
            invoice.amount = chInfo.price * invoice.fxRate;
            invoice.amount = math.round(invoice.amount, decimal);


            invoice.fee = terminalFee > 0 ? (invoice.amount * terminalFee) : 0;

            //apply static fee
            invoice.fee = terminalStaticFee > 0 ? (invoice.fee + terminalStaticFee) : invoice.fee;

            invoice.fee = math.round(invoice.fee, decimal);

            if (chInfo.feeAtOwnExpense == true) {
                invoice.amount = invoice.amount - invoice.fee;
                invoice.total = invoice.price - invoice.fee;
            } else {
                invoice.total = invoice.amount + invoice.fee;
            }


            //round amout to decimal currency
            invoice.total = math.round(invoice.total, decimal);

            /**
             * make description
             */

            let descrInvoice = [];

            if (invoice.priceCurrency != invoice.recieptCurrency) {
                descrInvoice.push(
                    `Сумма оплаты будет переведена в <b>${invoice.terminalCurrency}</b>, что составит <i>${invoice.amount}</i> <b>${invoice.terminalCurrency}</b>`,
                );
            }

            descrInvoice.push(
                `Комисия платежного терминала <i>${invoice.fee}</i> <b>${invoice.terminalCurrency}</b>`
            );

            descrInvoice.push(
                `Общая сумма снятия <i>${invoice.total}</i> <b>${invoice.terminalCurrency}</b>`
            );


            invoice.descr = [
                `Cтоимость подписки <i>${invoice.price}</i> <b>${invoice.priceCurrency}</b>`,
                ...descrInvoice
            ].join('\n');

            //

            invoice.disclamer = serviceInfo.disclamer(chInfo.feeAtOwnExpense);
            //create payment invoice

            let rsp = new BasicResponse(ResultStatus.OK, 'Create invoice done sucsessfully.', invoice);

            this._logger.info(rsp.toString());
            this._logger.info(`amount:[${invoice.amount}]`);
            this._logger.info(`fee:[${invoice.fee}]`);
            this._logger.info(`amount:[${invoice.amount}]`);
            this._logger.info(`total:[${invoice.total}]`);


            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //makeSubscribeRequestChannnel

    async makeFreeSubscribeOrder(invoice) {
        try {
            //after success creating invoice, create local order for subscribe
            let orderBody = {
                trackId: invoice.trackId,
                channelId: invoice.channelId,
                status: SUBSCRIBE_ORDER_STATUS.DONE,
                depositId: null,
                userId: invoice.userId,
                invoker: invoice.gateWay,
                sourceId: invoice.channelTlgId,
                feeAtOwnExpense: invoice.feeAtOwnExpense,
                isFree: invoice.isFree,
                price: invoice.price,
                currency: invoice.priceCurrency,
                fxRate: invoice.fxRate,
                description: `subscribe ${invoice.channelTlgId} free`,
                timeStamp: Date.now(),
                sn: `uso_${uniqid()}`,
            };

            let description = [
                `trackId:[${orderBody.trackId}]`,
                `sn:[${orderBody.sn}]`,
                `channelId:[${orderBody.channelId}]`,
                `status:[${orderBody.status}]`,
                `depositId:[${orderBody.depositId}]`,
                `userId:[${orderBody.userId}]`,
                `invoker:[${orderBody.invoker}]`,
                `sourceId:[${orderBody.sourceId}]`,
                `feeAtOwnExpense:[${orderBody.feeAtOwnExpense}]`,
                `isFree:[${orderBody.isFree}]`,
                `price:[${orderBody.price}]`,
                `currency:[${orderBody.currency}]`,
                `fxRate:[${orderBody.fxRate}]`,
                `description:[${orderBody.description}]`,
                `timeStamp:[${orderBody.timeStamp}]`,
            ].join(' ');

            this._logger.info(`Start make order ${description}`);

            /**
             * second stage stage create order
             */
            let order = await this._db.models.SubscribeOrder.createOne(orderBody);

            if (order.status != ResultStatus.OK) {
                throw order;
            }

            order = order.data;

            console.log('Order info');
            console.log(order);

            let channelInfo = await this._channelsList.getOneById({
                id: order.channelId
            });

            if (channelInfo.status != ResultStatus.OK) {
                throw channelInfo;
            }

            //make event
            let event = {
                type: SUBSCRIBE_REQUEST_EVENTS.ORDER_DONE,
                data: {
                    orderInfo: {
                        ...order
                    },
                    depositInfo: null,
                    channelInfo: {
                        ...channelInfo.data
                    }
                }
            };


            this._makeEvent(event);

            let makeSubscribe = await this._makeSubcribe({
                orderId: order.id,
                channelId: order.channelId,
                status: order.status,
                userId: order.userId,
                isFree: order.isFree,
                duration: channelInfo.data.subscriptionDuration,
                created: null
            });

            if (makeSubscribe.status != ResultStatus.OK) {
                throw makeSubscribe;
            }



            let rsp = new BasicResponse(ResultStatus.OK, 'Create order done succssfully.', {
                depositInfo: null,
                orderInfo: order
            });

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //

    async makeSubscribeOrder(invoice) {
        try {
            if (invoice == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Incoming Invoice have to be an Object');
            }

            const {
                isManual
            } = invoice;

            this._logger.info(`Start make deposit`);

            /**
             * first step making deposit
             */

            let deposit = new BasicResponse(ResultStatus.ERROR_NOT_READY, 'Deposit not init by method.');

            //heve to do it in payment system
            if (isManual === true) {
                deposit = await this._paymentsSystem.makeManualDeposit(invoice);
            } else {
                deposit = await this._paymentsSystem.makeDeposit(invoice);
            }

            if (deposit.status != ResultStatus.OK) {
                throw deposit;
            }

            this._logger.info(deposit.toString());

            //reduce data
            deposit = deposit.data;

            console.log('Deposit info');

            console.log(deposit);

            let descrSFields = invoice.serviceFields != null ? JSON.stringify(invoice.serviceFields) : '';

            //after success creating invoice, create local order for subscribe
            let orderBody = {
                trackId: invoice.trackId,
                channelId: invoice.channelId,
                status: SUBSCRIBE_ORDER_STATUS.PENDING,
                depositId: deposit.id,
                userId: invoice.userId,
                invoker: invoice.gateWay,
                sourceId: invoice.channelTlgId,
                feeAtOwnExpense: invoice.feeAtOwnExpense,
                isFree: invoice.isFree,
                price: invoice.price,
                currency: invoice.priceCurrency,
                fxRate: invoice.fxRate,
                description: `subscribe for ${invoice.channelTlgId} | ${descrSFields}`,
                timeStamp: Date.now(),
                sn: `uso_${uniqid()}`,
            };

            let description = [
                `trackId:[${orderBody.trackId}]`,
                `sn:[${orderBody.sn}]`,
                `channelId:[${orderBody.channelId}]`,
                `status:[${orderBody.status}]`,
                `depositId:[${orderBody.depositId}]`,
                `userId:[${orderBody.userId}]`,
                `invoker:[${orderBody.invoker}]`,
                `sourceId:[${orderBody.sourceId}]`,
                `feeAtOwnExpense:[${orderBody.feeAtOwnExpense}]`,
                `isFree:[${orderBody.isFree}]`,
                `price:[${orderBody.price}]`,
                `currency:[${orderBody.currency}]`,
                `fxRate:[${orderBody.fxRate}]`,
                `description:[${orderBody.description}]`,
                `timeStamp:[${orderBody.timeStamp}]`
            ].join(' ');

            this._logger.info(`Start make order ${description}`);

            /**
             * second stage stage create order
             */
            let order = await this._db.models.SubscribeOrder.createOne(orderBody);

            if (order.status != ResultStatus.OK) {
                throw order;
            }

            order = order.data;

            console.log('Order info');
            console.log(order);

            let channelInfo = await this._channelsList.getOneById({
                id: order.channelId
            });

            if (channelInfo.status != ResultStatus.OK) {
                throw channelInfo;
            }

            //make event
            let event = {
                type: SUBSCRIBE_REQUEST_EVENTS.CREATED,
                data: {
                    orderInfo: {
                        ...order
                    },
                    depositInfo: {
                        ...deposit
                    },
                    channelInfo: {
                        ...channelInfo.data
                    }
                }
            };

            this._makeEvent(event);


            let rsp = new BasicResponse(ResultStatus.OK, 'Create order done succssfully.', {
                depositInfo: deposit,
                orderInfo: order
            });

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //
} //PublicMethodsClass

class PrivateMethodsClass {
    async _setPendingRequestHandler(depositInfo) {
        try {
            const {
                id,
                referenceId,
            } = depositInfo;

            this._logger.info(`Set pending staus to order by deposit id:[${id}] referenceId:[${referenceId}]`);

            //find order with refernce id;
            let order = await this._db.models.SubscribeOrder.updateOneByDepositId({
                depositId: id,
                status: SUBSCRIBE_ORDER_STATUS.PENDING
            });

            order.isOK();

            console.log(order);

            return order;
        } catch (e) {
            const error = new BasicResponseByError(e);

            if (error.staus == ResultStatus.ERROR_NOT_FOUND) {
                console.log('ERROR_NOT_FOUND order not found');
            }

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _finishSubscribeRequest(depositInfo) {
        try {
            const {
                amount,
                currency,
                id,
                referenceId,
                status,
            } = depositInfo;

            this._logger.info(`Grand subscribe for deposit id:[${id}] referenceId:[${referenceId} amount:[${amount}]]`);

            //find order with refernce id;

            let order = await this._db.models.SubscribeOrder.getOneByDepositId({
                depositId: id
            });

            let precheck = null;

            if (order.status != ResultStatus.OK) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Deposit id:[${id}] order status:[${status}]`);
            }

            order = order.data

            //check status
            if (status !== DEPOSIT_STATUS.DONE) {
                precheck = new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Deposit id:[${id}] order currency:[${order.currency}] not equal depo currency:[${currency}]`);
            }

            //check amount 
            // if (order.currency !== currency) {
            //     precheck = new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Deposit id:[${id}] order currency:[${order.currency}] not equal depo currency:[${currency}]`);
            // }

            // // //check amount 
            // if (depositInfo.amount == amount) {
            //     precheck = new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Deposit id:[${id}] order amount:[${order.currency}] not equal depo amount:[${currency}]`);
            // }

            if (precheck && precheck.status != ResultStatus.OK) {
                this._logger.warn(`Get error on pre check ${precheck.toString()}`);

                depositInfo.description = order.description + precheck.toString();

                return await this._cancelSubscribeRequest(depositInfo)
            }

            let doneBody = {
                depositId: id,
                status: SUBSCRIBE_ORDER_STATUS.DONE
            }

            let orderDone = await this._db.models.SubscribeOrder.updateOneByDepositId(doneBody);

            if (orderDone.status != ResultStatus.OK) {
                throw orderDone;
            }

            let channelInfo = await this._channelsList.getOneById({
                id: orderDone.data.channelId
            });

            if (channelInfo.status != ResultStatus.OK) {
                throw channelInfo;
            }


            //make event
            let event = {
                type: SUBSCRIBE_REQUEST_EVENTS.ORDER_DONE,
                data: {
                    orderInfo: {
                        ...orderDone.data
                    },
                    depositInfo: {
                        ...depositInfo
                    },
                    channelInfo: {
                        ...channelInfo.data
                    }
                }
            };


            //create subcribe
            let makeSubscribe = await this._makeSubcribe({
                orderId: orderDone.data.id,
                channelId: orderDone.data.channelId,
                status: orderDone.data.status,
                userId: orderDone.data.userId,
                isFree: orderDone.data.isFree,
                duration: channelInfo.data.subscriptionDuration,
                created: depositInfo.created
            });

            if (makeSubscribe.status != ResultStatus.OK) {
                throw makeSubscribe;
            }

            this._logger.info(makeSubscribe);

            this._makeEvent(event);

            let rsp = new BasicResponse(ResultStatus.OK, `Grand subscribe for deposit id:[${id}] referenceId:[${referenceId}] done successfully.`);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            if (error.staus == ResultStatus.ERROR_NOT_FOUND) {
                console.log('ERROR_NOT_FOUND order not found');
            }

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _cancelSubscribeRequest(depositInfo) {
        try {
            const {
                id,
                referenceId,
                status,
                description
            } = depositInfo;

            this._logger.info(`Discard subscribe for deposit id:[${id}] referenceId:[${referenceId}] staus:[${status}]`);

            //find order with refernce id;
            //if correct update staus and info
            let cancelBody = {
                depositId: id,
                status: SUBSCRIBE_ORDER_STATUS.CANCEL,
                description
            }

            let orderCnacel = await this._db.models.SubscribeOrder.updateOneByDepositId(cancelBody);

            if (orderCnacel.status != ResultStatus.OK) {
                throw orderCnacel;
            }

            let channelInfo = await this._channelsList.getOneById({
                id: orderCnacel.data.channelId
            });

            if (channelInfo.status != ResultStatus.OK) {
                throw channelInfo;
            }

            //make event
            let event = {
                type: SUBSCRIBE_REQUEST_EVENTS.ORDER_CANCEL,
                data: {
                    orderInfo: {
                        ...orderCnacel.data
                    },
                    depositInfo: {
                        ...depositInfo
                    },
                    channelInfo: {
                        ...channelInfo.data
                    },
                    cause: depositInfo.error
                }
            };

            this._makeEvent(event);

            let rsp = new BasicResponse(ResultStatus.OK, `Cancel subscribe for deposit id:[${id}] referenceId:[${referenceId}] done successfully.`);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            if (error.staus == ResultStatus.ERROR_NOT_FOUND) {
                console.log('ERROR_NOT_FOUND order not found');
            }

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _makeSubcribe(orderInfo) {
        try {
            if (orderInfo == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Null icoming data. Have to be an Object');
            }

            let {
                orderId,
                channelId,
                status,
                userId,
                isFree,
                duration,
                created
            } = orderInfo;


            if (status != SUBSCRIBE_ORDER_STATUS.DONE) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Order have to has staus done');
            }

            if (created == null) {
                created = Date.now();
            } else {
                created = created < 90000000000 ? created * 1000 : created;
            }

            const expireTimeUtc = await moment.utc(created).add(duration, 'h').valueOf();


            let makeSubscribe = await this._db.models.Subcribe.createOne({
                status: SUBSCRIBE_STATUS.CREATED,
                subscriptionDuration: duration,
                expireTimeUtc,
                userId,
                orderId,
                channelId,
                sn: `us_${uniqid()}`,
                isFree
            });

            if (makeSubscribe.status != ResultStatus.OK) {
                throw makeSubscribe;
            }

            this._logger.info(makeSubscribe.toString());


            return makeSubscribe;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _depositUpdateEventHandler(event) {
        try {
            let {
                type,
                data
            } = event;

            this._logger.info(`Recive event type:[${type}] status:[${data.status}]`);

            let handleRes = new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Somting going wrong. Any hadler take thsis staus:[${data.status}].`);

            switch (data.status) {
                case DEPOSIT_STATUS.DONE:
                    handleRes = await this._finishSubscribeRequest(data);
                    break;
                case DEPOSIT_STATUS.CANCEL:
                case DEPOSIT_STATUS.ERROR:
                    handleRes = await this._cancelSubscribeRequest(data);
                    break;

                case DEPOSIT_STATUS.UNKNOWN:
                case DEPOSIT_STATUS.PENDING:
                case DEPOSIT_STATUS.MANUAL_PENDING:
                case DEPOSIT_STATUS.PROCESS_PENDING:
                    //do noting
                    handleRes = await this._setPendingRequestHandler(data);
                    break;

                default:
                    console.error(`Recived unknown status:[${data.status}]`);
            }

            this._logger.info(handleRes.toString());

            return handleRes;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //_depositUpdateEventHandler

    _makeEvent({
        type,
        data
    }) {
        this.emit(type, {
            type,
            data
        });
    } //makeEvents

}

class UserSubscribeProcessorClass extends Many(EvnetEmiter, PublicMethodsClass, PrivateMethodsClass) {
    constructor() {
        super();

        this.props = {
            interval: DEFAULT_INTEVAL_VALUE_MS,
            state: WATCHER_STATE.ACTIVE
        }

        this._logger = new AppLoggerClass('UserSubscribeProcessorClass', process.env.USER_SUBSCRIBE_PROCESSOR_CLASS_LOG_LEVEL);

        this._db = null;
        this._channelsList = new SubcribeChannelClass();

        this._paymentsSystem = new PaymentsSystemClass();
        this._grandSubscribeWatcher = new GrandSubscribeWatcherClass();
    }

    async init(db, props = {}) {
        try {
            this._db = db;
            this.props.interval = props.interval || DEFAULT_INTEVAL_VALUE_MS;
            this.props.state = props.state || this.props.state;

            this.props.channelSubscribeWatcher = props.channelSubscribeWatcher;

            let initFlow = await this._initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //init

    /**
     * INIT saction
     */
    async _initFlow() {
        return new Promise((resolve) => {
            waterfall([
                this._initPyamentsSytemFlowFunc.bind(this),
                this._initChannelListFlowFunc.bind(this),
                this._intitGrandSubscribeWatcher.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this._logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init core done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    } //initFlow

    async _initPyamentsSytemFlowFunc() {
        try {
            this._logger.info('Start init Payment System');
            //init models

            let resInit = await this._paymentsSystem.init(this._db);

            if (resInit.status != ResultStatus.OK) {
                throw resInit;
            }

            this._logger.info(`${resInit.toString()}`);

            //subcribe to events

            let subSt = await this._paymentsSystem._subscribeOnDepositUpdateEvent(this._depositUpdateEventHandler.bind(this));

            if (subSt.status != ResultStatus.OK) {
                throw subSt;
            }

            this._logger.info(subSt.toString());

            return resInit;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initPyamentsSytemFlowFunc

    async _initChannelListFlowFunc() {
        try {
            this._logger.info('Start init channel list');
            //init models

            let resInit = await this._channelsList.init(this._db);

            if (resInit.status != ResultStatus.OK) {
                throw resInit;
            }

            this._logger.info(`${resInit.toString()}`);

            return resInit;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initChannelListFlowFunc

    async _intitGrandSubscribeWatcher() {
        try {
            this._logger.info('Start init grand processor');
            //init models

            let resInit = await this._grandSubscribeWatcher.init(this, {
                interval: process.env.GRAND_SUBSCRIBE_WATCHER_INTERVAL,
                state: process.env.GRAND_SUBSCRIBE_WATCHER_STATE
            });

            if (resInit.status != ResultStatus.OK) {
                throw resInit;
            }

            this._logger.info(`${resInit.toString()}`);

            return resInit;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_intitGrandSubscribeProc
}

exports.UserSubscribeProcessorClass = UserSubscribeProcessorClass;
exports.SUBSCRIBE_REQUEST_EVENTS = SUBSCRIBE_REQUEST_EVENTS;
exports.SUBSCRIBE_ORDER_STATUS = SUBSCRIBE_ORDER_STATUS;
exports.SUBSCRIBE_STATUS = SUBSCRIBE_STATUS;
exports.SUBSCRIBE_STATUS_TRANSLATE = SUBSCRIBE_STATUS_TRANSLATE;