'use strict';


const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../interfaces/basicResponse');

const {
    AppLoggerClass
} = __UTILS;

class BtnHandlerHadlersClass {
    constructor() {}

    async enter_simple_sub_private_channel(args, tracker) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null');
            }

            const {
                ctx,
                channelId,
                button
            } = args;

            ctx.session.channelId = channelId;
            ctx.session.button = button;
            ctx.session.tracker = tracker;


            await ctx.scene.enter('enter_simple_sub_private_channel');

            let rsp = new BasicResponse(ResultStatus.OK, 'Enter to `enter_simple_sub_private_channel` done successfully.');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //enter_simple_sub_private_channel

    async enter_simple_sub_free_channel(args, tracker) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null');
            }

            const {
                ctx,
                channelId,
                button
            } = args;

            ctx.session.channelId = channelId;
            ctx.session.button = button;
            ctx.session.tracker = tracker;

            await ctx.scene.enter('enter_simple_sub_free_channel');

            let rsp = new BasicResponse(ResultStatus.OK, 'Enter to `enter_simple_sub_free_channel` done successfully.');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //enter_simple_sub_free_channel

    async enter_sub_private_channel(args, tracker) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null');
            }

            const {
                ctx,
                channelId,
                button
            } = args;

            ctx.session.tracker = tracker;
            ctx.session.button = button;

            await ctx.scene.enter('enter_sub_private_channel');

            let rsp = new BasicResponse(ResultStatus.OK, 'Enter to `enter_sub_private_channel` done successfully.');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //enter_sub_private_channel

    async enter_sub_free_channel(args, tracker) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null');
            }

            const {
                ctx,
                channelId,
                button
            } = args;

            ctx.session.button = button;
            ctx.session.tracker = tracker;

            await ctx.scene.enter('enter_sub_free_channel');

            let rsp = new BasicResponse(ResultStatus.OK, 'Enter to `enter_sub_free_channel` done successfully.');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //enter_sub_free_channel

    async simple_free_without_captcha_wizard_class(args, tracker) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null');
            }

            const {
                ctx,
                channelId,
                button
            } = args;

            ctx.session.channelId = channelId;
            ctx.session.button = button;
            ctx.session.tracker = tracker;

            await ctx.scene.enter('simple_free_without_captcha_wizard_class');

            let rsp = new BasicResponse(ResultStatus.OK, 'Enter to `enter_sub_free_channel` done successfully.');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //enter_sub_free_channel
}

class BtnHandlerProcessorClass {
    constructor() {
        this._db = null;
        this._ddClient = null;

        this._logger = new AppLoggerClass('BtnHandlerProcessorClass', process.env.BTN_HANDLER_PROCESSOR_LOG_LEVEL);
        this._handlers = new BtnHandlerHadlersClass();
    }

    async init(db, ddClient, tracker) {
        try {
            if (db == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Database connection is null. Should be an object');
            }

            if (ddClient == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'DataDog client connection is null. Should be an object');
            }

            if (tracker == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Tracker client connection is null. Should be an object');
            }


            this._db = db;
            this._ddClient = ddClient;
            this._tracker = tracker;


            // refactor - have to assign all butons hadlers like contrillers
            let rsp = new BasicResponse(ResultStatus.OK, 'Init button proccessor done successfully');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async callButtonHandlerById(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null');
            }

            const {
                ctx,
                buttonId,
                params,
                context
            } = args;

            let button = await this._db.models.AssistantButton.getOne(buttonId);

            button.isOK();

            button = button.data;

            // if (button.isActive == false) {
            //     throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Button id:${buttonId} name:[${button.name}] has disactive state.`)
            // }

            //start wizzard with cuurent channel
            let callButtonRes = await this.call({
                func: button.handler,
                args: {
                    button,
                    ctx,
                    ...params
                },
                context
            });

            if (callButtonRes.status != ResultStatus.OK) {
                throw callButtonRes;
            }

            this._ddClient.increment(`${button.handler}`);

            this._logger.info(callButtonRes);

            callButtonRes.isOK();

            this._logger.info(callButtonRes);

            return callButtonRes;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //callButtonHandlerById

    async call({
        func,
        args,
        context
    }) {
        try {
            if (func == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Func is null', {});
            }

            if (this._handlers[func] == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `No such method:[${func}]`, {});
            }

            return await this._handlers[func].bind(context || this, args, this._tracker)();
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
}

exports.BtnHandlerProcessorClass = BtnHandlerProcessorClass;