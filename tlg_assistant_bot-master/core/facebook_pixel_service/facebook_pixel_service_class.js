//cores modules
const {
    waterfall,
    queue
} = require('async');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;

const {
    AppLoggerClass
} = __UTILS;

const bizSdk = require('facebook-nodejs-business-sdk');

const {
    TRACK_STATE
} = require('../wizzards_processor/wizzards_types');

const {
    FacebookAdsApi,
    ServerEvent,
    EventRequest,
    UserData,
    CustomData,
    Content
} = bizSdk;

const DEF_PARALLEL_LIMIT = 1;
const puppeteer = require('puppeteer');

class FacebookPixelServicePropsClass {
    constructor() {
        this._values = {
            parallelLimit: 1,
            accessToken: null
        };

    }

    get parallelLimit() {
        return this._values.parallelLimit;
    }

    get accessToken() {
        return this._values.accessToken;
    }

    get values() {
        return this._values;
    }


    apply(props = {}) {
        for (const key in props) {
            if (this._values.hasOwnProperty(key) == true) {
                this._values[key] = props[key];
            }
        }

        return this._values;
    }
}

class FacebookPixelServiceClass {
    constructor() {
        this._logger = new AppLoggerClass('FacebookPixelServiceClass', process.env.FACEBOOK_PIXEL_SERVICE_LOG_LEVEL);

        this._client = null;
        this._props = new FacebookPixelServicePropsClass();

        this.queue = null;
    }

    async init(configs) {
        try {
            if (configs == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Wrong incoming configs.');
            }

            this._logger.info(`parallelLimit: ${configs.parallelLimit}`);

            this._props.apply(configs);

            let initFlow = await this.initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            return new BasicResponse(ResultStatus.OK, 'Init cFacebookPixelServiceClass done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //init

    _reduceTrackEventToPixelFrontEvent(track = {}) {
        const {
            status,
            stage,
            error,
            userId,
            tokenId,
            buttonId,
            subscribeId,
            sessionId,
            isNewUser,
            channelId,
            pixelId,
        } = track;


        switch (status) {
            case TRACK_STATE.CREATED:
            case TRACK_STATE.DONE_ERROR:
            case TRACK_STATE.WAITING_ORDER:
            case TRACK_STATE.DONE_OK:

                return {
                    func: 'trackCustom',
                        pixelId: pixelId,
                        event: status,
                        body: JSON.stringify(track)
                }
                break
        }
    }


    async initFlow() {
        return new Promise((resolve) => {
            waterfall([
                this._initQueueFlowFunc.bind(this),
                this._initApiClientFlowFunc.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this._logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init actions processor done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            });
        });
    } //initFlow

    async _sendEventToOrign(eventBody = {}) {
        try {
            const {
                func,
                event,
                body,
                pixelId,
                options = {}
            } = eventBody;

            const port = process.env.HTTP_PORT || 9001;

            const myURL = new URL(`http://113631ce.ngrok.io/facebook/send_event.html`);

            myURL.searchParams.set('func', func);
            myURL.searchParams.set('event', event);
            myURL.searchParams.set('body', body);
            myURL.searchParams.set('options', options);
            myURL.searchParams.set('pixelId', pixelId);


            //https://medium.com/@carlfredrik.hero/how-to-write-es2015-es6-code-for-phantomjs-using-webpack-7911c7511b41
            //promisifiy get url by browser
            const sendEvent = (actionUrl) => {
                return new Promise(async resolve => {
                    console.log('lanch')
                    const browser = await puppeteer.launch({
                        headless: false,
                        ignoreHTTPSErrors: true,
                        args: [
                            "--enable-cookie-deprecation-messages",
                            "--disable-web-security"
                        ]
                    });

                    const page = await browser.newPage();

                    // await page.setUserAgent(
                    //     "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36"
                    // );

                    console.log('actionUrl', actionUrl)
                    await page.goto(actionUrl);

                    // await page.setExtraHTTPHeaders({
                    //     'referer': actionUrl
                    // });

                    // var cookie = [ // cookie exported by google chrome plugin editthiscookie
                    //     {
                    //         "domain": "https://facebook.com/",
                    //         "expirationDate": 999999999,
                    //         "hostOnly": false,
                    //         "httpOnly": false,
                    //         "name": "key",
                    //         "path": "/",
                    //         "sameSite": "no_restriction",
                    //         "secure": false,
                    //         "session": false,
                    //         "storeId": "0",
                    //         "value": "value!",
                    //         "id": 1
                    //     }
                    // ]


                    // await page.setCookie(...cookie)

                    page.on('dialog', async dialog => {
                        console.log('dialog', dialog.message())
                        await dialog.dismiss()
                    })

                    console.log('goto');

                    await page.evaluate(() => {
                        // Make the evaluate callback wait until the export is done
                        return new Promise((resolve, reject) => {

                            // Call the export on the chart

                            //event();

                            // Wait for the download to get done
                            setTimeout(() => {
                                resolve();
                            }, 100000);
                        });
                    });

                    console.log('evualate');

                    await browser.close();

                    resolve();
                })
            }

            //set urls
            await sendEvent(myURL.href);

            const rsp = new BasicResponse(ResultStatus.OK, `Send func:[${func}] event:[${event}] body:[${body}] done successfully`, eventBody);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    }

    async _initApiClientFlowFunc() {
        try {
            this._logger.info(`Init facobox API client`);

            this._client = FacebookAdsApi.init(this._props.accessToken);

            const rsp = new BasicResponse(ResultStatus.OK, 'Init facobox API client done');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            throw error;
        }
    } //_initApiClientFlowFunc


    async _initQueueFlowFunc() {
        try {
            this._logger.info('Start');

            let {
                parallelLimit = 1,
            } = this._props;

            this._logger.info(`parallelLimit:[${parallelLimit}]`);

            // create a queue object with concurrency 2
            this.queue = queue(async (task) => {
                await this._queuFrontHandlerByCb(task);
            }, parallelLimit);

            // assign a callback
            this.queue.drain(() => {
                this._logger.info('Action queue - all items have been processed');
            });
            // // or await the end
            // await this.queue.drain()

            // assign an error callback
            this.queue.error((err, task) => {
                this._logger.error(`Action queue - task experienced an error. ${err.message}`);
            });

            let rsp = new BasicResponse(ResultStatus.OK, 'Init queue done successfully');

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    }

    async _reduceTarckToFacebookEvent() {

    }


    async _queuFrontHandlerByCb(track) {
        try {
            //get time 
            if (track == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Null track');
            }

            let descrBody = [
                `status:[${track.status}],`,
            ].join(' ');

            this._logger.info(`Start process action for track ${descrBody}`);

            const serverEvent = this._reduceTrackEventToPixelFrontEvent(track);

            const sendres = await this._sendEventToOrign(serverEvent);

            this._logger.info(sendres);

            return sendres;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error);

            return (error);
        }
    } //queuHandlerByCb


    tracksEventHandler(track, resolve) {
        try {
            if (track == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'track should be an object');
            }

            this.queue.push({
                track,
                cb: function (result) {
                    resolve(result);
                }
            });

            const rsp = new BasicResponse(ResultStatus.OK, 'Track push to queue success');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
} //ActionsProcessorClass

exports.FacebookPixelServiceClass = FacebookPixelServiceClass;
exports.FacebookPixelServicePropsClass = FacebookPixelServicePropsClass;