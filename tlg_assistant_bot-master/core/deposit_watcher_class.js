//cores modules
const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');

const {
    AppLoggerClass
} = __UTILS;

const {
    waterfall,
    queue
} = require('async');

const DEFAULT_INTEVAL_VALUE_MS = 5000;

const EventEmitter = require('events');

const EVENTS_LIST = Object.freeze({
    START: 'start_watch',
    STOP: 'stop_watch',
    START_CYCLE: 'start_cycle',
    STOP_CYCLE: 'stop_cycle',
    UPDATE: 'update'
});



const {
    delay
} = require('../utils/helpers/helpers');


const DEPOSIT_STAUTUS = Object.freeze({
    PENDING: 'pending',
    MANUAL_PENDING: 'manual_pending',
    PROCESS_PENDING: 'process_pending',
    UNKNOWN: 'unknown',
    DONE: 'done',
    CANCEL: 'canceled',
    ERROR: 'error',
    check: function (status = '') {
        status.toLowerCase();

        for (let key in this) {
            if (this[key] === status) {
                return status;
            }
        }

        return null;
    },
    list: function () {
        return Object.values(this).filter(a => typeof a == 'string');
    }
});

const WATCHER_STATE = Object.freeze({
    ACTIVE: 1,
    DISACTIVE: 0
});

class KunaCurrenciesClass {
    constructor(curr) {
        this._currencies = [...curr];
    }

    getByCode(code) {
        if (code) {
            code = code.toString();

            let cur = this._currencies.filter((c) => c.code == code);

            if (cur.length > 0) {
                return cur[0];
            }
        }
    }

    getById(id) {
        if (id) {
            let cur = this._currencies.filter((c) => c.id == id);

            if (cur.length > 0) {
                return cur[0];
            }
        }
    }
}


class DepositWatcherClass extends EventEmitter {
    constructor() {
        super();
        this._logger = new AppLoggerClass('DepositWatcherClass', process.env.DEPOSIT_WATCHER_CLASS_LOG_LEVEL);

        this._watchTimer = null;
        this._currencies = null;

        this.props = {
            interval: null,
            state: WATCHER_STATE.DISACTIVE
        }
    }

    async init(db, client, {
        interval,
        state
    }) {
        try {
            this._db = db;
            this._client = client;

            this._logger.info('Start init.')

            this.props.interval = interval || DEFAULT_INTEVAL_VALUE_MS;
            this.props.state = state || this.props.state;


            let initFlow = await this._initFlow();

            if (initFlow.status != ResultStatus.OK) {
                throw initFlow;
            }

            if (process.env.CHECK_LOST_ACTIONS != null) {
                this.checkLostActionsTask();
            }

            this._logger.info(initFlow.toString());

            return initFlow;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //init

    async _initFlow() {
        return new Promise((resolve) => {
            if (this.props.state == WATCHER_STATE.DISACTIVE) {
                return resolve(new BasicResponse(ResultStatus.OK, 'Skip init wather case it disable'));
            }

            waterfall([
                this._initWatcherTimerFlowFunc.bind(this),
                this._initFetchCurrenciesFlowFunc.bind(this)
            ], (error, result) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (result.status != ResultStatus.OK) {
                        throw result;
                    }

                    this._logger.info('Flow done successfully');

                    resolve(new BasicResponse(ResultStatus.OK, 'Init core done successfully.'));
                } catch (e) {
                    let error = new BasicResponseByError(e);

                    this._logger.error(error.toString());

                    resolve(error);
                }
            })
        });
    } //initFlow

    async _initWatcherTimerFlowFunc() {
        try {

            if (this.props.state == WATCHER_STATE.ACTIVE) {
                this._startNextCycle();
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Init and Start with state:[${this.props.state}] cycle.`);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initPyamentsSytemFlowFunc

    async _initFetchCurrenciesFlowFunc() {
        try {
            let currencies = await this._client.getCurrencies();

            if (currencies.status != ResultStatus.OK) {
                throw currencies
            }

            this._currencies = new KunaCurrenciesClass(currencies.data);

            let rsp = new BasicResponse(ResultStatus.OK, `Init currencies done.`);

            this._logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            throw error;
        }
    } //_initPyamentsSytemFlowFunc


    async _startNextCycle() {
        this._logger.debug('Start timer');
        this._watchTimer = setTimeout(this._watcherHandler.bind(this), this.props.interval)
    }

    async _watcherHandler() {
        try {
            let snaphot = Date.now();

            this.emit(EVENTS_LIST.START_CYCLE, {
                snaphot
            });

            this._logger.debug(`Start watch cycle: [${snaphot}]`);

            //read all deposit with staus pending;
            let deposits = await this._db.models.OrderDeposit.getWithStatus([
                DEPOSIT_STAUTUS.PENDING,
                DEPOSIT_STAUTUS.PROCESS_PENDING
            ]);

            if (deposits.status == ResultStatus.ERROR_NOT_FOUND) {
                this._logger.debug(deposits.toString());
            } else if (deposits.status != ResultStatus.OK) {
                throw deposits;
            } else {
                //check status 
                deposits = deposits.data;

                this._logger.info(`Pull [${deposits.length}] to processing`);

                for (let depo of deposits) {
                    //get fep staus on kuna

                    this._logger.info(`Get deposit details from kuna id:[${depo.id}] ref_id[${depo.referenceId}]`);

                    let kunaDepInfo = await this._client.getDepositDetails({
                        id: depo.referenceId
                    });

                    if (kunaDepInfo.status == ResultStatus.ERROR_NOT_FOUND) {
                        this._logger.warn(`Get deposit details from kuna id:[${depo.id}] ref_id[${depo.referenceId}]. ${kunaDepInfo.toString()}`);

                        let updSt = await this._db.models.OrderDeposit.updateOne({
                            id: depo.id,
                            status: DEPOSIT_STAUTUS.ERROR,
                            error: kunaDepInfo.toString()
                        });

                        if (updSt.status != ResultStatus.OK) {
                            this._logger.error(updSt.toString());
                        } else {
                            this._logger.info(updSt.toString());
                        }

                        //emit update
                        this.emit(EVENTS_LIST.UPDATE, {
                            event: EVENTS_LIST.UPDATE,
                            data: updSt.data
                        });

                        continue;
                    } else if (kunaDepInfo.status != ResultStatus.OK) {
                        this._logger.warn(kunaDepInfo.toString());

                        continue;
                    }

                    //** if we got normal data */

                    kunaDepInfo = kunaDepInfo.data;

                    if (
                        kunaDepInfo.status == DEPOSIT_STAUTUS.PENDING
                    ) {
                        this._logger.info(`Get deposit status:[${kunaDepInfo.status}] id:[${depo.id}] ref_id[${depo.referenceId}]`);

                        continue;
                    } //if staus pending

                    //if status isnot pending update
                    this._logger.info(`Get deposit new status:[${kunaDepInfo.status}] id:[${depo.id}] ref_id[${depo.referenceId}]`);


                    let precheck = new BasicResponse(ResultStatus.OK);

                    let curInfo = this._currencies.getById(kunaDepInfo.currency);

                    //make codes in same register
                    let curInfoCode = curInfo.code.toUpperCase();
                    let depoCurrency = depo.currency.toUpperCase();

                    //check currency and amount

                    if (curInfoCode !== depoCurrency) {
                        precheck = new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Deposit id:[${depo.id}] deposit currency:[${depo.currency}] not equal depo kuna currency:[${depInfo.currency}]`);
                    }

                    depo.amount = parseFloat(depo.amount);

                    //check amount
                    if (kunaDepInfo.amount != depo.amount) {
                        precheck = new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Deposit id:[${depo.id}] deposit amount:[${depo.amount}] not equal depo kuna amount:[${kunaDepInfo.amount}]`);
                    }

                    if (precheck && precheck.status != ResultStatus.OK) {
                        this._logger.warn(`Get error on pre check ${precheck.toString()}`);

                        kunaDepInfo.status = DEPOSIT_STAUTUS.ERROR;
                        kunaDepInfo.error = precheck.toString();
                    }

                    let updSt = await this._db.models.OrderDeposit.updateOne({
                        id: depo.id,
                        status: kunaDepInfo.status,
                        destination: kunaDepInfo.destination,
                        provider: kunaDepInfo.provider,
                        error: kunaDepInfo.error
                    });

                    if (updSt.status != ResultStatus.OK) {
                        this._logger.error(updSt.toString());

                        continue;
                    }

                    this._logger.info(updSt.toString());

                    //emit update
                    this.emit(EVENTS_LIST.UPDATE, {
                        type: EVENTS_LIST.UPDATE,
                        data: updSt.data
                    });
                    //get next
                } //if new status

                this._logger.debug(`Start delay:[${this.props.interval}]`);

                await delay(this.props.interval);

                this._logger.debug(`Stop delay:[${this.props.interval}]`);
            } //for all deposit

            this._startNextCycle();

            this._logger.debug(`End watch cycle: [${snaphot}]`);

            this.emit(EVENTS_LIST.START_CYCLE, {
                snaphot
            });
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            this._startNextCycle();
        }
    }

    async updateDepositStatus(updateBody) {
        try {
            if (updateBody == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'update body is null, have to be an object');
            }

            const {
                status,
                depositId,
                silent = false,
            } = updateBody;

            let checkStatus = DEPOSIT_STAUTUS.check(status);

            if (checkStatus == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, `Invalid status value:[${status}`, DEPOSIT_STATUS);
            }

            this._logger.info(`Try update deposit id:[${depositId}] status to [${status}]`);

            //update status
            const depoUpdateResult = await this._db.models.OrderDeposit.updateOne({
                id: depositId,
                status
            });


            depoUpdateResult.isOK();

            this._logger.info(depoUpdateResult);

            //make event if not silent
            if (silent != true) {
                //emit update
                this._logger.info(`Emmit event about update deposit id:[${depositId}] status to [${status}]`);

                this.emit(EVENTS_LIST.UPDATE, {
                    type: EVENTS_LIST.UPDATE,
                    data: depoUpdateResult.data
                });
            }

            //return depo info
            return depoUpdateResult;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
}


module.exports.DepositWatcherClass = DepositWatcherClass;
module.exports.EVENTS_LIST = EVENTS_LIST;
module.exports.DEPOSIT_STAUTUS = DEPOSIT_STAUTUS;