const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;


class RefferalTokenClass {
    constructor(values) {
        this._values = {
            id: values.id,
            linkId: values.linkId,

            status: values.status,
            userId: values.userId,

            sessions: values.sessions || [],

            createdAt: values.createdAt,
            updatedAt: values.updatedAt
        }
    }

    get tokenId() {
        return this._values.linkId;
    }

    set tokenId(id) {
        this._values.linkId = id;
    }

    get sessionId() {
        return this.getActiveSession().id
    }

    getActiveSession() {
        let session = this._values.sessions.filter(session => {
            if (
                (Date.now() >= session.startDate && session.endDate <= Date.now()) &&
                session.isDeleted == false
            ) {
                return true;
            } else {
                return false;
            }
        });

        if (session.length > 0) {
            return session[0];
        }

        return {};
    }
}

class SystemUserClass {
    constructor(db) {
        this._db = db;
        this._logger = new AppLoggerClass('SystemUserClass');

        this.values = {};
    }

    set id(id) {
        this.values.id = id;
    }

    get id() {
        return this.values.id
    }

    get isNew() {
        return this.values.isNew
    }

    getLastToken() {
        let tokens = this.values.tokens.sort((a, b) => {
            return new Date(b.createdAt) - new Date(a.createdAt);
        });

        let token = tokens[this.values.tokens.length - 1];

        return token;
    }

    async checkIn(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments should be an object');
            }

            let {
                firstName,
                lastName,
                username,
                tlgUserId,
                languageCode,
            } = args;

            this._logger.info(`user:[${tlgUserId}], firstName:[${firstName}],lastName:[${lastName}], username:[${username}] languageCode:[${languageCode}]`);


            let checkin = await this._db.models.User.checkIn(args);

            checkin.isOK();

            this._apply(checkin.data);

            this._logger.info(checkin);

            //make User class
            let rsp = new BasicResponse(ResultStatus.OK, checkin.message, this);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    /**
     * Fetch values from db and aplly this instance
     */
    async sync() {
        try {
            let user = await this._db.models.User.getOneById({
                id: this.values.id
            });

            user.isOK();

            this._apply(user.data);

            this._logger.info(user);

            //make User class
            return user;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }

    async assignInviteLink(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args isNull');
            }

            const {
                referalLink,
                userId
            } = args;

            let referalLinkInfo = await this._db.models.ReferalLink.findOneByLink({
                link: referalLink
            });

            referalLinkInfo.isOK();

            const {
                isActive,
                link
            } = referalLinkInfo.data;

            if (isActive == false) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_READY, `Refferal link ${link} is disabled`, referalLinkInfo.data);
            }

            let inviteActionPayload = {
                userId,
                referalId: referalLinkInfo.data.id
            }

            let createActionRequest = await this._db.models.LinkAction.createOne(inviteActionPayload);

            if (createActionRequest.isStatus(ResultStatus.ERROR_ALREADY_ASSIGNED)) {
                this._logger.warn(createActionRequest);

                let syncRes = await this.sync();

                syncRes.isOK();

                this._logger.info(syncRes);

                let rsp = new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED,
                    `User id:[${inviteActionPayload.userId}] already assigned to link id:[${inviteActionPayload.referalId}]`, {
                        ...referalLinkInfo.data
                    }
                );

                this._logger.info(rsp);

                return rsp;
            } else if (createActionRequest.isStatus(ResultStatus.OK)) {
                this._logger.info(createActionRequest);

                let syncRes = await this.sync();

                syncRes.isOK();

                this._logger.info(syncRes);

                let rsp = new BasicResponse(ResultStatus.OK,
                    `Assign user id:[${inviteActionPayload.userId}] to link id:[${inviteActionPayload.referalId}]`, {
                        ...referalLinkInfo.data
                    });

                this._logger.info(rsp);

                return rsp;
            }

            //if any good status - throw
            throw createActionRequest;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error);

            return error;
        }
    } //_assignInviteLink


    _apply(values) {
        this.values = {
            id: values.id,
            username: values.username,
            firstName: values.firstName,
            lastName: values.lastName,
            languageCode: values.languageCode,
            role: values.role,
            state: values.state,
            tlgUserId: values.tlgUserId,
            updatedAt: values.updatedAt,
            createdAt: values.createdAt,
            isNew: values.isNew == null ? this.values.isNew : values.isNew,
            // tokens: values.tokensActions.sort((a, b) => {
            //     return new Date(b.createdAt) - new Date(a.createdAt);
            // })
            tokens: values.tokensActions == null ? [] : values.tokensActions.map(token => new RefferalTokenClass(token)),
        }
    }
}

class SystemUserProcessorClass {
    constructor() {
        this._db = null;
        this._logger = new AppLoggerClass('SystemUserProcessorClass');
    }

    /**
     * @param {link} db database connection
     */
    init(db) {
        try {
            if (db === null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Database connection should be an provide');
            }

            this._db = db;

            let rsp = new BasicResponse(ResultStatus.OK, 'User processor initialized successfully');

            this._logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //db

    /**
     * @param {*} firstName,
     * @param {*} lastName
     * @param {*} username
     * @param {*} tlgUserId
     * @param {*} languageCode
     */
    async checkIn(args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'args should be an object')
            }

            let UserInstance = new SystemUserClass(this._db);

            let {
                firstName,
                lastName,
                username,
                tlgUserId,
                languageCode,
            } = args;

            this._logger.info(`user:[${tlgUserId}], firstName:[${firstName}],lastName:[${lastName}], username:[${username}]`);

            let checkin = await UserInstance.checkIn(args);

            this._logger.info(checkin);

            //make User class
            return checkin;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    }
}

exports.SystemUserProcessorClass = SystemUserProcessorClass;