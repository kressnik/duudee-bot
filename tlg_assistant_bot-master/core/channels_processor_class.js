'use strict'

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;

const {
    AppLoggerClass
} = __UTILS;


class ChannelProcessorClass {
    constructor() {
        this._db = null;
        this._logger = new AppLoggerClass('SubcribeChannelClass', process.env.CHANNEL_PROCESSOR_CLASS_LOG_LEVEL);
    }

    async init(db) {
        try {
            if (db == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'DB link not set');
            }

            this._db = db;

            return new BasicResponse(ResultStatus.OK, 'Init core done successfully.');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }

    }

    async getActiveList(filter) {
        try {
            let list = await this._db.models.Channel.getAllActive(filter);

            if (list.status != ResultStatus.OK) {
                throw list;
            }

            return list;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getActiveList

    async getOneByTlgId({
        tlgId
    }) {
        try {
            let channel = await this._db.models.Channel.getOneByTlgId({
                tlgId
            });

            if (channel.status != ResultStatus.OK) {
                throw channel;
            }

            return channel;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getActiveList

    async getOneById({
        id
    }) {
        try {
            let channel = await this._db.models.Channel.getOneById({
                id
            });

            if (channel.status != ResultStatus.OK) {
                throw channel;
            }

            return channel;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //getActiveList
}


exports.ChannelProcessorClass = ChannelProcessorClass;