'use strict'

module.exports.down = async function (queryInterface, Sequelize) {
    try {
        await queryInterface.removeColumn(
            'referal_links',
            'facebook_pixel_id'
        );
    } catch (_error) {
        console.error(_error);
    }

};

module.exports.up = async function (queryInterface, Sequelize) {
    try {
        console.log('Start add is_manual to order_deposits');

        await queryInterface.addColumn(
            'referal_links',
            'facebook_pixel_id', {
                type: Sequelize.STRING(50),
                allowNull: true,
                default: null
            }
        );
    } catch (_error) {
        console.error(_error);
    }
};