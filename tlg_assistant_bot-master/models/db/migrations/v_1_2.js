'use strict';

module.exports.up = async function up(queryInterface, Sequelize) {
    try {
        await queryInterface.addColumn(
            'referal_links',
            'title', {
                type: Sequelize.DataTypes.STRING(50),
                allowNull: true
            }
        );
    } catch (_error) {
        console.error(_error);
    }

};

module.exports.down = async function down(queryInterface, Sequelize) {
    try {
        await queryInterface.removeColumn(
            'referal_links',
            'title'
        );
    } catch (_error) {
        console.error(_error);
    }
};