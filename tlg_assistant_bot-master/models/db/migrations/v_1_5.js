'use strict'

module.exports.up = async function up(queryInterface, Sequelize) {
    try {
        await queryInterface.removeColumn(
            'wizzard_tracks',
            'track_id'
        );
    } catch (_error) {
        console.error(_error);
    }

};

module.exports.down = async function down(queryInterface, Sequelize) {
    try {
        await queryInterface.addColumn(
            'wizzard_tracks',
            'track_id', {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'token_sessions',
                    key: 'id',
                    as: 'order'
                }
            }
        );
    } catch (_error) {
        console.error(_error);
    }
};