'use strict';

module.exports.up = async function up(queryInterface, Sequelize) {
  try {
    await queryInterface.createTable(
      'versions', {
        id: {
          type: Sequelize.DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        version: {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
          defaultValue: 'base'

        }
      }
    )
  } catch (_error) {
    console.error(_error);
  }

};

module.exports.down = async function down(queryInterface, Sequelize) {
  try {
    await queryInterface.dropTable('versions');
  } catch (_error) {
    console.error(_error);
  }
};