async function addColumnTrackIdFlow(queryInterface, Sequelize) {
    try {
        let addTrackId = await queryInterface.addColumn(
            'subscribe_orders',
            'track_id', // name of the key we're adding 
            {
                type: Sequelize.INTEGER(11),
                references: {
                    model: 'wizzard_tracks', // name of Target model
                    key: 'id', // key in Target model that we're referencing
                }
            }
        );

        console.log('Add track id', addTrackId);

        return addTrackId;
    } catch (e) {
        console.error(e);

        return e;
    }
}

async function removeColumnTrackIdFlow(queryInterface, Sequelize) {
    try {
        let removeTrackId = await queryInterface.removeColumn(
            'subscribe_orders',
            'track_id'
        );

        console.log('Remove track id', removeTrackId);


        return removeTrackId;
    } catch (e) {
        console.error(e);

        return e;
    }
}

async function addColumnIsRefferalFlow(queryInterface, Sequelize) {
    try {
        const addIsRef = await queryInterface.addColumn(
            'assistant_buttons',
            'is_refferal', {
                type: Sequelize.BOOLEAN,
                allowNull: true
            }
        );

        console.log('Add is_refferal column to assistant_buttons', addIsRef);

        return addIsRef;
    } catch (e) {
        console.error(e);

        return e;
    }
}

async function removeIsRefferalFlow(queryInterface, Sequelize) {
    try {
        let removeIsRefferal = await queryInterface.removeColumn(
            'assistant_buttons',
            'is_refferal'
        );

        console.log('Remove track id', removeIsRefferal);


        return removeIsRefferal;
    } catch (e) {
        console.error(e);

        return e;
    }
}

async function addSessionTable(queryInterface, Sequelize) {
    try {
        let table = await queryInterface.createTable('token_sessions', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            token_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'referal_links',
                    key: 'id',
                }
            },
            title: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            start_date: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            end_date: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            is_deleted: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        console.log('Create `token_session`', table);
    } catch (e) {
        console.error(e);
    }
}

async function removeSessionTable(queryInterface, Sequelize) {
    try {
        let remove = await queryInterface.dropTable('token_sessions', {
            force: true,
            cascade: false,
        });

        console.log('Remove `token_session`', remove);
    } catch (error) {
        console.error(error);
    }
}

async function addTrackTable(queryInterface, Sequelize) {
    try {
        let table = await queryInterface.createTable('wizzard_tracks', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            status: {
                type: Sequelize.STRING(45),
                allowNull: false
            },
            error: {
                type: Sequelize.STRING(50),
                allowNull: true
            },
            stage: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
                allowNull: false
            },
            is_new_user: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0,
                allowNull: false
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'users',
                    key: 'id',
                    as: 'users'
                }
            },
            token_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'referal_links',
                    key: 'id',
                    as: 'token'
                }
            },
            channel_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'channels',
                    key: 'id',
                    as: 'channel'
                }
            },
            session_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'token_sessions',
                    key: 'id',
                    as: 'session'
                }
            },
            track_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'token_sessions',
                    key: 'id',
                    as: 'order'
                }
            },
            button_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'assistant_buttons',
                    key: 'id',
                    as: 'button'
                }
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        console.log('Create `wizzard_tracks`', table);
    } catch (e) {
        console.error(e);
    }
}


async function removeTrackTable(queryInterface, Sequelize) {
    try {
        let remove = await queryInterface.dropTable('wizzard_tracks', {
            force: true,
            cascade: false,
        });

        console.log('Remove `wizzard_tracks`', remove);
    } catch (error) {
        console.error(error);
    }
}

//   down: (queryInterface, Sequelize) => {
//   }


module.exports.up = async function up(queryInterface, Sequelize) {
    try {
        console.log('Start up function');

        await addSessionTable(queryInterface, Sequelize);
        await addTrackTable(queryInterface, Sequelize);
        await addColumnTrackIdFlow(queryInterface, Sequelize);
        await addColumnIsRefferalFlow(queryInterface, Sequelize);


        console.log('End up function');
    } catch (_error) {
        console.error(_error);
    }
};


module.exports.down = async function down(queryInterface, Sequelize) {
    try {
        console.log('Start down function');

        await removeColumnTrackIdFlow(queryInterface, Sequelize);
        await removeTrackTable(queryInterface, Sequelize);
        await removeSessionTable(queryInterface, Sequelize);
        await removeIsRefferalFlow(queryInterface, Sequelize);

        console.log('End up function');
    } catch (_error) {
        console.error(_error);
    }
};