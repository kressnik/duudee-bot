'use strict'

module.exports.down = async function up(queryInterface, Sequelize) {
    try {
        await queryInterface.removeColumn(
            'order_deposits',
            'is_manual'
        );
    } catch (_error) {
        console.error(_error);
    }

};

module.exports.up = async function down(queryInterface, Sequelize) {
    try {
        console.log('Start add is_manual to order_deposits');

        await queryInterface.addColumn(
            'order_deposits',
            'is_manual', {
                type: Sequelize.INTEGER,
                allowNull: true,
                default: false
            }
        );

        //fetch total records
        const [records, records2] = await queryInterface.sequelize.query('select * from order_deposits');

        let total = records.length;

        console.log(`Total records to update:[${total}]`);

        for (const record of records) {
            const {
                id,
                status,
                reference_id,
                service_alias
            } = record;

            console.log(id, service_alias);

            let is_manual = false;

            if (service_alias.match(/manualy_/)) {
                console.log('match', id);
                is_manual = true;
            }

            const [res] = await queryInterface.sequelize.query(`UPDATE order_deposits SET is_manual = ${is_manual} WHERE id = ${id}`);

            console.log(res);

            console.log(`Last:${--total}`);
        }

    } catch (_error) {
        console.error(_error);
    }
};