'use strict';

module.exports.up = async function up(queryInterface, Sequelize) {
    try {
        await queryInterface.addColumn(
            'channels',
            'color',
            {
                type: Sequelize.DataTypes.STRING(50),
                allowNull: false
            }
        );
    } catch (_error) {
        console.error(_error);
    }

},

    module.exports.down = async function down(queryInterface, Sequelize) {
        try {
            await queryInterface.removeColumn(
                'channels',
                'color'
            );
        } catch (_error) {
            console.error(_error);
        }
    };