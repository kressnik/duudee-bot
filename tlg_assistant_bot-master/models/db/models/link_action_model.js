'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;
    //LinkAction
    var InviteActionModel = sequelize.define('LinkAction', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.STRING(35),
            allowNull: false
        }
    }, {
        underscored: true,
        hooks: true
    });

    InviteActionModel.logger = new AppLoggerClass(`SEQUELIZE::LinkAction`);

    InviteActionModel.associate = async function (models) {

        await this.belongsTo(models.User, {
            foreignKey: {
                name: 'userId',
                allowNull: false,
            }
        });
    };

    InviteActionModel.default = async function (_models) {

    }

    InviteActionModel.createOne = async function ({
        userId,
        referalId
    }) {
        try {
            let [event, isNew] = await this.findOrCreate({
                where: {
                    userId,
                    linkId: referalId,
                },
                defaults: {
                    userId: userId,
                    linkId: referalId,
                    status: 'created'
                }
            });

            if (isNew == false) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED,
                    `Action for user id:[${userId}] and referal link id:[${referalId}] already assigned to system`,
                    event
                );
            }

            let response = new BasicResponse(ResultStatus.OK,
                `Add new action done successfully. User id:[${userId}], Referal link id:[${referalId}]`,
                event.dataValues);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            if (error.isStatus(ResultStatus.ERROR_ALREADY_ASSIGNED)) {
                this.logger.warn(error);

                return error;
            }

            this.logger.error(error.toString());

            return error;
        }
    }

    InviteActionModel.getInviteActionByUserId = async function (id) {
        try {
            let invAction = await this.findOne({
                where: {
                    userId: id
                }
            });

            if (invAction == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `InviteAction not found by userId:[${id}]`);
            }

            let response = new BasicResponse(ResultStatus.OK, 'InviteAction was found', invAction.dataValues);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    InviteActionModel.getAll = async function (params) {
        try {
            let resultActions = await this.findAll({
                attributes: ['link_id', [sequelize.fn('count', sequelize.col('referal_id')), 'count']],
                group: ['link_id'],
            });

            let actions = resultActions.map((_action) => {
                return _action.dataValues;
            });

            let where = '';

            let recordsTotal = await this.count();
            let recordsFiltered = await this.count({
                where: where
            });

            const resultData = { //Результаты запросов
                recordsFiltered: recordsFiltered,
                recordsTotal: recordsTotal,
                data: actions
            }

            let response = new BasicResponse(ResultStatus.OK, 'Getting invite actions done successfully', resultData);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    InviteActionModel.getUsersIdByLink = async function (params) {
        try {
            let {
                filter,
                offset = 0,
                limit = 100,
                referalId
            } = params;

            let where = filter == null ? {
                linkId: referalId,
            } : {
                [Op.and]: {
                    linkId: referalId,
                    ...filter
                }
            }

            let orders = await this.findAll({
                where,
                offset,
                limit
            });

            let packet = {
                recordsTotal: await this.count(),
                recordsFiltered: await this.count({
                    where
                }),
                limit,
                offset,
                filter,
                orders: []
            }

            if (orders != null && orders.length > 0) {
                packet.orders = orders.map(order => order.dataValues);
            }

            let response = new BasicResponse(ResultStatus.OK, 'Getting users by invite actions by link done successfully', packet);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    return InviteActionModel;
};