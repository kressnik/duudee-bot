'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


const UserRole = Object.freeze({
    ADMIN: 0,
    SUBSCRIBER: 1,
});

module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;


    var channelModel = sequelize.define('Channel', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        description: {
            type: DataTypes.STRING(400),
            allowNull: false
        },
        inUse: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        tlgId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        currency: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        color: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        feeAtOwnExpense: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        subscriptionDuration: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        isFree: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        maxSubcribesQty: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        underscored: true
    });

    channelModel.logger = new AppLoggerClass(`SEQUELIZE::Channel`);

    channelModel.associate = async function (models) {
        // associations can be defined here
        // await this.hasMany(models.SubscribeOrder, {
        //     as: 'SubscribeOrder',
        //     foreignKey: 'orderId',
        // });

        // await this.belongsTo(models.SubscribeOrder, {
        //     foreignKey: {
        //         name: 'orderId',
        //         allowNull: false
        //     }
        // });
    }

    channelModel.deleteOne = async function ({
        id
    }) {
        try {
            let channelInst = await this.findByPk(id);

            if (channelInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Channel with id:[${id}] doesnt found in system.`);
            }

            let result = await channelInst.destroy();


            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update channel properties done succsessfully.', result.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //deleteOne

    channelModel.editStateOne = async function ({
        id,
        state
    }) {
        try {
            let channelInst = await this.findByPk(id);

            if (channelInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Channel with id:[${id}] doesnt found in system.`);
            }

            let result = await channelInst.update({
                inUse: state == null ? channelInst.inUse : state,
            });


            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update channel properties done succsessfully.', result.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //editStateOne

    channelModel.createOne = async function ({
        name,
        tlgId,
        description,
        inUse,
        price,
        currency,
        feeAtOwnExpense,
        isFree,
        maxSubcribesQty,
        subscriptionDuration,
        color
    }) {
        try {
            let [channelInstance, isCreate] = await this.findOrCreate({
                where: {
                    tlgId,
                },
                defaults: {
                    name,
                    description,
                    inUse: inUse || 0,
                    tlgId,
                    price: 0,
                    currency,
                    feeAtOwnExpense: feeAtOwnExpense || 0,
                    isFree,
                    maxSubcribesQty: maxSubcribesQty || 1,
                    subscriptionDuration: subscriptionDuration || 2190,
                    color
                }
            });

            if (isCreate == false) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED, `Channel name:[${name}] tlgId[${tlgId}] is already added to db.`, {
                    tlgId
                })
            }

            return new BasicResponse(ResultStatus.OK, `Add Channel name:[${name}] tlgId[${tlgId} done succsessfully.`, channelInstance.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    channelModel.editOne = async function ({
        color,
        id,
        name,
        tlgId,
        description,
        inUse,
        price,
        currency,
        feeAtOwnExpense,
        isFree,
        maxSubcribesQty,
        subscriptionDuration
    }) {
        try {
            let channelInst = await this.findByPk(id);

            if (channelInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Channel with id:[${id}] doesnt found in system.`);
            }

            let result = await channelInst.update({
                name: name || channelInst.name,
                tlgId: tlgId || channelInst.tlgId,
                description: description || channelInst.description,
                inUse: inUse == null ? channelInst.inUse : inUse,
                price: price || channelInst.price,
                currency: currency || channelInst.currency,
                feeAtOwnExpense: feeAtOwnExpense || channelInst.feeAtOwnExpense,
                isFree: isFree == null ? channelInst.isFree : isFree,
                maxSubcribesQty: maxSubcribesQty || channelInst.maxSubcribesQty,
                subscriptionDuration: subscriptionDuration || channelInst.subscriptionDuration,
                color: color
            });


            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update channel properties done succsessfully.', result.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //updateOne

    channelModel.getOneByTlgId = async function ({
        tlgId
    }) {
        try {
            let channel = await this.findOne({
                where: {
                    tlgId
                }
            });

            if (
                channel == null
            ) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Channel with tlgId:[${tlgId}] not found.`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get Channel with tlgId:[${tlgId}] done successfully.`, channel.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    channelModel.getOneById = async function ({
        id
    }) {
        try {
            let channel = await this.findOne({
                where: {
                    id
                }
            });

            if (
                channel == null
            ) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Channel with id:[${id}] not found.`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get Channel with id:[${id}] done successfully.`, channel.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    channelModel.getAllActive = async function (filter) {
        try {
            let query = {
                where: {
                    inUse: true
                }
            };

            if (filter && filter.isFree != null) {
                query.where.isFree = filter.isFree;
            }

            let channels = await this.findAll(query);

            if (
                channels == null ||
                channels.length == 0
            ) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Channel with active status not found.`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get all active channels done successfully.`, channels.map(ch => ch.dataValues));

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    channelModel.getAll = async function (params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params have to be an object.');
            }

            let channels = [];
            let {
                limit = null,
                    offset = null
            } = params;

            if (limit != null && offset != null) {
                limit = parseInt(limit);
                offset = parseInt(offset);

                channels = await this.findAndCountAll({
                    limit,
                    offset
                });
            } else {
                channels = await this.findAndCountAll();
            }

            if (channels.rows == null || channels.rows.length == 0) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Channels not found with offset:[${offset}] limit:[${limit}]`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get channels with offset:[${offset}] limit:[${limit}] done successfully.`, channels);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAll

    return channelModel;
}