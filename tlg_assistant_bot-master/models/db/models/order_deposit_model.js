'use strict';
const {
    AppLoggerClass
} = __UTILS;

var uniqid = require('uniqid');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


const UserRole = Object.freeze({
    ADMIN: 0,
    SUBSCRIBER: 1,
});

module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;


    var depositModel = sequelize.define('OrderDeposit', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        isManual: {
            type: DataTypes.BOOLEAN,
            default: false,
            allowNull: false
        },
        referenceId: {
            type: DataTypes.STRING(100),
            uniqe: true,
            allowNull: false
        },
        serviceAlias: {
            type: DataTypes.STRING(40),
            allowNull: false
        },
        serviceCode: {
            type: DataTypes.STRING(80),
            allowNull: false
        },
        serviceMethod: {
            type: DataTypes.STRING(80),
            allowNull: false
        },
        amount: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        },
        fee: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        },
        currency: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        resolution: {
            type: DataTypes.STRING(30),
            allowNull: false
        },
        paymentId: {
            type: DataTypes.STRING(80),
            allowNull: false
        },
        paymentAmount: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        },
        serviceCurrency: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        distination: {
            type: DataTypes.STRING(25),
            allowNull: true
        },
        provider: {
            type: DataTypes.STRING(25),
            allowNull: true
        },
        created: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        sn: {
            type: DataTypes.STRING,
            allowNull: true
        },
        error: {
            type: DataTypes.STRING(255),
            allowNull: true,
            defafaultValue: null
        },
        action: {
            type: DataTypes.STRING(500),
            allowNull: false
        }
    }, {
        underscored: true
    });

    depositModel.logger = new AppLoggerClass(`SEQUELIZE::OrderDeposit`);

    depositModel.associate = async function (models) {

        // associations can be defined here
        // await this.belongsTo(models.SubscribeOrder, {
        //     foreignKey: {
        //         name: 'orderId',
        //         allowNull: false,
        //     }
        // });
    }

    depositModel.createOne = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments is null, should be Object')
            }

            const {
                amount,
                currency,
                status,
                referenceId,
                serviceAlias,
                serviceCode,
                serviceMethod,
                resolution,
                paymentId,
                paymentAmount,
                created,
                sn,
                serviceCurrency,
                action,
                fee,
                error = '',
                isManual
            } = args;

            let descr = [
                referenceId ? `referenceId:[${referenceId}]` : null,
                referenceId ? `isManual:[${isManual}]` : null,
                status ? `status:[${status}]` : null,
                amount ? `amount:[${amount}]` : null,
                currency ? `currency:[${currency}]` : null,
                paymentId ? `paymentId:[${paymentId}]` : null,
                fee ? `fee:[${fee}]` : null,
                sn ? `sn:[${sn}]` : null,
                error ? `error:[${error}]` : null,
                serviceAlias ? `serviceAlias:[${serviceAlias}]` : null,
                serviceCode ? `serviceCode:[${serviceCode}]` : null,
                serviceMethod ? `serviceMethod:[${serviceMethod}]` : null,
                resolution ? `resolution:[${resolution}]` : null,
                paymentAmount ? `paymentAmount:[${paymentAmount}]` : null,
                created ? `created:[${created}]` : null,
                serviceCurrency ? `serviceCurrency:[${serviceCurrency}]` : null,
                action ? `serviceCurrency:[${action}]` : null,
            ].join(' ');


            this.logger.info(`Start create deposit ${descr}`);

            let [depositInstance, isCreate] = await this.findOrCreate({
                where: {
                    referenceId,
                },
                defaults: {
                    isManual,
                    amount,
                    currency,
                    status,
                    referenceId,
                    serviceAlias,
                    serviceCode,
                    serviceMethod,
                    resolution,
                    paymentId,
                    paymentAmount,
                    created,
                    sn,
                    serviceCurrency,
                    action,
                    fee,
                    error
                }
            });

            if (isCreate == false) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED, `Deposit already added to db. ${descr}`, args);
            }

            return new BasicResponse(ResultStatus.OK, `Add deposit done succsessfully. ${descr}`, depositInstance.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    depositModel.updateOne = async function (info) {
        try {
            if (info == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Incoming value have to to be provided');
            }

            let {
                id
            } = info;


            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Incoming id have to be provided or null');
            }

            let depositInstance = await this.findOne({
                where: {
                    id
                }
            });

            if (depositInstance == null) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED, `Deposit id:[${id}] not found into db.`, {
                    ...info
                });
            }

            let updSt = await depositInstance.update(info);

            if (updSt == false) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED, `Deposit id:[${id}] uopdate wit erroro.`, {
                    ...info
                });
            }

            return new BasicResponse(ResultStatus.OK, `Update deposit with id:[${id}] done succsessfully.`, updSt.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }



    depositModel.getWithStatus = async function (statuses) {
        try {
            if (Array.isArray(statuses) == false) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'statuses have to be n aarray.')
            }

            let deposits = await this.findAll({
                where: {
                    [Op.and]: {
                        status: statuses
                    }
                }
            });

            statuses = statuses.join(',');

            if (deposits == null || deposits.length == 0) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Deposit not found with staus:[${statuses}]`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get deposits with status ${statuses} done successfully.`, deposits);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    depositModel.getDepoInfoById = async function ({
        id
    }) {
        try {
            let depo = await this.findOne({
                where: {
                    id
                }
            });

            if (depo == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Deposit not found with depositId:[${id}]`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get deposit with id:[${id}] done successfully.`, depo.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    return depositModel;
}