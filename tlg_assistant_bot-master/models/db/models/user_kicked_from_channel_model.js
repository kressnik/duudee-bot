'use strict';
const {
    AppLoggerClass
} = __UTILS;

var uniqid = require('uniqid');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');



module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;

    let UserBanedFromChannel = sequelize.define('UserBanedFromChannel', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        userTlgId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        snapshotId: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        cause: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
    }, {
        underscored: true
    });

    UserBanedFromChannel.logger = new AppLoggerClass(`SEQUELIZE::UserBanedFromChannel`);

    UserBanedFromChannel.associate = async function (models) {
        await this.belongsTo(models.Channel, {
            foreignKey: {
                name: 'channelId',
            }
        });
    }

    UserBanedFromChannel.default = async function (_models) {

    }

    UserBanedFromChannel.saveBatch = async function (batch) {
        try {
            if (Array.isArray(batch) == false) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Batch have to be an array.');
            }

            let saved = await this.bulkCreate(batch, {
                updateOnDuplicate: ['userTlgId', 'channelId']
            });

            if (saved == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'When save kicked users, has error', saved);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Save kicked user batch done successfully.`, saved);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //findOneById


    UserBanedFromChannel.getOneById = async function ({
        userId
    }) {
        try {
            let user = await this.findOne({
                where: {
                    userId
                }
            })

            let rsp = new BasicResponse(ResultStatus.OK, `Get user id:[${id}] done successfully.`, user.dataValues);

            if (user == null) {
                rsp = new BasicResponse(ResultStatus.OK, `User:[${tlgUserId}] not found in system`, event.dataValues);
            }

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //findOneById

    return UserBanedFromChannel;
};