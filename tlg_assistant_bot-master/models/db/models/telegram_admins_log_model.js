'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;
    //LinkAction
    var TelegramAdminsLogsModel = sequelize.define('TelegramAdminsLogs', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        logId: {
            type: DataTypes.BIGINT(32),
            allowNull: false,
            unique: true,
        },
        userId: {
            type: DataTypes.BIGINT(32),
            allowNull: false
        },
        kickedBy: {
            type: DataTypes.BIGINT(32),
            allowNull: true
        },
        date: {
            type: DataTypes.INTEGER(32),
            allowNull: false
        },
        type: {
            type: DataTypes.STRING(60),
            allowNull: false
        },
        json: {
            type: DataTypes.JSON,
            allowNull: true
        }
    }, {
        individualHooks: true,
        underscored: true,
        hooks: true
    });

    TelegramAdminsLogsModel.logger = new AppLoggerClass(`SEQUELIZE::TelegramAdminsLogsModel`);

    TelegramAdminsLogsModel.associate = async function (models) {
        await this.belongsTo(models.Channel, {
            as: 'channel',
            foreignKey: {
                name: 'channelId',
                allowNull: false,
            }
        });
    };

    TelegramAdminsLogsModel.default = async function (_models) {

    }

    TelegramAdminsLogsModel.getLastChannelLogId = async function (channelId) {
        try {
            if (channelId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'channelId should be provided');
            }

            channelId = parseInt(channelId);

            this.logger.info(`Start fetch last channel:[${channelId}] admin log`);

            const log = await this.max('logId', {
                where: {
                    channelId
                }
            });

            const rsp = new BasicResponse(ResultStatus.OK, `Fetch last admin log from db for channel:[${channelId}] done successfully`, log);

            this.logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    TelegramAdminsLogsModel.createOrUpdateBatch = async function (batch) {
        try {
            if (Array.isArray(batch) == false) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Batch should be an array');
            }

            this.logger.info(`Start save  admin logs batch length:[${batch.length}]`);

            const checkPromiseArr = await Promise.all(batch.map(async (log) => {
                const log_ = await this.findOne({
                    where: {
                        logId: log.logId,
                        channelId: log.channelId
                    }
                });

                return log_ == null ? null : log_.logId;
            }));

            const logsToSave = batch.filter(log => {
                const check = checkPromiseArr.some(logId => logId == log.logId);
                return !check;
            })


            let logs = await this.bulkCreate(logsToSave, {
                updateOnDuplicate: ["logId", "channelId"],
                individualHooks: true,
                validate: true
            });

            logs = logs.map(log => log.dataValues);

            const rsp = new BasicResponse(
                ResultStatus.OK,
                `Add telegram logs batch with size:[${batch.length}] has saved:[${logs.length}] db done successfully`, {
                    logs: batch,
                    saved: logs
                });

            this.logger.info(rsp);

            return rsp;
        } catch (e) {
            const error = new BasicResponseByError(e);

            this.logger.error('', error.toString());

            return error;
        }
    }

    TelegramAdminsLogsModel.getAllByFilter = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments is null, should be number or string');
            }

            let {
                filter,
                dateRange,
                limit,
                offset
            } = args;

            let where = {};

            if (filter != null) {
                for (let key in filter) {
                    if (Object.values(filter).length > 1) {
                        if (
                            Array.isArray(filter[key]) == true &&
                            filter[key].length > 0
                        ) {
                            where[Op.and] == null ? where[Op.and] = {} : null;

                            where[Op.and][key] = {
                                [Op.in]: [...filter[key]]
                            }
                        }
                    } else {
                        where[key] = {
                            [Op.in]: [...filter[key]]
                        }
                    }
                }
            }

            if (Array.isArray(dateRange) == true &&
                dateRange.length > 0 &&
                dateRange.length <= 2) {
                const [startDate, endDate] = dateRange;

                where.createdAt = {
                    [Op.between]: [new Date(startDate).toISOString(), new Date(endDate).toISOString()]
                };

            }

            const tracksRequest = await this.findAll({
                offset,
                limit,
                where,
                include: ['channel']
            });

            if (tracksRequest.length === 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, 'Not found any tracks', args);
            }

            this.logger.debug(`Get tracks from db done successfully. filter: ${JSON.stringify(filter)}`);

            const rsp = new BasicResponse(ResultStatus.OK, 'Get tracks from db done done successfully', tracksRequest.map(track => track.dataValues));

            return rsp;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    } //getAllByFilter


    return TelegramAdminsLogsModel;
}