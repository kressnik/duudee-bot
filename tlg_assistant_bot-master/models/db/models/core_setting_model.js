'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');

const SystemSettingDeafultBin = [{
    "name": "maintenanceMode", //maintanace mode
    "dataType": "boolean",
    "value": "true",
    "groupId": 0,
}];

module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;

    var CoreSetting = sequelize.define('CoreSetting', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        dataType: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                fn: function (val) {
                    if (val === null || val == "") {
                        throw new Error("'dataType' is null or empty");
                    }
                }
            }
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                fn: function (val) {
                    if (val === null || val == "") {
                        throw new Error("'name' is null or empty");
                    }
                }
            }
        },
        value: {
            type: DataTypes.STRING,
            allowNull: false
        },
        groupId: {
            type: DataTypes.INTEGER,
            allowNull: true
        }
    }, {
        underscored: true
    });

    CoreSetting._logger = new AppLoggerClass(`SEQUELIZE::CoreSetting`);


    CoreSetting.default = async function () {
        try {
            const getSystemSetting = await this.findAll();

            if (getSystemSetting != null &&
                getSystemSetting.length == 0
            ) {
                this._logger.warn("Init SystemSetting in base");

                this.destroy({
                    where: {},
                    truncate: true
                });

                for (let setting in SystemSettingDeafultBin) {
                    setting = SystemSettingDeafultBin[setting];

                    let res = await this.create(setting);

                    this._logger.info(res.toString());
                }

                return new BasicResponse(
                    ResultStatus.OK,
                    "Set to default settings done successfully.",
                    getSystemSetting.data
                );
            }

            return new BasicResponse(ResultStatus.OK, 'Alredy add to db in db');
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //defalt


    CoreSetting.setOne = async function (_setting) {
        try {
            if (_setting == null) {
                return new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER,
                    "Try to set null setting.",
                    arguments
                );
            } else {
                await this.sync();

                let qurey = {
                    where: {
                        id: _setting.id
                    }
                }

                _setting.value = _setting.value.toString();

                let setRes = await this.update(_setting, qurey);

                if (setRes == 0) {
                    return new BasicResponse(ResultStatus.ERROR,
                        "Update values in DB, done with errors.",
                        setRes
                    );
                } else {
                    return new BasicResponse(ResultStatus.OK,
                        "Update values in DB, done successfully.",
                        setRes
                    );
                } //check upd
            } //check null
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    } //set

    CoreSetting.getOneByName = async function ({
        name
    }) {
        try {
            await this.sync();

            let res = await this.findOne({
                where: {
                    name
                }
            });

            if (res == null) {
                throw new BasicResponse(
                    ResultStatus.ERROR_NOT_FOUND,
                    `Setting with name:[${name}] nor found`
                );
            }

            let setting = res.dataValues;

            switch (setting.dataType) {
                case 'string':
                    setting.value = setting.values
                    break;

                case 'number':
                    setting.value = parseInt(setting.value)
                    break;

                case 'float':
                    setting.value = parseFloat(setting.value);
                    break;

                case 'boolean':
                    setting.value = (setting.value == "true") ? true : false;
                    break
            }

            return new BasicResponse(
                ResultStatus.OK,
                `Get setting:[${name}] done successfully.`,
                setting
            );
        } catch (e) {
            let error = new BasicResponseByError(e);

            this._logger.error(error.toString());

            return error;
        }
    };

    return CoreSetting;
}