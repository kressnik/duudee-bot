'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;

    var ReferalLinkModel = sequelize.define('ReferalLink', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        link: {
            type: DataTypes.STRING(55),
            allowNull: false
        },
        type: {
            type: DataTypes.STRING(55),
            allowNull: false
        },
        description: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        isDelete: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        isActive: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        facebookPixelId: {
            type: DataTypes.STRING(50),
            allowNull: true,
            default: null
        }
    }, {
        underscored: true,
        hooks: true
    });

    ReferalLinkModel.logger = new AppLoggerClass(`SEQUELIZE::ReferalLink`);

    ReferalLinkModel.associate = async function (models) {
        this._models = models;

        await this.belongsTo(models.Channel, {
            as: 'channels',
            foreignKey: {
                name: 'channelId',
                allowNull: false,
            }
        });

        await this.belongsTo(models.AssistantButton, {
            as: 'button',
            foreignKey: {
                name: 'buttonId',
                allowNull: false,
            }
        });

        await this.hasMany(models.TokenSession, {
            as: 'sessions',
            foreignKey: {
                name: 'tokenId'
            }
        });

        await this.hasMany(models.LinkAction, {
            as: 'actions',
            foreignKey: {
                name: 'linkId',
                allowNull: false,
            }
        });

        await models.LinkAction.hasMany(models.TokenSession, {
            as: 'sessions',
            sourceKey: 'linkId',
            foreignKey: 'tokenId',
            //constraints: false
        });
    }

    ReferalLinkModel.default = async function (_models) {

    }

    ReferalLinkModel.createOne = async function ({
        link,
        type,
        description,
        channelId,
        buttonId,
        title,
        facebookPixelId
    }) {
        try {
            let [event, isNew] = await this.findOrCreate({
                where: {
                    link: link
                },
                defaults: {
                    facebookPixelId,
                    link: link,
                    type: type,
                    description: description || 'default_desc',
                    channelId: channelId,
                    buttonId: buttonId,
                    title: title,
                    isActive: 1,
                    isDelete: 0
                }
            })

            let response = new BasicResponse(ResultStatus.OK, `Add new referal link: [${link}] done successfully`, event.dataValues);

            if (isNew == false) {
                response = new BasicResponse(ResultStatus.OK, `Referal link: [${link}] already added to system`, event.dataValues);
            }

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    ReferalLinkModel.deleteOne = async function ({
        id
    }) {
        try {
            let referalLinkInst = await this.findByPk(id);

            if (referalLinkInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Referal link with id:[${id}] doesnt found in system.`);
            }

            if (referalLinkInst.isDelete == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Referal link with id:[${id}] already has delete status in system.`)
            }

            if (referalLinkInst.isActive == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, `Referal link with id:[${id}] has 'active' status in system.`)
            }

            let result = await referalLinkInst.update({
                isDelete: 1,
                isActive: 0
            });

            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Refferal link setuped status `delete` done succsessfully.', result.dataValues);
        } catch (e) {
            const error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //deleteOne

    ReferalLinkModel.editOne = async function ({
        id,
        link,
        type,
        description,
        isActive,
        channelId,
        buttonId,
        title,
        facebookPixelId
    }) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Id have to be set.');
            }

            let linkInst = await this.findByPk(id);

            if (linkInst == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Referal link with id:[${id}] doesnt found in system.`);
            }

            let result = await linkInst.update({
                link: link || linkInst.link,
                type: type || linkInst.type,
                description: description || linkInst.description,
                isActive: isActive != null ? isActive : linkInst.isActive,
                channelId: channelId || linkInst.channelId,
                buttonId: buttonId || linkInst.buttonId,
                title: title || linkInst.title,
                facebookPixelId: facebookPixelId || linkInst.facebookPixelId
            });

            if (result == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            return new BasicResponse(ResultStatus.OK, 'Update referal link properties done succsessfully.', result.dataValues);
        } catch (e) {
            const error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //editOne

    ReferalLinkModel.getAll = async function (params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params have to be an object.');
            }

            let {
                filter,
                limit = 10,
                offset = 0
            } = params;

            if ([offset, limit].some(a => a == null)) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Some parameter is null(offset, limit).');
            }


            let where = {};

            if (filter != null) {
                where = {
                    [Op.and]: {
                        ...filter
                    }
                }
            }

            limit = parseInt(limit);
            offset = parseInt(offset);

            const links = await this.findAll({
                where,
                limit,
                include: ['channels', 'button'],
                offset,
                order: [
                    ["id", "DESC"]
                ]
            });

            const packet = {
                recordsTotal: await this.count(),
                recordsFiltered: await this.count({
                    where
                }),
                limit,
                offset,
                filter,
                data: links.map(link => link.dataValues)
            }

            const rsp = new BasicResponse(ResultStatus.OK, `Get links with offset:[${offset}] limit:[${limit}] done successfully.`, packet);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAll

    ReferalLinkModel.findOneByLink = async function ({
        link
    }) {
        try {
            let linkInst = await this.findOne({
                where: {
                    link: link
                }
            });

            if (
                linkInst == null
            ) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Link: [${link}] not found.`, {});
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Find link id:[${linkInst.id}] with token:[${link}] done successfully.`, linkInst.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    ReferalLinkModel.findOneById = async function ({
        id
    }) {
        try {
            let linkInst = await this.findOne({
                where: {
                    id
                }
            });

            if (
                linkInst == null
            ) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Link with id: [${id}] not found.`, {});
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Find link with id: [${id}] done successfully.`, linkInst.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    ReferalLinkModel.getActiveSession = async function ({
        id
    }) {
        try {
            let linkInst = await this.findOne({
                where: {
                    id
                }
            });

            if (
                linkInst == null
            ) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Link with id: [${id}] not found.`, {});
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Find link with id: [${id}] done successfully.`, linkInst.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    return ReferalLinkModel;
};