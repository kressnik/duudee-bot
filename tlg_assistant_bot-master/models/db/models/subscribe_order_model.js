'use strict';
const {
    AppLoggerClass
} = __UTILS;


const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


const UserRole = Object.freeze({
    ADMIN: 0,
    SUBSCRIBER: 1,
});

module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;


    const orderModel = sequelize.define('SubscribeOrder', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.STRING(25),
            allowNull: false,
        },
        invoker: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        sourceId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        feeAtOwnExpense: {
            type: DataTypes.BOOLEAN,
            default: false
        },
        isFree: {
            type: DataTypes.BOOLEAN,
            default: false
        },
        price: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        },
        currency: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        fxRate: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: true
        },
        description: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        timeStamp: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        sn: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        underscored: true
    });

    orderModel.logger = new AppLoggerClass(`SEQUELIZE::SubscribeOrder`);

    orderModel.associate = async function (models) {
        // associations can be defined here
        await this.belongsTo(models.User, {
            as: 'userInfo',
            foreignKey: {
                name: 'userId',
                allowNull: false,
            }
        });

        await this.belongsTo(models.OrderDeposit, {
            as: 'depositInfo',
            foreignKey: {
                name: 'depositId',
                //allowNull: false,
            }
        });

        await this.belongsTo(models.Channel, {
            foreignKey: {
                name: 'channelId',
                allowNull: false,
                as: "Channel"
            },
            onUpdate: 'cascade',
            onDelete: 'cascade'
        });
    } //assosiate

    orderModel.createOne = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments is null, should be Object');
            }

            const {
                trackId,
                status,
                depositId,
                userId,
                invoker,
                sourceId,
                feeAtOwnExpense,
                isFree,
                price,
                currency,
                fxRate,
                description,
                timeStamp,
                sn,
                channelId
            } = args;

            const [depositInstance, isCreate] = await this.findOrCreate({
                where: {
                    depositId,
                    sn
                },
                defaults: {
                    trackId,
                    status,
                    depositId,
                    userId,
                    invoker,
                    sourceId,
                    feeAtOwnExpense,
                    isFree,
                    price,
                    currency,
                    fxRate,
                    description,
                    timeStamp,
                    sn,
                    channelId
                }
            });

            if (isCreate == false) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED, `Deposit depositId:[${depositId}]  is already added to db.`, {
                    depositId
                })
            }

            return new BasicResponse(ResultStatus.OK, 'Add pool done succsessfully.', depositInstance.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    orderModel.updateOneById = async function (info) {
        try {
            let order = await this.findOne({
                where: {
                    id: info.id
                }
            });

            if (order == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Order not found with id:[${info.id}]`);
            }

            let updOrder = await order.update(info);

            if (updOrder == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Update order done with unknown error.');
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get order with id ${info.id} done successfully.`, updOrder.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //updateOneByIds

    orderModel.updateOneByDepositId = async function (info) {
        try {
            const order = await this.findOne({
                where: {
                    depositId: info.depositId
                }
            });

            if (order == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Order not found with depositId:[${info.depositId}]`);
            }

            const updOrder = await order.update(info);

            if (updOrder == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_HANDLE, 'Update order done with unknown error.');
            }

            const rsp = new BasicResponse(ResultStatus.OK, `Get order with depositId:[${info.depositId}] done successfully.`, updOrder.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //

    orderModel.getOneByDepositId = async function ({
        depositId
    }) {
        try {
            let order = await this.findOne({
                where: {
                    depositId
                }
            });

            if (order == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Order not found with depositId:[${depositId}]`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get order with depositId:[${depositId}] done successfully.`, order);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    orderModel.getOrderInfo = async function ({
        id
    }) {
        try {
            let order = await this.findOne({
                where: {
                    id
                }
            });

            if (order == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Order not found with orderId:[${id}]`);
            }

            // if (order.isFree == true) {
            //     return new BasicResponse(ResultStatus.OK, `This subscribe is free`);
            // }

            let rsp = new BasicResponse(ResultStatus.OK, `Get order with id:[${id}] done successfully.`, order);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    orderModel.getAll = async function (params) {
        try {
            if (params == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Params should be an object');
            }

            let {
                filter = null,
                    offset,
                    limit,
                    whereLike = null
            } = params;

            let where = filter == null ? {} : {
                [Op.and]: {
                    ...filter.order || {},
                }
            }

            let whereDepParams = {
                [Op.and]: {
                    ...filter.deposit || {}
                }
            };

            if (whereLike != null && whereLike.length != 0) {
                let whereL = {};
                let attributesArr = [];

                for (let key in this.rawAttributes) {
                    let attributeName = key;
                    let type = this.rawAttributes[key].type.key;
                    if (type === "STRING") {
                        attributesArr[attributesArr.length] = attributeName;
                    }
                }

                for (const key in attributesArr) {

                    whereL[attributesArr[key]] = {
                        [Op.like]: `%${whereLike}%`
                    }
                }

                where = {
                    ...where,
                    [Op.or]: whereL
                };
            }

            let orders = await this.findAll({
                where,
                offset,
                limit,
                include: [{
                        model: this.sequelize.models.OrderDeposit,
                        where: whereDepParams,
                        as: 'depositInfo',
                        required: filter.deposit != null ? true : false,
                    },
                    'userInfo'
                ],
                order: [
                    ["id", "DESC"]
                ]
            });

            let packet = {
                recordsTotal: 0,
                recordsFiltered: await this.count({
                    where,
                    include: [{
                            model: this.sequelize.models.OrderDeposit,
                            where: whereDepParams,
                            as: 'depositInfo',
                            required: filter.deposit != null ? true : false,
                        },
                        'userInfo'
                    ]
                }),
                limit,
                offset,
                filter,
                orders: []
            }

            if (orders != null && orders.length > 0) {
                packet.orders = orders.map(order => order.dataValues);
                packet.recordsTotal = await this.count(); //tital records recive if records are in packet only
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get subscribe orders done successfully.`, packet);

            this.logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    return orderModel;
}