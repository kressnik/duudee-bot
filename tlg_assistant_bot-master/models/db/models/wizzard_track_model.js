'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;
    //LinkAction
    const wizzardTrackModel = sequelize.define('WizzardTrack', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        error: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        stage: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false
        },
        isNewUser: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
            allowNull: false
        }
    }, {
        underscored: true,
        hooks: true
    });

    wizzardTrackModel.logger = new AppLoggerClass(`SEQUELIZE::WizzardTrack`, process.env.WIZZARD_TRACK_LOG_LEVEL);

    wizzardTrackModel.associate = async function (models) {
        await this.belongsTo(models.User, {
            as: 'user',
            foreignKey: {
                name: 'userId',
                allowNull: false,
            }
        });

        await this.belongsTo(models.ReferalLink, {
            as: 'token',
            foreignKey: {
                name: 'tokenId',
                allowNull: true,
            }
        });

        await this.belongsTo(models.AssistantButton, {
            as: 'button',
            foreignKey: {
                name: 'buttonId',
                allowNull: false,
            }
        });

        await this.belongsTo(models.Channel, {
            as: 'channel',
            foreignKey: {
                name: 'channelId',
                allowNull: true,
            }
        });

        await this.belongsTo(models.TokenSession, {
            as: 'session',
            foreignKey: {
                name: 'sessionId',
                allowNull: true,
            }
        });

        //add key to SubscribeOrder
        await this.hasOne(models.SubscribeOrder, {
            as: 'order',
            foreignKey: {
                name: 'trackId',
                allowNull: true,
            }
        });
    }

    wizzardTrackModel.default = async function (_models) {

    }

    wizzardTrackModel.createOne = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null, should be an object');
            }

            const {
                status,
                stage,
                error,
                userId,
                tokenId,
                buttonId,
                subscribeId,
                sessionId,
                isNewUser,
                channelId
            } = args;

            const model = await this.create({
                status,
                userId,
                tokenId,
                buttonId,
                subscribeId,
                sessionId,
                isNewUser,
                channelId
            });

            const response = new BasicResponse(ResultStatus.OK,
                [`Add new track done successfully for `,
                    `user id:[${userId}] isNew:[${isNewUser}]`,
                    status ? `status:[${status}]` : null,
                    error ? `error:[${error}]` : null,
                    stage ? `stage:[${stage}]` : null,
                    subscribeId ? `subscribeId:[${subscribeId}]` : null,
                    buttonId ? `orderId:[${buttonId}]` : null,
                    sessionId ? `sessionId:[${sessionId}]` : null
                ].join(' '),
                model.dataValues
            );

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //createOne

    wizzardTrackModel.updateOne = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null, should be an object');
            }

            const {
                id,
                status,
                error,
                stage,
                subscribeId,
                channelId
            } = args;

            const descr = [`Update track id:[${id}]`,
            status ? `status:[${status}]` : null,
            error ? `error:[${error}]` : null,
            stage ? `stage:[${stage}]` : null,
            subscribeId ? `subscribeId:[${subscribeId}]` : null,
            channelId ? `channelId:[${channelId}]` : null
            ].join(' ');

            this.logger.info(descr);

            let model = await this.findByPk(id);

            if (model == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Track with id:[${id}] not found. ${descr}`);
            }

            const result = await model.update({
                status,
                error,
                stage,
                subscribeId,
                channelId
            });

            const response = new BasicResponse(
                ResultStatus.OK,
                `${descr} done successfully`,
                result.dataValues
            );

            this.logger.info(response);

            return response;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //updateOne

    wizzardTrackModel.getAllWithTokeId = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments is null, should be number or string');
            }

            const {
                tokenId
            } = args;


            this.logger.info(`Get all tracks by token:[${tokenId}]`);

            const tracks = await this.findAll({
                where: {
                    tokenId,
                },
            });

            if (tracks == null || tracks.length == 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Tracks with token id:[${tokenId}] not found`);
            }

            const response = new BasicResponse(ResultStatus.OK, `Get tracks with token id:[${tokenId}]`, tracks.map(track => track.dataValues));

            this.logger.info(response);

            return response;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    } //getAllWithTokeId


    wizzardTrackModel.getAllByFilter = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments is null, should be number or string');
            }

            let {
                filter,
                dateRange,
            } = args;

            let where = {};

            if (filter != null) {
                for (let key in filter) {
                    if (Object.values(filter).length > 1) {
                        if (
                            Array.isArray(filter[key]) == true &&
                            filter[key].length > 0
                        ) {
                            where[Op.and] == null ? where[Op.and] = {} : null;

                            where[Op.and][key] = {
                                [Op.in]: [...filter[key]]
                            }
                        }
                    } else {
                        where[key] = {
                            [Op.in]: [...filter[key]]
                        }
                    }
                }
            }

            if (Array.isArray(dateRange) == true &&
                dateRange.length > 0 &&
                dateRange.length <= 2) {
                const [startDate, endDate] = dateRange;

                where.createdAt = {
                    [Op.between]: [new Date(startDate).toISOString(), new Date(endDate).toISOString()]
                };

            }

            const tracksRequest = await this.findAll({
                where,
                include: ['token', 'button', 'session', 'channel', 'order']
            });

            if (tracksRequest.length === 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, 'Not found any tracks', args);
            }

            this.logger.debug(`Get tracks from db done successfully. filter: ${JSON.stringify(filter)}`);

            const rsp = new BasicResponse(ResultStatus.OK, 'Get tracks from db done done successfully', tracksRequest.map(track => track.dataValues));

            return rsp;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    } //getAllByFilter

    wizzardTrackModel.getAllByFilterAndPagination = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments is null, should be number or string');
            }

            let {
                limit,
                offset,
                filter,
                dateRange,
            } = args;

            let where = {};

            if (filter != null) {
                for (let key in filter) {
                    if (Object.values(filter).length > 1) {
                        if (
                            Array.isArray(filter[key]) == true &&
                            filter[key].length > 0
                        ) {
                            where[Op.and] == null ? where[Op.and] = {} : null;

                            where[Op.and][key] = {
                                [Op.in]: [...filter[key]]
                            }
                        }
                    } else {
                        where[key] = {
                            [Op.in]: [...filter[key]]
                        }
                    }
                }
            }

            if (Array.isArray(dateRange) == true &&
                dateRange.length > 0 &&
                dateRange.length <= 2) {
                const [startDate, endDate] = dateRange;

                where.createdAt = {
                    [Op.between]: [new Date(startDate).toISOString(), new Date(endDate).toISOString()]
                };

            }

            const tracksRequest = await this.findAndCountAll({
                where,
                limit: parseInt(limit),
                offset: parseInt(offset),
                include: ['user', 'channel'],
                order: [
                    ["id", "DESC"]
                ]
            });

            if (tracksRequest.rows.length === 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, 'Not found any tracks', {
                    data: [],
                    recordsTotal: tracksRequest.rows.length,
                    recordsFiltered: tracksRequest.rows.length
                });
            }

            this.logger.debug(`Get tracks from db with limit: [${limit}] and offset: [${offset}] done successfully. filter: ${JSON.stringify(filter)}`);

            const rsp = new BasicResponse(ResultStatus.OK, `Get tracks from db with limit: [${limit}] and offset: [${offset}] done successfully`, {
                data: tracksRequest.rows.map(track => track.dataValues),
                recordsTotal: await this.count(),
                recordsFiltered: tracksRequest.count
            });

            return rsp;
        } catch (e) {
            const error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    } //getAllByFilterAndPagination


    return wizzardTrackModel;
};