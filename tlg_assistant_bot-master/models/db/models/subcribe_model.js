'use strict';
const {
    AppLoggerClass,
    Loglevel
} = require('../../../utils/logger/AppLoggerClass');

const uniqid = require('uniqid');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


const UserRole = Object.freeze({
    ADMIN: 0,
    SUBSCRIBER: 1,
});



module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;


    var subcribeModel = sequelize.define('Subcribe', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        subscriptionDuration: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        expireTimeUtc: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        sn: {
            type: DataTypes.STRING(30),
            allowNull: false,
            default: function () {
                return uniqid();
            }
        }
    }, {
        underscored: true
    });

    subcribeModel.logger = new AppLoggerClass(`SEQUELIZE::Subcribe`);

    subcribeModel.associate = async function (models) {
        await this.belongsTo(models.User, {
            foreignKey: {
                name: 'userId',
                allowNull: false,
                as: 'user'
            }
        });

        await this.belongsTo(models.SubscribeOrder, {
            as: 'order',
            foreignKey: {
                name: 'orderId',
            }
        });

        await this.belongsTo(models.Channel, {
            foreignKey: {
                name: 'channelId',
            }
        });
    } //associate

    subcribeModel.createOne = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Arguments is null, should be Object');
            }

            const {
                status,
                subscriptionDuration,
                expireTimeUtc,
                userId,
                orderId,
                channelId,
                sn
            } = args;

            let descr = [
                orderId ? `orderId:[${orderId}]` : null,
                status ? `status:[${status}]` : null,
                userId ? `userId:[${userId}]` : null,
                channelId ? `channelId:[${channelId}]` : null,
                sn ? `sn:[${sn}]` : null,
                expireTimeUtc ? `expireTimeUtc:[${expireTimeUtc}]` : null,
                subscriptionDuration ? `subscriptionDuration:[${subscriptionDuration}]` : null
            ].join(' ');


            this.logger.info(`Start crate or update. ${descr}`);

            let [channelInstance, isCreate] = await this.findOrCreate({
                where: {
                    orderId,
                },
                defaults: {
                    sn,
                    status,
                    subscriptionDuration,
                    expireTimeUtc,
                    userId,
                    orderId,
                    channelId
                }
            });

            let response = new BasicResponse(ResultStatus.OK, `${isCreate ? 'Create' : 'Update'} Subscribe update done successfully. ${descr}`, channelInstance.dataValues);

            this.logger.info(response);

            return response;
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    subcribeModel.updateOne = async function (info) {
        try {
            let subcribeInstance = await this.findOne({
                where: {
                    id: info.id
                }
            });

            if (subcribeInstance == null) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED, `Subscribe id:[${info.id}] not found into db.`, {
                    ...info
                });
            }

            let updSt = await subcribeInstance.update(info);

            if (updSt == false) {
                throw new BasicResponse(ResultStatus.ERROR_ALREADY_ASSIGNED, `Subscribe id:[${info.id}] uopdate wit erroro.`, {
                    ...info
                });
            }

            return new BasicResponse(ResultStatus.OK, 'Update Subscribe done succsessfully.', updSt.dataValues);
        } catch (e) {
            let error = new BasicResponseByError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //updateOne



    subcribeModel.getOneById = async function ({
        id
    }) {
        try {
            let subscribe = await this.findOne({
                where: {
                    id
                }
            });

            if (
                subscribe == null
            ) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Subscribe with id:[${id}] not found.`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get Subscribe with id:[${id}] done successfully.`, subscribe.dataValues);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    subcribeModel.getWithStatus = async function (statuses) {
        try {
            if (Array.isArray(statuses) == false) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'statuses have to be n array.');
            }

            let deposits = await this.findAll({
                where: {
                    [Op.and]: {
                        status: statuses
                    }
                }
            });

            statuses = statuses.join(',');

            if (deposits == null || deposits.length == 0) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Subscribe not found with status:[${statuses}]`);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get Subscribes with status ${statuses} done successfully.`, deposits);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    subcribeModel.getAll = async function (_queryParameters) {
        try {
            const {
                limit,
                offset,
                whereLike
            } = _queryParameters;

            const argsCheck = [limit, offset].some(_a => _a == null);

            if (argsCheck == true) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Wrong incoming data', arguments);
            };

            let where = {};

            if (whereLike != null && whereLike.length != 0) {
                let attributesArr = [];

                for (let key in this.rawAttributes) {
                    let attributeName = key;
                    let type = this.rawAttributes[key].type.key;

                    if (type === "STRING") {
                        attributesArr[attributesArr.length] = attributeName;
                    };
                };

                for (const key in attributesArr) {

                    where[attributesArr[key]] = {
                        [Op.like]: `%${whereLike}%`
                    };
                };

                where = {
                    [Op.or]: where
                };
            }

            await this.sync();

            let resultUsers = await this.findAll({
                limit: limit,
                offset: offset,
                where: where,
                order: [
                    ["id", "DESC"]
                ]
            });

            const resultData = {
                recordsFiltered: await this.count({
                    where: where
                }),
                recordsTotal: await this.count(),
                data: []
            };

            if (resultUsers.length <= 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, 'Empty log list', resultData);
            };

            resultData.data = resultUsers.map((_user) => {
                return _user.dataValues;
            });

            return new BasicResponse(ResultStatus.OK, "Get subscribes done successfully", resultData);
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAll

    subcribeModel.getAllWithUser = async function ({
        userId
    }) {
        try {
            if (userId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'userId is null.')
            }

            let subcribes = await this.findAll({
                where: {
                    userId
                }
            });

            if (subcribes == null || subcribes.length == 0) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `not found Subscribes with userId:[${userId}]`);
            }

            let out = [];

            for (let subscribe of subcribes) {
                let channelInfo = await subscribe.getChannel();

                out.push({
                    ...subscribe.dataValues,
                    channelInfo: channelInfo != null ? channelInfo.dataValues : null
                })
            }


            let rsp = new BasicResponse(ResultStatus.OK, `Get subscribes with userId ${userId} done successfully.`, out);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }



    subcribeModel.getByUserAndChannelWithStatuses = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args should be an object');
            }

            const {
                userId,
                channelId,
                statuses
            } = args;

            let subscribes = await this.findAll({
                where: {
                    userId,
                    channelId,
                    [Op.or]: {
                        status: statuses
                    }

                }
            });

            if (subscribes.length > 0) {
                subscribes = subscribes.map(s => s.dataValues);
                // subscribes = subscribes[0];
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Check subscribes done successfully.`, subscribes);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    subcribeModel.getByChannelWithStatus = async function ({
        channelId,
        status
    }) {
        try {
            let subscribes = await this.findAll({
                where: {
                    channelId,
                    status
                }
            });

            if (subscribes.length == 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `There are no subscribes in the system for ch:[${channelId}] with status:[${status}]`, [])
            }

            subscribes = await Promise.all(subscribes.map(async s => {
                let user = await s.getUser();

                return {
                    ...s.dataValues,
                    user: user.dataValues
                }
            }))


            let rsp = new BasicResponse(ResultStatus.OK, `Check subscribes done successfully.`, subscribes);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    subcribeModel.checkToExpire = async function (timesatmp) {
        try {
            if (Array.isArray(statuses) == false) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'statuses have to be n aarray.')
            }

            let subcribes = await this.findAll({
                where: {
                    [Op.gte]: {
                        expireTimeUtc: timesatmp
                    }
                }
            });

            if (subcribes == null || subcribes.length == 0) {
                return new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Subscribe not found with staus expired time.`);
            }


            let rsp = new BasicResponse(ResultStatus.OK, `Get Subscribe expired time done successfully.`, subcribes);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    return subcribeModel;
}