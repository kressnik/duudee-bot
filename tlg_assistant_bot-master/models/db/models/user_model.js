'use strict';
const {
    AppLoggerClass
} = __UTILS;

var uniqid = require('uniqid');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');



module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;


    var User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        tlgUserId: {
            type: DataTypes.INTEGER,
            unique: true,
            allowNull: false
        },
        accessHash: {
            type: DataTypes.STRING,
            allowNull: false
        },
        languageCode: {
            type: DataTypes.STRING,
            allowNull: false
        },
        role: { //0 - DEACTIVATE||1 - ACTIVE||2 - REMOVED
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
        state: { //0 - DEACTIVATE||1 - ACTIVE||2 - REMOVED
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        }
    }, {
        underscored: true
    });

    User.logger = new AppLoggerClass(`SEQUELIZE::User`);

    User.associate = async function (models) {
        // await this.belongsTo(models.InviteAction, {
        //     foreignKey: {
        //         name: 'inviteId',
        //         allowNull: true,
        //     }
        // });
        User._models = models;

        await this.hasMany(models.LinkAction, {
            as: 'tokensActions'
        });
    }

    User.default = async function (_models) {

    }

    User.getUserIdByHash = async function (hash) {
        try {
            let user = await this.findOne({
                where: {
                    accessHash: hash
                }
            });

            if (user == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User not found by hash:[${hash}]`);
            }

            let response = new BasicResponse(ResultStatus.OK, 'Add new user done successfully.', user.dataValues);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }


    User.getUsersIdByRole = async function (role) {
        try {
            let users = await this.findAll({
                where: {
                    role
                }
            });

            if (users == null || users.length == 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `User not found by role:[${role}]`);
            }

            let response = new BasicResponse(ResultStatus.OK, 'Add new user done successfully.', users);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    User.getOneById = async function ({
        id
    }) {
        try {
            let userModel = await this.findOne({
                where: {
                    id
                },
                include: [{
                    model: this._models.LinkAction,
                    as: 'tokensActions',
                    required: false,
                    include: [{
                        model: this._models.TokenSession,
                        as: 'sessions',
                        required: false,
                    }]
                }]
            });

            if (userModel == null) {
                rsp = new BasicResponse(ResultStatus.OK, `User id:[${id}] not found in system`, {});
            }


            let rsp = new BasicResponse(ResultStatus.OK, `Get user id:[${id}] done successfully.`, userModel);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //findOneById


    /**
     * @param {*} firstName,
     * @param {*} lastName
     * @param {*} username
     * @param {*} tlgUserId
     * @param {*} languageCode
     */
    User.checkIn = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Args is null');
            }

            const {
                firstName = '',
                    lastName = '',
                    username = '',
                    tlgUserId,
                    languageCode = 'not_set',
            } = args;

            let descr = `user:[${tlgUserId}], firstName:[${firstName}],lastName:[${lastName}], username:[${username}]`

            this.logger.info(`Checkin ${descr}`);

            let [userModel, isNew] = await this.findOrCreate({
                where: {
                    tlgUserId: tlgUserId
                },
                include: [{
                    model: this._models.LinkAction,
                    as: 'tokensActions',
                    required: false,
                    include: [{
                        model: this._models.TokenSession,
                        as: 'sessions',
                        required: false,
                    }]
                }],
                defaults: {
                    firstName: firstName,
                    lastName: lastName,
                    username: username,
                    tlgUserId: tlgUserId,
                    languageCode: languageCode
                }
            });

            let response = new BasicResponse(ResultStatus.OK, `Add new ${descr} done successfully.`, {
                ...userModel.dataValues,
                isNew
            });

            if (isNew == false) {
                response.message = `Checkin ${descr} already added to system`;
            }

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    User.getAll = async function (_queryParameters) {
        try {

            const {
                limit,
                offset,
                whereLike
            } = _queryParameters;

            const argsCheck = [limit, offset].some(_a => _a == null); //Проверка есть ли обязательные параметры

            if (argsCheck == true) { //Проверка получены ли все обязательные параметры
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Wrong incoming data', arguments);
            }

            let where = {};

            if (whereLike.length != 0) {

                let attributesArr = [];

                for (let key in this.rawAttributes) {
                    let attributeName = key;
                    let type = this.rawAttributes[key].type.key;
                    if (type === "STRING") {
                        attributesArr[attributesArr.length] = attributeName;
                    }
                }

                for (const key in attributesArr) {

                    where[attributesArr[key]] = {
                        [Op.like]: `%${whereLike}%`
                    }
                }
                where = {
                    [Op.or]: where
                };
            }

            await this.sync();

            let resultData = {
                recordsFiltered: await this.count({
                    where: where
                }),
                recordsTotal: await this.count(),
                data: []
            }

            let resultUsers = await this.findAll({
                limit: limit,
                offset: offset,
                where: where,
                order: [
                    ["id", "DESC"]
                ]
            });

            if (resultUsers.length <= 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, 'Empty log list', resultData);
            }

            resultData.data = resultUsers.map((_user) => {
                return _user.dataValues;
            });

            return new BasicResponse(ResultStatus.OK, "Pull data ok", resultData);
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    User.beforeValidate(function (model, options, cb) {
        if (model.isNewRecord) {
            model.accessHash = uniqid();
        }

        return model;
    });

    return User;
};