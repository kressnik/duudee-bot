'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = __UTILS.INTERFACES;


module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;
    //LinkAction
    var TokenSession = sequelize.define('TokenSession', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        startDate: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        endDate: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        underscored: true,
        hooks: true
    });

    TokenSession.logger = new AppLoggerClass(`SEQUELIZE::TokenSession`);

    TokenSession.associate = async function (models) {
        await this.belongsTo(models.ReferalLink, {
            foreignKey: {
                name: 'tokenId',
                allowNull: false,
            }
        });

        // await this.hasOne(models.ReferalLink, {
        //     as: 'token',
        //     foreignKey: {
        //         name: 'tokenId',
        //         allowNull: false,
        //     }
        // });
    }

    TokenSession.default = async function (_models) {

    }

    TokenSession.getAllByTokenId = async function (tokenId) {
        try {
            if (tokenId == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Value of tokenId should be an string or number');
            }

            this.logger.info(`Start get all seessions for token id:[${tokenId}]`);

            let sessionModel = await this.findAll({
                where: {
                    tokenId
                }
            });

            if (sessionModel.length === 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND,
                    `Not found any seessions for track id:[${tokenId}]`,
                    []);
            }

            let sessions = sessionModel.map(session => session.dataValues);

            let response = new BasicResponse(ResultStatus.OK,
                `Get all seessions for token id:[${tokenId}] done successfully.`,
                sessions);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    }


    TokenSession.deleteOne = async function (id) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Value of id should be an string or number');
            }

            this.logger.info(`Start delete seession id:[${id}]`);

            let sessionModel = await this.findByPk(id);

            if (sessionModel == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND,
                    `Not found any seessions by id:[${id}]`,
                    []);
            }

            let deleteResult = await sessionModel.update({
                isDeleted: 1
            });

            if (deleteResult == null) {
                throw new BasicResponse(ResultStatus.ERROR, 'Somthin going wrong when update to db.')
            }

            let response = new BasicResponse(ResultStatus.OK,
                `Delete seessions by id:[${id}] done successfully.`,
                deleteResult);

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    }

    /**
     * @param {*} tokenId id of reffreal link or token
     * @param {*} startDate Unix timestamp begin session start
     * @param {*} endDate Unix timestamp end session end
     * @param {*} title title
     */
    TokenSession.createOne = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Arguments should be an object');
            }

            const {
                tokenId,
                startDate,
                endDate,
                title
            } = args;

            if ([tokenId, startDate, endDate].some(a => a == null)) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Some of parameters is null', args);
            }

            let checkPeriod = await this.checkPeriod(args);

            checkPeriod.isOK();

            let sessionModel = await this.create({
                tokenId,
                startDate,
                endDate,
                title
            });

            let response = new BasicResponse(ResultStatus.OK,
                `Add new session to token id:[${tokenId}] with start date:[${startDate}] end date:[${endDate}] title:[${title}]cdone successfully.`,
                sessionModel.dataValues
            );

            this.logger.info(response.toString());

            return response;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    } //createOne


    TokenSession.checkPeriod = async function (args) {
        try {
            if (args == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Arguments should be an object');
            }

            const {
                tokenId,
                startDate,
                endDate,
            } = args;

            if ([tokenId, startDate, endDate].some(a => a == null)) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, 'Some of parameters is null', args);
            }

            if (startDate > endDate) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, `Start date:[${startDate} should be less then end date:[${endDate}]`, args);
            }

            this.logger.info(`Start add new session to token id:[${tokenId}] with start date:[${startDate}] end date:[${endDate}] `)

            let findres = await this.findAll({
                where: {
                    [Op.and]: [{
                        tokenId
                    }, {
                        [Op.or]: [{
                                [Op.and]: [{
                                    startDate: {
                                        [Op.lte]: startDate
                                    }
                                }, {
                                    startDate: {
                                        [Op.lte]: endDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.gte]: startDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.gte]: endDate
                                    }
                                }]
                            }, {
                                [Op.and]: [{
                                    startDate: {
                                        [Op.lte]: startDate
                                    }
                                }, {
                                    startDate: {
                                        [Op.lte]: endDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.gte]: startDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.lte]: endDate
                                    }
                                }]
                            },
                            {
                                [Op.and]: [{
                                    startDate: {
                                        [Op.gte]: startDate
                                    }
                                }, {
                                    startDate: {
                                        [Op.lte]: endDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.gte]: startDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.gte]: endDate
                                    }
                                }]
                            },
                            {
                                [Op.and]: [{
                                    startDate: {
                                        [Op.gte]: startDate
                                    }
                                }, {
                                    startDate: {
                                        [Op.lte]: endDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.lte]: endDate
                                    }
                                }, {
                                    endDate: {
                                        [Op.gte]: startDate
                                    }
                                }]
                            }
                        ] //[Op.or]
                    }] //[Op.and]
                } //where
            });

            if (findres.length > 0) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_PARAMETER, `Wrong date range period for token id:[${tokenId}] start date:[${startDate}] end date:[${endDate}]`, findres);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Check sessions period for token id:[${tokenId}] start date:[${startDate}] end date:[${endDate}]`);

            this.logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    }

    return TokenSession;
};