'use strict';
const {
    AppLoggerClass
} = __UTILS;

var uniqid = require('uniqid');

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');


const UserRole = Object.freeze({
    ADMIN: 0,
    SUBSCRIBER: 1,
});

module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;


    let buttonModel = sequelize.define('AssistantButton', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        isActive: {
            type: DataTypes.BOOLEAN,
            default: false
        },
        isRefferal: {
            type: DataTypes.BOOLEAN,
            default: false
        },
        name: {
            type: DataTypes.STRING(25),
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        userGroup: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        handler: {
            type: DataTypes.STRING(40),
            allowNull: false
        }
    }, {
        underscored: true
    });

    buttonModel.logger = new AppLoggerClass(`SEQUELIZE::AssistantButton`);

    buttonModel.associate = async function (models) {

    }

    buttonModel.getAllActive = async function () {
        try {
            let buttons = await this.findAll({
                where: {
                    isActive: true
                }
            });

            if (buttons == null || buttons.length == 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Any buttons found in system.`, []);
            }

            buttons = buttons.map(b => b.dataValues);

            let rsp = new BasicResponse(ResultStatus.OK, `Get buttons done successfully.`, buttons);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    buttonModel.getAll = async function () {
        try {
            let buttons = await this.findAll();

            if (buttons == null || buttons.length == 0) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Any buttons found in system.`, []);
            }

            buttons = buttons.map(b => b.dataValues);

            let rsp = new BasicResponse(ResultStatus.OK, `Get buttons done successfully.`, buttons);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    }

    buttonModel.getOne = async function (id) {
        try {
            if (id == null) {
                throw new BasicResponse(ResultStatus.ERROR_INVALID_DATA, 'Button id must to be provided');
            }


            let button = await this.findByPk(id);

            if (button == null) {
                throw new BasicResponse(ResultStatus.ERROR_NOT_FOUND, `Buttonwith id:[${id}] not found in system.`, []);
            }

            let rsp = new BasicResponse(ResultStatus.OK, `Get buttons done successfully.`, button.dataValues);

            this.logger.info(rsp);

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error);

            return error;
        }
    }


    return buttonModel;
}