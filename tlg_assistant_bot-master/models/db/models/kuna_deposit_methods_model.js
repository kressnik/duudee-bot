'use strict';
const {
    AppLoggerClass
} = __UTILS;

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../../../interfaces/basicResponse');



module.exports = function (sequelize, DataTypes) {
    const Op = sequelize.Op;


    let kunaChannelModel = sequelize.define('KunaDepoChannel', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        settings: {
            type: DataTypes.JSON,
            allowNull: true
        },
        isActive: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        isForce: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        underscored: true
    });

    kunaChannelModel.logger = new AppLoggerClass(`SEQUELIZE::KunaDepoChannel`);

    kunaChannelModel.associate = async function (models) {
        // associations can be defined here
        // await this.belongsTo(models.SubscribeOrder, {
        //     foreignKey: {
        //         name: 'orderId',
        //         allowNull: false,
        //     }
        // });
    }

    kunaChannelModel.getActiveMethods = async function () {
        try {
            let methods = await this.findAll({
                where: {
                    isActive: true
                }
            });

            if (methods == null || methods.length == 0) {
                return new BasicResponse(ResultStatus.OK, `Any methods found in system.`, []);
            }

            methods = methods.map(b => b.dataValues);

            methods = methods.reduce((prev, curr) => {
                prev[curr.name] = curr;
                return prev;
            }, {});

            let rsp = new BasicResponse(ResultStatus.OK, `Get methods done successfully.`, methods);

            this.logger.info(rsp.toString());

            return rsp;
        } catch (e) {
            let error = sequelize.utils.makeBasicResponseFromError(e);

            this.logger.error(error.toString());

            return error;
        }
    } //getAllActive

    return kunaChannelModel;
}