module.exports = {
    'test.ping': pingMethod,
}

async function pingMethod(args, core, done) {
    try {
        if (args.e) {
            throw args.e
        }

        return {
            pong: args
        }
    } catch (e) {
        throw e;
    }
}