module.exports = {
    'subscribe.order.get.all': subscribeGetAllMethod,
    'subscribe.order.get.statuses': getSubscribeOrderStatuses,
}

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');


const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass('SUBSCRIBE_ORDER_RPC_METHODS', process.env.EVENT_RPC_METHODS_LOG_LELVEL);

async function subscribeGetAllMethod(args, ctx, done) {
    try {
        let subs = await this.assistantProcessor.getAllSubscribesOrdres(args);

        if (subs.status != ResultStatus.OK) {
            throw subs;
        }

        moduleLogger.info(subs.toString());

        return subs;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}

async function getSubscribeOrderStatuses(args, ctx, done) {
    try {
        let subs = await this.assistantProcessor.getSubscribeOrderStatuses(args);

        if (subs.status != ResultStatus.OK) {
            throw subs;
        }

        moduleLogger.info(subs.toString());

        return subs;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}