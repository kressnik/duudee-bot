module.exports = {
    'channels.get.all': channelsGetAllMethod,
    'channel.create.one': channelCreateOneMethod,
    'channel.edit.one': channelEditOneMethod,
    'channel.edit.state.one':channelEditStateMethod,
    'channel.delete.state.one':channelDeleteOneMethod

}

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');


const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass('CHANNELS_RPC_METHODS', process.env.CHANNELS_RPC_METHODS_LOG_LEVEL);

async function channelsGetAllMethod(args, ctx, done) {
    try {
        let channels = await this.assistantProcessor.getAllChannels(args);

        if (channels.status != ResultStatus.OK) {
            throw channels;
        }

        moduleLogger.info(channels.toString());

        return channels;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //

async function channelCreateOneMethod(args, ctx, done) {
    try {
        let channels = await this.assistantProcessor.createChannel(args);

        if (channels.status != ResultStatus.OK) {
            throw channels;
        }

        moduleLogger.info(channels.toString());

        return channels;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //

async function channelEditOneMethod(args, ctx, done) {
    try {
        let channels = await this.assistantProcessor.editChannelInfo(args);

        if (channels.status != ResultStatus.OK) {
            throw channels;
        }

        moduleLogger.info(channels.toString());

        return channels;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //

async function channelDeleteOneMethod(args, ctx, done) {
    try {
        let channels = await this.assistantProcessor.deleteChannel(args);

        if (channels.status != ResultStatus.OK) {
            throw channels;
        }

        moduleLogger.info(channels.toString());

        return channels;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //

async function channelEditStateMethod(args, ctx, done) {
    try {
        let channels = await this.assistantProcessor.editStateChannel(args);

        if (channels.status != ResultStatus.OK) {
            throw channels;
        }

        moduleLogger.info(channels.toString());

        return channels;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //