module.exports = {
    'subscribe.get.all': subscribeGetAllMethod,
    'subscribe.create.one': subscribeCreateOneMethod,
    'deposit.get.one': getDepositInfo,
    'deposit.update.status': updateDepositStatus,
    'order.update.info': updateOrderInfo,
    'deposit.get.statuses': getDepositStatuses,
}

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');


const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass('SUBSCRIBE_RPC_METHODS', process.env.EVENT_RPC_METHODS_LOG_LELVEL);

async function subscribeGetAllMethod(args, ctx, done) {
    try {
        let subs = await this.assistantProcessor.getAllSubscribes(args);

        if (subs.status != ResultStatus.OK) {
            throw subs;
        }

        moduleLogger.info(subs.toString());

        return subs;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //

async function subscribeCreateOneMethod(args, ctx, done) {
    try {
        if (args.invoker == null) {
            args.invoker = 'rpc';
        }

        let createInfo = await this.assistantProcessor.createSubscribe(args);

        if (createInfo.status != ResultStatus.OK) {
            throw createInfo;
        }

        moduleLogger.info(createInfo.toString());

        return createInfo;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //

async function getDepositInfo(args, ctx, done) {
    try {
        let depoInfo = await this.assistantProcessor.getDepoInfo(args);

        if (depoInfo.status != ResultStatus.OK) {
            throw depoInfo;
        }

        moduleLogger.info(depoInfo.toString());

        return depoInfo;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}

async function updateOrderInfo(args, ctx, done) {
    try {
        let depoUpdateStatus = await this.assistantProcessor.updateOrderInfo(args);

        depoUpdateStatus.isOK();

        moduleLogger.info(depoUpdateStatus.toString());

        return depoUpdateStatus;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}

async function updateDepositStatus(args, ctx, done) {
    try {
        let orderUpdateRequest = await this.assistantProcessor.updateDepoStatus(args);

        orderUpdateRequest.isOK();

        moduleLogger.info(orderUpdateRequest.toString());

        return orderUpdateRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}

async function getDepositStatuses(args, ctx, done) {
    try {
        let getDepositStatusesRequest = await this.assistantProcessor.getDepositStatuses();

        getDepositStatusesRequest.isOK();

        moduleLogger.info(getDepositStatusesRequest.toString());

        return getDepositStatusesRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}