module.exports = {
    'users.by.link.get.all': getUsersByLinkMethod,

    'refferal.link.delete.one': deleteLinkMethod,
    'refferal.link.edit.one': editLinkMethod,
    'refferal.link.create.one': createLinkMethod,
    'refferal.link.get.types': getLinkTypesMethod,
    'refferal.link.get.all': getAllLinksMethod,
    'refferal.link.get.by.id': getreffLinkByIdMethod,

    'refferal.link.session.get.all': getLinkSessionsMethod,
    'refferal.link.session.create': createLinkSessionMethod,
    'refferal.link.session.delete': deleteLinkSessionMethod,
    'refferal.link.session.check.period': checkLinkSessionPeriodMethod,

    'refferal.track.get.all': getTracksMethod,
    'refferal.track.get.all.with.pagination': getTracksWithPaginMethod,

}

const {
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');


const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass('REFERAL_RPC_METHODS', process.env.EVENT_RPC_METHODS_LOG_LELVEL);

async function getUsersByLinkMethod(args, ctx, done) {
    try {
        let usersInfo = await this.assistantProcessor.getUsersByLink(args);

        if (usersInfo.status != ResultStatus.OK) {
            throw usersInfo;
        }

        moduleLogger.info(usersInfo.toString());

        return usersInfo;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // getUsersByLinkMethod

async function getLinkTypesMethod(args, ctx, done) {
    try {
        let getLinkTypesRequest = await this.assistantProcessor.getLinkTypes();

        getLinkTypesRequest.isOK();

        moduleLogger.info(getLinkTypesRequest.toString());

        return getLinkTypesRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // getLinkTypesMethod

async function createLinkMethod(args, ctx, done) {
    try {
        let createLinkRequest = await this.assistantProcessor.createReferalLink(args);

        createLinkRequest.isOK();

        moduleLogger.info(createLinkRequest.toString());

        return createLinkRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // createLinkMethod

async function deleteLinkMethod(args, ctx, done) {
    try {
        let deleteLinkRequest = await this.assistantProcessor.softDeleteReferalLink(args);

        deleteLinkRequest.isOK();

        moduleLogger.info(deleteLinkRequest.toString());

        return deleteLinkRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // deleteLinkMethod

async function editLinkMethod(args, ctx, done) {
    try {
        let editLinkRequest = await this.assistantProcessor.editReferalLink(args);

        editLinkRequest.isOK();

        moduleLogger.info(editLinkRequest.toString());

        return editLinkRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // editLinkMethod

async function getAllLinksMethod(args, ctx, done) {
    try {
        let getLinksRequest = await this.assistantProcessor.getAllReferalLinks(args);

        getLinksRequest.isOK();

        moduleLogger.info(getLinksRequest.toString());

        return getLinksRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // getAllLinksMethod 

async function getreffLinkByIdMethod(args, ctx, done) {
    try {
        let getLinksRequest = await this.assistantProcessor.getreffLinkById(args);

        getLinksRequest.isOK();

        moduleLogger.info(getLinksRequest.toString());

        return getLinksRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // getreffLinkByIdMethod

async function getLinkSessionsMethod(args, ctx, done) {
    try {
        let getLinkSessionsRequest = await this.assistantProcessor.getLinkSessions(args);

        getLinkSessionsRequest.isOK();

        moduleLogger.info(getLinkSessionsRequest);

        return getLinkSessionsRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(error);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // getLinkSessionsMethod


async function createLinkSessionMethod(args, ctx, done) {
    try {
        let createLinkSessionRequest = await this.assistantProcessor.createLinkSession(args);

        createLinkSessionRequest.isOK();

        moduleLogger.info(createLinkSessionRequest);

        return createLinkSessionRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(error);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // createLinkSessionMethod

async function deleteLinkSessionMethod(args, ctx, done) {
    try {
        let deleteLinkSessionRequest = await this.assistantProcessor.deleteLinkSession(args);

        deleteLinkSessionRequest.isOK();

        moduleLogger.info(deleteLinkSessionRequest);

        return deleteLinkSessionRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(error);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // deleteLinkSessionMethod


async function checkLinkSessionPeriodMethod(args, ctx, done) {
    try {
        let checkPeriodRequest = await this.assistantProcessor.checkPeriod(args);

        checkPeriodRequest.isOK();

        moduleLogger.info(checkPeriodRequest);

        return checkPeriodRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(error);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} // checkPeriodMethod


/**
 * @param {Object} args
 * @param {Object} filter [tokenId<Array>, buttonId<Array>, channelId<Array>, orderId<Array>, sessionId<Array>, error<Array>, status<Array>, id<Array>] 
 * @param {Array} timeRange: [start, end],
 */
async function getTracksMethod(args, ctx, done) {
    try {
        moduleLogger.info('Recive RPC call get al tracks request');

        let getAllTracksMethod = await this.assistantProcessor.getAllTracksByFilter(args);

        getAllTracksMethod.isOK();

        moduleLogger.info(getAllTracksMethod);

        return getAllTracksMethod;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(error);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}

/**
 * @param {Object} args
 * @param {Object} filter [tokenId<Array>, buttonId<Array>, channelId<Array>, orderId<Array>, sessionId<Array>, error<Array>, status<Array>, id<Array>] 
 * @param {Array} timeRange: [start, end],
 * @param {Number} limit: [25],
 * @param {Number} offset: [25]
 */
async function getTracksWithPaginMethod(args, ctx, done) {
    try {
        moduleLogger.info('Recive RPC call get al tracks request');

        let getAllTracksMethod = await this.assistantProcessor.getAllTracksByFilterWithPagin(args);

        getAllTracksMethod.isOK();

        moduleLogger.info(getAllTracksMethod);

        return getAllTracksMethod;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(error);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}