module.exports = {
    'user.sendStartAuth': startAuthfucntion,
    'user.sendAuthCode': sendAuthCode,
    'users.get.all': getAllUsersHandler,
    'buttons.get.all': getAllButtonsHandler
}

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');


const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass('assistantProcessor', process.env.EVENT_RPC_METHODS_LOG_LELVEL);

async function getAllButtonsHandler(args, ctx, done) {
    try {
        let buttonsRequest = await this.assistantProcessor.getAllTelegramButtons(args);

        buttonsRequest.isOK();
       
        moduleLogger.info(buttonsRequest);

        return buttonsRequest;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(error);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}

async function getAllUsersHandler(args, ctx, done) {
    try {
        let users = await this.assistantProcessor.getAllUsers(args);

        if (users.status != ResultStatus.OK) {
            throw users;
        }

        moduleLogger.info(users.toString());

        return users;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
}

async function startAuthfucntion(args, ctx, done) {
    try {
        let sendRes = await this.assistantProcessor.startAuth({
            code: args.code
        });

        moduleLogger.info(sendRes.toString());

        return sendRes;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //

async function sendAuthCode(args, ctx, done) {
    try {
        let sendRes = await this.assistantProcessor.sendAuthCode({
            code: args.code
        });

        moduleLogger.info(sendRes.toString());

        return sendRes;
    } catch (e) {
        let error = new BasicResponseByError(e);

        moduleLogger.error(`${error.message}`);

        let rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //


