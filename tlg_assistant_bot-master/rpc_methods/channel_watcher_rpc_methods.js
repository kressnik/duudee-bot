module.exports = {
    'channel.watcher.auth.get.status': watcherAuthGetStatus,
    'channel.watcher.auth.send.start': watcherUserSendStartAuth,
    'channel.watcher.auth.send.code': watcherUserSendAuthCode,
    'channel.watcher.send.api.info': watcherUserSendApiInfo,
}

const {
    BasicResponse,
    ResultStatus,
    BasicResponseByError
} = require('../interfaces/basicResponse');


const {
    AppLoggerClass
} = __UTILS;

const moduleLogger = new AppLoggerClass('assistantProcessor', process.env.EVENT_RPC_METHODS_LOG_LELVEL);

/**
 * Get auth status
 * @param void
 */
async function watcherAuthGetStatus() {
    try {
        const request = await this.assistantProcessor.watcherAuthGetStatus();

        request.isOK();

        return request;
    } catch (e) {
        const error = new BasicResponseByError(e);

        moduleLogger.error(error);

        const rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //authGetStatus

/**
 * Send and seup api information
 * @param {*} apiId
 * @param {*} apiHash
 */
async function watcherUserSendApiInfo(args) {
    try {
        const request = await this.assistantProcessor.watcherUserSendApiInfo(args);

        request.isOK();

        this._logger.info(request);

        return request;
    } catch (e) {
        const error = new BasicResponseByError(e);

        moduleLogger.error(error);

        const rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //userSendApiInfo

/**
 * Start initiate auth process.
 * @param {*} phone phone for recived code
 */
async function watcherUserSendStartAuth(args = {}) {
    try {
        const {
            phone
        } = args;

        const request = await this.assistantProcessor.watcherUserSendStartAuth(phone);

        request.isOK();

        this._logger.info(request);

        return request;
    } catch (e) {
        const error = new BasicResponseByError(e);

        moduleLogger.error(error);

        const rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //watcherUserSendStartAuth

/**
 * Send auth code for auth process.
 * @param {*} phone phone for recived code
 */
async function watcherUserSendAuthCode(args = {}) {
    try {
        const {
            code
        } = args;

        const request = await this.assistantProcessor.watcherUserSendAuthCode(code);

        request.isOK();

        this._logger.info(request);

        return request;
    } catch (e) {
        const error = new BasicResponseByError(e);

        moduleLogger.error(error);

        const rpcError = this.rpcProcessor.reduceBasicToRpc(error);

        throw rpcError;
    }
} //watcherUserSendAuthCode