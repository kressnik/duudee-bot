const HttpStatus = Object.freeze({
    /**
     * @property 200
     */
    OK: 200,
    NO_CONTENT: 201,
    /**
     * @property 400
     */
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    INTERNAL_SERVER_ERROR: 500
});

module.exports.HttpStatus = HttpStatus;

module.exports.HttpRestResponse = class HttpRestResponse {
    constructor(_statusCode, _message, _messageCode, _data) {
        try {
            this.code = _statusCode
            this.message = _message
            this.messageCode = _messageCode

            if (_data) {
                this.data = _data;
            }

            this.time = Date.now()
        } catch (error) {
            console.error(error)
        }
    }
}