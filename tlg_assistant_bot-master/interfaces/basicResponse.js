'use strict'
var keyMirror = require('keymirror');

const ResultStatus = keyMirror(Object.freeze({

    OK: "OK",
    SUCCESS_NO_CONTENT: null,

    ERROR: "ERROR",
    ERROR_INVALID_FUNCTION: "ERROR_INVALID_FUNCTION",

    ERROR_NOT_FOUND: null,
    ERROR_PATH_NOT_FOUND: null,

    ERROR_ACCESS_DENIED: null,
    ERROR_INVALID_HANDLE: null,

    ERROR_BAD_FORMAT: null,

    ERROR_INVALID_ACCESS: null,

    ERROR_INVALID_DATA: null,
    ERROR_INVALID_PARAMETER: null,

    ERROR_NOT_READY: null,
    ERROR_BAD_COMMAND: null,
    ERROR_CRC: null,
    ERROR_BAD_LENGTH: null,

    ERROR_WRITE_FAULT: null,
    ERROR_READ_FAULT: null,

    ERROR_NOT_SUPPORTED: null,
    ERROR_BUSY: null,

    ERROR_UNEXP_ERR: null,
    ERROR_ALREADY_ASSIGNED: null,

    ERROR_TIMEOUT: null,
    ERROR_ALREADY_EXIST: null,

    ERROR_RECIVE_STOP_SIGNAL: null,
    ERROR_RESOURCE_IS_NOT_ACTIVE: null,
    ERRROR_HAS_NOT_ACTIVE_PAYMENT_METHODS: null,
    ERRROR_HAS_NOT_ACTIVE_PAYMENT_CURRENCY: null,
    ERROR_CANCEL: null,

    ERROR_BAD_REQUEST: null,
    ERROR_FORBIDDEN: null,

    ERROR_UNAUTHORIZED: null, // resolve uath required\
    ERROR_SEE_OTHER: null,
    ERROR_NOT_ACCEPTABLE: null,
    ERROR_FLOOD: null,
    ERROR_INTERNAL: null
}));

module.exports.ResultStatus = ResultStatus;


class BasicResponseFucntionClass {
    toString() {
        return `${this.message || ''}::${this.status || ''} \n ${this.stack || ''}`
    }

    isOK() {
        if (this.status != ResultStatus.OK) {
            throw this;
        }
    }

    isStatus(status) {
        return (this.status === status);
    }

    isStatuses(...statuses) {
        return statuses.some(status => this.status === status);
    }
}

module.exports.BasicResponse = class BasicResponse extends BasicResponseFucntionClass {
    constructor(_statusCode, _message, _data) {
        super();

        this.status = _statusCode;
        this.message = _message;

        if (_data != undefined) {
            this.data = _data;
        }
    }
}

module.exports.BasicResponseByError = class BasicResponseByError extends BasicResponseFucntionClass {
    constructor(error) {
        super();

        if (error instanceof module.exports.BasicResponse ||
            error instanceof BasicResponseByError
        ) {
            return error;
        } else {
            this.status = ResultStatus.ERROR_UNEXP_ERR;

            this.message = error.message;

            this.data = error;

            this.stack = error.stack;
        }
    }
}